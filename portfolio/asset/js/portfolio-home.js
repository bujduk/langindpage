$(document).ready(() => {

    $('.share__item').click(function() {

        let jsShareItem = $(this);

        let jsShareList = jsShareItem.parent().parent().children('.share__list')

        if (jsShareList.hasClass('hidden')) {
            $('.share__list').addClass('hidden')
            jsShareList.removeClass('hidden');
        } else {
            jsShareList.addClass('hidden');
        }
    })

    $('.delete__btn').click(function () {
        if (!$('.delete__unshow ').hasClass('hidden')) {
            modalDeleteConfirmShow();
        } else  {
            modalDeleteConfirmUnshow();
        }
    })

    function modalDeleteConfirmUnshow() {
        $('.delete__show').addClass('hidden')
        $('.delete__unshow').removeClass('hidden');
        $('.delete__show--text').addClass('hidden');
    }
    function modalDeleteConfirmShow() {
        $('.delete__show').removeClass('hidden');
        $('.delete__show--text').removeClass('hidden');
        $('.delete__unshow').addClass('hidden');
    }

    $('.js_modal__edit-name').click(function () {
        $('#js_modal__body--name-input').css("border","0.5px solid #A6A6A6");
        $('.js_modal__edit--name-agree').removeClass('hidden');
        $(this).addClass('hidden');
        $("#js_modal__body--link-input").prop('disabled', true);
        $("#js_modal__body--name-input").prop('disabled', false);

        if (!$('.js_modal__edit--link-agree').hasClass('hidden')) {
            $('.js_modal__edit--link-agree').addClass('hidden');
            $('.js_modal__edit-link').removeClass('hidden');
            $('#js_modal__body--link-input').css("border", "");
        }
    })

    $('.edit__confirm--name-accept').click(function () {
        $('#js_modal__body--name-input').css("border", "");
        $('.js_modal__edit-name').removeClass('hidden');
        $('.js_modal__edit--name-agree').addClass('hidden');
        $(".modal__body--name-input").prop('disabled', true);
        $(".modal__body--link-input").prop('disabled', true);
    })

    $('.edit__confirm--name-cancel').click(function () {
        $('#js_modal__body--name-input').css("border", "");
        $('.js_modal__edit-name').removeClass('hidden');
        $('.js_modal__edit--name-agree').addClass('hidden')
        $(".modal__body--name-input").prop('disabled', true);
        $(".modal__body--link-input").prop('disabled', true);
    })

    $('.edit__confirm--link-cancel').click(function () {
        $('#js_modal__body--link-input').css("border", "");
        $('.js_modal__edit-link').removeClass('hidden');
        $(".modal__body--name-input").prop('disabled', true);
        $('.js_modal__edit--link-agree').addClass('hidden');
        $(".modal__body--link-input").prop('disabled', true);
    })

    $('.edit__confirm--link-accept').click(function () {
        $('#js_modal__body--link-input').css("border", "");
        $('.js_modal__edit-link').removeClass('hidden');
        $(".modal__body--name-input").prop('disabled', true);
        $('.js_modal__edit--link-agree').addClass('hidden');
        $(".modal__body--link-input").prop('disabled', true);
    })

    $('.js_modal__edit-link').click(function () {
        $('#js_modal__body--link-input').css("border","0.5px solid #A6A6A6");
        $(this).addClass('hidden');
        $('.js_modal__edit--link-agree').removeClass('hidden');
        $('#js_modal__body--name-input').prop('disabled', true)
        $('#js_modal__body--link-input').prop('disabled', false)

        if (!$('.js_modal__edit--name-agree').hasClass('hidden')) {
            $('.js_modal__edit--name-agree').addClass('hidden');
            $('.js_modal__edit-name').removeClass('hidden');
            $('#js_modal__body--name-input').css("border", "");
        }
    })

    $('.page-link').click(function () {
        if ($('.page-link').hasClass('active')) {
            $('.page-link').removeClass('active');
        }
        if (!$('.page-link').hasClass('active')) {
            $(this).addClass('active');
        }
    })
})

function copyLink(inputID) {
    var copyText = document.getElementById(inputID);

    copyText.select();

    copyText.setSelectionRange(0, 99999);

    document.execCommand("copy");

    $('.share__list').addClass('hidden');
    toastr.success('Copy link thành công');
}

document.querySelectorAll('.button').forEach(button => {

    let div = document.createElement('div'),
        letters = button.textContent.trim().split('');

    function elements(letter, index, array) {

        let element = document.createElement('span'),
            part = (index >= array.length / 2) ? -1 : 1,
            position = (index >= array.length / 2) ? array.length / 2 - index + (array.length / 2 - 1) : index,
            move = position / (array.length / 2),
            rotate = 1 - move;

        element.innerHTML = !letter.trim() ? '&nbsp;' : letter;
        element.style.setProperty('--move', move);
        element.style.setProperty('--rotate', rotate);
        element.style.setProperty('--part', part);

        div.appendChild(element);

    }

    letters.forEach(elements);

    button.innerHTML = div.outerHTML;

    button.addEventListener('mouseenter', e => {
        if(!button.classList.contains('out')) {
            button.classList.add('in');
        }
    });

    button.addEventListener('mouseleave', e => {
        if(button.classList.contains('in')) {
            button.classList.add('out');
            setTimeout(() => button.classList.remove('in', 'out'), 950);
        }
    });

});

