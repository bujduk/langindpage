$(document).ready(() => {
    $('.n-search__location--select').select2({
        dropdownCssClass: "navi-location-select2",
        minimumResultsForSearch: -1,
    });

    var navbar_logo = $('.app-navigation');

    if (window.innerWidth >= 992) {
        $(window).scroll(function () {
            var top = $(this).scrollTop();

            if (top > 100) {
                if (!navbar_logo.hasClass('scroll')) {
                    navbar_logo.addClass('scroll')
                }
            } else {
                if (navbar_logo.hasClass('scroll')) {
                    navbar_logo.removeClass('scroll')
                }
            }
        });

        $('.app-navigation-portfolio .n-search__name--input').click(() => {
            let nNaviList = $('.app-navigation-portfolio .n-navi .n-navi__list');
            let nSearch = $('.app-navigation-portfolio .n-search');

            if (!nNaviList.hasClass('n-navi__list--hide')) {
                nNaviList.addClass('n-navi__list--hide');
            }

            if (nSearch.hasClass('n-search__hideitem')) {
                nSearch.removeClass('n-search__hideitem');
            }
        });

        $('.app-navigation-portfolio .n-navi__hideitem--button').click(() => {
            let nNaviList = $('.app-navigation-portfolio .n-navi .n-navi__list');
            let nSearch = $('.app-navigation-portfolio .n-search');

            if (nNaviList.hasClass('n-navi__list--hide')) {
                nNaviList.removeClass('n-navi__list--hide');
            }

            if (!nSearch.hasClass('n-search__hideitem')) {
                nSearch.addClass('n-search__hideitem');
            }
        });

    } else {
        $('.app-navigation-portfolio .n-search__name--input').click(() => {
            let mobileMenu = $('.app-navigation .n-mobile__menu');
            let nSearch = $('.app-navigation-portfolio .n-search');

            if (!nSearch.hasClass('mobile-show')) {
                nSearch.addClass('mobile-show');
                $('body').addClass('banned-scroll');
                mobileMenu.removeClass('show');
            }
        });
        $('.app-navigation-portfolio .js_n-search__mobile--close').click(() => {
            let mobileMenu = $('.app-navigation .n-mobile__menu');
            let nSearch = $('.app-navigation-portfolio .n-search');

            if (nSearch.hasClass('mobile-show')) {
                nSearch.removeClass('mobile-show');
                $('body').removeClass('banned-scroll');
                mobileMenu.removeClass('show')
            }
        });
        $('.dropdown-input').click(() => {
            let cpSearch = $('.mobile-search');
            let mobileMenu = $('.app-navigation .n-mobile__menu');

            if (!cpSearch.hasClass('mobile-search--show')) {
                cpSearch.addClass('mobile-search--show');
                $('body').addClass('banned-scroll');
                mobileMenu.removeClass('show');
            }
        });
        $('.js_mobile-search__close').click(() => {
            let cpSearch = $('.mobile-search');
            let mobileMenu = $('.app-navigation .n-mobile__menu');

            if (cpSearch.hasClass('mobile-search--show')) {
                cpSearch.removeClass('mobile-search--show');
                $('body').removeClass('banned-scroll');    
                mobileMenu.removeClass('show');
            }
        });
        $('.portfolio__bg, .portfolio__templates, .app-footer, .portfolio-tab').click(() => {
            let mobileMenu = $('.app-navigation .n-mobile__menu');
            if (mobileMenu.hasClass('show')) {
                mobileMenu.removeClass('show');
            }
        });
        
        $('.app-navigation .n-mobile__toggle--button').click(() => {
            let mobileMenu = $('.app-navigation .n-mobile__menu');
            let nSearch = $('.app-navigation-portfolio .n-search');
            if (mobileMenu.hasClass('show')) {
                mobileMenu.removeClass('show')
            } else {
                mobileMenu.addClass('show')
                nSearch.removeClass('mobile-show');
                $('body').removeClass('banned-scroll');
            }
        });
    }
    $('#js_n-search__form').submit(() => {
        console.log('Form submit!');
        event.preventDefault();
    });
})