const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.sass('theme-popup/aveo/assets/scss/edit-style.scss', 'theme-popup/aveo/assets/css');

mix.sass('theme-popup/optio/scss/popup-edit.scss', 'theme-popup/optio/css');

mix.sass('theme-popup/material/scss/popup-edit.scss', 'theme-popup/material/stylesheets/_popup-edit.css');

mix.sass('theme-popup/material/scss/portfolio-editbar.scss', 'theme-popup/material/stylesheets/_portfolio-editbar.css');

/* ================= Mix CSS and JS Theme Maha ================= */
mix.sass('theme-popup/maha/assets/scss/edit.scss', 'theme-popup/maha/assets/css/edit.css');

mix.styles([
    'theme-popup/maha/assets/plugins/css/bootstrap.min.css',
    'theme-popup/maha/assets/plugins/css/animate.css',
    'theme-popup/maha/assets/plugins/css/owl.css',
    'theme-popup/maha/assets/plugins/css/jquery.fancybox.min.css',
    'theme-popup/maha/assets/css/styles.css',
    'theme-popup/maha/assets/css/responsive.css',
    'theme-popup/maha/assets/css/demo.css',
    // 'theme-popup/maha/assets/css/edit.css',
], 'theme-popup/maha/assets/css/maha.css');

mix.scripts([
    'theme-popup/maha/assets/plugins/js/jquery.min.js',
    'theme-popup/maha/assets/plugins/js/popper.min.js',
    'theme-popup/maha/assets/plugins/js/bootstrap.min.js',
    'theme-popup/maha/assets/plugins/js/owl.carousel.js',
    'theme-popup/maha/assets/plugins/js/validator.min.js',
    'theme-popup/maha/assets/plugins/js/wow.min.js',
    'theme-popup/maha/assets/plugins/js/jquery.mixitup.min.js',
    'theme-popup/maha/assets/plugins/js/circle-progress.js',
    'theme-popup/maha/assets/plugins/js/jquery.nav.js',
    'theme-popup/maha/assets/plugins/js/jquery.fancybox.min.js',
    'theme-popup/maha/assets/plugins/js/isotope.pkgd.js',
    'theme-popup/maha/assets/plugins/js/packery-mode.pkgd.js',
    'theme-popup/maha/assets/js/custom-scripts.js',
    'theme-popup/maha/assets/js/styleswitcher.js',
    'theme-popup/maha/assets/js/demo.js',
    'theme-popup/maha/assets/js/notiflix-2.5.0.min.js',
    'theme-popup/maha/assets/js/notiflix-aio-2.5.0.min.js',
    // 'theme-popup/maha/assets/js/_common.js',
    // 'theme-popup/maha/assets/js/_validate.js',
    // 'theme-popup/maha/assets/js/_form-submit.js',
],'theme-popup/maha/assets/js/maha.js');
/* ================= End Mix CSS and JS Theme Maha ================= */

/* ================= Edit Bar ================= */
mix.sass('theme-popup/assets/scss/portfolio-editbar.scss', 'theme-popup/assets/css/portfolio-editbar.css');
mix.sass('theme-popup/assets/scss/portfolio-homebar.scss', 'theme-popup/assets/css/portfolio-homebar.css');
/* ================= End Edit Bar ================= */


/* ================= Employer ================= */


/* Static */
mix.sass('employer/assets/frontend/static/scss/static.scss', 'employer/assets/frontend/static/css/static.css');
/* End Static */

/* Navbar */
mix.sass('employer/assets/frontend/navbar/scss/employer-navbar.scss', 'employer/assets/frontend/navbar/css/employer-navbar.css');
/* End Navbar */

/* Footer */
mix.sass('employer/assets/frontend/footer/scss/footer.scss', 'employer/assets/frontend/footer/css/footer.css');
/* End Footer */

/* Dashboard */
mix.sass('employer/assets/frontend/dashboard/scss/employer-dashboard.scss', 'employer/assets/css/employer-dashboard.css');

mix.scripts([
    './employer/assets/frontend/dashboard/js/action.js',
    './employer/assets/frontend/dashboard/js/generate.js',
    './employer/assets/frontend/dashboard/js/script.js',
], './employer/assets/js/employer-dashboard.js');
/* End Dashboard */

/* Job-management */
mix.sass('employer/assets/frontend/job_management/scss/employer-job_management.scss', 'employer/assets/css/employer-job_management.css');

mix.scripts([
    './employer/assets/frontend/job_management/js/action.js',
    './employer/assets/frontend/job_management/js/generate.js',
    './employer/assets/frontend/job_management/js/script.js',
], './employer/assets/js/employer-job_management.js');
/* End Job-management */

/* Candidate management */
mix.sass('employer/assets/frontend/candidate_management/scss/employer-candidate_management.scss', 'employer/assets/css/employer-candidate_management.css');

mix.scripts([
    './employer/assets/frontend/candidate_management/js/action.js',
    './employer/assets/frontend/candidate_management/js/generate.js',
    './employer/assets/frontend/candidate_management/js/script.js',
], './employer/assets/js/employer-candidate_management.js');
/* End Candidate management */

/* Search CV */
mix.sass('employer/assets/frontend/search_cv/scss/employer-searchcv.scss', 'employer/assets/css/employer-searchcv.css');

mix.scripts([
    './employer/assets/frontend/search_cv/js/variable.js',
    './employer/assets/frontend/search_cv/js/search-cv.js',
    './employer/assets/frontend/search_cv/js/binding.js',
    './employer/assets/frontend/search_cv/js/action.js',
    './employer/assets/frontend/search_cv/js/init.js',
], './employer/assets/js/employer-search_cv.js');
/* End Search CV */

/* Question */
mix.sass('employer/assets/frontend/questions/scss/employer-questions.scss', 'employer/assets/css/employer-questions.css');

mix.scripts([
    './employer/assets/frontend/questions/js/script.js',
], './employer/assets/js/employer-questions.js');
/* End Question */

/* Post Job */
mix.sass('employer/assets/frontend/post_job/scss/employer-postjob.scss', 'employer/assets/css/employer-postjob.css');

mix.scripts([
    './employer/assets/frontend/post_job/js/post-job.js',
    './employer/assets/frontend/post_job/js/binding.js',
    './employer/assets/frontend/post_job/js/action.js',
    './employer/assets/frontend/post_job/js/init.js',
], './employer/assets/js/employer-postjob.js');
/* End Post Job */

/* Buy Point */
mix.sass('employer/assets/frontend/buy_point/scss/buy-point.scss', 'employer/assets/css/employer-buy_point.css');

mix.scripts([
    './employer/assets/frontend/buy_point/js/buy-point.js',
], './employer/assets/js/employer-buy_point.js');
/* End Buy Point */

/* Landing Page */
mix.sass('employer/assets/frontend/landing_page/scss/landing-page.scss',
    './employer/assets/css/employer-landing_page.css');

mix.scripts([
    './employer/assets/frontend/landing_page/js/landing-page.js',
], './employer/assets/js/employer-landing_page.js');
/* End Landing Page */

/* Company Information */
mix.sass('employer/assets/frontend/company_information/scss/company-information.scss', 'employer/assets/css/employer-company_information.css');

mix.scripts([
    './employer/assets/frontend/company_information/js/variable.js',
    './employer/assets/frontend/company_information/js/script.js',
    './employer/assets/frontend/company_information/js/binding.js',
    './employer/assets/frontend/company_information/js/action.js',
    './employer/assets/frontend/company_information/js/init.js',
], './employer/assets/js/employer-company_information.js');
/* End Company Information */

/* Authenticate */
mix.sass('employer/assets/frontend/auth/scss/auth.scss',
    'employer/assets/css/employer-auth.css');

mix.scripts([
    './employer/assets/frontend/auth/js/variable.js',
    './employer/assets/frontend/auth/js/script.js',
    './employer/assets/frontend/auth/js/binding.js',
    './employer/assets/frontend/auth/js/action.js',
    './employer/assets/frontend/auth/js/init.js',
], './employer/assets/js/employer-auth.js');
/* End Authenticate */

/* Authenticate */
mix.sass('employer/assets/frontend/report_error_modal/scss/report_error.scss',
    'employer/assets/frontend/report_error_modal/css/employer-report_error.css');

mix.scripts([
    './employer/assets/frontend/report_error_modal/js/script.js',
    './employer/assets/frontend/report_error_modal/js/action.js',
    './employer/assets/frontend/report_error_modal/js/init.js',
], './employer/assets/frontend/report_error_modal/js/employer-report_error.js');
/* End Authenticate */

/* Common */
mix.styles([
    './employer/assets/frontend/static/css/notiflix-2.5.0.min.css',
    './employer/assets/frontend/static/css/static.css',
    './employer/assets/frontend/navbar/css/employer-navbar.css',
    './employer/assets/frontend/report_error_modal/css/employer-report_error.css',
    './employer/assets/frontend/footer/css/footer.css',
], './employer/assets/css/employer.css');

mix.scripts([
    './employer/assets/frontend/static/js/notiflix-2.5.0.min.js',
    './employer/assets/frontend/static/js/notiflix-aio-2.5.0.min.js',
    './employer/assets/frontend/static/js/request.js',
    './employer/assets/frontend/static/js/common.js',
    './employer/assets/frontend/static/js/validation.js',
    './employer/assets/frontend/report_error_modal/js/employer-report_error.js',
], './employer/assets/js/employer.js');
/* End Common */

/* ================= End Employer ================= */

/* ================= Candidate ================= */
/* Static */
mix.sass('candidate/assets/frontend/static/scss/static.scss', 'candidate/assets/frontend/static/css/static.css');
/* End Static */

/* Header */
mix.sass('candidate/assets/frontend/header/scss/header.scss', 'candidate/assets/frontend/header/css/header.css');
/* End Header */

/* Footer */
mix.sass('candidate/assets/frontend/footer/scss/footer.scss', 'candidate/assets/frontend/footer/css/footer.css');
/* End Footer */

/* Authenticate */
mix.sass('candidate/assets/frontend/auth/scss/auth.scss',
    'candidate/assets/css/candidate-auth.css');

mix.scripts([
    './candidate/assets/frontend/auth/js/variable.js',
    './candidate/assets/frontend/auth/js/script.js',
    './candidate/assets/frontend/auth/js/binding.js',
    './candidate/assets/frontend/auth/js/action.js',
    './candidate/assets/frontend/auth/js/init.js',
], './candidate/assets/js/candidate-auth.js');
/* End Authenticate */


/* Home */
mix.sass('candidate/assets/frontend/home/scss/home.scss', 'candidate/assets/frontend/home/css/home.css');

mix.styles([
    './candidate/assets/frontend/static/css/bootstrap.min.css',
    './candidate/assets/frontend/static/css/notiflix-2.5.0.min.css',
    './candidate/assets/frontend/static/css/select2.min.css',
    './candidate/assets/frontend/static/css/animate.css',
    './candidate/assets/frontend/static/css/slick.css',
    './candidate/assets/frontend/static/css/static.css',
    './candidate/assets/frontend/header/css/header.css',
    './candidate/assets/frontend/footer/css/footer.css',
    './candidate/assets/frontend/home/css/home.css'
],  './candidate/assets/css/candidate-home.css');

mix.scripts([
    './candidate/assets/frontend/static/js/jquery.min.js',
    './candidate/assets/frontend/static/js/popper.min.js',
    './candidate/assets/frontend/static/js/bootstrap.min.js',
    './candidate/assets/frontend/static/js/lodash.min.js',
    './candidate/assets/frontend/static/js/notiflix-2.5.0.min.js',
    './candidate/assets/frontend/static/js/notiflix-aio-2.5.0.min.js',
    './candidate/assets/frontend/static/js/slick.js',
    './candidate/assets/frontend/static/js/select2.min.js',
    './candidate/assets/frontend/static/js/request.js',
    './candidate/assets/frontend/static/js/common.js',
    './candidate/assets/frontend/static/js/validation.js',
    './candidate/assets/frontend/home/js/variable.js',
    './candidate/assets/frontend/home/js/script.js',
    './candidate/assets/frontend/home/js/binding.js',
    './candidate/assets/frontend/home/js/action.js',
    './candidate/assets/frontend/home/js/init.js',
],  './candidate/assets/js/candidate-home.js');
/* End Home */

/* Search Jobs */
mix.sass('candidate/assets/frontend/search_jobs/scss/search_jobs.scss', 'candidate/assets/css/candidate-search_jobs.css');

mix.scripts([
    './candidate/assets/frontend/search_jobs/js/variable.js',
    './candidate/assets/frontend/search_jobs/js/script.js',
    './candidate/assets/frontend/search_jobs/js/binding.js',
    './candidate/assets/frontend/search_jobs/js/action.js',
    './candidate/assets/frontend/search_jobs/js/init.js',
],  './candidate/assets/js/candidate-search_jobs.js');
/* End Search Jobs */

/* End Detail Jobs */
mix.sass('candidate/assets/frontend/detail_job/scss/detail_job.scss', 'candidate/assets/css/candidate-detail_job.css');

mix.scripts([
    './candidate/assets/frontend/detail_job/js/variable.js',
    './candidate/assets/frontend/detail_job/js/script.js',
    './candidate/assets/frontend/detail_job/js/binding.js',
    './candidate/assets/frontend/detail_job/js/action.js',
    './candidate/assets/frontend/detail_job/js/init.js',
],  './candidate/assets/js/candidate-detail_job.js');
/* End Detail Jobs */

/* Common */
mix.styles([
    './candidate/assets/frontend/static/css/bootstrap.min.css',
    './candidate/assets/frontend/static/css/notiflix-2.5.0.min.css',
    './candidate/assets/frontend/static/css/owl.carousel.min.css',
    './candidate/assets/frontend/static/css/owl.theme.default.min.css',
    './candidate/assets/frontend/static/css/daterangepicker.css',
    './candidate/assets/frontend/static/css/lightgallery.min.css',
    './candidate/assets/frontend/static/css/select2.min.css',
    './candidate/assets/frontend/static/css/tagsinput.css',
    './candidate/assets/frontend/static/css/animate.css',
    './candidate/assets/frontend/static/css/slick.css',
    './candidate/assets/frontend/static/css/static.css',
    './candidate/assets/frontend/header/css/header.css',
    './candidate/assets/frontend/footer/css/footer.css',
],  './candidate/assets/css/candidate.css');

mix.scripts([
    './candidate/assets/frontend/static/js/jquery.min.js',
    './candidate/assets/frontend/static/js/popper.min.js',
    './candidate/assets/frontend/static/js/bootstrap.min.js',
    './candidate/assets/frontend/static/js/jquery.mousewheel.min.js',
    './candidate/assets/frontend/static/js/lightgallery-all.min.js',
    './candidate/assets/frontend/static/js/lodash.min.js',
    './candidate/assets/frontend/static/js/moment.min.js',
    './candidate/assets/frontend/static/js/daterangepicker.js',
    './candidate/assets/frontend/static/js/owl.carousel.min.js',
    './candidate/assets/frontend/static/js/notiflix-2.5.0.min.js',
    './candidate/assets/frontend/static/js/notiflix-aio-2.5.0.min.js',
    './candidate/assets/frontend/static/js/lazy.js',
    './candidate/assets/frontend/static/js/slick.js',
    './candidate/assets/frontend/static/js/select2.min.js',
    './candidate/assets/frontend/static/js/tagsinput.js',
    './candidate/assets/frontend/static/js/request.js',
    './candidate/assets/frontend/static/js/common.js',
    './candidate/assets/frontend/static/js/validation.js',
    './candidate/assets/frontend/header/js/script.js',
],  './candidate/assets/js/candidate.js');
/* End Common */
/* ================= End Candidate ================= */

/* ================= Start Honor ================= */

/* Static */
mix.sass('honor/assets/frontend/static/scss/static.scss', 'honor/assets/frontend/static/css/static.css');
/* End Static */

/* Header */
mix.sass('honor/assets/frontend/header/scss/header.scss', 'honor/assets/frontend/header/css/header.css');
/* End header */

/* Footer */
mix.sass('honor/assets/frontend/footer/scss/footer.scss', 'honor/assets/frontend/footer/css/footer.css');
/* End Footer */

/* Common */
mix.styles([
    './honor/assets/frontend/static/css/static.css',
    './honor/assets/frontend/header/css/header.css',
    './honor/assets/frontend/footer/css/footer.css',
], './honor/assets/css/honor.css');

mix.sass('honor/assets/frontend/honor_detail/scss/honor_detail.scss', 'honor/assets/css/honor_detail.css');

mix.sass('honor/assets/frontend/honor_homepage/scss/honor_homepage.scss', 'honor/assets/css/honor_homepage.css');


/* ================= End Honor ================= */
