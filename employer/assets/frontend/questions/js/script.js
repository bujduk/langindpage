(function ($) {

})(jQuery);

_handleNavItemClicked = ($nav) => {
    let id = $nav.attr('data-id');

    $('.qs-nav__item').removeClass('active');
    $nav.addClass('active');
    $('.qs_switch-id').next().removeClass('active');
    $(`#${id}`).next().addClass('active');
}