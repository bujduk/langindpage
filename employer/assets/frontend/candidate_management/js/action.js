let contentSearchTimeout = null;
let sendRequestLazy = false;
let lastPageCandidate = 6;

const  listCandidateStatus = {
    "offer_accepted": "offer_accepted",
    "pending_review": "pending_review",
    "pass_interview": "pass_interview",
    "fail_interview": "fail_interview",
    "offer_refused": "offer_refused",
    "cv_qualified": "cv_qualified",
    "cancel_interview": "cancel_interview",
    "cv_not_qualified": "cv_not_qualified",
    "went_to_work": "went_to_work",
};

$(document).ready(() => {
    /*==================SELECT STATUS===================*/
    $("#list-candidate_status").on("select2:select",  (e) => {
        filterData($(e.target));
    });

    $("#list-candidate__search-job").on("select2:select", (e) => {
        filterData($(e.target));
    })

    $("#user-upload").on("select2:select", (e) => {
        filterData($(e.target));
    })

    calenderDateRager();

    $('#listCandidate-calender-date-ranger').on('apply.daterangepicker', (e) => {
        filterData($(e.target));
    });

    $('#search_content').on('input change', (e) => {

        clearTimeout(contentSearchTimeout);

        contentSearchTimeout = setTimeout(() => {
            filterData($(e.target));
        }, 1000);
    })

    /* ================================ FORM SUBMIT ================================= */
    $('#edit_status_candidate_form').submit((e) => {
        e.preventDefault();

        let modal = $('#edit_status_candidate_modal');
        let id = modal.attr('data-id');
        let value = $('#edit_status').val()

        $(`#list-candidate_status_${id}`).html(`<span class="candidate__status--${value}">${listCandidateStatus[value]}</span>`);

        modal.modal('hide');
    });

    $('#go-to').on('keyup', (e) => {
        if (e.keyCode === 13) {
            e.preventDefault();
            indexPagination($(e.target));
        }
    })
})

listActionOpen = (_this, prefixClass, prefixId) => {
    let id = _this.attr('data-id');

    $(`#${prefixClass}_${id} .dropdown`).on('hidden.bs.dropdown', () => {
        $(`#${prefixClass}_${id} .list-action--button`).removeClass('button-active');
    })

    $(`#${prefixClass}_${id} .dropdown`).on('show.bs.dropdown', () => {
        $(`#${prefixClass}_${id} .list-action--button`).addClass('button-active');
    })

    $('.status-action').remove();
}

/*==================================PAGIGNATION======================*/

nextPage = (_this, prefixId) => {
    let value = parseInt($(`#${prefixId}--present`).attr('data-page'));

    if (value !== lastPageJob) {
        value++;

        $('html,body').animate({
                scrollTop: $('.calender').offset().top - 20},
            500);

        pageCss(prefixId, value);
        filterListData(_this, prefixId);
    }

    if (value === lastPageJob) {
        pageNextCss(prefixId);
    }
}

prevPage = (_this, prefixId) => {
    let value = parseInt($(`#${prefixId}--present`).attr('data-page'));
    let min = 1;

    if (value !== min) {
        value--;

        $('html,body').animate({
                scrollTop: $('.calender').offset().top - 20},
            500);

        pageCss(prefixId, value);
        filterListData(_this, prefixId);
    }
    if (value === min) {
        pagePrevCss(prefixId);
    }
}

indexPagination = (goToInput) => {
    let value = _.toNumber(goToInput.val());
    let presentPage = $('#list-candidate--present');
    let totalPage = _.toNumber($('#total-page').attr('data-total'));

    $('html,body').animate({
            scrollTop: $('.calender').offset().top - 20},
        500);

    value = value < 1 ? 1 : value;
    value = value > lastPageCandidate ? lastPageCandidate : value;

    presentPage.html(value);
    presentPage.attr('data-page', value);
    pageCss('list-candidate', value);

    filterListData();
    goToInput.val('');

    if (value === 1) {
        pagePrevCss('list-candidate');
    }

    if (value === totalPage) {
        pageNextCss('list-candidate');
    }
}

/*==============================SCROLL APPEND DATA================================*/
listBodyScroll = (_this, prefixId) => {
    let contentListBody = _this.get(0);

    Notiflix.Block.Dots('.lazy-loading');
    if (contentListBody.scrollTop + contentListBody.clientHeight === (contentListBody.scrollHeight)) {
        setTimeout(() => {
            if (!sendRequestLazy) {
                sendRequestLazy = true;
                appendListCandidate();
                sendRequestLazy = false;
            }
        }, 1000);
    }
}

/* ============== SORT ITEM =============== */
sortItem = (_this) => {
    let headerIndex = _this.attr('data-sort');
    if (headerIndex === '2') {
        headerIndex = 0;
    } else {
        headerIndex++;
    }
    _this.attr('data-sort', headerIndex);

    $('.text-list-title').attr('data-sort', '0');

    if (headerIndex === 1) {
        resetSortIcon();
        sortItemArrowUp(_this);
        sortAppend();
    } else if (headerIndex === 2) {
        resetSortIcon();
        sortItemArrowDown(_this);
        sortAppend();
    } else {
        resetSortIcon();
        sortAppend();
    }
}

/*===============BINDING DATA==============*/
filterData = (e) => {
    let searchName = $('#search_content').val();
    let userUpload = $('#user-upload').val();
    let statusVal = $('#list-candidate_status').val();
    let startTime = $('#listCandidate-calender-date-ranger').val().split(' - ')[0];
    let endTime = $('#listCandidate-calender-date-ranger').val().split(' - ')[1];
    let searchJob = $('.form-control').val();
    let url = `itnavi.com.vn/job/search?keyword=${searchName}&searchJob=${searchJob}&searchUserUpload=${userUpload}&status=${statusVal}&startTime=${startTime}&endTime=${endTime}`;

    alert(url)
}

filterListData = (_this) => {
    Notiflix.Loading.Pulse();

    setTimeout(() => {
        removeListItem();
        appendListCandidate();
        Notiflix.Loading.Remove();
    }, 1000);
}