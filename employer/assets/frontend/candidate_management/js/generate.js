generateCandidateItem = (key) => {
    let html = `<div class="list__content--item d-flex align-items-center" id="list-candidate_${key}">
                            <div class="numerical-order text-left d-flex align-items-center justify-content-center app-desktop">
                                <span class="text-list-title"></span>
                            </div>
                            <div class="d-flex align-items-center flex-column flex-lg-row w-100">
                                <div class="name-candidate d-flex align-items-center">
                                    <div class="name-avt" style="background-image: url(assets/images/avt.png)"></div>
                                    <div class="name-info">
                                        <span class="text-default">Nguyễn Lê Nam</span>
                                        <span class="job-title">UI & UX Designer</span>
                                    </div>
                                </div>
                                <div class="d-flex align-items-center basic-info">
                                    <div class="vacancies basic-info--content"
                                        title="Sr Android Dev (Java, Kotlin) Up to1500$">
                                        <span class="basic-info__title app-mobile">Vị trí ứng
                                            tuyển</span>
                                        <span class="basic-info__job">Sr Android Dev (Java, Kotlin) Up
                                            to1500$</span>
                                    </div>
                                    <div class="application-date basic-info--content">
                                        <span class="basic-info__title app-mobile">Ngày ứng
                                            tuyển</span>
                                        <span class="basic-info__job">10/09/2020</span>
                                    </div>
                                </div>
                                <div class="status" id="list-candidate_status_${key}">
                                    <span class="candidate__status--offer_accepted">
                                        Offer Accepted
                                    </span>
                                </div>
                                <div class="list-action" id="list-action_${key}">
                                    <div class="dropdown show">
                                        <button class="list-action--button btn" data-id="${key}"
                                                data-toggle="dropdown"
                                                onclick="listActionOpen($(this), 'list-action', 'list-candidate')"
                                                role="button">
                                            Lựa chọn
                                            <svg fill="none" height="6" viewBox="0 0 9 6" width="9"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M8.69812 0H0.301875C0.0500391 0 -0.0905801 0.294357 0.0653794 0.495312L4.2635 5.88431C4.38367 6.03856 4.61505 6.03856 4.7365 5.88431L8.93462 0.495312C9.09058 0.294357 8.94996 0 8.69812 0Z"
                                                    fill="#1C4355"/>
                                            </svg>
                                        </button>
                                        <div class="dropdown-menu list__action--dropdown">
                                            <button class="dropdown-item"
                                                    data-id="${key}" data-status="offer_accepted"
                                                    onclick="modalCandidateOpen($(this))">
                                                Đổi trạng thái
                                            </button>
                                            <button class="dropdown-item" value="edit" onclick="noteModal($(this))">Ghi chú</button>
                                            <button class="dropdown-item" onclick="downloadCv($(this))"
                                                    readonly>Tải xuống CV
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>`;

    $('.height__list--body').append(html);
}

/*===============GENERATE LIST JOB==============*/
appendListCandidate = (_this) => {
    for (let i = 0; i < 15; i++) {
        let key = generateKey();

        generateCandidateItem(key);
    }
}

sortAppend = () => {
    let arraySort = [''];

    $('.list__content--item').remove();

    arraySort.forEach(() => {
        appendListCandidate();
    })

}
