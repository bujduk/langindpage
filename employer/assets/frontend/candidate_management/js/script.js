$(document).ready(() => {

    $('#edit_status_candidate_modal').on('hidden.bs.modal', () => {
        $('#edit_statusCandidate_modal').val(null).trigger('change');
    })

    $('#candidateNote_modal').on('hidden.bs.modal', () => {
        $('.modal-body--note').val('');
    })

    /*==================PAGINATION=================*/
    $('.page-link').on('click', (e) => {
        let _this = $(e.target);
        let pageLink = $('.page-link')

        if (pageLink.hasClass('page-link-active')) {
            pageLink.removeClass('page-link-active');
        }
        if (!pageLink.hasClass('page-link-active')) {
            _this.addClass('page-link-active');
        }
    })

    /*===============STATUS=======================*/
    $('#list-candidate_status').select2({
        placeholder: 'Trạng thái',
        minimumResultsForSearch: -1,
        dropdownCssClass: 'candidate-select2-dropdown',
        selectionCssClass: 'candidate-select2-selection',
        width: '100%'
    });

    $('#edit_status').select2({
        placeholder: 'Thay đổi trạng thái',
        minimumResultsForSearch: -1,
        dropdownCssClass: 'candidate-select2-dropdown',
        selectionCssClass: 'candidate-select2-selection',
        width: '100%',
    });

    $(".form-control").select2({
        placeholder: 'Vị trí tuyển dụng',
        tags: true,
        width: '100%',
        selectionCssClass: 'candidate-select2-selection',
    });

    $('#user-upload').select2({
        placeholder: 'Người đăng',
        minimumResultsForSearch: -1,
        dropdownCssClass: 'candidate-select2-dropdown user-upload-select2-dropdown',
        selectionCssClass: 'candidate-select2-selection',
        width: '132px',
    })

    /*==================SHADOW LIST JOB================*/
    $('.js_list-body').scroll(function () {
        if ($(this).scrollTop()) {
            $('.job__list--header').addClass('header-shadow');
        } else {
            $('.job__list--header').removeClass('header-shadow');
        }
    })

    /*===================TOTAL LIST======================*/
    $('.total-list span').html($('.list__content--item').length);

    Notiflix.Block.Init({
        svgColor: "#32c682",
    });
})

/* Generate key */
generateKey = () => {
    return Math.random().toString(20).substr(2, 10);
}

/*=======================SORT ITEM ARROW=======================*/
sortItemArrowUp = (_this) => {
    $('.arrow-up', _this).addClass('sort');
    $('.arrow-down', _this).removeClass('sort');
    _this.attr('data-sort', 1);
}

sortItemArrowDown = (_this) => {
    $('.arrow-down', _this).addClass('sort');
    $('.arrow-up', _this).removeClass('sort');
    _this.attr('data-sort', 2);
}

/*=======================RESET SORT ICON======================*/

resetSortIcon = () => {
    $('.arrow-down').removeClass('sort')
    $('.arrow-up').removeClass('sort');
}

/*==================CALENDER===================*/
calenderDateRager = () => {
    $('#listCandidate-calender-date-ranger').daterangepicker({
        opens: 'left',
        autoUpdateInput: false,
        locale: {
            cancelLabel: 'Clear',
        },

        ranges: {
            'Hôm nay': [moment(), moment()],
            'Hôm qua': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            '7 ngày trước': [moment().subtract(6, 'days'), moment()],
            '30 ngày trước': [moment().subtract(29, 'days'), moment()],
            'Tháng này': [moment().startOf('month'), moment().endOf('month')],
            'Tháng trước': [moment().subtract(29, 'days').startOf('month'), moment().subtract(29, 'days').endOf('month')],
        },
        "alwaysShowCalendars": $(window).innerWidth() > 992,
    }).on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
    }).on('cancel.daterangepicker', function (ev, picker) {
        $(this).val('');
    });
}

/* ======================= STATUS CHANGE ========================== */
statusChangeCss = (prefixId) => {
    $(`#${prefixId} .js_list-content :checkbox`).prop('checked', false).attr('checked', false);
    $(`#${prefixId} .candidate__list--title`).removeClass('element-hidden');
    $(`#${prefixId} .js_content-list--action`).addClass('element-hidden');
    $(`#${prefixId} .selectAllCheckbox`).prop('checked', false);
    $(`#${prefixId}_status-change`).val(null).trigger('change');
}

/* ======================= PAGE ========================== */
$('#list-candidate--present').html(1);


pageCss = (prefixId, value) => {
    $(`#${prefixId}--present`).attr('data-page', value);
    $(`#${prefixId}--present`).html($(`#${prefixId}--present`).attr('data-page'));
    $(`#${prefixId} .next`).removeClass('disabled');
    $(`#${prefixId} .prev`).removeClass('disabled');
}

pageNextCss = (prefixId) => {
    $(`#${prefixId} .next`).addClass('disabled');
}

pagePrevCss = (prefixId) => {
    $(`#${prefixId} .prev`).addClass('disabled');
}

/* ============================= OPEN POPUP ===================================== */
modalCandidateOpen = (_this) => {
    let id = _this.attr('data-id');
    let status = _this.attr('data-status');
    let modal = $('#edit_status_candidate_modal');

    modal.attr('data-id', `${id}`);
    $('#edit_status').val(status).trigger('change');

    modal.modal('show');
};

noteModal = (_this, id) => {
    let modal = $('#candidate_note_modal');

    modal.attr('data-id', id);
    modal.modal('show');
}
