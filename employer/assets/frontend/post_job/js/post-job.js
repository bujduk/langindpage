let currentPostTabIndex = '1';
let thumbnailImage;

let handleTinyMceOnchange = (prefixClass, value) => {
    $(prefixClass).val(value);
}


let checkMaxItemInList = (prefixClass) => {
    let count = $(`.pj_${prefixClass}_item`).length;

    if (count === 0) {
        generateNewJobAddress();
    }

    $(`.pj_${prefixClass}_list`).attr('data-count', count);
    $(`.pj_${prefixClass}_add`).attr('hidden', count >= 3).is(':hidden', count >= 3);
}

let removeItem = (prefixClass, $button) => {
    let id = $button.attr('data-id')
    let count = $(`.pj_${prefixClass}_item`).length;

    if (count <= 1) {
        $(`#${prefixClass}_${id}`).find(':input').val(null).trigger('change');

    } else {
        $(`#${prefixClass}_${id}`).remove();
    }

    checkMaxItemInList(prefixClass);
}

let pjNavBorderChange = (itemHover) => {
    let navItem = $('.pj-nav');
    let itemActive = $('.pj-nav__item.active');
    let itemBorder = $('.pj-nav__border');

    let itemHoverTop = itemHover.offset().top - navItem.offset().top;
    let itemHoverHeight = itemHover.height();

    let itemActiveTop = itemActive.offset().top - navItem.offset().top;
    let itemActiveHeight = itemActive.height();

    let currentTop = itemActiveTop;
    let currentHeight = itemActiveHeight;

    if (itemHoverTop > itemActiveTop) {
        itemBorder.css({
            'top': currentTop,
            'height': currentHeight + (itemHoverTop - itemActiveTop)
        });
    } else if (itemHoverTop < itemActiveTop) {
        itemBorder.css({
            'top': currentTop - (itemActiveTop - itemHoverTop),
            'height': currentHeight + (itemActiveTop - itemHoverTop)
        });
    } else {
        itemBorder.css({
            'top': currentTop,
            'height': currentHeight
        });
    }
};

let pjBorderBack = () => {
    let navItem = $('.pj-nav');
    let itemActive = $('.pj-nav__item.active');
    let itemBorder = $('.pj-nav__border');

    itemBorder.css({
        'top': itemActive.offset().top - navItem.offset().top,
        'height': itemActive.height()
    });
}

let pjNavClicked = (navItem) => {
    let currentNavItem = $(`.pj-nav__item[data-tab="${currentPostTabIndex}"]`);
    let nextPostTabIndex = navItem.attr('data-tab');

    let currentForm = $(`.pj_form_switch[data-tab="${currentPostTabIndex}"]`);
    let nextForm = $(`.pj_form_switch[data-tab="${nextPostTabIndex}"]`);

    let formErrorBuzz = $('.form-error-buzz')

    formErrorBuzz.removeClass('animate__headShake animate__faster');

    if (currentPostTabIndex !== nextPostTabIndex) {

        if (currentPostTabIndex < nextPostTabIndex) {

            let checkValidate = checkFormValidate(currentPostTabIndex);

            if (checkValidate) {
                Notiflix.Block.Pulse('.emp-box');

                currentForm.collapse('hide');
                nextForm.collapse('show');

                currentNavItem.addClass('successfully');

                currentPostTabIndex = nextPostTabIndex;

                $('.pj-nav__item').removeClass('active');
                navItem.addClass('active');

                if (currentPostTabIndex === '4') {
                    checkAllTabSuccessed();
                }

                pjBorderBack();
            } else {
                formErrorBuzz.removeClass('animate__fadeIn').addClass('animate__headShake animate__faster');

                currentNavItem.removeClass('successfully');
            }
        } else {
            Notiflix.Block.Pulse('.emp-box');

            currentForm.collapse('hide');
            nextForm.collapse('show');

            currentPostTabIndex = nextPostTabIndex;

            $('.pj-nav__item').removeClass('active');
            navItem.addClass('active');

            pjBorderBack();
        }

    }


}

let handleToggleCheckboxChange = (prefixClass, toggleCheckbox) => {
    let formField = $(`.${prefixClass}_list`);
    let listInput = formField.find(':input');
    let toggleIsChecked = toggleCheckbox.is(":checked");
    let addButton = $(`.${prefixClass}_add`);

    listInput.attr('disabled', toggleIsChecked).is('disabled', toggleIsChecked);
    addButton.attr('disabled', toggleIsChecked).is('disabled', toggleIsChecked);

    formField.find('.error-text').text('');
    formField.find('.error-input').removeClass('error-input');
    formField.find('.error-form').removeClass('error-form');
}

let checkFormValidate = (tabIndex = null) => {
    let rulesTab = {
        '1': {
            // 'name': 'required|maxlength:255',
            // 'employment_level': 'required',
            // 'employment_type': 'required',
            // 'employment_language': 'required',
            'address[%][province]': `required`,
            'address[%][address]': `required|maxlength:255`,
            // 'email_receiving_cv': 'required|listemail'
        },
        '2': {
            'description': 'required|maxlength:2000',
            'requirement': 'required|maxlength:2000',
            'tech_stack': 'required'
        },
        '3': {
            'benefit': 'maxlength:1000',
            'salary': 'maxlength:50'
        },
    }

    return $('#pj_form').validationObject({
        rules: rulesTab[tabIndex],
        elementInnerForm: `.pj_form_switch[data-tab="${tabIndex}"]`
    })

}

let pjServiceClick = ($button) => {
    if (!$button.hasClass('disabled') && !$('#select_service_modal .have-no-job').length) {
        $('.modal-body__item').removeClass('active');

        $('#post_service').val($button.attr('data-value'));

        $('.select_service_submit').prop('disabled', false).is(':disabled', false);

        $button.addClass('active');
    } else {
        $('#post_service').val(null);
        $('.select_service_submit').prop('disabled', true).is(':disabled', true);
    }
}

(function ($) {
    $('.pj_job_address_list').on('change input', ':input', () => {
        $('#is_current_location').prop('checked', false).is(':checked', false);
    });
    $('.pj_job_address_add').on('click', () => {
        $('#is_current_location').prop('checked', false).is(':checked', false);
    });
})(jQuery)

let checkAllTabSuccessed = () => {
    let tabSuccess = $(`.pj-nav__item-check.successfully`);
    let navItem = $(`.pj-nav__item[data-tab="4"]`);
    let postButton = $('.pj_form_post');

    if (!!tabSuccess.length && tabSuccess.length === 3) {
        navItem.addClass('successfully');
        postButton.prop('disabled', false).attr('disabled', false);
    } else {
        navItem.removeClass('successfully');
        postButton.prop('disabled', true).attr('disabled', true);
    }
}

let _handleThumbnailChange = ($input) => {
    let files = $input[0].files;
    let imgThumbnailError = $('#img_thumbnail_error');
    let imgThumbnailItem = $('.pj-form__thumbnail--item');
    let imgThumbnailView = $('.pj-form__thumbnail--background');
    let imgThumbnailTitle = $('.pj-form__thumbnail--title');
    let imgThumbnailValue = $('#thumbnail_img');

    imgThumbnailError.empty();

    let file = files[0];
    if (file.size > 5097152) {
        let html = `<span style="display:block;">${image} ${file.name} too 5MB</span>`;

        imgThumbnailError.append(html);
    } else if (file.type !== "image/jpeg" && file.type !== "image/png" && file.type !== "image/jpg") {
        let html = `<span style="display:block;">${image} ${file.name} ${gallery_incorrect_format}</span>`;

        imgThumbnailError.append(html);
    } else {
        let fileReader = new FileReader();

        thumbnailImage = file;

        fileReader.onload = ((fileParam) => {
            return (e) => {
                imgThumbnailItem.removeClass('element-hidden');
                imgThumbnailView.css('background-image', `url(${e.target.result})`);
                imgThumbnailTitle.text(fileParam.name);
                imgThumbnailValue.val(e.target.result);
            }
        })(file);
        fileReader.readAsDataURL(file);
    }
}

let _handleThumbnailRemove = () => {
    let imgThumbnailError = $('#img_thumbnail_error');
    let imgThumbnailItem = $('.pj-form__thumbnail--item');
    let imgThumbnailView = $('.pj-form__thumbnail--background');
    let imgThumbnailTitle = $('.pj-form__thumbnail--title');
    let imgThumbnailValue = $('#thumbnail_img');

    imgThumbnailError.empty();

    imgThumbnailItem.addClass('element-hidden');
    imgThumbnailView.css('background-image', `unset`);
    imgThumbnailTitle.empty();
    imgThumbnailValue.val(null);
}