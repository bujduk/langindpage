let listProvince = {
    'Ha Noi': 'Hà Nội',
    'Da Nang': 'Đà Nẵng',
    'Ho Chi Minh': 'Hồ Chí Minh',
}

let generateNewJobAddress = (animation = 'animate__animated animate__fadeInDown animate__faster', data = null) => {
    let jobLocationList = $('.pj_job_address_list');
    let randomKey = generateKey();
    let provinceSelect = '';

    jobLocationList.append(`
        <div id="job_address_${randomKey}"
             class="pj_job_address_item list-item row mx-n12 mb-1 d-flex position-relative ${animation}">
            <div class="col-12 col-md-5 col-xxl-4 px-12 pj-form__item">
                <div class=" d-flex align-items-center">
                    <label for="job_address_${randomKey}_province" class="pj-form__label mb-0 pj-form__label--inline ">
                        Tỉnh thành
                    </label>
                    <select id="address_${randomKey}_province" name="address[${randomKey}][province]"
                            class="pj_form_select2 invisible" data-placeholder="">
                            <option></option>
                    </select>
                </div>
                <p id="address_${randomKey}_province_error" class="error-text no-min-height"></p>
            </div>
            <div class="col-12 col-md-7 col-xxl-8 px-12 pj-form__item">
                <div class=" d-flex align-items-center">
                    <label for="address_${randomKey}_address"
                           class="pj-form__label mb-0 pj-form__label--inline ">
                        Địa chỉ
                    </label>
                    <input id="address_${randomKey}_address" name="address[${randomKey}][address]"
                             type="text" required maxlength="255"
                             class="pj-form__input w-100">
                </div>
                <p id="address_${randomKey}_address_error" class="error-text no-min-height text-left text-md-right"></p>
            </div>
            <div class="d-flex align-items-center justify-content-center pj-list-remove">
                <button type="button" tabindex="-1" data-id="${randomKey}"
                        onclick="removeItem('job_address', $(this))"
                        class="pj-list-remove__button pj_job_address_remove">
                    <i class="far fa-times-circle"></i>
                </button>
            </div>
        </div>
    `);

    $.each(listProvince, (name, value) => {
        provinceSelect += `
            <option value="${name}" 
                    ${(!!data && !!data.province && data.province === name) ? 'selected' : ''} >
                ${value}
            </option>
        `
    })

    $(`#address_${randomKey}_province`).append(provinceSelect);
    $(`#address_${randomKey}_address`).val((!!data && !!data.address) ? data.address : '');

    initSelect2Single($(`#address_${randomKey}_province`));
    checkMaxItemInList('job_address');
}

let generateListAddressFromData = (listAddress) => {
    let jobLocationList = $('.pj_job_address_list');

    jobLocationList.html('')

    _.forEach(listAddress, (item) => {
        generateNewJobAddress('', item);
    })
}