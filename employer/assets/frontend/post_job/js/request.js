getFormRequest = (url, formData = null) => {
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'json',
            data: formData,
            global: false
        }).done(function (response) {
            resolve(response);
        }).fail(function (error) {
            reject(error);
        });
    });
};