

let initSelect2Single = ($select) => {
    $($select).select2({
        minimumResultsForSearch: -1,
        selectionCssClass: 'pj_form_select2-selection',
        dropdownCssClass: 'pj_form_select2-dropdown',
        allowClear: (!!$select.attr('data-allowclear')),
        width: '100%',
        placeholder: $select.attr('data-placeholder'),
    });
}

let initSelect2Multiple = ($select, minimumResultsForSearch = 0) => {
    $($select).select2({
        selectionCssClass: 'pj_form_select2-multiple-selection',
        dropdownCssClass: 'pj_form_select2-multiple-dropdown',
        maximumSelectionLength: (!!$select.attr('data-length')) ? _.toNumber($select.attr('data-length')) : -1,
        width: '100%',
        tags: true,
        placeholder: $select.attr('data-placeholder'),
        language:{
            "noResults" :  () => {
                return null
            }
        }
    });
}

let pageInit = () => {
    checkMaxItemInList('job_address');
}

(function ($) {
    let pjNavItem = $('.pj-nav__item');
    let techStack = $('#tech_stack');

    Notiflix.Loading.Init({
        backgroundColor: "rgba(255,255,255,0.6)",
        svgColor: "#06202D",
    });

    pjNavItem.on('mouseover', (e) => {
        pjNavBorderChange($(e.target));
    });

    pjNavItem.on('mouseout', (e) => {
        pjBorderBack($(e.target));
    });

    $('.pj_form_select2').each((key, value) => {
        let $select = $(value);
        initSelect2Single($select);
    });

    $('.pj_form_select2_multiple').each((key, value) => {
        let $select = $(value);
        initSelect2Multiple($select);
    });

    $('.pj_form_select2_multiple_nosearch').each((key, value) => {
        let $select = $(value);
        initSelect2Multiple($select, -1);
    });

    $('.post_job_settime').daterangepicker({
        singleDatePicker: true,
        timePicker: true,
        timePicker24Hour: true,
        showDropdowns: true,
        opens: "left",
        drops: "up",
        minDate: moment().startOf('minute'),
        locale: {
            format: 'HH:mm DD/MM/YYYY'
        },
    }).on('cancel.daterangepicker', (e) => {
        $(e.target).addClass('disabled');
    }).on('apply.daterangepicker', (e) => {
        $(e.target).removeClass('disabled');
    });

    $('.pj_collapse').collapse({
        toggle: false
    });

    $('.pj_form_switch').on('shown.bs.collapse', () => {
        Notiflix.Block.Remove('.emp-box');
    });

    $('#select_service_modal').on('show.bs.modal', () => {
        if (!!$('#select_service_modal .have-no-job').length) {
            $('.post_job_settime').prop('disabled', true).is(':disabled', true);
        } else {
            $('.post_job_settime').prop('disabled', false).is(':disabled', false);
        }
    }).on('hidden.bs.modal', () => {
        $('.modal-body__item').removeClass('active');
        $('#post_service').val(null);
        $('.post_job_settime').val('').addClass('disabled');
        $('.select_service_submit').prop('disabled', true).is(':disabled', true);
    });

    techStack.select2({
        selectionCssClass: 'pj_form_select2-multiple-selection',
        dropdownCssClass: 'pj_form_select2-multiple-dropdown',
        maximumInputLength: 100,
        width: '100%',
        tags: true,
        placeholder: techStack.attr('data-placeholder'),
        ajax: {
            url: techStack.attr('data-url'),
            data: (params) => {
                console.log(params)
                return {
                    name: params.term
                }
            },
            processResults: function (response) {
                let output = {};
                let projectSelected = techStack.val();

                output.results = response.data.map((item) => {
                    let selected = _.includes(projectSelected, item.name)

                    return {
                        "id": item.name,
                        "text": item.name,
                        "selected": selected
                    }
                });

                return !!output ? output : null;
            }
        }
    })

    pageInit();
})(jQuery);