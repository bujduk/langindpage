/* Address */
let handleAddressJobChange = ($toggle) => {
    let toggleValue = $toggle.is(':checked');

    if (toggleValue) {
        getFormRequest('./assets/frontend/post-job/js/json/address.json')
            .then((responseData) => {
                generateListAddressFromData(responseData.data);
            }, function (responseErr) {
                if (responseErr.status === 422) {
                    commonShowNotifyErr();
                } else {
                    commonShowNotifyErr();
                }
            });
    }
}
/* End Address */

/* Form Submit */
(function ($) {
    $('#pj_form').on('submit', (e) => {
        e.preventDefault();

        let $form = $(e.target);
        let formData = $form.serializeObject();
        let convertAddress = [];

        /* Convert Data */
        formData['salary_is_negotiate'] = (formData['salary_is_negotiate'] === 'on');

        $.each(formData, (name, value) => {
            if (name.endsWith('][province]')) {
                convertAddress.push({
                    province: value,
                    address: formData[name.replace('][province]','][address]')]
                });

                delete formData[name];
                delete formData[name.replace('][province]','][address]')];
            }
        });
        formData['address'] = convertAddress;

    })
})(jQuery)
/* End Form Submit */
