const openS5Modal = (_this) => {
    let $modal = $('#lps5_modal');

    $modal
        .attr('data-binding')
        .split(',')
        .forEach(name => {
            $(`.modal-lps5-${name}`)
                .text(_this.attr(`data-${name}`));
            $(`.modal-lps5-${name}-background`)
                .css('background-image', `url(${_this.attr(`data-${name}`)})`);
        });

    $modal.modal('show');
}

const closeS5Modal = () => {
    $('#lps5_modal')
        .attr('data-binding')
        .split(',')
        .forEach(name => {
            $(`.modal-lps5-${name}`).text(null);
        });
}

const navbarScroll = () => {
    let top = $(this).scrollTop();

    $('.lp-navbar')
        .addClass((top > 250) ? 'scroll' : '')
        .removeClass((top > 250) ? '' : 'scroll');
}

const submitFormS6 = (_this) => {
    let checkValidate = _this.validationObject({
        rules: {
            'company_name': 'required',
            'phone': 'required|number|minlength:10|maxlength:20',
            'email': 'required|email',
        },
        customTranslateName: {
            'company_name': 'Tên công ty, doanh nghiệp',
            'phone': 'Số điện thoại',
            'email': 'Email',
        }
    });

    if (checkValidate) {
        Notiflix.Notify.Success('Success');
    } else {

    }
}

(function ($) {
    let $modal = $('#lps5_modal');

    new WOW().init();

    Notiflix.Loading.Init({
        backgroundColor: "rgba(255,255,255,0.6)",
        svgColor: "#06202D",
    });

    $('.lps5_slick').slick({
        dots: false,
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 3000,
        prevArrow: false,
        nextArrow: false,
        swipeToSlide: true,
        responsive: [
            {
                breakpoint: 1500,
                settings: {
                    slidesToShow: 3,
                }
            },
            {
                breakpoint: 992,
                settings: {
                    slidesToShow: 2,
                }
            },
            {
                breakpoint: 768,
                settings: {
                    slidesToShow: 1,
                }
            }
        ],
    });

    $(document).on('scroll', () => {
        navbarScroll();
    });

    $modal.on('hidden.bs.modal', () => {
        closeS5Modal();
    })

    $('#lps6_form').on('submit', (e) => {
        e.preventDefault();

        submitFormS6($(e.target));
    })
})(jQuery);