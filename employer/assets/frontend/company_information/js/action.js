(function ($) {
    $('#user_form').on('submit', (e) => {
        e.preventDefault();

        let $form = $(e.target);

        let checkValidate = $form.validationObject({
            rules: {
                'user_name': 'required|maxlength:255',
                'user_phone': 'required|number',
            },
            customTranslateName: {
                'user_name': 'Tên',
                'user_phone': 'Số điện thoại'
            }
        });

        if (checkValidate) {
            Notiflix.Notify.Success('Success');
        } else {

        }

    });

    $('#password_form').on('submit', (e) => {
        e.preventDefault();

        let $form = $(e.target);

        let checkValidate = $form.validationObject({
            rules: {
                'old_password': 'required|minlength:20',
                'new_password': 'required|minlength:20',
                'renew_password': 'required|minlength:20|equal:new_password',
            },
            customTranslateName: {
                'old_password': 'Mật khẩu hiện tại',
                'new_password': 'Mật khẩu mới',
                'renew_password': 'Xác nhận mật khẩu',
            }
        });

        if (checkValidate) {
            Notiflix.Notify.Success('Success');
        } else {

        }

    });

    $('#company_form').on('submit', (e) => {
        e.preventDefault();

        let $form = $(e.target);

        let checkValidate = $form.validationObject({
            rules: {
                'name': 'required|maxlength:255',
                'address[%][province]': `required`,
                'address[%][address]': `required|maxlength:255`,
                'introduction': 'required|maxlength:2000',
            },
        });

        if (checkValidate) {
            Notiflix.Notify.Success('Success');
        } else {

        }

    });
})(jQuery);

let paginationChange = (pageIndex = 1) => {
    getFormRequest('./assets/frontend/company_information/js/json/trade_pagination.json')
        .then((responseData) => {
                let current = $('.trade_history_current');
                let total = $('.trade_history_total');
                let previous = $('.trade_history_previous');
                let next = $('.trade_history_next');
                let input = $('.trade_history_input');

                total.text(responseData.total).attr('data-value', responseData.total);
                current.text(responseData.current).attr('data-value', responseData.current);

                next
                    .prop('disabled', responseData.current === responseData.total)
                    .is(':disabled', responseData.current === responseData.total);

                previous
                    .prop('disabled', responseData.current === 1)
                    .is(':disabled', responseData.current === 1);

                input.val('');

            }, (responseErr) => {
                commonShowNotifyErr();
            }
        );
}

let uploadCompanyLogo = (file, src) => {
    Notiflix.Block.Pulse('.img_logo_view');

    setTimeout(() => {
        let imgLogoView = $('.img_logo_view');

        imgLogoView.css('background-image', `url(${src})`);

        Notiflix.Block.Remove('.img_logo_view');
    }, 1000);
}

let uploadCompanyPicture = (data) => {
    $('#img_pictures').prop('disabled', true).is(':disabled', true);
    $('.ci-form__company-images').addClass('disabled');

    console.log(data)

    setTimeout(() => {
        $('#img_pictures').prop('disabled', false).is(':disabled', false);
        $('.ci-form__company-images').removeClass('disabled');
    }, 1000);
}

let deletePictureItem = ($button, id) => {
    let $item = $(`#img_pictures_${id}`);

    Notiflix.Confirm.Show(
        'Xác nhận',
        'Bạn chắc chắn muốn xóa ảnh ?',
        'Đồng ý',
        'Huỷ',
        () => {
            delete companyListPicture[$item.attr(id)];

            $item.remove();
            Notiflix.Notify.Success('Success');
        }
    );
}
