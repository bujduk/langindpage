let checkCiMaxItemInList = (prefixClass) => {
    let count = $(`.ci_${prefixClass}_item`).length;

    $(`.ci_${prefixClass}_list`).attr('data-count', count);
    $(`.ci_${prefixClass}_add`).prop('hidden', count >= 3).is(':hidden', count >= 3);
}

let removeCiItem = (prefixClass, $button) => {
    let id = $button.attr('data-id');
    let count = $(`.ci_${prefixClass}_item`).length;

    if (count <= 1) {
        $(`#${prefixClass}_${id}`).find(':input').val(null).trigger('change');
    } else {
        $(`#${prefixClass}_${id}`).remove();
    }

    checkCiMaxItemInList(prefixClass);
}

let ciBorderBack = () => {
    let navItem = $('.ci-nav');
    let itemActive = $('.ci-nav__item.active');
    let itemBorder = $('.ci-nav__border');

    itemBorder.css({
        'top': itemActive.offset().top - navItem.offset().top,
        'height': itemActive.height()
    });
}

let ciNavClicked = (navItem) => {
    let nextPostTabIndex = navItem.attr('data-tab');

    let currentForm = $(`.ci_form_switch[data-tab="${currentCiIndex}"]`);
    let nextForm = $(`.ci_form_switch[data-tab="${nextPostTabIndex}"]`);

    if (currentCiIndex !== nextPostTabIndex) {
        Notiflix.Block.Pulse('.emp-box');

        currentForm.collapse('hide');
        nextForm.collapse('show');

        currentCiIndex = nextPostTabIndex;

        $('.ci-nav__item').removeClass('active');
        navItem.addClass('active');

        ciBorderBack();
    }
}

let ciNavBorderChange = (itemHover) => {
    let navItem = $('.ci-nav');
    let itemActive = $('.ci-nav__item.active');
    let itemBorder = $('.ci-nav__border');

    let itemHoverTop = itemHover.offset().top - navItem.offset().top;
    let itemHoverHeight = itemHover.height();

    let itemActiveTop = itemActive.offset().top - navItem.offset().top;
    let itemActiveHeight = itemActive.height();

    let currentTop = itemActiveTop;
    let currentHeight = itemActiveHeight;

    if (itemHoverTop > itemActiveTop) {
        itemBorder.css({
            'top': currentTop,
            'height': currentHeight + (itemHoverTop - itemActiveTop)
        });
    } else if (itemHoverTop < itemActiveTop) {
        itemBorder.css({
            'top': currentTop - (itemActiveTop - itemHoverTop),
            'height': currentHeight + (itemActiveTop - itemHoverTop)
        });
    } else {
        itemBorder.css({
            'top': currentTop,
            'height': currentHeight
        });
    }
};

let _handleCompanyLogoChange = (_this) => {
    let files = _this[0].files;
    let imgLogoError = $('#img_logo_error');

    imgLogoError.html('');

    let file = files[0];
    if (file.size > 2097152) {
        let html = `<span style="display:block;">${image} ${file.name} ${too_2mb}</span>`;

        imgLogoError.append(html);
    } else if (file.type !== "image/jpeg" && file.type !== "image/png" && file.type !== "image/jpg") {
        let html = `<span style="display:block;">${image} ${file.name} ${gallery_incorrect_format}</span>`;

        imgLogoError.append(html);
    } else {
        let fileReader = new FileReader();

        fileReader.onload = ((fileParam) => {
            return (e) => {
                uploadCompanyLogo(file, e.target.result);
            }
        })(file);
        fileReader.readAsDataURL(file);
    }

    _this.val('');
}

let _handleListPictureChange = (_this) => {
    let files = _this[0].files;
    let imgPicturesError = $('#img_pictures_error');
    let listImageCurrent = $(`.img_pictures_item`);
    let listImageCurrentLength = listImageCurrent.length;

    imgPicturesError.html('');
    companyListPicture = {};

    for (let i = 0; i < files.length; i++) {

        let file = files[i];

        if (listImageCurrentLength + i >= 12) {
            let html = `<span style="display:block;">${gallery_more_than_12_item}</span>`;

            imgPicturesError.append(html);
            break;

        } else if (file.size > 2097152) {
            let html = `<span style="display:block;">${image} ${file.name} ${too_2mb}</span>`;

            imgPicturesError.append(html);

        } else if (file.type !== "image/jpeg" && file.type !== "image/png" && file.type !== "image/jpg") {
            let html = `<span style="display:block;">${image} ${file.name} ${gallery_incorrect_format}</span>`;

            imgPicturesError.append(html);

        } else {
            let fileReader = new FileReader();
            let id = generateKey();

            fileReader.onload = ((fileParam) => {
                return (e) => {
                    generatePicturesItem(id, e.target.result, fileParam.name, i);
                }
            })(file);

            companyListPicture[i] = file;
            fileReader.readAsDataURL(file);
        }
    }

    if (companyListPicture) uploadCompanyPicture(companyListPicture);
}

let _handleTradeHistoryNextClick = () => {
    let current = _.toNumber($('.trade_history_current').attr('data-value'));
    let total = _.toNumber($('.trade_history_total').attr('data-value'));

    if (current < total) {
        paginationChange(current + 1)
    }
}

let _handleTradeHistoryPreviousClick = () => {
    let current = _.toNumber($('.trade_history_current').attr('data-value'));

    if (current > 1) {
        paginationChange(current - 1);
    }
}

let _handleTradeHistoryInputChange = ($input) => {
    let total = _.toNumber($('.trade_history_total').attr('data-value'));
    let nextIndex = _.toNumber($input.val());

    if (!!nextIndex && nextIndex <= total) {
        paginationChange(nextIndex);
    }
}

let _handleTradeHistoryFilter = () => {
    let service = $('#trade_service').val();
    let range = $('#trade_range').val().split(' - ');
    let timeFrom = range[0];
    let timeTo = range[1];

    console.log(service, timeFrom, timeTo);
}