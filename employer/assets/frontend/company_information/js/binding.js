let generateNewCompanyAddress = (animation = 'animate__animated animate__fadeInDown animate__faster') => {
    let companyLocationList = $('.ci_address_list');
    let randomKey = generateKey();

    companyLocationList.append(`
        <div id="address_${randomKey}"
        class="ci_address_item list-item row mx-n12 mb-1 d-flex position-relative ${animation}">
            <div class="col-12 col-md-5 px-12 ci-form__item">
                <div class=" d-flex align-items-center">
                    <label for="address_${randomKey}_province" class="ci-form__label mb-0 ci-form__label--inline ">
                        Tỉnh thành
                    </label>
                    <select id="address_${randomKey}_province" name="address[${randomKey}][province]"
                            class="ci_form_select2 invisible" data-placeholder="">
                        <option></option>
                        <option value="0">Hà Nội</option>
                        <option value="1">Hồ Chí Minh</option>
                        <option value="2">Đà Nẵng</option>
                    </select>
                </div>
                <p id="address_${randomKey}_province_error" class="my-0 error-text text-right no-min-height"></p>
            </div>
            <div class="col-12 col-md-7 px-12 ci-form__item">
                <div class=" d-flex align-items-center">
                    <label for="address_${randomKey}_address"
                            class="ci-form__label mb-0 ci-form__label--inline ">
                        Địa chỉ
                    </label>
                    <input id="address_${randomKey}_address" name="address[${randomKey}][address]"
                            class="ci-form__input w-100" type="text">
                </div>
                <p id="address_${randomKey}_address_error" class="my-0 error-text text-right no-min-height"></p>
            </div>
            <div class="d-flex align-items-center justify-content-center ci-list-remove">
                <button type="button" tabindex="-1" data-id="${randomKey}"
                        onclick="removeCiItem('address', $(this))"
                        class="ci-list-remove__button ci_location_remove">
                    <i class="far fa-times-circle"></i>
                </button>
            </div>
        </div>  
    `);

    ciInitSelect2Single($(`#address_${randomKey}_province`));
    checkCiMaxItemInList('address');
}

let generateNewCompanyWhyYouLove = (animation = 'animate__animated animate__fadeInDown animate__faster') => {
    let companyWhyYouLoveList = $('.ci_why_you_love_list');
    let randomKey = generateKey();

    companyWhyYouLoveList.append(`
        <div id="why_you_love_${randomKey}"
                class="ci_why_you_love_item list-item row mx-n12 mb-1 d-flex position-relative ${animation}">
            <label class="col-12 px-12 ci-form__item">
                <input id="why_you_love_${randomKey}" name="why_you_love[${randomKey}]"
                        class="ci-form__input w-100" type="text" required>
                <p id="why_you_love_${randomKey}_error" class="error-text no-min-height"></p>
            </label>
            <div class="d-flex align-items-center justify-content-center ci-list-remove">
                <button type="button" tabindex="-1" data-id="${randomKey}"
                        onclick="removeCiItem('why_you_love', $(this))"
                        class="ci-list-remove__button ci_why_love_remove">
                    <i class="far fa-times-circle"></i>
                </button>
            </div>
        </div>
    `);

    checkCiMaxItemInList('why_you_love');
}

let generatePicturesItem = (id, src, name, key) => {
    $('.company_images_list').append(`
        <div id="img_pictures_${id}" data-key="${key}"
             class="col-6 col-md-6 col-lg-4 col-xl-12 px-2 img_pictures_item">
            <div class="company-images__item d-flex align-items-center my-md-1">
                <div style="background-image: url(${src})"
                     class="company-images__background"></div>
                <p class="company-images__title mr-auto">
                    ${name}
                </p>
                <button type="button" class="company-images__remove"
                        onclick="deletePictureItem($(this), '${id}')"
                        data-url=""
                        tabindex="-1">
                    <i class="fas fa-trash"></i>
                </button>
            </div>
        </div>
    `)
}

let generateListTrade = (data) => {
    let tradeHistoryList = $('.trade_history_list');
    tradeHistoryList.html('');

    data.forEach((item) => {
        generateTradeItem(item);
    });
}

let generateTradeItem = (data) => {
    let tradeHistoryList = $('.trade_history_list');

    tradeHistoryList.append(`
        <div class="trow row mt-3 mt-md-0">
            <div class="tcol col-12 col-md-5 px-0 px-md-3">
                <div class="trade-history__name">
                    ${data.type}
                </div>
            </div>
            <div class="tcol col-12 col-md-4 px-0 px-md-3">
                <div class="trade-history__time">
                    <span class="d-inline-block d-md-none mr-2 trade-history__bold">
                        Ngày thực hiện
                    </span>
                    ${data.time}
                </div>
            </div>
            <div class="tcol col-md-3 px-0 px-md-3 trade-history__absolute">
                <span class="point-less">
                    ${data.point}
                    <span class="d-inline-block d-md-none">
                        points
                    </span>
                </span>
            </div>
        </div>
    `)
}