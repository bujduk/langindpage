let ciInitSelect2Single = ($select) => {
    $($select).select2({
        minimumResultsForSearch: -1,
        selectionCssClass: 'ci_form_select2-selection',
        dropdownCssClass: 'ci_form_select2-dropdown',
        allowClear: (!!$select.attr('data-allowclear')),
        width: '100%',
        placeholder: $select.attr('data-placeholder'),
    });
}

let openTabIndex = () => {

    const params = new URLSearchParams(window.location.search);
    let tabIndex = params.get('tabindex');
    if (!!tabIndex) $(`.ci-nav__item[data-tab=${tabIndex}]`).trigger('click');
}

ciPageInit = () => {

    if (!$('.ci_address_item').length) {
        generateNewCompanyAddress('');
    } else {
        checkCiMaxItemInList('address');
    }

    if (!$('.ci_why_you_love_item').length) {
        generateNewCompanyWhyYouLove('');
    } else {
        checkCiMaxItemInList('why_you_love');
    }

    openTabIndex();
}

(function ($) {
    let ciNavItem = $('.ci-nav__item');
    let sizeSelect2 = $('.size_select2');

    Notiflix.Loading.Init({
        backgroundColor: "rgba(255,255,255,0.6)",
        svgColor: "#06202D",
    });

    ciNavItem.on('mouseover', (e) => {
        ciNavBorderChange($(e.target));
    });

    ciNavItem.on('mouseout', (e) => {
        ciBorderBack($(e.target));
    });

    $('.ci_form_select2').each((key, value) => {
        let $select = $(value);
        ciInitSelect2Single($select);
    });

    $('.dichvu_select2').select2({
        minimumResultsForSearch: -1,
        selectionCssClass: 'filtercv-select2-selection',
        dropdownCssClass: 'filtercv-select2-dropdown dichvu',
        allowClear: true,
        placeholder: 'Dịch vụ',
        width: 'auto',
    });

    sizeSelect2.select2({
        selectionCssClass: 'ci_form_select2-selection',
        dropdownCssClass: 'ci_form_select2-dropdown',
        maximumInputLength: 20,
        width: '100%',
        tags: true,
        placeholder: sizeSelect2.attr('data-placeholder'),
        ajax: {
            url: sizeSelect2.attr('data-url'),
            data: (params) => {
                return {
                    keyword: params.term
                }
            },
            processResults: function (response) {
                let output = {};
                let projectSelected = sizeSelect2.val();

                output.results = response.data.map((item) => {
                    let selected = _.includes(projectSelected, item.name)

                    return {
                        "id": item.name,
                        "text": item.name,
                        "selected": selected
                    }
                });

                return !!output ? output : null;
            }
        }
    })

    $('#trade_range').daterangepicker({
        locale: {
            format: 'DD/MM/YYYY'
        },
        showDropdowns: true,
        ranges: inputRangesTranslation,
        alwaysShowCalendars: true,
        opens: 'left'
    });

    $('.ci_form_tagsinput')
        .on('focus', (e) => {
            $(e.target).prev().find('input').focus();
        })
        .tagsinput({
            tagClass: 'badge ci-form__tags',
            onTagExists: function (item, $tag) {
                $tag.hide().fadeIn();
            },
            focusClass: 'tagsinput-focus',
        });

    $('.ci_collapse').collapse({
        toggle: false
    });

    $('.ci_form_switch').on('hidden.bs.collapse', () => {
        Notiflix.Block.Remove('.emp-box');
    });

    $('#img_logo').on('change', (e) => {
        _handleCompanyLogoChange($(e.target));
    });

    $('#img_pictures').on('change', (e) => {
        _handleListPictureChange($(e.target));
    });

    $('.trade_history_previous').on('click', (e) => {
        _handleTradeHistoryPreviousClick($(e.target));
    });

    $('.trade_history_next').on('click', (e) => {
        _handleTradeHistoryNextClick($(e.target));
    });

    $('.trade_history_input').on('input change', (e) => {
        clearTimeout(paginationTimeOut);
        if (e.which === 13) {
            _handleTradeHistoryInputChange($(e.target));
        } else {
            paginationTimeOut = setTimeout(() => {
                _handleTradeHistoryInputChange($(e.target));
            }, 1000);
        }
    });

    $('#trade_service').on('change', () => {
        _handleTradeHistoryFilter();
    });

    $('#trade_range').on('input change', () => {
        _handleTradeHistoryFilter();
    })

    ciPageInit();
})(jQuery);