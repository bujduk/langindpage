let galleryImageList;

$(document).ready(() => {
    let errorReportModal = $('#error_report_modal');

    errorReportModal.on('hidden.bs.modal', () => {
        modalClearContent();
        modalClearFormErr();
    });

    /*==================PAGINATION=================*/
    $('.page-link').on('click', (e) => {
        let _this = $(e.target);
        let pageLink = $('.page-link')

        if (pageLink.hasClass('page-link-active')) {
            pageLink.removeClass('page-link-active');
        }
        if (!pageLink.hasClass('page-link-active')) {
            _this.addClass('page-link-active');
        }
    })

    /*===============STATUS=======================*/
    $('.search_status_select2').select2({
        placeholder: 'Trạng thái',
        minimumResultsForSearch: -1,
        dropdownCssClass: 'dashboard-select2-dropdown',
        selectionCssClass: 'dashboard-select2-selection',
        width: '140px',
    });

    $('.edit_status_modal').select2({
        placeholder: 'Thay đổi trạng thái',
        minimumResultsForSearch: -1,
        dropdownCssClass: 'dashboard-select2-dropdown',
        selectionCssClass: 'dashboard-select2-selection',
        width: '100%',
    });

    /*==================LIST SCROLL ADD SHADOW================*/
    $('.js_list-body').scroll(function () {
        if ($(this).scrollTop()) {
            $('.content__list--title').addClass('header-shadow');
        } else {
            $('.content__list--title').removeClass('header-shadow');
        }
    })

    /*===================COUNT ANIMATION======================*/
    $('.db_count').each(function () {
        $(this).prop('Counter',0).animate({
            Counter: $(this).text()
        }, {
            duration: 1000,
            easing: 'swing',
            step: function (now) {
                let number = "0" + Math.ceil(now);

                if (Math.ceil(now) < 10) {
                    $(this).html(number);
                } else {
                    $(this).text(Math.ceil(now));
                }
            }
        });
    });

    /*===================Report Error Img Modal====================*/
    $('.modal__upload-file').on('change', () => {
        $('.modal__upload-file--input').trigger('click');
    })
})

/* Generate key */
generateKey = () => {
    return Math.random().toString(20).substr(2, 10);
}

/*========================SHOW LIST ACTION=======================*/
dbShowActionContentListBody = (_this, prefixId) => {
    $(`#${prefixId} .list-title`).addClass('element-hidden');
    $(`#${prefixId} .js_content-list--action`).removeClass('element-hidden');
    $(`#${prefixId} .amount-checkbox`).html($(`#${prefixId} .list__content--item input:checked`).length);
}

/*========================SHOW LIST TITLE=======================*/
dbShowTitleContentListBody = (_this, prefixId) => {
    $(`#${prefixId} .list-title`).removeClass('element-hidden');
    $(`#${prefixId} .js_content-list--action`).addClass('element-hidden');
    $(`#${prefixId}_selectALLCheckbox`).prop('checked', false);
}

/*======================MODAL UPLOAD IMG========================*/
let galleryLoadImage = (_this) => {
    let files = _this[0].files;
    let gallery_add_list_error = $('#gallery_add_list_error');
    let imgItem = $('.gallery-item');
    let imgView = $('.gallery-item__image');
    let imgTitle = $('.gallery-item__title');
    gallery_add_list_error.html('');

    let file = files[0];

    if (file.size > 2097152) {
        let html = `<span style="display:block;">${image} ${file.name} ${too_2mb}</span>`;

        gallery_add_list_error.append(html);
    } else if (file.type !== "image/jpeg" && file.type !== "image/png" && file.type !== "image/jpg") {
        let html = `<span style="display:block;">${image} ${file.name} ${gallery_incorrect_format}</span>`;

        gallery_add_list_error.append(html);
    } else {
        let fileReader = new FileReader();

        galleryImageList = file;

        fileReader.onload = ((fileParam) => {
            return (e) => {
                imgItem.removeClass('element-hidden');
                imgView.css('background-image', `url(${e.target.result})`);
                imgTitle.text(fileParam.name);
            }
        })(file);
        fileReader.readAsDataURL(file);
    }
}

/*===============================DELETE IMG=============================*/

let deleteImgErrorReportItem = () => {
    Notiflix.Confirm.Show(
        'Xác nhận',
        'Bạn chắc chắn muốn xoá?',
        'Đồng ý',
        'Huỷ',
        () => {

            let gallery_add_list_error = $('#gallery_add_list_error');
            let imgItem = $('.gallery-item');
            let imgView = $('.gallery-item__image');
            let imgTitle = $('.gallery-item__title');
            let imgValue = $('#gallery_add_file');

            gallery_add_list_error.empty();

            imgItem.addClass('element-hidden');
            imgView.css('background-image', `unset`);
            imgTitle.empty();
            imgValue.val(null);
        },
    );
}

modalClearFormErr = () => {
    $('.error-text').text('');
    $('#gallery_file_error').text('');
    $('.modal__input--warning').text('');
    $('.invalid-input').removeClass('invalid-input');
}

modalClearContent = () => {

    $('#errorReport_form').trigger('reset');

    $('.errorReport_form').attr({
        'data-url': null,
        'data-type': null,
    });

    galleryImageList = [];

    $('#errorReport_form .modal-body--list-file').html('');
}

/* ============================= OPEN POPUP ===================================== */

modalReportErrorOpen = () => {
    $('#error_report_modal').modal('show');
}

/* ======================= STATUS CHANGE ========================== */
statusChangeCss = (prefixId) => {
    $(`#${prefixId} .js_list-content :checkbox`).prop('checked', false).attr('checked', false);
    $(`#${prefixId} .list-title`).removeClass('element-hidden');
    $(`#${prefixId} .js_content-list--action`).addClass('element-hidden');
    $(`#${prefixId} .selectAllCheckbox`).prop('checked', false);
    $(`#${prefixId}_status-change`).val(null).trigger('change');
    $(`#${prefixId} .list--checkbox`).removeClass('checkbox-all checkbox-remove');
}

/* ======================= PAGE ========================== */
$('#db_list-candidate--present').html(1);
$('#db_list-job--present').html(1);

pageCss = (prefixId, value) => {
    $(`#${prefixId}--present`).attr('data-page', `${value}`);
    $(`#${prefixId}--present`).html($(`#${prefixId}--present`).attr('data-page'));
    $(`#${prefixId} .next`).removeClass('disabled');
    $(`#${prefixId} .prev`).removeClass('disabled');
    $(`#${prefixId} .list__content--item`).remove();
}

pageNextCss = (prefixId) => {
    $(`#${prefixId} .next`).addClass('disabled');
}

pagePrevCss = (prefixId) => {
    $(`#${prefixId} .prev`).addClass('disabled');
}

/* ======================= SELECT ALL ========================== */
selectAllCheckedCss = (_this, prefixId) => {
    $(`#${prefixId} .js_list-content :checkbox`).prop('checked', true).attr('checked', true);
    $(`#${prefixId} .list--checkbox`).addClass('checkbox-all');
}

selectAllUnCheckedCss = (_this, prefixId) => {
    $(`#${prefixId} .js_list-content :checkbox`).prop('checked', false).attr('checked', false);
    $(`#${prefixId} .list--checkbox`).removeClass('checkbox-all checkbox-remove');
}

/* =================== SELECT ITEM ======================== */
selectItemCheckCss = (prefixId) => {
    $(`#${prefixId} .content__list--title :checkbox`).prop('checked', true).attr('checked', true);
    $(`#${prefixId} .list--checkbox`).addClass('checkbox-remove');
}

selectItemUnCheckCss = (prefixId) => {
    $(`#${prefixId} .list--checkbox`).removeClass('checkbox-all checkbox-remove');
    $(`#${prefixId} .content__list--title :checkbox`).prop('checked', false).attr('checked', false);
}

selectItemUnCheckAllCss = (prefixId) => {
    $(`#${prefixId} .list--checkbox`).addClass('checkbox-remove').removeClass('checkbox-all');
}

selectItemCheckAllCss = (prefixId) => {
    $(`#${prefixId} .list--checkbox`).addClass('checkbox-all');
}

/*=============================== SERVICE DETAIL MODAL ==========================*/
serviceDetail = (_this) => {
    $('#service_detail_modal').modal('show');
}
