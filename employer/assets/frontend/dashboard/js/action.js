const  dbListJobStatus = {
    "active": "Hoạt động",
    "pending-review": "Chờ duyệt",
    "approved": "Đã duyệt",
    "draft": "Nháp",
    "inactive": "Không hoạt động",
    "reject": "Từ chối",
    "expired": "Hết hạn",
};

const  dbListCandidateStatus = {
    "passed": "Trúng tuyển",
    "interview-joined": "Đã phỏng vấn",
    "fail": "Không trúng tuyển",
    "pending-review": "Chờ duyệt",
    "interview-invited": "Đã hẹn lịch",
    "interview-refused": "Từ chối phỏng vấn",
    "rejected": "Không duyệt",
};

$(document).ready(() => {

    /*====================JS MOBILE, DESKTOP=========================*/
    if ($(window).innerWidth() < 992) {
        $('.height__list--body').attr('data-type', 'mobile');

        console.log('mobile')
    } else {
        $('.height__list--body').attr('data-type', 'desktop');

        console.log('desktop')
    }
})

/*================================== CHANGE STATUS ================================*/
dbStatusChange = (_this, prefixId) => {
    Notiflix.Confirm.Show(
        'Xác nhận',
        'Bạn chắc chắn muốn thay đổi trạng thái ?',
        'Đồng ý',
        'Huỷ',
        () => {
            let inputChecked = $(`#${prefixId} .list__content--item input:checked`);

            $(inputChecked).each(function () {
                let id = $(this).attr("data-id");

                switch (prefixId) {
                    case 'db_list-candidate' :
                        $(`#${prefixId}_status_${id}`).html(`<span class="candidate__status--${$(_this).val()}">${dbListCandidateStatus[$(_this).val()]}</span>`)
                        break;

                    case 'db_list-job' :
                        $(`#${prefixId}_status_${id}`).html(`<span class="job__status--${$(_this).val()}">${dbListJobStatus[$(_this).val()]}</span>`)
                        break;
                }
            })

            statusChangeCss(prefixId);
        },

        () => {
            $(`#${prefixId}_status-change`).val(null).trigger('change');
        }
    );
}

/* ============== SELECT CHECKBOX =============== */
dbSelectAllCheckBox = (_this, prefixId) => {
    if (_this.is(':checked')) {
        selectAllCheckedCss(_this, prefixId);
        dbShowActionContentListBody(_this, prefixId);

    } else {
        dbShowTitleContentListBody(_this, prefixId);
        selectAllUnCheckedCss(_this, prefixId);
    }
}

dbSelectItemCheckBox = (_this, prefixId) => {
    dbShowActionContentListBody(_this, prefixId);

    selectItemCheckCss(prefixId);

    if ($(`#${prefixId} .list__content--item input:checked`).length === 0) {
        dbShowTitleContentListBody(_this, prefixId);
        selectItemUnCheckCss(prefixId);
    }

    if ($(`#${prefixId} .selectAllCheckbox`).prop("checked")) {
        selectItemUnCheckAllCss(prefixId);
    }

    if ($(`#${prefixId} .js_list-body :checkbox`).length === $(`#${prefixId} .list__content--item input:checked`).length) {
        selectItemCheckAllCss(prefixId);
    }
}

/*==================================PAGIGNATION======================*/

nextPage = (_this, prefixId) => {
    let value = parseInt($(`#${prefixId}--present`).attr('data-page'));

    if (value !== 6) {
        value++;

        pageCss(prefixId, value);

        switch (prefixId) {
            case 'db_list-candidate':
                dbAppendListCandidate();
                break;

            case 'db_list-job':
                dbAppendListJob();
                break;
        }
    }

    if (value === 6) {
        pageNextCss(prefixId);
    }
}

prevPage = (_this, prefixId) => {
    let value = parseInt($(`#${prefixId}--present`).attr('data-page'));

    if (value !== 1) {
        value--;

        pageCss(prefixId, value);

        switch (prefixId) {
            case 'db_list-candidate':
                dbAppendListCandidate();
                break;

            case 'db_list-job':
                dbAppendListJob();
                break;
        }
    }

    if (value === 1) {
        pagePrevCss(prefixId);
    }
}

/*================== Delete Item==================== */
dbDeleteItem = (_this, prefixId) => {
    Notiflix.Confirm.Show(
        'Xác nhận',
        'Bạn chắc chắn muốn xoá?',
        'Đồng ý',
        'Huỷ',
        () => {
            let deleteChecked = $(`#${prefixId} .js_list-body input:checked`);

            deleteChecked.each((key, value) => {
                let item = $(value);
                let id = item.attr('data-id');

                $(`#${prefixId}_${id}`).remove();
                dbShowTitleContentListBody(_this, prefixId);
            });
        },

        () => {
            let id = _this.attr('data-id');

            $(`#${prefixId}_${id}`).remove();
        }
    );
}

dbDeleteItemMobile = (_this, prefixId) => {
    Notiflix.Confirm.Show(
        'Xác nhận',
        'Bạn chắc chắn muốn xoá?',
        'Đồng ý',
        'Huỷ',
        () => {
            let id = _this.attr('data-id');

            $(`#${prefixId}_${id}`).remove();
        }
    );
}
