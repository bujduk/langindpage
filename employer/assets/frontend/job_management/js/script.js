
$(document).ready(() => {
    $('#editStatusJob_modal').on('hidden.bs.modal', () => {
        $('#edit_statusJob_modal').val(null).trigger('change');
    });

    $('#select_service_modal').on('hidden.bs.modal', () => {
        resetItemActive();
        $('.post_job_settime').addClass('disabled');
    });

    $('#time_countdown-inactive_modal').on('hidden.bs.modal', () => {
        clearTimeout(intervalCountdown);
    });

    $('#time_countdown-approved_modal').on('hidden.bs.modal', () => {
        clearTimeout(intervalCountdown);
    });

    /*===============SELECT STATUS=======================*/
    $('#list-job_status').select2({
        placeholder: 'Trạng thái',
        minimumResultsForSearch: -1,
        dropdownCssClass: 'job_management-select2-dropdown job_status-select2-dropdown',
        selectionCssClass: 'job_management-select2-selection',
        width: '100%',
    });

    $('#user-upload').select2({
        placeholder: 'Người đăng',
        minimumResultsForSearch: -1,
        dropdownCssClass: 'job_management-select2-dropdown user-upload-select2-dropdown',
        selectionCssClass: 'job_management-select2-selection',
        width: '132px',
    })

    /*==================SHADOW LIST JOB================*/
    $('.js_list-body').scroll(function () {
        if ($(this).scrollTop()) {
            $('.content__list--title').addClass('header-shadow');
        } else {
            $('.content__list--title').removeClass('header-shadow');
        }
    })

    /*===================TOTAL LIST======================*/
    $('.total-list span').html($(`.list__content--item`).length);

    /*===================COUNT ANIMATION======================*/
    $('.count').each(function () {
        $(this).prop('Counter', 0).animate({
            Counter: $(this).text()
        }, {
            duration: 1000,
            easing: 'swing',
            step: function (now) {
                if (now < 9) {
                    $(this).text('0' + Math.ceil(now));
                } else {
                    $(this).text(Math.ceil(now));
                }
            }
        });
    });

    $('.post_job_settime').daterangepicker({
        singleDatePicker: true,
        timePicker: true,
        timePicker24Hour: true,
        showDropdowns: true,
        opens: "left",
        drops: "up",
        minDate: moment().startOf('minute'),
        locale: {
            format: 'YYYY-MM-DD HH:mm:ss'
        },
    }).on('cancel.daterangepicker', (e) => {
        $(e.target).addClass('disabled');
    }).on('apply.daterangepicker', (e) => {
        $(e.target).removeClass('disabled');
    });

    Notiflix.Block.Init({
        svgColor: "#32c682",
    });
})

/* Generate key */
generateKey = () => {
    return Math.random().toString(20).substr(2, 10);
}

/*======================== SHOW LIST ACTION ======================*/
showActionContentListBody = (_this, prefixId) => {
    $(`#${prefixId} .job__list--title `).addClass('element-hidden');
    $(`#${prefixId} .js_content-list--action`).removeClass('element-hidden');
    $(`#${prefixId} .amount-checkbox`).html($(`#${prefixId} .list__content--item input:checked`).length);
}

/*======================== SHOW LIST TITLE ======================*/
showTitleContentListBody = (_this, prefixId) => {
    $(`#${prefixId} .job__list--title`).removeClass('element-hidden');
    $(`#${prefixId} .js_content-list--action`).addClass('element-hidden');
    $(`#${prefixId} .selectAllCheckbox`).prop('checked', false);
}

/*=========================SORT ARROW=========================*/
sortItemArrowUp = (_this) => {
    $('.arrow-up', _this).addClass('sort');
    $('.arrow-down', _this).removeClass('sort');
    _this.attr('data-sort', 1);
}

sortItemArrowDown = (_this) => {
    $('.arrow-down', _this).addClass('sort');
    $('.arrow-up', _this).removeClass('sort');
    _this.attr('data-sort', 2);
}

/*=======================RESET SORT ICON======================*/
resetSortIcon = () => {
    $('.arrow-down').removeClass('sort')
    $('.arrow-up').removeClass('sort');
}

/*==================CALENDER===================*/
calenderDateRager = () => {
    $('#listJob-calender-date-ranger').daterangepicker({
        opens: 'left',
        autoUpdateInput: false,
        locale: {
            cancelLabel: 'Clear',
        },

        ranges: {
            'Hôm nay': [moment(), moment()],
            'Hôm qua': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            '7 ngày trước': [moment().subtract(6, 'days'), moment()],
            '30 ngày trước': [moment().subtract(29, 'days'), moment()],
            'Tháng này': [moment().startOf('month'), moment().endOf('month')],
            'Tháng trước': [moment().subtract(29, 'days').startOf('month'), moment().subtract(29, 'days').endOf('month')],
        },
        "alwaysShowCalendars": $(window).innerWidth() > 992,
    }).on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
    }).on('cancel.daterangepicker', function (ev, picker) {
        $(this).val('');
    });
}

itemActive = (_this) => {

    $('.modal-body--item').removeClass('modal-body--item-active');

    if (_this.hasClass('job-disabled')) {
        _this.prop('onclick', null).off('click');
    } else {
        _this.addClass('modal-body--item-active');
        $('#select_service_modal .btn-save').removeAttr('disabled');
    }

    $('.modal-body--item-input').val(_this.attr('data-value'));
}

resetItemActive = (_this) => {
    $('.modal-body--item').removeClass('modal-body--item-active');
    $('.modal-body--item-input').val('');
}

/* ======================= STATUS CHANGE ========================== */
statusChangeCss = (prefixId) => {
    $(`#${prefixId} .js_list-content :checkbox`).prop('checked', false).attr('checked', false);
    $(`#${prefixId} .job__list--title`).removeClass('element-hidden');
    $(`#${prefixId} .js_content-list--action`).addClass('element-hidden');
    $(`#${prefixId} .selectAllCheckbox`).prop('checked', false);
    $(`#${prefixId}_status-change`).val(null).trigger('change');
}

/* ======================= PAGE ========================== */
$('#list-job--present').html(currentPage);

pageCss = (prefixId, value) => {
    $(`#${prefixId}--present`).attr('data-page', value);
    $(`#${prefixId}--present`).html($(`#${prefixId}--present`).attr('data-page'));
    $(`#${prefixId} .next`).removeClass('disabled');
    $(`#${prefixId} .prev`).removeClass('disabled');
}

removeListItem = () => {
    $('#list-job .list__content--item').remove();
}

pageNextCss = (prefixId) => {
    $(`#${prefixId} .next`).addClass('disabled');
}

pagePrevCss = (prefixId) => {
    $(`#${prefixId} .prev`).addClass('disabled');
}

/* ======================= SELECT ALL ========================== */
selectAllCheckedCss = (_this, prefixId) => {
    $(`#${prefixId} .js_list-content :checkbox`).prop('checked', true).attr('checked', true);
    $(`#${prefixId} .list--checkbox`).addClass('checkbox-all');
}

selectAllUnCheckedCss = (_this, prefixId) => {
    $(`#${prefixId} .js_list-content :checkbox`).prop('checked', false).attr('checked', false);
    $(`#${prefixId} .list--checkbox`).removeClass('checkbox-all checkbox-remove');
}

/* =================== SELECT ITEM ======================== */
selectItemCheckCss = (prefixId) => {
    $(`#${prefixId} .list--checkbox`).addClass('checkbox-remove');
    $(`#${prefixId} .content__list--title :checkbox`).prop('checked', true).attr('checked', true);
}

selectItemUnCheckCss = (prefixId) => {
    $(`#${prefixId} .list--checkbox`).removeClass('checkbox-all checkbox-remove');
    $(`#${prefixId} .content__list--title :checkbox`).prop('checked', false).attr('checked', false);
}

selectItemUnCheckAllCss = (prefixId) => {
    $(`#${prefixId} .list--checkbox`).addClass('checkbox-remove').removeClass('checkbox-all');
}

selectItemCheckAllCss = (prefixId) => {
    $(`#${prefixId} .list--checkbox`).addClass('checkbox-all');
}

/* ============== SELECT CHECKBOX =============== */
selectAllCheckBox = (_this, prefixId) => {
    if (_this.is(':checked')) {
        selectAllCheckedCss(_this, prefixId);
        showActionContentListBody(_this, prefixId);

    } else {
        selectAllUnCheckedCss(_this, prefixId);
        showTitleContentListBody(_this, prefixId);
    }
}

selectItemCheckBox = (_this, prefixId) => {
    showActionContentListBody(_this, prefixId);
    selectItemCheckCss(prefixId);

    if ($(`#${prefixId} .list__content--item input:checked`).length === 0) {
        showTitleContentListBody(_this, prefixId);
        selectItemUnCheckCss(prefixId);
    }

    if ($(`#${prefixId} .selectAllCheckbox`).prop("checked")) {
        selectItemUnCheckAllCss(prefixId);
    }

    if ($(`#${prefixId} .js_list-body :checkbox`).length === $(`#${prefixId} .list__content--item input:checked`).length) {
        selectItemCheckAllCss(prefixId);
    }
}

/*=============================== SERVICE DETAIL MODAL ==========================*/
serviceDetail = (_this) => {
    $('#service_detail_modal').modal('show');
}

selectService = (_this) => {
    let id = _this.attr('data-id');
    let btnSave = $('#select_service_modal .btn-save');
    let modal = $('#select_service_modal')

    btnSave.attr('disabled', 'true');

    $('.post_job_settime').val('');

    modal.attr('data-id', id);

    modal.modal('show');
}

/*=================================COUNTDOWN TIME==========================*/
timeCountdown = (prefixId, expired) => {
    let dateRemain = $('.date-time');
    let hourRemain = $('.hour-time');
    let minutesRemain = $('.minutes-time');
    let secondsRemain = $('.seconds-time');

    let remain = (new Date(expired)).getTime() - (new Date()).getTime();

    if (remain > 0) {
        let date = Math.floor(remain / (1000 * 3600 * 24));
        let hour = Math.floor((remain) / (1000 * 3600) % 24);
        let minutes = Math.floor((remain) / (1000 * 60) % 60);
        let second = Math.floor((remain) / 1000 % 60);

        dateRemain.text(String(`${date}`).padStart(2, '0'));
        hourRemain.text(String(`${hour}`).padStart(2, '0'));
        minutesRemain.text(String(`${minutes}`).padStart(2, '0'));
        secondsRemain.text(String(`${second}`).padStart(2, '0'));

        if (prefixId === '#time_countdown-inactive_modal') {
            $(`${prefixId} .btn-save`).removeAttr('disabled');
        }
    } else {
        clearTimeout(intervalCountdown);

        dateRemain.text('00');
        hourRemain.text('00');
        minutesRemain.text('00');
        secondsRemain.text('00');

        if (prefixId === '#time_countdown-inactive_modal') {
            $(`${prefixId} .btn-save`).attr('disabled', 'true');
        }
    }
}