let ContentSearchTimeout = null;
let intervalCountdown;
let currentPage = 1;
let lastPageJob = 6;

const changeStatus = {
    "draft": "Chờ duyệt",
    "active": "Không hoạt động",
    "expired": "Hoạt động",
    "inactive": "Hoạt động",
    "approved": "Hoạt động",
};

const changeStatusClass = {
    "active": "inactive",
    "draft": "pending-review",
    "inactive": "active",
    "expired": "active",
    "approved": "active",
};

const statusAction = {
    "active": "Tạm dừng tin tuyển dụng",
    "draft": "Đăng tin tuyển dụng",
    "inactive": "Đăng tin tuyển dụng",
    "expired": "Gia hạn tin tuyển dụng",
    "approved": "Đăng tin tuyển dụng",
};

$(document).ready(() => {

    /*==================SELECT STATUS===================*/
    $("#list-job_status").on("select2:select", (e) => {
        filterData($(e.target));
    });

    $("#user-upload").on("select2:select", (e) => {
        filterData($(e.target));
    });

    calenderDateRager();

    $('#listJob-calender-date-ranger').on('apply.daterangepicker', (e) => {
        filterData($(e.target));
    });

    $('#search_content').on('input', (e) => {
        clearTimeout(ContentSearchTimeout);

        ContentSearchTimeout = setTimeout(() => {
            filterData($(e.target));
        }, 1000);
    })

    $('#go-to').on('keyup', (e) => {
        if (e.keyCode === 13) {
            e.preventDefault();
            indexPagination($(e.target));
        }
    })

    /* ================================ FORM SUBMIT ================================= */
    $('#select_service_form').submit((e) => {
        e.preventDefault();

        let modal = $('#select_service_modal')
        let id = modal.attr('data-id');
        let status = $(`#list-job_status_${id}`);
        let dataStatus = status.attr('data-status');
        let inputValue = $('#published_at');

        $(`#list-job_status_${id} span`).remove();

        if (dataStatus === 'draft') {
            status.append(`
                        <span class="job__status--${changeStatusClass[dataStatus]}">${changeStatus[dataStatus]}</span>
                    `)

            status.attr('data-status', `${changeStatusClass[dataStatus]}`);
        } else if (dataStatus === 'expired') {
            if (inputValue.val() === '') {
                let now = new Date();
                inputValue.val(moment(now).format('YYYY-MM-DD hh:mm:ss'));
                console.log(inputValue.val());

                status.append(`
                        <span class="job__status--${changeStatusClass[dataStatus]}">${changeStatus[dataStatus]}</span>
                    `)

                status.attr('data-status', `${changeStatusClass[dataStatus]}`);
            } else {
                status.append(`
                        <span class="job__status--approved">Đã duyệt</span>
                    `)

                status.attr('data-status', 'approved');
            }
        }
        
        modal.modal('hide');
    });

    $('#time_countdown-inactive_form').submit((e) => {
        e.preventDefault();

        let modal = $('#time_countdown-inactive_modal')
        let id = modal.attr('data-id');
        let status = $(`#list-job_status_${id}`);
        let dataStatus = status.attr('data-status');

        $(`#list-job_status_${id} span`).remove();

        status.append(`
                        <span class="job__status--${changeStatusClass[dataStatus]}">${changeStatus[dataStatus]}</span>
                    `)

        status.attr('data-status', `${changeStatusClass[dataStatus]}`);

        modal.modal('hide');
    })

    $('#time_countdown-approved_form').submit((e) => {
        e.preventDefault();

        let modal = $('#time_countdown-approved_modal')
        let id = modal.attr('data-id');
        let status = $(`#list-job_status_${id}`);
        let dataStatus = status.attr('data-status');

        $(`#list-job_status_${id} span`).remove();

        status.append(`
                        <span class="job__status--${changeStatusClass[dataStatus]}">${changeStatus[dataStatus]}</span>
                    `)

        status.attr('data-status', `${changeStatusClass[dataStatus]}`);

        modal.modal('hide');
    })
});

/*================================== CHANGE STATUS ================================*/

statusChange = (_this, prefixId) => {
    Notiflix.Confirm.Show(
        'Xác nhận',
        'Bạn chắc chắn muốn thay đổi trạng thái ?',
        'Đồng ý',
        'Huỷ',
        () => {
            let id = _this.attr('data-id');
            let status = $(`#${prefixId}_status_${id}`).attr('data-status');

            $(`#${prefixId}_status_${id} span`).remove();

            $(`#${prefixId}_status_${id}`).append(`
                        <span class="job__status--${changeStatusClass[status]}">${changeStatus[status]}</span>
                    `)
            /*$(`#${prefixId}_status_${id}`).attr('data-status', `${changeStatusClass[status]}`);*/
        },
    );
}

listActionOpen = (_this, prefixClass, prefixId) => {
    let id = _this.attr('data-id');
    let status = $(`#${prefixId}_status_${id}`).attr('data-status');
    let listActionDropdown = $('.list__action--dropdown');

    $(`#${prefixClass}_${id} .dropdown`).on('hidden.bs.dropdown', () => {
        $(`#${prefixClass}_${id} .list-action--button`).removeClass('button-active');
    })

    $(`#${prefixClass}_${id} .dropdown`).on('show.bs.dropdown', () => {
        $(`#${prefixClass}_${id} .list-action--button`).addClass('button-active');
    })

    $('.status-action').remove();

    switch (status) {
        case 'draft':
        case 'expired':
            listActionDropdown.prepend(`
                <button onclick="selectService($(this), 'list-job')" data-id="${id}" class="dropdown-item status-action">${statusAction[status]}</button>
            `);
            break;
        case 'active':
            listActionDropdown.prepend(`
                <button onclick="statusChange($(this), 'list-job')" data-id="${id}" class="dropdown-item status-action">${statusAction[status]}</button>
            `);
            break;
        case 'inactive':
            listActionDropdown.prepend(`
                <button onclick="timeRemaining('#time_countdown-inactive_modal')" data-id="${id}" class="dropdown-item status-action">${statusAction[status]}</button>
            `);
            break;
        case 'approved':
            listActionDropdown.prepend(`
                <button onclick="timeRemaining('#time_countdown-approved_modal')" data-id="${id}" class="dropdown-item status-action">${statusAction[status]}</button>
            `);
            break;

    }
}

/*===============SCROLL APPEND DATA==============*/

listBodyScroll = (_this, prefixId) => {
    let contentListBody = _this.get(0);
    let lazyLoading = $('.lazy-loading');

    lazyLoading.text('');
    Notiflix.Block.Dots('.lazy-loading');
    if (contentListBody.scrollTop + contentListBody.clientHeight === contentListBody.scrollHeight) {
        setTimeout(() => {
            if (currentPage < lastPageJob) {
                currentPage++;
                appendListJob();
                selectItemUnCheckAllCss(prefixId);
            } else {
                lazyLoading.text('No record ...');
            }
        }, 1000);
    }
}

/*==================================PAGIGNATION======================*/
nextPage = (_this, prefixId) => {
    let value = parseInt($(`#${prefixId}--present`).attr('data-page'));

    if (value !== lastPageJob) {
        value++;

        $('html,body').animate({
                scrollTop: $('.calender').offset().top - 20},
            500);

        pageCss(prefixId, value);
        filterListData(_this, prefixId);
    }

    if (value === lastPageJob) {
        pageNextCss(prefixId);
    }
}

prevPage = (_this, prefixId) => {
    let value = parseInt($(`#${prefixId}--present`).attr('data-page'));
    let min = 1;

    if (value !== min) {
        value--;
        $('html,body').animate({
                scrollTop: $('.calender').offset().top - 20},
            500);

        pageCss(prefixId, value);
        filterListData(_this, prefixId);
    }
    if (value === min) {
        pagePrevCss(prefixId);
    }
}

indexPagination = (goToInput) => {
    let value = _.toNumber(goToInput.val());
    let presentPage = $('#list-job--present');
    let totalPage = _.toNumber($('#total-page').attr('data-total'));

    $('html,body').animate({
            scrollTop: $('.calender').offset().top - 20},
        500);

    value = value < 1 ? 1 : value;
    value = value > lastPageJob ? lastPageJob : value;

    presentPage.html(value);
    presentPage.attr('data-page', value);
    pageCss('list-job', value);

    filterListData();
    goToInput.val('');

    if (value === 1) {
        pagePrevCss('list-job');
    }

    if (value === totalPage) {
        pageNextCss('list-job');
    }
}

/*================== DELETE ALL ITEM ==================== */
deleteAllItem = (_this, prefixId) => {

    Notiflix.Confirm.Show(
        'Xác nhận',
        'Bạn chắc chắn muốn xoá?',
        'Đồng ý',
        'Huỷ',
        () => {
            let deleteChecked = $(`#${prefixId} .js_list-body input:checked`);

            deleteChecked.each((key, value) => {
                let item = $(value);
                let id = item.attr('data-id');

                $(`#${prefixId}_${id}`).remove();
                showTitleContentListBody(_this, prefixId);
            });
        },
    );
}

/*================== DELETE ITEM ==================== */
deleteItem = (_this, prefixId) => {
    Notiflix.Confirm.Show(
        'Xác nhận',
        'Bạn chắc chắn muốn xoá?',
        'Đồng ý',
        'Huỷ',
        () => {
            let id = _this.attr('data-id');

            $(`#${prefixId}_${id}`).remove();

            showTitleContentListBody(_this, prefixId);
        },
    )
}

/* ============== SORT ITEM =============== */
sortItem = (_this) => {
    let headerIndex = _this.attr('data-sort');
    if (headerIndex === '2') {
        headerIndex = 0;
    } else {
        headerIndex++;
    }

    _this.attr('data-sort', headerIndex);

    $('.text-list-title').attr('data-sort', '0');

    if (headerIndex === 1) {
        resetSortIcon();
        sortItemArrowUp(_this);
        sortAppend();
    } else if (headerIndex === 2) {
        resetSortIcon();
        sortItemArrowDown(_this);
        sortAppend();
    } else {
        resetSortIcon();
        sortAppend();
    }
}

/*===============BINDING DATA==============*/
filterData = (e) => {
    let searchVal = $('#search_content').val();
    let statusVal = $('#list-job_status').val();
    let userUpload = $('#user-upload').val();
    let startTime = $('#listJob-calender-date-ranger').val().split(' - ')[0];
    let endTime = $('#listJob-calender-date-ranger').val().split(' - ')[1];
    let url = `itnavi.com.vn/job/search?keyword=${searchVal}&status=${statusVal}&userUpload=${userUpload}&startTime=${startTime}&endTime=${endTime}`;
}

filterListData = (_this) => {

    Notiflix.Loading.Pulse();

    setTimeout(() => {
        removeListItem();
        appendListJob();
        Notiflix.Loading.Remove();
    }, 1000);
}

/*=============================MODAL TIME==========================*/
timeRemaining = (prefixId) => {
    let modal = $(prefixId);

    intervalCountdown = setInterval(() => {
        timeCountdown(prefixId, '2021-01-08 14:12:00');
    }, 1000);

    timeCountdown(prefixId, '2021-01-08 14:12:00');

    modal.modal('show');
}