generateJobItem = (key) => {
    let html = `<div class="list__content--item d-flex align-items-center
                                    flex-column flex-lg-row" id="list-job_${key}">
                            <div class="checkbox-label d-flex justify-content-center align-items-center app-tablet app-desktop">
                                <label>
                                    <input class="input-hidden" data-id="${key}" id="list-job-checkbox_${key}"
                                           onchange="selectItemCheckBox($(this), 'list-job')" type="checkbox">
                                    <span
                                        class="list--checkbox position-relative d-flex justify-content-center align-items-center">
                                            <i class="fas fa-check"></i>
                                    </span>
                                </label>
                            </div>
                            <div class="job-title d-flex flex-column">
                                    <a class="text-default" href="#" target="_blank"
                                       title="UI-UX Designer (HTML5) Up to 1500$ UI-UX Designer (HTML5) Up to 1500$">
                                        <span>UI-UX Designer (HTML5) Up to 1500$</span>
                                    </a>
                                    <span class="premium-commitment" id="service-job_1">
                                        Premium job - Có cam kết
                                    </span>
                            </div>
                            <div class="d-flex align-items-center flex-column flex-lg-row w-100">
                                <div class="d-flex basic-info">
                                    <div class="basic-info--left">
                                        <div class="basic-info--content">
                                            <span class="app-mobile title--mobile">Ngày đăng</span>
                                            <span
                                                class="date-upload text-list-default">10/09/2020</span>
                                        </div>
                                        <div class="basic-info--content">
                                                <span class="app-mobile title--mobile">Ngày hết
                                                    hạn</span>
                                            <span
                                                class="date-expired text-list-default" id="date-expired_${key}" data-time="2020/09/10 10:10:10">10/09/2020</span>
                                        </div>
                                    </div>
                                    <div class="basic-info--right">
                                        <div class="basic-info--content">
                                            <span class="app-mobile title--mobile">Lượt xem</span>
                                            <span class="view text-list-default">6996</span>
                                        </div>
                                        <div class="basic-info--content recruitment">
                                            <span class="app-mobile title--mobile">Ứng tuyển</span>
                                            <a class="text-list-default" href="#" target="_blank">
                                                <span>6996</span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="status" id="list-job_status_${key}" data-status="expired">
                                        <span class="job__status--expired">
                                            Hết hạn
                                        </span>
                                    </div>
                                </div>
                                <div class="list-action" id="list-action_${key}">
                                    <div class="dropdown show">
                                        <button class="list-action--button btn" role="button" onclick="listActionOpen($(this), 'list-action', 'list-job')" data-id="${key}" data-toggle="dropdown">
                                            Lựa chọn
                                            <svg fill="none" height="6" viewBox="0 0 9 6" width="9"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M8.69812 0H0.301875C0.0500391 0 -0.0905801 0.294357 0.0653794 0.495312L4.2635 5.88431C4.38367 6.03856 4.61505 6.03856 4.7365 5.88431L8.93462 0.495312C9.09058 0.294357 8.94996 0 8.69812 0Z"
                                                    fill="#1C4355"/>
                                            </svg>
                                        </button>
                                        <div class="dropdown-menu list__action--dropdown">
                                            <button value="edit" class="dropdown-item">Sửa nội dung</button>
                                            <button onclick="duplicate($(this))" readonly value="duplicate" class="dropdown-item">Tạo bản sao</button>
                                            <button data-id="${key}" onclick="deleteItem($(this), 'list-job')" readonly value="delete" class="dropdown-item">Xóa công việc</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>`;

    $('.height__list--body').append(html);
}

/*===============GENERATE LIST JOB==============*/
appendListJob = (_this) => {
    for (let i = 0; i < 10; i++) {
        let key = generateKey();

        generateJobItem(key);
    }
    
}

sortAppend = () => {
    let arraySort = [''];

    $('.list__content--item').remove();

    arraySort.forEach(() => {
        appendListJob();
    })

}

/*=======================DUPLICATE============================*/
duplicate = (_this) => {
    Notiflix.Confirm.Show(
        'Xác nhận',
        'Bạn chắc chắn muốn tạo bản sao?',
        'Đồng ý',
        'Huỷ',
        () => {
            console.log(true)
        },

        () => {
            console.log(false)
        }
    );
}
