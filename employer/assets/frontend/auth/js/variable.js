const validateForm = {
    'employer_login': {
        rules: {
            'auth_email': 'required|email|maxlength:255',
            'auth_password': 'required',
        },
        customTranslateName: {
            'auth_email': 'Email',
            'auth_password': 'Mật khẩu'
        }
    },
    'employer_register': {
        rules: {
            'auth_email': 'required|email|maxlength:255',
            'auth_password': 'required',
            'auth_confirm_password': 'required|equal:auth_password',
            'auth_company': 'required|maxlength:255',
            'auth_phone': 'required|number|maxlength:20',
        },
        customTranslateName: {
            'auth_email': 'Email',
            'auth_password': 'Mật khẩu',
            'auth_confirm_password': 'Xác nhận mật khẩu',
            'auth_company': 'Tên công ty',
            'auth_phone': 'Số điện thoại',
        }
    },
    'auth_send_email': {
        rules: {
            'auth_email': 'required|email|maxlength:255'
        },
        customTranslateName: {
            'auth_email': 'Email'
        }
    },
    'auth_reset_password': {
        rules: {
            'auth_password': 'required',
            'auth_confirm_password': 'required|equal:auth_password',
        },
        customTranslateName: {
            'auth_password': 'Mật khẩu',
            'auth_confirm_password': 'Xác nhận mật khẩu',
        }
    }
}