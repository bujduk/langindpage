(function ($) {
    new WOW().init();

    $(document).on('scroll', () => {
        navbarScroll();
    });

    $('.form_show_password').on('click', (e) => {
        e.stopPropagation();
        showPassword($(e.target));
    })

    $('.auth-form').on('submit', (e) => {
        e.preventDefault();
        submitForm($(e.target));
    })
})(jQuery);