const $ = jQuery;

parseURLParamToKeyValue = (data) => {
    return JSON.parse('{"' + decodeURI(data).replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g,'":"') + '"}')
};

/* Generate key */
let generateKey = () => {
    return Math.random().toString(20).substr(2, 10);
}

$(function () {
    $.fn.serializeObject = function () {
        let o = {};
        let a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };

    if (window.innerWidth < 992) {
        Notiflix.Loading.Init({
            backgroundColor: "rgba(255,255,255,0.6)",
            svgColor: "#06202D",
        });
    }
});

commonShowNotifyErr = () => {
    Notiflix.Notify.Failure(common_error);
}