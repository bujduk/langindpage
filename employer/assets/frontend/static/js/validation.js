/*
* Yêu cầu:
*   + Phải có jQuery và Lodash
*
* */

let ruleTest = {
    required: function (value, validateValue) {
        if (_.isArray(value)) {
            return !!value && value.length > 0;
        }
        return value !== undefined && value !== null && value.trim().length > 0;
    },
    email: function (value, validateValue) {
        return /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(value);
    },
    url: function (value, validateValue) {
        return /^(?:(?:(?:https?|ftp):)?\/\/)(?:\S+(?::\S*)?@)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})).?)(?::\d{2,5})?(?:[/?#]\S*)?$/i.test(value);
    },
    number: function (value, validateValue) {
        return /^(?:-?\d+|-?\d{1,3}(?:,\d{3})+)?(?:\.\d+)?$/.test(value);
    },
    digits: function (value, validateValue) {
        return /^\d+$/.test(value);
    },
    minlength: function (value, validateValue) {
        let length = value.length;
        return length >= validateValue;
    },
    maxlength: function (value, validateValue) {
        let length = value.length;
        return length <= validateValue;
    },
    min: function (value, validateValue) {
        return value >= validateValue;
    },
    max: function (value, validateValue) {
        return value <= validateValue;
    },
    listemail: function (value, validateValue) {
        let output = true;

        if (_.isString(value)) {
            if (_.isString(validateValue)) {
                value = value.split(validateValue)
            } else {
                let temp = value;
                value = [];
                value.push(temp);
            }
        }

        _.forEach(
            value,
            (item) => {
                if (!ruleTest.email(item)) {
                    output = false;
                    return false;
                }
            }
        );

        return output;
    },
    equal: function (value, validateValue)  {
        let equalValue = $(`#${validateValue}`).val();

        console.log(validateValue);
        return _.isEqual(value, equalValue);
    }
}

$(() => {
    $.fn.validationObject = function (validateForm) {

        let $form = $(this);
        let $dataRaw = $form.serializeObject();
        let $data = {};
        let errors = {};
        let alreadyFocus = false;

        let rules = !!validateForm.rules ? validateForm.rules : {};
        let customTranslateName = !!validateForm.customTranslateName ? validateForm.customTranslateName : {}
        let elementInnerForm = !!validateForm.elementInnerForm ? validateForm.elementInnerForm : null

        let clearError = () => {
            $('.error-text').text(null);
            $('.error-input').removeClass('error-input');
            $('.error-form').removeClass('error-form');
        }

        let startTestEachItem = (inputFieldName, listRuleValue, originalRuleName = null) => {

            let inputName = inputFieldName;

            /* Kiểm tra required trước */
            if (!!listRuleValue['required']) {
                if (inputName === 'language') {
                }

                /* Kiểm tra nếu validation fail thì thêm item message vào errors */
                if (!ruleTest['required']($data[inputName], listRuleValue['required'])) {

                    inputName = !!originalRuleName ? originalRuleName : inputName;
                    inputName = !!customTranslateName[inputName] ? customTranslateName[inputName] : inputName;

                    errors[inputFieldName] = validationMessages(
                        'required',
                        inputName
                    );

                    if (!alreadyFocus) {
                        alreadyFocus = true;
                        $form.find(`[name="${inputName}"]`).focus();
                    }

                    return;
                }

                /* Sau khi kiểm tra required xong thì xóa required trước*/
                delete listRuleValue['required'];
            }

            /* Kiểm tra lần lượt các trường hợp còn lại */
            $.each(listRuleValue, (ruleName, ruleValue) => {

                if (ruleName !== 'equal') {
                    ruleValue = _.isString(ruleValue) ? _.toNumber(ruleValue) : ruleValue;
                }

                /* Kiểm tra nếu trường cần validate có trong $data */
                if (!!$data[inputName]) {
                    if (!ruleTest[ruleName]($data[inputName], ruleValue)) {


                        inputName = !!originalRuleName ? originalRuleName : inputName;
                        inputName = !!customTranslateName[inputName] ? customTranslateName[inputName] : inputName;

                        errors[inputFieldName] = validationMessages(ruleName, inputName, ruleValue);
                        return false;
                    }
                }
            })
        }

        let showError = (errors) => {
            $.each(errors, (inputName, errorMessage) => {
                $form.find(`[name="${inputName}"]`).each((key, element) => {
                    let id = $(element).attr('id');

                    $(`#${id}_error`).text(errorMessage).addClass('error-text');
                    $(`#${id}`).addClass('error-input').parent().addClass('error-form');
                });
            })
        }

        /* Chỉ test trong các ô input được chỉ định bên trong element elementInnerForm */
        if (!!elementInnerForm) {
            $.each($dataRaw, (name, value) => {
                if(!!$(`${elementInnerForm} [name="${name}"]`).length) {
                    $data[name] = value;
                }
            });
        } else {
            $data = {...$dataRaw}
        }

        clearError();

        /* Truy xuất từng validation trong list validations */
        $.each(rules, (ruleName, ruleValue) => {

            if (!!ruleValue) {


                /* Phân tách rules trong validation sang dạng Object */
                let listRulesValue = _.reduce(
                    ruleValue.split('|'),
                    (obj, item) => {
                        let itemSplit = item.split(':');

                        if (!!itemSplit[1]) {

                            if (itemSplit[0] === 'equal') {
                                obj[itemSplit[0]] = itemSplit[1];

                            } else {
                                switch (itemSplit[1]) {
                                    case 'true':
                                        obj[itemSplit[0]] = true;
                                        break;
                                    case 'false':
                                        break;
                                    case ',':
                                        obj[itemSplit[0]] = ',';
                                        break;
                                    default:
                                        obj[itemSplit[0]] = _.toNumber(itemSplit[1]);
                                }
                            }
                        } else {
                            obj[itemSplit[0]] = true
                        }

                        return obj;
                    },
                    {}
                );

                /* Validate các ruleName không có wildcard */
                if (!ruleName.includes('%')) {
                    startTestEachItem(ruleName, { ...listRulesValue });
                } else {
                    let ruleNameSelector = _.reduce(
                        ruleName.split('%'),
                        (stringAppend, item) => {
                            return stringAppend + `[name*="${item}"]`;
                        },
                        ''
                    )

                    $(ruleNameSelector).each((name, element) => {
                        let elementName = $(element).attr('name');
                        startTestEachItem(elementName, { ...listRulesValue }, ruleName);
                    })
                }
            }
        });

        showError(errors);

        return _.isEmpty(errors);
    }

});