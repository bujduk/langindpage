let generateListCandidates = (list) => {

    $('.sc-cv__list').empty('');
    list.forEach((item) => {
        generateCandidateItem(item);
    })
}

let generateCandidateItem = (data) => {
    $('.sc-cv__list').append(`
        <div id="candidate_${data.id}" 
        data-url="${data.url}"
        onclick="_handleViewDetail(${data.id})"
        class="sc-cv__item d-flex position-relative">
            <button class="item-bookmark ${(!!data.is_saved) ? 'bookmarked' : ''}">
                <img class="empty" src="assets/images/icon-sc-search-bookmark.svg" alt="">
                <img class="fill" src="assets/images/icon-sc-search-bookmarked.svg" alt="">
            </button>
            <div class="item-left">
                <div style="background-image: url(${data.img_profile})"
                     class="item-image">
                </div>
            </div>
            <div class="item-right">
                <div class="item-name">
                    <span class="candidate-${data.id}-name"></span>
                    
                    ${(data.is_viewed) ? `<span class="tiny-badge tiny-badge__yellow">Đã xem</span>` : ''}
                </div>
                <p class="item-job candidate-${data.id}-job">
                    
                </p>
                <p class="item-moreinfo">
                    Năm kinh nghiệm:
                    <span class="item-exyear">
                        <span class="candidate-${data.id}-year_of_experience"></span> năm
                    </span>
                </p>
                <p class="item-moreinfo">
                    Địa điểm:
                    <span class="item-location candidate-${data.id}-province">
                        ${data.address.province}
                    </span>
                </p>
            </div>
        </div>
    `);

    $(`.candidate-${data.id}-name`).text(data.name);
    $(`.candidate-${data.id}-job`).text(data.job);
    $(`.candidate-${data.id}-year_of_experience`).text(data.year_of_experience);
    $(`.candidate-${data.id}-province`).text(data.address.province);

}