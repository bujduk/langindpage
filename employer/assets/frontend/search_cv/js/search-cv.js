_handleShowUnlockCVModal = ($button) => {
    let cost = _.toNumber($button.attr('data-cost'));

    $('.search-unlockcv-cost').text(cost);
    $('.search-user-point').text(userPoint);

    if (cost > userPoint) {
        $('.search-user-enough-point').removeClass('element-hidden');
        $('.search-unlockcv-button').prop('disabled', true).is(':disabled', true);
    }

    $('#search_unlockcv_modal').modal('show');
}

_handleHiddenUnlockCVModal = () => {
    $('.search-unlockcv-cost').text('cost');
    $('.search-user-point').text('');
    $('.search-user-enough-point').addClass('element-hidden');
    $('.search-unlockcv-button').prop('disabled', false).is(':disabled', false);
}

_handleSearchYearOfExperienceResetClick = () => {
    let yearOfExperienceView = $('.search_yoe_view');
    let yearOfExperienceFrom = $('#search_yoe_from');
    let yearOfExperienceTo = $('#search_yoe_to');

    yearOfExperienceView
        .attr({
            'data-from': '',
            'data-to': ''
        })
        .text('Năm kinh nghiệm');

    yearOfExperienceFrom.val(yearOfExperienceFrom.attr('data-default'));
    yearOfExperienceTo.val(yearOfExperienceTo.attr('data-default'));
}

_handleSearchYearOfExperienceSubmitClick = () => {
    let yearOfExperienceView = $('.search_yoe_view');
    let yearOfExperienceFrom = _.toNumber($('#search_yoe_from').val());
    let yearOfExperienceTo = _.toNumber($('#search_yoe_to').val());

    if (!!yearOfExperienceFrom && !!yearOfExperienceTo) {

        yearOfExperienceView
            .attr({
                'data-from': yearOfExperienceFrom,
                'data-to': yearOfExperienceTo
            })
            .text(`Kinh nghiệm từ ${yearOfExperienceFrom} - ${yearOfExperienceTo} năm`);
    }
}

_handleViewDetail = (candidateId) => {
    let moduleMobileList = $('.module_mobile_list');
    let moduleMobileDetail = $('.module_mobile_detail');

    moduleMobileList.collapse('hide');
    moduleMobileDetail.collapse('show');
    getCandidateDetail(candidateId);
}

_handleBackToList = () => {
    let moduleMobileList = $('.module_mobile_list');
    let moduleMobileDetail = $('.module_mobile_detail');

    moduleMobileList.collapse('show');
    moduleMobileDetail.collapse('hide');

    window.scrollTo(0, 0);
}