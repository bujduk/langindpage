let getListCandidates = () => {
    Notiflix.Block.Pulse('.sc-body__left');
    setTimeout(() => {
        getFormRequest('./assets/frontend/search_cv/js/json/list_candidates.json')
            .then((responseData) => {

                    let data = responseData.data;
                    let bindingTotalCV = $('.search_total_cv');

                    bindingTotalCV.text(data.total);

                    generateListCandidates(data.candidates);

                    Notiflix.Block.Remove('.sc-body__left');
                }, function (responseErr) {
                    if (responseErr.status === 422) {
                        commonShowNotifyErr();
                    } else {
                        commonShowNotifyErr();
                    }
                }
            );
    }, 1000);
}

let getUserPointFromRequest = () => {

    setTimeout(() => {
        getFormRequest('./assets/frontend/search_cv/js/json/user_point.json')
            .then((responseData) => {
                    userPoint = responseData.data.points;
                }, function (responseErr) {
                    if (responseErr.status === 422) {
                        commonShowNotifyErr();
                    } else {
                        commonShowNotifyErr();
                    }
                }
            );
    }, 1000);
}

let getFullContactByPoints = ($button) => {
    Notiflix.Loading.Pulse();

    getFormRequest('./assets/frontend/search_cv/js/json/full_contact.json')
        .then((responseData) => {
                let data = responseData.data;
                let unlockcvButton = $('.search_unlockcv_button');

                $('.search_unlocked').removeClass('element-hidden');
                $('.search_unlocked_phone').text(data.mobile);
                $('.search_unlocked_email').text(data.email);

                Notiflix.Notify.Success('Hiển thị thông tin thành công!');

                // unlockcvButton.removeClass('viewed')
                //     .prop('disabled', false)
                //     .is(':disabled', false)
                // unlockcvButton.find('span')
                //     .text('Xem thông tin bị ẩn')
                //     .attr('data-status', 'locked');

                unlockcvButton.addClass('viewed')
                    .prop('disabled', true)
                    .is(':disabled', true);

                unlockcvButton.find('span')
                    .text('Đã mở CV')
                    .attr('data-status', 'unlock');

                Notiflix.Loading.Remove();

                $('#search_unlockcv_modal').modal('hide');

            }, function (responseErr) {
                if (responseErr.status === 422) {
                    commonShowNotifyErr();
                } else {
                    commonShowNotifyErr();
                }
            }
        );
}

let getCandidateDetail = (candidateId) => {

    getFormRequest('./assets/frontend/search_cv/js/json/candidate_detail.json')
        .then((responseData) => {

                // let moduleMobileList = $('.module_mobile_list');
                // let moduleMobileDetail = $('.module_mobile_detail');
                //
                // moduleMobileList.addClass('module-mobile__hidden');
                // moduleMobileDetail.removeClass('module-mobile__hidden');

                window.scrollTo(0, 0);

            }, function (responseErr) {
                if (responseErr.status === 422) {
                    commonShowNotifyErr();
                } else {
                    commonShowNotifyErr();
                }
            }
        );
}