let init = () => {
    getListCandidates();
    getUserPointFromRequest();
}

(function ($) {
    let $body = $('body');

    $('.search_location_select2').select2({
        minimumResultsForSearch: -1,
        selectionCssClass: 'searchcv-select2-selection',
        dropdownCssClass: 'searchcv-select2-dropdown diadiem',
        placeholder: $(this).attr('data-placeholder'),
        allowClear: true,
        width: 'auto',
    });

    $('.ngonngu_select2').select2({
        minimumResultsForSearch: -1,
        selectionCssClass: 'filtercv-select2-selection',
        dropdownCssClass: 'filtercv-select2-dropdown ngonngu',
        placeholder: $(this).attr('data-placeholder'),
        allowClear: true,
        width: 'auto',
    });

    $('.trinhdo_select2').select2({
        minimumResultsForSearch: -1,
        selectionCssClass: 'filtercv-select2-selection',
        dropdownCssClass: 'filtercv-select2-dropdown trinhdo',
        placeholder: $(this).attr('data-placeholder'),
        allowClear: true,
        width: 'auto',
    });

    $('.phanloai_select2').select2({
        minimumResultsForSearch: -1,
        selectionCssClass: 'filtercv-select2-selection',
        dropdownCssClass: 'filtercv-select2-dropdown phanloai',
        placeholder: $(this).attr('data-placeholder'),
        allowClear: true,
        width: 'auto',
    });

    $('.sapxep_select2').select2({
        minimumResultsForSearch: -1,
        selectionCssClass: 'filtercv-select2-selection',
        dropdownCssClass: 'filtercv-select2-dropdown sapxep',
        placeholder: $(this).attr('data-placeholder'),
        allowClear: true,
        width: 'auto',
    });

    $('.loaicv_select2').select2({
        minimumResultsForSearch: -1,
        selectionCssClass: 'filtercv-select2-selection',
        dropdownCssClass: 'filtercv-select2-dropdown loaicv',
        width: 'auto',
    });

    $('#search_content').on('focus', () => {
        if (window.innerWidth < 992) {
            $('.sc_search').addClass('mobile-show');
            $body.addClass('no-scroll');
        }
    });

    $('.search-mobile__close').on('click', () => {
        $('.sc_search').removeClass('mobile-show');
        $body.removeClass('no-scroll');
    })

    $('#search_unlockcv_modal').on('hidden.bs.modal', () => {
        _handleHiddenUnlockCVModal();
    });

    $('.search_collapse').collapse({
        toggle: false
    });

    init();
})(jQuery);
