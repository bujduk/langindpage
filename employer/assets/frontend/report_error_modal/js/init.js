(function ($) {
    let $modalReportError = $('#modal_report_error');
    let $formReportError = $('#form_report_error');

    $modalReportError.on('hidden.bs.modal', () => {

        $formReportError.find('input, textarea').val(null);

        $('.modal-file__item').fadeOut();
        $('.modal-file__image').css('background', 'none');
        $('.modal-file__title').text(null);
        $('#file_report').val(null);
    })

    $formReportError.on('submit', (e) => {
        e.preventDefault();
    })
})(jQuery);