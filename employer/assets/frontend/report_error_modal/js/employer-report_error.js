const errorReportFileChange = (_this) => {
    let file = _this[0].files[0];
    let imgItem = $('.modal-file__item');
    let imgView = $('.modal-file__image');
    let imgTitle = $('.modal-file__title');
    let gallery_add_list_error = $('#file_report_error');

    gallery_add_list_error.html('');

    if (file.size > 2097152) {
        let html = `<span style="display:block;">fffff</span>`;

        gallery_add_list_error.append(html);
    } else if (file.type !== "image/jpeg" && file.type !== "image/png" && file.type !== "image/jpg") {
        let html = `<span style="display:block;">aaaaa</span>`;

        gallery_add_list_error.append(html);
    } else {
        let fileReader = new FileReader();

        galleryImageList = file;

        fileReader.onload = ((fileParam) => {
            return (e) => {
                imgItem.fadeIn();
                imgView.css('background-image', `url(${e.target.result})`);
                imgTitle.text(fileParam.name);
            }
        })(file);

        fileReader.readAsDataURL(file);
    }
};

const deleteErrorReportItem = () => {
    Notiflix.Confirm.Show(
        'Xác nhận',
        'Bạn chắc chắn muốn xoá?',
        'Đồng ý',
        'Huỷ',
        () => {
            $('.modal-file__item').fadeOut();
            $('.modal-file__image').css('background', 'none');
            $('.modal-file__title').text(null);
            $('#file_report').val(null);
        }
    );
}
const submitReportForm = ($form) => {

}
(function ($) {
    let $modalReportError = $('#modal_report_error');
    let $formReportError = $('#form_report_error');

    $modalReportError.on('hidden.bs.modal', () => {

        $formReportError.find('input, textarea').val(null);

        $('.modal-file__item').fadeOut();
        $('.modal-file__image').css('background', 'none');
        $('.modal-file__title').text(null);
        $('#file_report').val(null);
    })

    $formReportError.on('submit', (e) => {
        e.preventDefault();
    })
})(jQuery);