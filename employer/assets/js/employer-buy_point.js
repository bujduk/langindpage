
(function ($) {
    Notiflix.Loading.Init({
        backgroundColor: "rgba(255,255,255,0.6)",
        svgColor: "#06202D",
    });

    $('.bp_service_select2').select2({
        minimumResultsForSearch: -1,
        selectionCssClass: 'bp-service-select2-selection',
        dropdownCssClass: 'bp-service-select2-dropdown',
        width: '100%',
    });
})(jQuery);