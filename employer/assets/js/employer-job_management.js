let ContentSearchTimeout = null;
let intervalCountdown;
let currentPage = 1;
let lastPageJob = 6;

const changeStatus = {
    "draft": "Chờ duyệt",
    "active": "Không hoạt động",
    "expired": "Hoạt động",
    "inactive": "Hoạt động",
    "approved": "Hoạt động",
};

const changeStatusClass = {
    "active": "inactive",
    "draft": "pending-review",
    "inactive": "active",
    "expired": "active",
    "approved": "active",
};

const statusAction = {
    "active": "Tạm dừng tin tuyển dụng",
    "draft": "Đăng tin tuyển dụng",
    "inactive": "Đăng tin tuyển dụng",
    "expired": "Gia hạn tin tuyển dụng",
    "approved": "Đăng tin tuyển dụng",
};

$(document).ready(() => {

    /*==================SELECT STATUS===================*/
    $("#list-job_status").on("select2:select", (e) => {
        filterData($(e.target));
    });

    $("#user-upload").on("select2:select", (e) => {
        filterData($(e.target));
    });

    calenderDateRager();

    $('#listJob-calender-date-ranger').on('apply.daterangepicker', (e) => {
        filterData($(e.target));
    });

    $('#search_content').on('input', (e) => {
        clearTimeout(ContentSearchTimeout);

        ContentSearchTimeout = setTimeout(() => {
            filterData($(e.target));
        }, 1000);
    })

    $('#go-to').on('keyup', (e) => {
        if (e.keyCode === 13) {
            e.preventDefault();
            indexPagination($(e.target));
        }
    })

    /* ================================ FORM SUBMIT ================================= */
    $('#select_service_form').submit((e) => {
        e.preventDefault();

        let modal = $('#select_service_modal')
        let id = modal.attr('data-id');
        let status = $(`#list-job_status_${id}`);
        let dataStatus = status.attr('data-status');
        let inputValue = $('#published_at');

        $(`#list-job_status_${id} span`).remove();

        if (dataStatus === 'draft') {
            status.append(`
                        <span class="job__status--${changeStatusClass[dataStatus]}">${changeStatus[dataStatus]}</span>
                    `)

            status.attr('data-status', `${changeStatusClass[dataStatus]}`);
        } else if (dataStatus === 'expired') {
            if (inputValue.val() === '') {
                let now = new Date();
                inputValue.val(moment(now).format('YYYY-MM-DD hh:mm:ss'));
                console.log(inputValue.val());

                status.append(`
                        <span class="job__status--${changeStatusClass[dataStatus]}">${changeStatus[dataStatus]}</span>
                    `)

                status.attr('data-status', `${changeStatusClass[dataStatus]}`);
            } else {
                status.append(`
                        <span class="job__status--approved">Đã duyệt</span>
                    `)

                status.attr('data-status', 'approved');
            }
        }
        
        modal.modal('hide');
    });

    $('#time_countdown-inactive_form').submit((e) => {
        e.preventDefault();

        let modal = $('#time_countdown-inactive_modal')
        let id = modal.attr('data-id');
        let status = $(`#list-job_status_${id}`);
        let dataStatus = status.attr('data-status');

        $(`#list-job_status_${id} span`).remove();

        status.append(`
                        <span class="job__status--${changeStatusClass[dataStatus]}">${changeStatus[dataStatus]}</span>
                    `)

        status.attr('data-status', `${changeStatusClass[dataStatus]}`);

        modal.modal('hide');
    })

    $('#time_countdown-approved_form').submit((e) => {
        e.preventDefault();

        let modal = $('#time_countdown-approved_modal')
        let id = modal.attr('data-id');
        let status = $(`#list-job_status_${id}`);
        let dataStatus = status.attr('data-status');

        $(`#list-job_status_${id} span`).remove();

        status.append(`
                        <span class="job__status--${changeStatusClass[dataStatus]}">${changeStatus[dataStatus]}</span>
                    `)

        status.attr('data-status', `${changeStatusClass[dataStatus]}`);

        modal.modal('hide');
    })
});

/*================================== CHANGE STATUS ================================*/

statusChange = (_this, prefixId) => {
    Notiflix.Confirm.Show(
        'Xác nhận',
        'Bạn chắc chắn muốn thay đổi trạng thái ?',
        'Đồng ý',
        'Huỷ',
        () => {
            let id = _this.attr('data-id');
            let status = $(`#${prefixId}_status_${id}`).attr('data-status');

            $(`#${prefixId}_status_${id} span`).remove();

            $(`#${prefixId}_status_${id}`).append(`
                        <span class="job__status--${changeStatusClass[status]}">${changeStatus[status]}</span>
                    `)
            /*$(`#${prefixId}_status_${id}`).attr('data-status', `${changeStatusClass[status]}`);*/
        },
    );
}

listActionOpen = (_this, prefixClass, prefixId) => {
    let id = _this.attr('data-id');
    let status = $(`#${prefixId}_status_${id}`).attr('data-status');
    let listActionDropdown = $('.list__action--dropdown');

    $(`#${prefixClass}_${id} .dropdown`).on('hidden.bs.dropdown', () => {
        $(`#${prefixClass}_${id} .list-action--button`).removeClass('button-active');
    })

    $(`#${prefixClass}_${id} .dropdown`).on('show.bs.dropdown', () => {
        $(`#${prefixClass}_${id} .list-action--button`).addClass('button-active');
    })

    $('.status-action').remove();

    switch (status) {
        case 'draft':
        case 'expired':
            listActionDropdown.prepend(`
                <button onclick="selectService($(this), 'list-job')" data-id="${id}" class="dropdown-item status-action">${statusAction[status]}</button>
            `);
            break;
        case 'active':
            listActionDropdown.prepend(`
                <button onclick="statusChange($(this), 'list-job')" data-id="${id}" class="dropdown-item status-action">${statusAction[status]}</button>
            `);
            break;
        case 'inactive':
            listActionDropdown.prepend(`
                <button onclick="timeRemaining('#time_countdown-inactive_modal')" data-id="${id}" class="dropdown-item status-action">${statusAction[status]}</button>
            `);
            break;
        case 'approved':
            listActionDropdown.prepend(`
                <button onclick="timeRemaining('#time_countdown-approved_modal')" data-id="${id}" class="dropdown-item status-action">${statusAction[status]}</button>
            `);
            break;

    }
}

/*===============SCROLL APPEND DATA==============*/

listBodyScroll = (_this, prefixId) => {
    let contentListBody = _this.get(0);
    let lazyLoading = $('.lazy-loading');

    lazyLoading.text('');
    Notiflix.Block.Dots('.lazy-loading');
    if (contentListBody.scrollTop + contentListBody.clientHeight === contentListBody.scrollHeight) {
        setTimeout(() => {
            if (currentPage < lastPageJob) {
                currentPage++;
                appendListJob();
                selectItemUnCheckAllCss(prefixId);
            } else {
                lazyLoading.text('No record ...');
            }
        }, 1000);
    }
}

/*==================================PAGIGNATION======================*/
nextPage = (_this, prefixId) => {
    let value = parseInt($(`#${prefixId}--present`).attr('data-page'));

    if (value !== lastPageJob) {
        value++;

        $('html,body').animate({
                scrollTop: $('.calender').offset().top - 20},
            500);

        pageCss(prefixId, value);
        filterListData(_this, prefixId);
    }

    if (value === lastPageJob) {
        pageNextCss(prefixId);
    }
}

prevPage = (_this, prefixId) => {
    let value = parseInt($(`#${prefixId}--present`).attr('data-page'));
    let min = 1;

    if (value !== min) {
        value--;
        $('html,body').animate({
                scrollTop: $('.calender').offset().top - 20},
            500);

        pageCss(prefixId, value);
        filterListData(_this, prefixId);
    }
    if (value === min) {
        pagePrevCss(prefixId);
    }
}

indexPagination = (goToInput) => {
    let value = _.toNumber(goToInput.val());
    let presentPage = $('#list-job--present');
    let totalPage = _.toNumber($('#total-page').attr('data-total'));

    $('html,body').animate({
            scrollTop: $('.calender').offset().top - 20},
        500);

    value = value < 1 ? 1 : value;
    value = value > lastPageJob ? lastPageJob : value;

    presentPage.html(value);
    presentPage.attr('data-page', value);
    pageCss('list-job', value);

    filterListData();
    goToInput.val('');

    if (value === 1) {
        pagePrevCss('list-job');
    }

    if (value === totalPage) {
        pageNextCss('list-job');
    }
}

/*================== DELETE ALL ITEM ==================== */
deleteAllItem = (_this, prefixId) => {

    Notiflix.Confirm.Show(
        'Xác nhận',
        'Bạn chắc chắn muốn xoá?',
        'Đồng ý',
        'Huỷ',
        () => {
            let deleteChecked = $(`#${prefixId} .js_list-body input:checked`);

            deleteChecked.each((key, value) => {
                let item = $(value);
                let id = item.attr('data-id');

                $(`#${prefixId}_${id}`).remove();
                showTitleContentListBody(_this, prefixId);
            });
        },
    );
}

/*================== DELETE ITEM ==================== */
deleteItem = (_this, prefixId) => {
    Notiflix.Confirm.Show(
        'Xác nhận',
        'Bạn chắc chắn muốn xoá?',
        'Đồng ý',
        'Huỷ',
        () => {
            let id = _this.attr('data-id');

            $(`#${prefixId}_${id}`).remove();

            showTitleContentListBody(_this, prefixId);
        },
    )
}

/* ============== SORT ITEM =============== */
sortItem = (_this) => {
    let headerIndex = _this.attr('data-sort');
    if (headerIndex === '2') {
        headerIndex = 0;
    } else {
        headerIndex++;
    }

    _this.attr('data-sort', headerIndex);

    $('.text-list-title').attr('data-sort', '0');

    if (headerIndex === 1) {
        resetSortIcon();
        sortItemArrowUp(_this);
        sortAppend();
    } else if (headerIndex === 2) {
        resetSortIcon();
        sortItemArrowDown(_this);
        sortAppend();
    } else {
        resetSortIcon();
        sortAppend();
    }
}

/*===============BINDING DATA==============*/
filterData = (e) => {
    let searchVal = $('#search_content').val();
    let statusVal = $('#list-job_status').val();
    let userUpload = $('#user-upload').val();
    let startTime = $('#listJob-calender-date-ranger').val().split(' - ')[0];
    let endTime = $('#listJob-calender-date-ranger').val().split(' - ')[1];
    let url = `itnavi.com.vn/job/search?keyword=${searchVal}&status=${statusVal}&userUpload=${userUpload}&startTime=${startTime}&endTime=${endTime}`;
}

filterListData = (_this) => {

    Notiflix.Loading.Pulse();

    setTimeout(() => {
        removeListItem();
        appendListJob();
        Notiflix.Loading.Remove();
    }, 1000);
}

/*=============================MODAL TIME==========================*/
timeRemaining = (prefixId) => {
    let modal = $(prefixId);

    intervalCountdown = setInterval(() => {
        timeCountdown(prefixId, '2021-01-08 14:12:00');
    }, 1000);

    timeCountdown(prefixId, '2021-01-08 14:12:00');

    modal.modal('show');
}
generateJobItem = (key) => {
    let html = `<div class="list__content--item d-flex align-items-center
                                    flex-column flex-lg-row" id="list-job_${key}">
                            <div class="checkbox-label d-flex justify-content-center align-items-center app-tablet app-desktop">
                                <label>
                                    <input class="input-hidden" data-id="${key}" id="list-job-checkbox_${key}"
                                           onchange="selectItemCheckBox($(this), 'list-job')" type="checkbox">
                                    <span
                                        class="list--checkbox position-relative d-flex justify-content-center align-items-center">
                                            <i class="fas fa-check"></i>
                                    </span>
                                </label>
                            </div>
                            <div class="job-title d-flex flex-column">
                                    <a class="text-default" href="#" target="_blank"
                                       title="UI-UX Designer (HTML5) Up to 1500$ UI-UX Designer (HTML5) Up to 1500$">
                                        <span>UI-UX Designer (HTML5) Up to 1500$</span>
                                    </a>
                                    <span class="premium-commitment" id="service-job_1">
                                        Premium job - Có cam kết
                                    </span>
                            </div>
                            <div class="d-flex align-items-center flex-column flex-lg-row w-100">
                                <div class="d-flex basic-info">
                                    <div class="basic-info--left">
                                        <div class="basic-info--content">
                                            <span class="app-mobile title--mobile">Ngày đăng</span>
                                            <span
                                                class="date-upload text-list-default">10/09/2020</span>
                                        </div>
                                        <div class="basic-info--content">
                                                <span class="app-mobile title--mobile">Ngày hết
                                                    hạn</span>
                                            <span
                                                class="date-expired text-list-default" id="date-expired_${key}" data-time="2020/09/10 10:10:10">10/09/2020</span>
                                        </div>
                                    </div>
                                    <div class="basic-info--right">
                                        <div class="basic-info--content">
                                            <span class="app-mobile title--mobile">Lượt xem</span>
                                            <span class="view text-list-default">6996</span>
                                        </div>
                                        <div class="basic-info--content recruitment">
                                            <span class="app-mobile title--mobile">Ứng tuyển</span>
                                            <a class="text-list-default" href="#" target="_blank">
                                                <span>6996</span>
                                            </a>
                                        </div>
                                    </div>
                                    <div class="status" id="list-job_status_${key}" data-status="expired">
                                        <span class="job__status--expired">
                                            Hết hạn
                                        </span>
                                    </div>
                                </div>
                                <div class="list-action" id="list-action_${key}">
                                    <div class="dropdown show">
                                        <button class="list-action--button btn" role="button" onclick="listActionOpen($(this), 'list-action', 'list-job')" data-id="${key}" data-toggle="dropdown">
                                            Lựa chọn
                                            <svg fill="none" height="6" viewBox="0 0 9 6" width="9"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M8.69812 0H0.301875C0.0500391 0 -0.0905801 0.294357 0.0653794 0.495312L4.2635 5.88431C4.38367 6.03856 4.61505 6.03856 4.7365 5.88431L8.93462 0.495312C9.09058 0.294357 8.94996 0 8.69812 0Z"
                                                    fill="#1C4355"/>
                                            </svg>
                                        </button>
                                        <div class="dropdown-menu list__action--dropdown">
                                            <button value="edit" class="dropdown-item">Sửa nội dung</button>
                                            <button onclick="duplicate($(this))" readonly value="duplicate" class="dropdown-item">Tạo bản sao</button>
                                            <button data-id="${key}" onclick="deleteItem($(this), 'list-job')" readonly value="delete" class="dropdown-item">Xóa công việc</button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>`;

    $('.height__list--body').append(html);
}

/*===============GENERATE LIST JOB==============*/
appendListJob = (_this) => {
    for (let i = 0; i < 10; i++) {
        let key = generateKey();

        generateJobItem(key);
    }
    
}

sortAppend = () => {
    let arraySort = [''];

    $('.list__content--item').remove();

    arraySort.forEach(() => {
        appendListJob();
    })

}

/*=======================DUPLICATE============================*/
duplicate = (_this) => {
    Notiflix.Confirm.Show(
        'Xác nhận',
        'Bạn chắc chắn muốn tạo bản sao?',
        'Đồng ý',
        'Huỷ',
        () => {
            console.log(true)
        },

        () => {
            console.log(false)
        }
    );
}


$(document).ready(() => {
    $('#editStatusJob_modal').on('hidden.bs.modal', () => {
        $('#edit_statusJob_modal').val(null).trigger('change');
    });

    $('#select_service_modal').on('hidden.bs.modal', () => {
        resetItemActive();
        $('.post_job_settime').addClass('disabled');
    });

    $('#time_countdown-inactive_modal').on('hidden.bs.modal', () => {
        clearTimeout(intervalCountdown);
    });

    $('#time_countdown-approved_modal').on('hidden.bs.modal', () => {
        clearTimeout(intervalCountdown);
    });

    /*===============SELECT STATUS=======================*/
    $('#list-job_status').select2({
        placeholder: 'Trạng thái',
        minimumResultsForSearch: -1,
        dropdownCssClass: 'job_management-select2-dropdown job_status-select2-dropdown',
        selectionCssClass: 'job_management-select2-selection',
        width: '100%',
    });

    $('#user-upload').select2({
        placeholder: 'Người đăng',
        minimumResultsForSearch: -1,
        dropdownCssClass: 'job_management-select2-dropdown user-upload-select2-dropdown',
        selectionCssClass: 'job_management-select2-selection',
        width: '132px',
    })

    /*==================SHADOW LIST JOB================*/
    $('.js_list-body').scroll(function () {
        if ($(this).scrollTop()) {
            $('.content__list--title').addClass('header-shadow');
        } else {
            $('.content__list--title').removeClass('header-shadow');
        }
    })

    /*===================TOTAL LIST======================*/
    $('.total-list span').html($(`.list__content--item`).length);

    /*===================COUNT ANIMATION======================*/
    $('.count').each(function () {
        $(this).prop('Counter', 0).animate({
            Counter: $(this).text()
        }, {
            duration: 1000,
            easing: 'swing',
            step: function (now) {
                if (now < 9) {
                    $(this).text('0' + Math.ceil(now));
                } else {
                    $(this).text(Math.ceil(now));
                }
            }
        });
    });

    $('.post_job_settime').daterangepicker({
        singleDatePicker: true,
        timePicker: true,
        timePicker24Hour: true,
        showDropdowns: true,
        opens: "left",
        drops: "up",
        minDate: moment().startOf('minute'),
        locale: {
            format: 'YYYY-MM-DD HH:mm:ss'
        },
    }).on('cancel.daterangepicker', (e) => {
        $(e.target).addClass('disabled');
    }).on('apply.daterangepicker', (e) => {
        $(e.target).removeClass('disabled');
    });

    Notiflix.Block.Init({
        svgColor: "#32c682",
    });
})

/* Generate key */
generateKey = () => {
    return Math.random().toString(20).substr(2, 10);
}

/*======================== SHOW LIST ACTION ======================*/
showActionContentListBody = (_this, prefixId) => {
    $(`#${prefixId} .job__list--title `).addClass('element-hidden');
    $(`#${prefixId} .js_content-list--action`).removeClass('element-hidden');
    $(`#${prefixId} .amount-checkbox`).html($(`#${prefixId} .list__content--item input:checked`).length);
}

/*======================== SHOW LIST TITLE ======================*/
showTitleContentListBody = (_this, prefixId) => {
    $(`#${prefixId} .job__list--title`).removeClass('element-hidden');
    $(`#${prefixId} .js_content-list--action`).addClass('element-hidden');
    $(`#${prefixId} .selectAllCheckbox`).prop('checked', false);
}

/*=========================SORT ARROW=========================*/
sortItemArrowUp = (_this) => {
    $('.arrow-up', _this).addClass('sort');
    $('.arrow-down', _this).removeClass('sort');
    _this.attr('data-sort', 1);
}

sortItemArrowDown = (_this) => {
    $('.arrow-down', _this).addClass('sort');
    $('.arrow-up', _this).removeClass('sort');
    _this.attr('data-sort', 2);
}

/*=======================RESET SORT ICON======================*/
resetSortIcon = () => {
    $('.arrow-down').removeClass('sort')
    $('.arrow-up').removeClass('sort');
}

/*==================CALENDER===================*/
calenderDateRager = () => {
    $('#listJob-calender-date-ranger').daterangepicker({
        opens: 'left',
        autoUpdateInput: false,
        locale: {
            cancelLabel: 'Clear',
        },

        ranges: {
            'Hôm nay': [moment(), moment()],
            'Hôm qua': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            '7 ngày trước': [moment().subtract(6, 'days'), moment()],
            '30 ngày trước': [moment().subtract(29, 'days'), moment()],
            'Tháng này': [moment().startOf('month'), moment().endOf('month')],
            'Tháng trước': [moment().subtract(29, 'days').startOf('month'), moment().subtract(29, 'days').endOf('month')],
        },
        "alwaysShowCalendars": $(window).innerWidth() > 992,
    }).on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
    }).on('cancel.daterangepicker', function (ev, picker) {
        $(this).val('');
    });
}

itemActive = (_this) => {

    $('.modal-body--item').removeClass('modal-body--item-active');

    if (_this.hasClass('job-disabled')) {
        _this.prop('onclick', null).off('click');
    } else {
        _this.addClass('modal-body--item-active');
        $('#select_service_modal .btn-save').removeAttr('disabled');
    }

    $('.modal-body--item-input').val(_this.attr('data-value'));
}

resetItemActive = (_this) => {
    $('.modal-body--item').removeClass('modal-body--item-active');
    $('.modal-body--item-input').val('');
}

/* ======================= STATUS CHANGE ========================== */
statusChangeCss = (prefixId) => {
    $(`#${prefixId} .js_list-content :checkbox`).prop('checked', false).attr('checked', false);
    $(`#${prefixId} .job__list--title`).removeClass('element-hidden');
    $(`#${prefixId} .js_content-list--action`).addClass('element-hidden');
    $(`#${prefixId} .selectAllCheckbox`).prop('checked', false);
    $(`#${prefixId}_status-change`).val(null).trigger('change');
}

/* ======================= PAGE ========================== */
$('#list-job--present').html(currentPage);

pageCss = (prefixId, value) => {
    $(`#${prefixId}--present`).attr('data-page', value);
    $(`#${prefixId}--present`).html($(`#${prefixId}--present`).attr('data-page'));
    $(`#${prefixId} .next`).removeClass('disabled');
    $(`#${prefixId} .prev`).removeClass('disabled');
}

removeListItem = () => {
    $('#list-job .list__content--item').remove();
}

pageNextCss = (prefixId) => {
    $(`#${prefixId} .next`).addClass('disabled');
}

pagePrevCss = (prefixId) => {
    $(`#${prefixId} .prev`).addClass('disabled');
}

/* ======================= SELECT ALL ========================== */
selectAllCheckedCss = (_this, prefixId) => {
    $(`#${prefixId} .js_list-content :checkbox`).prop('checked', true).attr('checked', true);
    $(`#${prefixId} .list--checkbox`).addClass('checkbox-all');
}

selectAllUnCheckedCss = (_this, prefixId) => {
    $(`#${prefixId} .js_list-content :checkbox`).prop('checked', false).attr('checked', false);
    $(`#${prefixId} .list--checkbox`).removeClass('checkbox-all checkbox-remove');
}

/* =================== SELECT ITEM ======================== */
selectItemCheckCss = (prefixId) => {
    $(`#${prefixId} .list--checkbox`).addClass('checkbox-remove');
    $(`#${prefixId} .content__list--title :checkbox`).prop('checked', true).attr('checked', true);
}

selectItemUnCheckCss = (prefixId) => {
    $(`#${prefixId} .list--checkbox`).removeClass('checkbox-all checkbox-remove');
    $(`#${prefixId} .content__list--title :checkbox`).prop('checked', false).attr('checked', false);
}

selectItemUnCheckAllCss = (prefixId) => {
    $(`#${prefixId} .list--checkbox`).addClass('checkbox-remove').removeClass('checkbox-all');
}

selectItemCheckAllCss = (prefixId) => {
    $(`#${prefixId} .list--checkbox`).addClass('checkbox-all');
}

/* ============== SELECT CHECKBOX =============== */
selectAllCheckBox = (_this, prefixId) => {
    if (_this.is(':checked')) {
        selectAllCheckedCss(_this, prefixId);
        showActionContentListBody(_this, prefixId);

    } else {
        selectAllUnCheckedCss(_this, prefixId);
        showTitleContentListBody(_this, prefixId);
    }
}

selectItemCheckBox = (_this, prefixId) => {
    showActionContentListBody(_this, prefixId);
    selectItemCheckCss(prefixId);

    if ($(`#${prefixId} .list__content--item input:checked`).length === 0) {
        showTitleContentListBody(_this, prefixId);
        selectItemUnCheckCss(prefixId);
    }

    if ($(`#${prefixId} .selectAllCheckbox`).prop("checked")) {
        selectItemUnCheckAllCss(prefixId);
    }

    if ($(`#${prefixId} .js_list-body :checkbox`).length === $(`#${prefixId} .list__content--item input:checked`).length) {
        selectItemCheckAllCss(prefixId);
    }
}

/*=============================== SERVICE DETAIL MODAL ==========================*/
serviceDetail = (_this) => {
    $('#service_detail_modal').modal('show');
}

selectService = (_this) => {
    let id = _this.attr('data-id');
    let btnSave = $('#select_service_modal .btn-save');
    let modal = $('#select_service_modal')

    btnSave.attr('disabled', 'true');

    $('.post_job_settime').val('');

    modal.attr('data-id', id);

    modal.modal('show');
}

/*=================================COUNTDOWN TIME==========================*/
timeCountdown = (prefixId, expired) => {
    let dateRemain = $('.date-time');
    let hourRemain = $('.hour-time');
    let minutesRemain = $('.minutes-time');
    let secondsRemain = $('.seconds-time');

    let remain = (new Date(expired)).getTime() - (new Date()).getTime();

    if (remain > 0) {
        let date = Math.floor(remain / (1000 * 3600 * 24));
        let hour = Math.floor((remain) / (1000 * 3600) % 24);
        let minutes = Math.floor((remain) / (1000 * 60) % 60);
        let second = Math.floor((remain) / 1000 % 60);

        dateRemain.text(String(`${date}`).padStart(2, '0'));
        hourRemain.text(String(`${hour}`).padStart(2, '0'));
        minutesRemain.text(String(`${minutes}`).padStart(2, '0'));
        secondsRemain.text(String(`${second}`).padStart(2, '0'));

        if (prefixId === '#time_countdown-inactive_modal') {
            $(`${prefixId} .btn-save`).removeAttr('disabled');
        }
    } else {
        clearTimeout(intervalCountdown);

        dateRemain.text('00');
        hourRemain.text('00');
        minutesRemain.text('00');
        secondsRemain.text('00');

        if (prefixId === '#time_countdown-inactive_modal') {
            $(`${prefixId} .btn-save`).attr('disabled', 'true');
        }
    }
}