
_handleShowUnlockCVModal = ($button) => {
    let cost = _.toNumber($button.attr('data-cost'));

    $('.search-unlockcv-cost').text(cost);
    $('.search-user-point').text(userPoint);

    if (cost > userPoint) {
        $('.search-user-enough-point').removeClass('element-hidden');
        $('.search-unlockcv-button').prop('disabled', true).is(':disabled', true);
    }

    $('#search_unlockcv_modal').modal('show');
}

_handleHiddenUnlockCVModal = () => {
    $('.search-unlockcv-cost').text('cost');
    $('.search-user-point').text('');
    $('.search-user-enough-point').addClass('element-hidden');
    $('.search-unlockcv-button').prop('disabled', false).is(':disabled', false);
}

_handleSearchYearOfExperienceResetClick = () => {
    let yearOfExperienceView = $('.search_yoe_view');
    let yearOfExperienceFrom = $('#search_yoe_from');
    let yearOfExperienceTo = $('#search_yoe_to');

    yearOfExperienceView
        .attr({
            'data-from': '',
            'data-to': ''
        })
        .text('Năm kinh nghiệm');

    yearOfExperienceFrom.val(yearOfExperienceFrom.attr('data-default'));
    yearOfExperienceTo.val(yearOfExperienceTo.attr('data-default'));
}

_handleSearchYearOfExperienceSubmitClick = () => {
    let yearOfExperienceView = $('.search_yoe_view');
    let yearOfExperienceFrom = _.toNumber($('#search_yoe_from').val());
    let yearOfExperienceTo = _.toNumber($('#search_yoe_to').val());

    if (!!yearOfExperienceFrom && !!yearOfExperienceTo) {

        yearOfExperienceView
            .attr({
                'data-from': yearOfExperienceFrom,
                'data-to': yearOfExperienceTo
            })
            .text(`Kinh nghiệm từ ${yearOfExperienceFrom} - ${yearOfExperienceTo} năm`);
    }
}

_handleViewDetail = (candidateId) => {
    let moduleMobileList = $('.module_mobile_list');
    let moduleMobileDetail = $('.module_mobile_detail');

    moduleMobileList.collapse('hide');
    moduleMobileDetail.collapse('show');
    getCandidateDetail(candidateId);
}

_handleBackToList = () => {
    let moduleMobileList = $('.module_mobile_list');
    let moduleMobileDetail = $('.module_mobile_detail');

    moduleMobileList.collapse('show');
    moduleMobileDetail.collapse('hide');

    window.scrollTo(0, 0);
}
let generateListCandidates = (list) => {

    $('.sc-cv__list').empty('');
    list.forEach((item) => {
        generateCandidateItem(item);
    })
}

let generateCandidateItem = (data) => {
    $('.sc-cv__list').append(`
        <div id="candidate_${data.id}" 
        data-url="${data.url}"
        onclick="_handleViewDetail(${data.id})"
        class="sc-cv__item d-flex position-relative">
            <button class="item-bookmark ${(!!data.is_saved) ? 'bookmarked' : ''}">
                <img class="empty" src="assets/images/icon-sc-search-bookmark.svg" alt="">
                <img class="fill" src="assets/images/icon-sc-search-bookmarked.svg" alt="">
            </button>
            <div class="item-left">
                <div style="background-image: url(${data.img_profile})"
                     class="item-image">
                </div>
            </div>
            <div class="item-right">
                <div class="item-name">
                    <span class="candidate-${data.id}-name"></span>
                    
                    ${(data.is_viewed) ? `<span class="tiny-badge tiny-badge__yellow">Đã xem</span>` : ''}
                </div>
                <p class="item-job candidate-${data.id}-job">
                    
                </p>
                <p class="item-moreinfo">
                    Năm kinh nghiệm:
                    <span class="item-exyear">
                        <span class="candidate-${data.id}-year_of_experience"></span> năm
                    </span>
                </p>
                <p class="item-moreinfo">
                    Địa điểm:
                    <span class="item-location candidate-${data.id}-province">
                        ${data.address.province}
                    </span>
                </p>
            </div>
        </div>
    `);

    $(`.candidate-${data.id}-name`).text(data.name);
    $(`.candidate-${data.id}-job`).text(data.job);
    $(`.candidate-${data.id}-year_of_experience`).text(data.year_of_experience);
    $(`.candidate-${data.id}-province`).text(data.address.province);

}
let getListCandidates = () => {
    Notiflix.Block.Pulse('.sc-body__left');
    setTimeout(() => {
        getFormRequest('./assets/frontend/search_cv/js/json/list_candidates.json')
            .then((responseData) => {

                    let data = responseData.data;
                    let bindingTotalCV = $('.search_total_cv');

                    bindingTotalCV.text(data.total);

                    generateListCandidates(data.candidates);

                    Notiflix.Block.Remove('.sc-body__left');
                }, function (responseErr) {
                    if (responseErr.status === 422) {
                        commonShowNotifyErr();
                    } else {
                        commonShowNotifyErr();
                    }
                }
            );
    }, 1000);
}

let getUserPointFromRequest = () => {

    setTimeout(() => {
        getFormRequest('./assets/frontend/search_cv/js/json/user_point.json')
            .then((responseData) => {
                    userPoint = responseData.data.points;
                }, function (responseErr) {
                    if (responseErr.status === 422) {
                        commonShowNotifyErr();
                    } else {
                        commonShowNotifyErr();
                    }
                }
            );
    }, 1000);
}

let getFullContactByPoints = ($button) => {
    Notiflix.Loading.Pulse();

    getFormRequest('./assets/frontend/search_cv/js/json/full_contact.json')
        .then((responseData) => {
                let data = responseData.data;
                let unlockcvButton = $('.search_unlockcv_button');

                $('.search_unlocked').removeClass('element-hidden');
                $('.search_unlocked_phone').text(data.mobile);
                $('.search_unlocked_email').text(data.email);

                Notiflix.Notify.Success('Hiển thị thông tin thành công!');

                // unlockcvButton.removeClass('viewed')
                //     .prop('disabled', false)
                //     .is(':disabled', false)
                // unlockcvButton.find('span')
                //     .text('Xem thông tin bị ẩn')
                //     .attr('data-status', 'locked');

                unlockcvButton.addClass('viewed')
                    .prop('disabled', true)
                    .is(':disabled', true);

                unlockcvButton.find('span')
                    .text('Đã mở CV')
                    .attr('data-status', 'unlock');

                Notiflix.Loading.Remove();

                $('#search_unlockcv_modal').modal('hide');

            }, function (responseErr) {
                if (responseErr.status === 422) {
                    commonShowNotifyErr();
                } else {
                    commonShowNotifyErr();
                }
            }
        );
}

let getCandidateDetail = (candidateId) => {

    getFormRequest('./assets/frontend/search_cv/js/json/candidate_detail.json')
        .then((responseData) => {

                // let moduleMobileList = $('.module_mobile_list');
                // let moduleMobileDetail = $('.module_mobile_detail');
                //
                // moduleMobileList.addClass('module-mobile__hidden');
                // moduleMobileDetail.removeClass('module-mobile__hidden');

                window.scrollTo(0, 0);

            }, function (responseErr) {
                if (responseErr.status === 422) {
                    commonShowNotifyErr();
                } else {
                    commonShowNotifyErr();
                }
            }
        );
}
let init = () => {
    getListCandidates();
    getUserPointFromRequest();
}

(function ($) {
    let $body = $('body');

    $('.search_location_select2').select2({
        minimumResultsForSearch: -1,
        selectionCssClass: 'searchcv-select2-selection',
        dropdownCssClass: 'searchcv-select2-dropdown diadiem',
        placeholder: $(this).attr('data-placeholder'),
        allowClear: true,
        width: 'auto',
    });

    $('.ngonngu_select2').select2({
        minimumResultsForSearch: -1,
        selectionCssClass: 'filtercv-select2-selection',
        dropdownCssClass: 'filtercv-select2-dropdown ngonngu',
        placeholder: $(this).attr('data-placeholder'),
        allowClear: true,
        width: 'auto',
    });

    $('.trinhdo_select2').select2({
        minimumResultsForSearch: -1,
        selectionCssClass: 'filtercv-select2-selection',
        dropdownCssClass: 'filtercv-select2-dropdown trinhdo',
        placeholder: $(this).attr('data-placeholder'),
        allowClear: true,
        width: 'auto',
    });

    $('.phanloai_select2').select2({
        minimumResultsForSearch: -1,
        selectionCssClass: 'filtercv-select2-selection',
        dropdownCssClass: 'filtercv-select2-dropdown phanloai',
        placeholder: $(this).attr('data-placeholder'),
        allowClear: true,
        width: 'auto',
    });

    $('.sapxep_select2').select2({
        minimumResultsForSearch: -1,
        selectionCssClass: 'filtercv-select2-selection',
        dropdownCssClass: 'filtercv-select2-dropdown sapxep',
        placeholder: $(this).attr('data-placeholder'),
        allowClear: true,
        width: 'auto',
    });

    $('.loaicv_select2').select2({
        minimumResultsForSearch: -1,
        selectionCssClass: 'filtercv-select2-selection',
        dropdownCssClass: 'filtercv-select2-dropdown loaicv',
        width: 'auto',
    });

    $('#search_content').on('focus', () => {
        if (window.innerWidth < 992) {
            $('.sc_search').addClass('mobile-show');
            $body.addClass('no-scroll');
        }
    });

    $('.search-mobile__close').on('click', () => {
        $('.sc_search').removeClass('mobile-show');
        $body.removeClass('no-scroll');
    })

    $('#search_unlockcv_modal').on('hidden.bs.modal', () => {
        _handleHiddenUnlockCVModal();
    });

    $('.search_collapse').collapse({
        toggle: false
    });

    init();
})(jQuery);
