let currentCiIndex = '1';
let companyLogo = null;
let companyListPicture = {};
let paginationTimeOut = null;

let inputRangesTranslation = {
    'Hôm nay': [moment(), moment()],
    'Hôm qua': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
    '7 ngày trước': [moment().subtract(6, 'days'), moment()],
    '14 ngày trước': [moment().subtract(29, 'days'), moment()],
    'Tháng này': [moment().startOf('month'), moment().endOf('month')],
    'Tháng trước': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')],
}
let checkCiMaxItemInList = (prefixClass) => {
    let count = $(`.ci_${prefixClass}_item`).length;

    $(`.ci_${prefixClass}_list`).attr('data-count', count);
    $(`.ci_${prefixClass}_add`).prop('hidden', count >= 3).is(':hidden', count >= 3);
}

let removeCiItem = (prefixClass, $button) => {
    let id = $button.attr('data-id');
    let count = $(`.ci_${prefixClass}_item`).length;

    if (count <= 1) {
        $(`#${prefixClass}_${id}`).find(':input').val(null).trigger('change');
    } else {
        $(`#${prefixClass}_${id}`).remove();
    }

    checkCiMaxItemInList(prefixClass);
}

let ciBorderBack = () => {
    let navItem = $('.ci-nav');
    let itemActive = $('.ci-nav__item.active');
    let itemBorder = $('.ci-nav__border');

    itemBorder.css({
        'top': itemActive.offset().top - navItem.offset().top,
        'height': itemActive.height()
    });
}

let ciNavClicked = (navItem) => {
    let nextPostTabIndex = navItem.attr('data-tab');

    let currentForm = $(`.ci_form_switch[data-tab="${currentCiIndex}"]`);
    let nextForm = $(`.ci_form_switch[data-tab="${nextPostTabIndex}"]`);

    if (currentCiIndex !== nextPostTabIndex) {
        Notiflix.Block.Pulse('.emp-box');

        currentForm.collapse('hide');
        nextForm.collapse('show');

        currentCiIndex = nextPostTabIndex;

        $('.ci-nav__item').removeClass('active');
        navItem.addClass('active');

        ciBorderBack();
    }
}

let ciNavBorderChange = (itemHover) => {
    let navItem = $('.ci-nav');
    let itemActive = $('.ci-nav__item.active');
    let itemBorder = $('.ci-nav__border');

    let itemHoverTop = itemHover.offset().top - navItem.offset().top;
    let itemHoverHeight = itemHover.height();

    let itemActiveTop = itemActive.offset().top - navItem.offset().top;
    let itemActiveHeight = itemActive.height();

    let currentTop = itemActiveTop;
    let currentHeight = itemActiveHeight;

    if (itemHoverTop > itemActiveTop) {
        itemBorder.css({
            'top': currentTop,
            'height': currentHeight + (itemHoverTop - itemActiveTop)
        });
    } else if (itemHoverTop < itemActiveTop) {
        itemBorder.css({
            'top': currentTop - (itemActiveTop - itemHoverTop),
            'height': currentHeight + (itemActiveTop - itemHoverTop)
        });
    } else {
        itemBorder.css({
            'top': currentTop,
            'height': currentHeight
        });
    }
};

let _handleCompanyLogoChange = (_this) => {
    let files = _this[0].files;
    let imgLogoError = $('#img_logo_error');

    imgLogoError.html('');

    let file = files[0];
    if (file.size > 2097152) {
        let html = `<span style="display:block;">${image} ${file.name} ${too_2mb}</span>`;

        imgLogoError.append(html);
    } else if (file.type !== "image/jpeg" && file.type !== "image/png" && file.type !== "image/jpg") {
        let html = `<span style="display:block;">${image} ${file.name} ${gallery_incorrect_format}</span>`;

        imgLogoError.append(html);
    } else {
        let fileReader = new FileReader();

        fileReader.onload = ((fileParam) => {
            return (e) => {
                uploadCompanyLogo(file, e.target.result);
            }
        })(file);
        fileReader.readAsDataURL(file);
    }

    _this.val('');
}

let _handleListPictureChange = (_this) => {
    let files = _this[0].files;
    let imgPicturesError = $('#img_pictures_error');
    let listImageCurrent = $(`.img_pictures_item`);
    let listImageCurrentLength = listImageCurrent.length;

    imgPicturesError.html('');
    companyListPicture = {};

    for (let i = 0; i < files.length; i++) {

        let file = files[i];

        if (listImageCurrentLength + i >= 12) {
            let html = `<span style="display:block;">${gallery_more_than_12_item}</span>`;

            imgPicturesError.append(html);
            break;

        } else if (file.size > 2097152) {
            let html = `<span style="display:block;">${image} ${file.name} ${too_2mb}</span>`;

            imgPicturesError.append(html);

        } else if (file.type !== "image/jpeg" && file.type !== "image/png" && file.type !== "image/jpg") {
            let html = `<span style="display:block;">${image} ${file.name} ${gallery_incorrect_format}</span>`;

            imgPicturesError.append(html);

        } else {
            let fileReader = new FileReader();
            let id = generateKey();

            fileReader.onload = ((fileParam) => {
                return (e) => {
                    generatePicturesItem(id, e.target.result, fileParam.name, i);
                }
            })(file);

            companyListPicture[i] = file;
            fileReader.readAsDataURL(file);
        }
    }

    if (companyListPicture) uploadCompanyPicture(companyListPicture);
}

let _handleTradeHistoryNextClick = () => {
    let current = _.toNumber($('.trade_history_current').attr('data-value'));
    let total = _.toNumber($('.trade_history_total').attr('data-value'));

    if (current < total) {
        paginationChange(current + 1)
    }
}

let _handleTradeHistoryPreviousClick = () => {
    let current = _.toNumber($('.trade_history_current').attr('data-value'));

    if (current > 1) {
        paginationChange(current - 1);
    }
}

let _handleTradeHistoryInputChange = ($input) => {
    let total = _.toNumber($('.trade_history_total').attr('data-value'));
    let nextIndex = _.toNumber($input.val());

    if (!!nextIndex && nextIndex <= total) {
        paginationChange(nextIndex);
    }
}

let _handleTradeHistoryFilter = () => {
    let service = $('#trade_service').val();
    let range = $('#trade_range').val().split(' - ');
    let timeFrom = range[0];
    let timeTo = range[1];

    console.log(service, timeFrom, timeTo);
}
let generateNewCompanyAddress = (animation = 'animate__animated animate__fadeInDown animate__faster') => {
    let companyLocationList = $('.ci_address_list');
    let randomKey = generateKey();

    companyLocationList.append(`
        <div id="address_${randomKey}"
        class="ci_address_item list-item row mx-n12 mb-1 d-flex position-relative ${animation}">
            <div class="col-12 col-md-5 px-12 ci-form__item">
                <div class=" d-flex align-items-center">
                    <label for="address_${randomKey}_province" class="ci-form__label mb-0 ci-form__label--inline ">
                        Tỉnh thành
                    </label>
                    <select id="address_${randomKey}_province" name="address[${randomKey}][province]"
                            class="ci_form_select2 invisible" data-placeholder="">
                        <option></option>
                        <option value="0">Hà Nội</option>
                        <option value="1">Hồ Chí Minh</option>
                        <option value="2">Đà Nẵng</option>
                    </select>
                </div>
                <p id="address_${randomKey}_province_error" class="my-0 error-text text-right no-min-height"></p>
            </div>
            <div class="col-12 col-md-7 px-12 ci-form__item">
                <div class=" d-flex align-items-center">
                    <label for="address_${randomKey}_address"
                            class="ci-form__label mb-0 ci-form__label--inline ">
                        Địa chỉ
                    </label>
                    <input id="address_${randomKey}_address" name="address[${randomKey}][address]"
                            class="ci-form__input w-100" type="text">
                </div>
                <p id="address_${randomKey}_address_error" class="my-0 error-text text-right no-min-height"></p>
            </div>
            <div class="d-flex align-items-center justify-content-center ci-list-remove">
                <button type="button" tabindex="-1" data-id="${randomKey}"
                        onclick="removeCiItem('address', $(this))"
                        class="ci-list-remove__button ci_location_remove">
                    <i class="far fa-times-circle"></i>
                </button>
            </div>
        </div>  
    `);

    ciInitSelect2Single($(`#address_${randomKey}_province`));
    checkCiMaxItemInList('address');
}

let generateNewCompanyWhyYouLove = (animation = 'animate__animated animate__fadeInDown animate__faster') => {
    let companyWhyYouLoveList = $('.ci_why_you_love_list');
    let randomKey = generateKey();

    companyWhyYouLoveList.append(`
        <div id="why_you_love_${randomKey}"
                class="ci_why_you_love_item list-item row mx-n12 mb-1 d-flex position-relative ${animation}">
            <label class="col-12 px-12 ci-form__item">
                <input id="why_you_love_${randomKey}" name="why_you_love[${randomKey}]"
                        class="ci-form__input w-100" type="text" required>
                <p id="why_you_love_${randomKey}_error" class="error-text no-min-height"></p>
            </label>
            <div class="d-flex align-items-center justify-content-center ci-list-remove">
                <button type="button" tabindex="-1" data-id="${randomKey}"
                        onclick="removeCiItem('why_you_love', $(this))"
                        class="ci-list-remove__button ci_why_love_remove">
                    <i class="far fa-times-circle"></i>
                </button>
            </div>
        </div>
    `);

    checkCiMaxItemInList('why_you_love');
}

let generatePicturesItem = (id, src, name, key) => {
    $('.company_images_list').append(`
        <div id="img_pictures_${id}" data-key="${key}"
             class="col-6 col-md-6 col-lg-4 col-xl-12 px-2 img_pictures_item">
            <div class="company-images__item d-flex align-items-center my-md-1">
                <div style="background-image: url(${src})"
                     class="company-images__background"></div>
                <p class="company-images__title mr-auto">
                    ${name}
                </p>
                <button type="button" class="company-images__remove"
                        onclick="deletePictureItem($(this), '${id}')"
                        data-url=""
                        tabindex="-1">
                    <i class="fas fa-trash"></i>
                </button>
            </div>
        </div>
    `)
}

let generateListTrade = (data) => {
    let tradeHistoryList = $('.trade_history_list');
    tradeHistoryList.html('');

    data.forEach((item) => {
        generateTradeItem(item);
    });
}

let generateTradeItem = (data) => {
    let tradeHistoryList = $('.trade_history_list');

    tradeHistoryList.append(`
        <div class="trow row mt-3 mt-md-0">
            <div class="tcol col-12 col-md-5 px-0 px-md-3">
                <div class="trade-history__name">
                    ${data.type}
                </div>
            </div>
            <div class="tcol col-12 col-md-4 px-0 px-md-3">
                <div class="trade-history__time">
                    <span class="d-inline-block d-md-none mr-2 trade-history__bold">
                        Ngày thực hiện
                    </span>
                    ${data.time}
                </div>
            </div>
            <div class="tcol col-md-3 px-0 px-md-3 trade-history__absolute">
                <span class="point-less">
                    ${data.point}
                    <span class="d-inline-block d-md-none">
                        points
                    </span>
                </span>
            </div>
        </div>
    `)
}
(function ($) {
    $('#user_form').on('submit', (e) => {
        e.preventDefault();

        let $form = $(e.target);

        let checkValidate = $form.validationObject({
            rules: {
                'user_name': 'required|maxlength:255',
                'user_phone': 'required|number',
            },
            customTranslateName: {
                'user_name': 'Tên',
                'user_phone': 'Số điện thoại'
            }
        });

        if (checkValidate) {
            Notiflix.Notify.Success('Success');
        } else {

        }

    });

    $('#password_form').on('submit', (e) => {
        e.preventDefault();

        let $form = $(e.target);

        let checkValidate = $form.validationObject({
            rules: {
                'old_password': 'required|minlength:20',
                'new_password': 'required|minlength:20',
                'renew_password': 'required|minlength:20|equal:new_password',
            },
            customTranslateName: {
                'old_password': 'Mật khẩu hiện tại',
                'new_password': 'Mật khẩu mới',
                'renew_password': 'Xác nhận mật khẩu',
            }
        });

        if (checkValidate) {
            Notiflix.Notify.Success('Success');
        } else {

        }

    });

    $('#company_form').on('submit', (e) => {
        e.preventDefault();

        let $form = $(e.target);

        let checkValidate = $form.validationObject({
            rules: {
                'name': 'required|maxlength:255',
                'address[%][province]': `required`,
                'address[%][address]': `required|maxlength:255`,
                'introduction': 'required|maxlength:2000',
            },
        });

        if (checkValidate) {
            Notiflix.Notify.Success('Success');
        } else {

        }

    });
})(jQuery);

let paginationChange = (pageIndex = 1) => {
    getFormRequest('./assets/frontend/company_information/js/json/trade_pagination.json')
        .then((responseData) => {
                let current = $('.trade_history_current');
                let total = $('.trade_history_total');
                let previous = $('.trade_history_previous');
                let next = $('.trade_history_next');
                let input = $('.trade_history_input');

                total.text(responseData.total).attr('data-value', responseData.total);
                current.text(responseData.current).attr('data-value', responseData.current);

                next
                    .prop('disabled', responseData.current === responseData.total)
                    .is(':disabled', responseData.current === responseData.total);

                previous
                    .prop('disabled', responseData.current === 1)
                    .is(':disabled', responseData.current === 1);

                input.val('');

            }, (responseErr) => {
                commonShowNotifyErr();
            }
        );
}

let uploadCompanyLogo = (file, src) => {
    Notiflix.Block.Pulse('.img_logo_view');

    setTimeout(() => {
        let imgLogoView = $('.img_logo_view');

        imgLogoView.css('background-image', `url(${src})`);

        Notiflix.Block.Remove('.img_logo_view');
    }, 1000);
}

let uploadCompanyPicture = (data) => {
    $('#img_pictures').prop('disabled', true).is(':disabled', true);
    $('.ci-form__company-images').addClass('disabled');

    console.log(data)

    setTimeout(() => {
        $('#img_pictures').prop('disabled', false).is(':disabled', false);
        $('.ci-form__company-images').removeClass('disabled');
    }, 1000);
}

let deletePictureItem = ($button, id) => {
    let $item = $(`#img_pictures_${id}`);

    Notiflix.Confirm.Show(
        'Xác nhận',
        'Bạn chắc chắn muốn xóa ảnh ?',
        'Đồng ý',
        'Huỷ',
        () => {
            delete companyListPicture[$item.attr(id)];

            $item.remove();
            Notiflix.Notify.Success('Success');
        }
    );
}

let ciInitSelect2Single = ($select) => {
    $($select).select2({
        minimumResultsForSearch: -1,
        selectionCssClass: 'ci_form_select2-selection',
        dropdownCssClass: 'ci_form_select2-dropdown',
        allowClear: (!!$select.attr('data-allowclear')),
        width: '100%',
        placeholder: $select.attr('data-placeholder'),
    });
}

let openTabIndex = () => {

    const params = new URLSearchParams(window.location.search);
    let tabIndex = params.get('tabindex');
    if (!!tabIndex) $(`.ci-nav__item[data-tab=${tabIndex}]`).trigger('click');
}

ciPageInit = () => {

    if (!$('.ci_address_item').length) {
        generateNewCompanyAddress('');
    } else {
        checkCiMaxItemInList('address');
    }

    if (!$('.ci_why_you_love_item').length) {
        generateNewCompanyWhyYouLove('');
    } else {
        checkCiMaxItemInList('why_you_love');
    }

    openTabIndex();
}

(function ($) {
    let ciNavItem = $('.ci-nav__item');
    let sizeSelect2 = $('.size_select2');

    Notiflix.Loading.Init({
        backgroundColor: "rgba(255,255,255,0.6)",
        svgColor: "#06202D",
    });

    ciNavItem.on('mouseover', (e) => {
        ciNavBorderChange($(e.target));
    });

    ciNavItem.on('mouseout', (e) => {
        ciBorderBack($(e.target));
    });

    $('.ci_form_select2').each((key, value) => {
        let $select = $(value);
        ciInitSelect2Single($select);
    });

    $('.dichvu_select2').select2({
        minimumResultsForSearch: -1,
        selectionCssClass: 'filtercv-select2-selection',
        dropdownCssClass: 'filtercv-select2-dropdown dichvu',
        allowClear: true,
        placeholder: 'Dịch vụ',
        width: 'auto',
    });

    sizeSelect2.select2({
        selectionCssClass: 'ci_form_select2-selection',
        dropdownCssClass: 'ci_form_select2-dropdown',
        maximumInputLength: 20,
        width: '100%',
        tags: true,
        placeholder: sizeSelect2.attr('data-placeholder'),
        ajax: {
            url: sizeSelect2.attr('data-url'),
            data: (params) => {
                return {
                    keyword: params.term
                }
            },
            processResults: function (response) {
                let output = {};
                let projectSelected = sizeSelect2.val();

                output.results = response.data.map((item) => {
                    let selected = _.includes(projectSelected, item.name)

                    return {
                        "id": item.name,
                        "text": item.name,
                        "selected": selected
                    }
                });

                return !!output ? output : null;
            }
        }
    })

    $('#trade_range').daterangepicker({
        locale: {
            format: 'DD/MM/YYYY'
        },
        showDropdowns: true,
        ranges: inputRangesTranslation,
        alwaysShowCalendars: true,
        opens: 'left'
    });

    $('.ci_form_tagsinput')
        .on('focus', (e) => {
            $(e.target).prev().find('input').focus();
        })
        .tagsinput({
            tagClass: 'badge ci-form__tags',
            onTagExists: function (item, $tag) {
                $tag.hide().fadeIn();
            },
            focusClass: 'tagsinput-focus',
        });

    $('.ci_collapse').collapse({
        toggle: false
    });

    $('.ci_form_switch').on('hidden.bs.collapse', () => {
        Notiflix.Block.Remove('.emp-box');
    });

    $('#img_logo').on('change', (e) => {
        _handleCompanyLogoChange($(e.target));
    });

    $('#img_pictures').on('change', (e) => {
        _handleListPictureChange($(e.target));
    });

    $('.trade_history_previous').on('click', (e) => {
        _handleTradeHistoryPreviousClick($(e.target));
    });

    $('.trade_history_next').on('click', (e) => {
        _handleTradeHistoryNextClick($(e.target));
    });

    $('.trade_history_input').on('input change', (e) => {
        clearTimeout(paginationTimeOut);
        if (e.which === 13) {
            _handleTradeHistoryInputChange($(e.target));
        } else {
            paginationTimeOut = setTimeout(() => {
                _handleTradeHistoryInputChange($(e.target));
            }, 1000);
        }
    });

    $('#trade_service').on('change', () => {
        _handleTradeHistoryFilter();
    });

    $('#trade_range').on('input change', () => {
        _handleTradeHistoryFilter();
    })

    ciPageInit();
})(jQuery);