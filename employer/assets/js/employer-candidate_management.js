let contentSearchTimeout = null;
let sendRequestLazy = false;
let lastPageCandidate = 6;

const  listCandidateStatus = {
    "offer_accepted": "offer_accepted",
    "pending_review": "pending_review",
    "pass_interview": "pass_interview",
    "fail_interview": "fail_interview",
    "offer_refused": "offer_refused",
    "cv_qualified": "cv_qualified",
    "cancel_interview": "cancel_interview",
    "cv_not_qualified": "cv_not_qualified",
    "went_to_work": "went_to_work",
};

$(document).ready(() => {
    /*==================SELECT STATUS===================*/
    $("#list-candidate_status").on("select2:select",  (e) => {
        filterData($(e.target));
    });

    $("#list-candidate__search-job").on("select2:select", (e) => {
        filterData($(e.target));
    })

    $("#user-upload").on("select2:select", (e) => {
        filterData($(e.target));
    })

    calenderDateRager();

    $('#listCandidate-calender-date-ranger').on('apply.daterangepicker', (e) => {
        filterData($(e.target));
    });

    $('#search_content').on('input change', (e) => {

        clearTimeout(contentSearchTimeout);

        contentSearchTimeout = setTimeout(() => {
            filterData($(e.target));
        }, 1000);
    })

    /* ================================ FORM SUBMIT ================================= */
    $('#edit_status_candidate_form').submit((e) => {
        e.preventDefault();

        let modal = $('#edit_status_candidate_modal');
        let id = modal.attr('data-id');
        let value = $('#edit_status').val()

        $(`#list-candidate_status_${id}`).html(`<span class="candidate__status--${value}">${listCandidateStatus[value]}</span>`);

        modal.modal('hide');
    });

    $('#go-to').on('keyup', (e) => {
        if (e.keyCode === 13) {
            e.preventDefault();
            indexPagination($(e.target));
        }
    })
})

listActionOpen = (_this, prefixClass, prefixId) => {
    let id = _this.attr('data-id');

    $(`#${prefixClass}_${id} .dropdown`).on('hidden.bs.dropdown', () => {
        $(`#${prefixClass}_${id} .list-action--button`).removeClass('button-active');
    })

    $(`#${prefixClass}_${id} .dropdown`).on('show.bs.dropdown', () => {
        $(`#${prefixClass}_${id} .list-action--button`).addClass('button-active');
    })

    $('.status-action').remove();
}

/*==================================PAGIGNATION======================*/

nextPage = (_this, prefixId) => {
    let value = parseInt($(`#${prefixId}--present`).attr('data-page'));

    if (value !== lastPageJob) {
        value++;

        $('html,body').animate({
                scrollTop: $('.calender').offset().top - 20},
            500);

        pageCss(prefixId, value);
        filterListData(_this, prefixId);
    }

    if (value === lastPageJob) {
        pageNextCss(prefixId);
    }
}

prevPage = (_this, prefixId) => {
    let value = parseInt($(`#${prefixId}--present`).attr('data-page'));
    let min = 1;

    if (value !== min) {
        value--;

        $('html,body').animate({
                scrollTop: $('.calender').offset().top - 20},
            500);

        pageCss(prefixId, value);
        filterListData(_this, prefixId);
    }
    if (value === min) {
        pagePrevCss(prefixId);
    }
}

indexPagination = (goToInput) => {
    let value = _.toNumber(goToInput.val());
    let presentPage = $('#list-candidate--present');
    let totalPage = _.toNumber($('#total-page').attr('data-total'));

    $('html,body').animate({
            scrollTop: $('.calender').offset().top - 20},
        500);

    value = value < 1 ? 1 : value;
    value = value > lastPageCandidate ? lastPageCandidate : value;

    presentPage.html(value);
    presentPage.attr('data-page', value);
    pageCss('list-candidate', value);

    filterListData();
    goToInput.val('');

    if (value === 1) {
        pagePrevCss('list-candidate');
    }

    if (value === totalPage) {
        pageNextCss('list-candidate');
    }
}

/*==============================SCROLL APPEND DATA================================*/
listBodyScroll = (_this, prefixId) => {
    let contentListBody = _this.get(0);

    Notiflix.Block.Dots('.lazy-loading');
    if (contentListBody.scrollTop + contentListBody.clientHeight === (contentListBody.scrollHeight)) {
        setTimeout(() => {
            if (!sendRequestLazy) {
                sendRequestLazy = true;
                appendListCandidate();
                sendRequestLazy = false;
            }
        }, 1000);
    }
}

/* ============== SORT ITEM =============== */
sortItem = (_this) => {
    let headerIndex = _this.attr('data-sort');
    if (headerIndex === '2') {
        headerIndex = 0;
    } else {
        headerIndex++;
    }
    _this.attr('data-sort', headerIndex);

    $('.text-list-title').attr('data-sort', '0');

    if (headerIndex === 1) {
        resetSortIcon();
        sortItemArrowUp(_this);
        sortAppend();
    } else if (headerIndex === 2) {
        resetSortIcon();
        sortItemArrowDown(_this);
        sortAppend();
    } else {
        resetSortIcon();
        sortAppend();
    }
}

/*===============BINDING DATA==============*/
filterData = (e) => {
    let searchName = $('#search_content').val();
    let userUpload = $('#user-upload').val();
    let statusVal = $('#list-candidate_status').val();
    let startTime = $('#listCandidate-calender-date-ranger').val().split(' - ')[0];
    let endTime = $('#listCandidate-calender-date-ranger').val().split(' - ')[1];
    let searchJob = $('.form-control').val();
    let url = `itnavi.com.vn/job/search?keyword=${searchName}&searchJob=${searchJob}&searchUserUpload=${userUpload}&status=${statusVal}&startTime=${startTime}&endTime=${endTime}`;

    alert(url)
}

filterListData = (_this) => {
    Notiflix.Loading.Pulse();

    setTimeout(() => {
        removeListItem();
        appendListCandidate();
        Notiflix.Loading.Remove();
    }, 1000);
}
generateCandidateItem = (key) => {
    let html = `<div class="list__content--item d-flex align-items-center" id="list-candidate_${key}">
                            <div class="numerical-order text-left d-flex align-items-center justify-content-center app-desktop">
                                <span class="text-list-title"></span>
                            </div>
                            <div class="d-flex align-items-center flex-column flex-lg-row w-100">
                                <div class="name-candidate d-flex align-items-center">
                                    <div class="name-avt" style="background-image: url(assets/images/avt.png)"></div>
                                    <div class="name-info">
                                        <span class="text-default">Nguyễn Lê Nam</span>
                                        <span class="job-title">UI & UX Designer</span>
                                    </div>
                                </div>
                                <div class="d-flex align-items-center basic-info">
                                    <div class="vacancies basic-info--content"
                                        title="Sr Android Dev (Java, Kotlin) Up to1500$">
                                        <span class="basic-info__title app-mobile">Vị trí ứng
                                            tuyển</span>
                                        <span class="basic-info__job">Sr Android Dev (Java, Kotlin) Up
                                            to1500$</span>
                                    </div>
                                    <div class="application-date basic-info--content">
                                        <span class="basic-info__title app-mobile">Ngày ứng
                                            tuyển</span>
                                        <span class="basic-info__job">10/09/2020</span>
                                    </div>
                                </div>
                                <div class="status" id="list-candidate_status_${key}">
                                    <span class="candidate__status--offer_accepted">
                                        Offer Accepted
                                    </span>
                                </div>
                                <div class="list-action" id="list-action_${key}">
                                    <div class="dropdown show">
                                        <button class="list-action--button btn" data-id="${key}"
                                                data-toggle="dropdown"
                                                onclick="listActionOpen($(this), 'list-action', 'list-candidate')"
                                                role="button">
                                            Lựa chọn
                                            <svg fill="none" height="6" viewBox="0 0 9 6" width="9"
                                                 xmlns="http://www.w3.org/2000/svg">
                                                <path
                                                    d="M8.69812 0H0.301875C0.0500391 0 -0.0905801 0.294357 0.0653794 0.495312L4.2635 5.88431C4.38367 6.03856 4.61505 6.03856 4.7365 5.88431L8.93462 0.495312C9.09058 0.294357 8.94996 0 8.69812 0Z"
                                                    fill="#1C4355"/>
                                            </svg>
                                        </button>
                                        <div class="dropdown-menu list__action--dropdown">
                                            <button class="dropdown-item"
                                                    data-id="${key}" data-status="offer_accepted"
                                                    onclick="modalCandidateOpen($(this))">
                                                Đổi trạng thái
                                            </button>
                                            <button class="dropdown-item" value="edit" onclick="noteModal($(this))">Ghi chú</button>
                                            <button class="dropdown-item" onclick="downloadCv($(this))"
                                                    readonly>Tải xuống CV
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>`;

    $('.height__list--body').append(html);
}

/*===============GENERATE LIST JOB==============*/
appendListCandidate = (_this) => {
    for (let i = 0; i < 15; i++) {
        let key = generateKey();

        generateCandidateItem(key);
    }
}

sortAppend = () => {
    let arraySort = [''];

    $('.list__content--item').remove();

    arraySort.forEach(() => {
        appendListCandidate();
    })

}

$(document).ready(() => {

    $('#edit_status_candidate_modal').on('hidden.bs.modal', () => {
        $('#edit_statusCandidate_modal').val(null).trigger('change');
    })

    $('#candidateNote_modal').on('hidden.bs.modal', () => {
        $('.modal-body--note').val('');
    })

    /*==================PAGINATION=================*/
    $('.page-link').on('click', (e) => {
        let _this = $(e.target);
        let pageLink = $('.page-link')

        if (pageLink.hasClass('page-link-active')) {
            pageLink.removeClass('page-link-active');
        }
        if (!pageLink.hasClass('page-link-active')) {
            _this.addClass('page-link-active');
        }
    })

    /*===============STATUS=======================*/
    $('#list-candidate_status').select2({
        placeholder: 'Trạng thái',
        minimumResultsForSearch: -1,
        dropdownCssClass: 'candidate-select2-dropdown',
        selectionCssClass: 'candidate-select2-selection',
        width: '100%'
    });

    $('#edit_status').select2({
        placeholder: 'Thay đổi trạng thái',
        minimumResultsForSearch: -1,
        dropdownCssClass: 'candidate-select2-dropdown',
        selectionCssClass: 'candidate-select2-selection',
        width: '100%',
    });

    $(".form-control").select2({
        placeholder: 'Vị trí tuyển dụng',
        tags: true,
        width: '100%',
        selectionCssClass: 'candidate-select2-selection',
    });

    $('#user-upload').select2({
        placeholder: 'Người đăng',
        minimumResultsForSearch: -1,
        dropdownCssClass: 'candidate-select2-dropdown user-upload-select2-dropdown',
        selectionCssClass: 'candidate-select2-selection',
        width: '132px',
    })

    /*==================SHADOW LIST JOB================*/
    $('.js_list-body').scroll(function () {
        if ($(this).scrollTop()) {
            $('.job__list--header').addClass('header-shadow');
        } else {
            $('.job__list--header').removeClass('header-shadow');
        }
    })

    /*===================TOTAL LIST======================*/
    $('.total-list span').html($('.list__content--item').length);

    Notiflix.Block.Init({
        svgColor: "#32c682",
    });
})

/* Generate key */
generateKey = () => {
    return Math.random().toString(20).substr(2, 10);
}

/*=======================SORT ITEM ARROW=======================*/
sortItemArrowUp = (_this) => {
    $('.arrow-up', _this).addClass('sort');
    $('.arrow-down', _this).removeClass('sort');
    _this.attr('data-sort', 1);
}

sortItemArrowDown = (_this) => {
    $('.arrow-down', _this).addClass('sort');
    $('.arrow-up', _this).removeClass('sort');
    _this.attr('data-sort', 2);
}

/*=======================RESET SORT ICON======================*/

resetSortIcon = () => {
    $('.arrow-down').removeClass('sort')
    $('.arrow-up').removeClass('sort');
}

/*==================CALENDER===================*/
calenderDateRager = () => {
    $('#listCandidate-calender-date-ranger').daterangepicker({
        opens: 'left',
        autoUpdateInput: false,
        locale: {
            cancelLabel: 'Clear',
        },

        ranges: {
            'Hôm nay': [moment(), moment()],
            'Hôm qua': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
            '7 ngày trước': [moment().subtract(6, 'days'), moment()],
            '30 ngày trước': [moment().subtract(29, 'days'), moment()],
            'Tháng này': [moment().startOf('month'), moment().endOf('month')],
            'Tháng trước': [moment().subtract(29, 'days').startOf('month'), moment().subtract(29, 'days').endOf('month')],
        },
        "alwaysShowCalendars": $(window).innerWidth() > 992,
    }).on('apply.daterangepicker', function (ev, picker) {
        $(this).val(picker.startDate.format('DD/MM/YYYY') + ' - ' + picker.endDate.format('DD/MM/YYYY'));
    }).on('cancel.daterangepicker', function (ev, picker) {
        $(this).val('');
    });
}

/* ======================= STATUS CHANGE ========================== */
statusChangeCss = (prefixId) => {
    $(`#${prefixId} .js_list-content :checkbox`).prop('checked', false).attr('checked', false);
    $(`#${prefixId} .candidate__list--title`).removeClass('element-hidden');
    $(`#${prefixId} .js_content-list--action`).addClass('element-hidden');
    $(`#${prefixId} .selectAllCheckbox`).prop('checked', false);
    $(`#${prefixId}_status-change`).val(null).trigger('change');
}

/* ======================= PAGE ========================== */
$('#list-candidate--present').html(1);


pageCss = (prefixId, value) => {
    $(`#${prefixId}--present`).attr('data-page', value);
    $(`#${prefixId}--present`).html($(`#${prefixId}--present`).attr('data-page'));
    $(`#${prefixId} .next`).removeClass('disabled');
    $(`#${prefixId} .prev`).removeClass('disabled');
}

pageNextCss = (prefixId) => {
    $(`#${prefixId} .next`).addClass('disabled');
}

pagePrevCss = (prefixId) => {
    $(`#${prefixId} .prev`).addClass('disabled');
}

/* ============================= OPEN POPUP ===================================== */
modalCandidateOpen = (_this) => {
    let id = _this.attr('data-id');
    let status = _this.attr('data-status');
    let modal = $('#edit_status_candidate_modal');

    modal.attr('data-id', `${id}`);
    $('#edit_status').val(status).trigger('change');

    modal.modal('show');
};

noteModal = (_this, id) => {
    let modal = $('#candidate_note_modal');

    modal.attr('data-id', id);
    modal.modal('show');
}
