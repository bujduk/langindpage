const  dbListJobStatus = {
    "active": "Hoạt động",
    "pending-review": "Chờ duyệt",
    "approved": "Đã duyệt",
    "draft": "Nháp",
    "inactive": "Không hoạt động",
    "reject": "Từ chối",
    "expired": "Hết hạn",
};

const  dbListCandidateStatus = {
    "passed": "Trúng tuyển",
    "interview-joined": "Đã phỏng vấn",
    "fail": "Không trúng tuyển",
    "pending-review": "Chờ duyệt",
    "interview-invited": "Đã hẹn lịch",
    "interview-refused": "Từ chối phỏng vấn",
    "rejected": "Không duyệt",
};

$(document).ready(() => {

    /*====================JS MOBILE, DESKTOP=========================*/
    if ($(window).innerWidth() < 992) {
        $('.height__list--body').attr('data-type', 'mobile');

        console.log('mobile')
    } else {
        $('.height__list--body').attr('data-type', 'desktop');

        console.log('desktop')
    }
})

/*================================== CHANGE STATUS ================================*/
dbStatusChange = (_this, prefixId) => {
    Notiflix.Confirm.Show(
        'Xác nhận',
        'Bạn chắc chắn muốn thay đổi trạng thái ?',
        'Đồng ý',
        'Huỷ',
        () => {
            let inputChecked = $(`#${prefixId} .list__content--item input:checked`);

            $(inputChecked).each(function () {
                let id = $(this).attr("data-id");

                switch (prefixId) {
                    case 'db_list-candidate' :
                        $(`#${prefixId}_status_${id}`).html(`<span class="candidate__status--${$(_this).val()}">${dbListCandidateStatus[$(_this).val()]}</span>`)
                        break;

                    case 'db_list-job' :
                        $(`#${prefixId}_status_${id}`).html(`<span class="job__status--${$(_this).val()}">${dbListJobStatus[$(_this).val()]}</span>`)
                        break;
                }
            })

            statusChangeCss(prefixId);
        },

        () => {
            $(`#${prefixId}_status-change`).val(null).trigger('change');
        }
    );
}

/* ============== SELECT CHECKBOX =============== */
dbSelectAllCheckBox = (_this, prefixId) => {
    if (_this.is(':checked')) {
        selectAllCheckedCss(_this, prefixId);
        dbShowActionContentListBody(_this, prefixId);

    } else {
        dbShowTitleContentListBody(_this, prefixId);
        selectAllUnCheckedCss(_this, prefixId);
    }
}

dbSelectItemCheckBox = (_this, prefixId) => {
    dbShowActionContentListBody(_this, prefixId);

    selectItemCheckCss(prefixId);

    if ($(`#${prefixId} .list__content--item input:checked`).length === 0) {
        dbShowTitleContentListBody(_this, prefixId);
        selectItemUnCheckCss(prefixId);
    }

    if ($(`#${prefixId} .selectAllCheckbox`).prop("checked")) {
        selectItemUnCheckAllCss(prefixId);
    }

    if ($(`#${prefixId} .js_list-body :checkbox`).length === $(`#${prefixId} .list__content--item input:checked`).length) {
        selectItemCheckAllCss(prefixId);
    }
}

/*==================================PAGIGNATION======================*/

nextPage = (_this, prefixId) => {
    let value = parseInt($(`#${prefixId}--present`).attr('data-page'));

    if (value !== 6) {
        value++;

        pageCss(prefixId, value);

        switch (prefixId) {
            case 'db_list-candidate':
                dbAppendListCandidate();
                break;

            case 'db_list-job':
                dbAppendListJob();
                break;
        }
    }

    if (value === 6) {
        pageNextCss(prefixId);
    }
}

prevPage = (_this, prefixId) => {
    let value = parseInt($(`#${prefixId}--present`).attr('data-page'));

    if (value !== 1) {
        value--;

        pageCss(prefixId, value);

        switch (prefixId) {
            case 'db_list-candidate':
                dbAppendListCandidate();
                break;

            case 'db_list-job':
                dbAppendListJob();
                break;
        }
    }

    if (value === 1) {
        pagePrevCss(prefixId);
    }
}

/*================== Delete Item==================== */
dbDeleteItem = (_this, prefixId) => {
    Notiflix.Confirm.Show(
        'Xác nhận',
        'Bạn chắc chắn muốn xoá?',
        'Đồng ý',
        'Huỷ',
        () => {
            let deleteChecked = $(`#${prefixId} .js_list-body input:checked`);

            deleteChecked.each((key, value) => {
                let item = $(value);
                let id = item.attr('data-id');

                $(`#${prefixId}_${id}`).remove();
                dbShowTitleContentListBody(_this, prefixId);
            });
        },

        () => {
            let id = _this.attr('data-id');

            $(`#${prefixId}_${id}`).remove();
        }
    );
}

dbDeleteItemMobile = (_this, prefixId) => {
    Notiflix.Confirm.Show(
        'Xác nhận',
        'Bạn chắc chắn muốn xoá?',
        'Đồng ý',
        'Huỷ',
        () => {
            let id = _this.attr('data-id');

            $(`#${prefixId}_${id}`).remove();
        }
    );
}

/*==============================APPEND LIST CANDIDATE================================*/
dbAppendListCandidate = (_this) => {
    let htmlListCandidate = '';

    for (let i = 0; i < 15; i++) {
        let key = generateKey();

        htmlListCandidate += `<div class="row ml-md-0 mr-md-0 list__content--item position-relative animate__animated animate__fadeIn animate__fast "
             id="db_list-candidate_${key}">
            <div class="col-12 pl-0 pr-0 d-flex card-mobile">
                <label class="checkbox-label d-flex justify-content-center align-items-center app-desktop">
                    <input type="checkbox" class="input-hidden"
                           id="db_list-candidate-checkbox_${key}"
                           data-id="${key}"
                           onchange="dbSelectItemCheckBox($(this), 'db_list-candidate')">
                    <span class="list--checkbox position-relative d-flex justify-content-center align-items-center">
                        <i class="fas fa-check"></i>
                    </span>
                </label>
                <div class="body__list--item-text d-flex align-items-center">
                    <div class="title__card--mobile">
                        <span class="text-list-default" title="Nguyễn Lê Nam">Nguyễn Lê Nam Nguyễn Lê Nam Nguyễn Lê Nam Nguyễn Lê Nam Nguyễn Lê Nam Nguyễn Lê Nam Nguyễn Lê Nam Nguyễn Lê Nam Nguyễn Lê Nam</span>
                    </div>
                    <div class="basic-info d-flex align-items-center">
                        <div class="basic-info--content">
                            <span class="app-mobile title--mobile app-tablet">Công việc ứng tuyển</span>
                            <span class="text-list-default job-title"
                                  title="Junior Software Developer (C#, C++) ">Junior Software Developer (C#, C++) </span>
                        </div>
                        <div class="basic-info--content">
                            <span class="app-mobile title--mobile">Ngày ứng tuyển</span>
                            <span class="text-list-default date-apply">10/09/2020</span>
                        </div>
                        <div id="db_list-candidate_status_${key}" class="status">
                            <span class="candidate__status--passed">Trúng tuyển</span>
                        </div>
                    </div>
                    <div class="action__mobile app-mobile app-tablet">
                        <button>
                            <svg width="17" height="12" viewBox="0 0 23 18"
                                    fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M14.7662 13.7488H12.6053V7.2157C12.6053 7.09707 12.5003 7 12.372 7H10.6222C10.4939 7 10.3889 7.09707 10.3889 7.2157V13.7488H8.23377C8.03838 13.7488 7.93048 13.9564 8.05005 14.0966L11.3163 17.9172C11.3381 17.943 11.366 17.9639 11.3978 17.9782C11.4296 17.9925 11.4646 18 11.5 18C11.5354 18 11.5704 17.9925 11.6022 17.9782C11.634 17.9639 11.6619 17.943 11.6837 17.9172L14.95 14.0966C15.0695 13.9564 14.9616 13.7488 14.7662 13.7488Z"
                                    fill="#595959"/>
                                <path d="M19.1855 5.1675C18.0098 2.1475 15.0142 0 11.5051 0C7.99609 0 5.00045 2.145 3.82478 5.165C1.62489 5.7275 0 7.68 0 10C0 12.7625 2.29743 15 5.13136 15H6.16071C6.27366 15 6.36607 14.91 6.36607 14.8V13.3C6.36607 13.19 6.27366 13.1 6.16071 13.1H5.13136C4.26629 13.1 3.45257 12.765 2.84676 12.1575C2.24353 11.5525 1.92266 10.7375 1.95089 9.8925C1.974 9.2325 2.20502 8.6125 2.62344 8.09C3.05212 7.5575 3.65279 7.17 4.3202 6.9975L5.29308 6.75L5.64989 5.835C5.87065 5.265 6.17868 4.7325 6.56629 4.25C6.94896 3.77177 7.40224 3.35137 7.91138 3.0025C8.96641 2.28 10.2088 1.8975 11.5051 1.8975C12.8015 1.8975 14.0439 2.28 15.0989 3.0025C15.6097 3.3525 16.0615 3.7725 16.444 4.25C16.8316 4.7325 17.1396 5.2675 17.3604 5.835L17.7146 6.7475L18.6849 6.9975C20.0762 7.3625 21.0491 8.595 21.0491 10C21.0491 10.8275 20.718 11.6075 20.1173 12.1925C19.8227 12.4811 19.4723 12.7099 19.0863 12.8656C18.7003 13.0214 18.2864 13.1011 17.8686 13.1H16.8393C16.7263 13.1 16.6339 13.19 16.6339 13.3V14.8C16.6339 14.91 16.7263 15 16.8393 15H17.8686C20.7026 15 23 12.7625 23 10C23 7.6825 21.3802 5.7325 19.1855 5.1675Z"
                                    fill="#595959"/>
                            </svg>
                        </button>
                        <button onclick="modalCandidateOpen($(this))" data-id="${key}" data-status="passed">
                            <svg width="9" height="16" viewBox="0 0 11 18"
                                     fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M10.8215 3.17081L7.04015 0.11754C6.70916 -0.149747 6.1875 0.0672368 6.1875 0.482442V2.09065C2.73649 2.12759 0 2.77432 0 5.83243C0 7.06674 0.850373 8.28953 1.79036 8.92883C2.08368 9.12834 2.50173 8.87794 2.39357 8.55455C1.41939 5.64134 2.85564 4.86795 6.1875 4.82313V6.58927C6.1875 7.00512 6.70957 7.22114 7.04015 6.95418L10.8215 3.90061C11.0593 3.70852 11.0597 3.36316 10.8215 3.17081Z"
                                      fill="#595959"/>
                                <path d="M0.197647 13.9253L3.97849 16.9006C4.30943 17.1611 4.82904 16.9476 4.82774 16.5417L4.82273 14.9694C8.26449 14.9243 10.9917 14.2849 10.9822 11.2951C10.9783 10.0883 10.1264 8.89502 9.18692 8.27243C8.89375 8.07813 8.47759 8.32402 8.58647 8.63992C9.56715 11.4856 8.13711 12.2454 4.81421 12.2979L4.80871 10.5711C4.80741 10.1646 4.28605 9.95473 3.95718 10.2166L0.195373 13.2118C-0.0412524 13.4002 -0.0404974 13.7379 0.197647 13.9253Z"
                                      fill="#595959"/>
                            </svg>
                        </button>
                        <button onclick="dbDeleteItemMobile($(this), 'db_list-candidate')" data-id="${key}">
                            <svg width="12" height="13" viewBox="0 0 12 13"
                                     fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M3.625 1.17H3.5C3.56875 1.17 3.625 1.1115 3.625 1.04V1.17H8.375V1.04C8.375 1.1115 8.43125 1.17 8.5 1.17H8.375V2.34H9.5V1.04C9.5 0.466375 9.05156 0 8.5 0H3.5C2.94844 0 2.5 0.466375 2.5 1.04V2.34H3.625V1.17ZM11.5 2.34H0.5C0.223437 2.34 0 2.57237 0 2.86V3.38C0 3.4515 0.05625 3.51 0.125 3.51H1.06875L1.45469 12.0087C1.47969 12.5629 1.92031 13 2.45312 13H9.54688C10.0813 13 10.5203 12.5645 10.5453 12.0087L10.9312 3.51H11.875C11.9438 3.51 12 3.4515 12 3.38V2.86C12 2.57237 11.7766 2.34 11.5 2.34ZM9.42656 11.83H2.57344L2.19531 3.51H9.80469L9.42656 11.83Z"
                                      fill="#595959"/>
                            </svg>
                        </button>
                    </div>
                </div>
            </div>
        </div>`;
    }

    $('#db_list-candidate .height__list--body').append(htmlListCandidate);
}

/*==============================APPEND LIST JOB================================*/
dbAppendListJob = (_this) => {
    let htmlListJob = '';

    for (let i = 0; i < 15; i++) {
        let key = generateKey();

        htmlListJob += `<div class="row list__content--item position-relative align-items-center animate__animated animate__fadeIn animate__fast"
             id="db_list-job_${key}">
            <div class="col-12 pl-0 pr-0 d-flex card-mobile">
                <label class="checkbox-label d-flex justify-content-center align-items-center app-desktop">
                    <input type="checkbox" class="input-hidden" data-id="${key}"
                           id="db_list-job-checkbox_${key}"
                           onchange="dbSelectItemCheckBox($(this), 'db_list-job')">
                    <span class="list--checkbox position-relative d-flex justify-content-center align-items-center">
                        <i class="fas fa-check"></i>
                    </span>
                </label>
                <div class="body__list--item-text d-flex align-items-center">
                    <div class="title__card--mobile">
                        <span class="text-list-default">Junior Software Developer (C#, C++) </span>
                    </div>
                    <div class="basic-info d-flex align-items-center">
                        <div class="basic-info--content">
                            <span class="app-mobile app-desktop title--mobile">Ngày ứng tuyển</span>
                            <span class="text-list-default date-upload">10/09/2020</span>
                        </div>
                        <div class="status" id="db_list-job_status_${key}">
                            <span class="job__status--active">Hoạt động</span>
                        </div>
                    </div>
                    <div class="action__mobile app-mobile app-tablet">
                        <button>
                            <svg width="17" height="12" viewBox="0 0 23 18"
                                 fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M14.7662 13.7488H12.6053V7.2157C12.6053 7.09707 12.5003 7 12.372 7H10.6222C10.4939 7 10.3889 7.09707 10.3889 7.2157V13.7488H8.23377C8.03838 13.7488 7.93048 13.9564 8.05005 14.0966L11.3163 17.9172C11.3381 17.943 11.366 17.9639 11.3978 17.9782C11.4296 17.9925 11.4646 18 11.5 18C11.5354 18 11.5704 17.9925 11.6022 17.9782C11.634 17.9639 11.6619 17.943 11.6837 17.9172L14.95 14.0966C15.0695 13.9564 14.9616 13.7488 14.7662 13.7488Z"
                                      fill="#595959"/>
                                <path d="M19.1855 5.1675C18.0098 2.1475 15.0142 0 11.5051 0C7.99609 0 5.00045 2.145 3.82478 5.165C1.62489 5.7275 0 7.68 0 10C0 12.7625 2.29743 15 5.13136 15H6.16071C6.27366 15 6.36607 14.91 6.36607 14.8V13.3C6.36607 13.19 6.27366 13.1 6.16071 13.1H5.13136C4.26629 13.1 3.45257 12.765 2.84676 12.1575C2.24353 11.5525 1.92266 10.7375 1.95089 9.8925C1.974 9.2325 2.20502 8.6125 2.62344 8.09C3.05212 7.5575 3.65279 7.17 4.3202 6.9975L5.29308 6.75L5.64989 5.835C5.87065 5.265 6.17868 4.7325 6.56629 4.25C6.94896 3.77177 7.40224 3.35137 7.91138 3.0025C8.96641 2.28 10.2088 1.8975 11.5051 1.8975C12.8015 1.8975 14.0439 2.28 15.0989 3.0025C15.6097 3.3525 16.0615 3.7725 16.444 4.25C16.8316 4.7325 17.1396 5.2675 17.3604 5.835L17.7146 6.7475L18.6849 6.9975C20.0762 7.3625 21.0491 8.595 21.0491 10C21.0491 10.8275 20.718 11.6075 20.1173 12.1925C19.8227 12.4811 19.4723 12.7099 19.0863 12.8656C18.7003 13.0214 18.2864 13.1011 17.8686 13.1H16.8393C16.7263 13.1 16.6339 13.19 16.6339 13.3V14.8C16.6339 14.91 16.7263 15 16.8393 15H17.8686C20.7026 15 23 12.7625 23 10C23 7.6825 21.3802 5.7325 19.1855 5.1675Z"
                                      fill="#595959"/>
                            </svg>
                        </button>
                        <button onclick="modalJobOpen($(this))" data-id="${key}" data-status="active">
                            <svg width="9" height="16" viewBox="0 0 11 18"
                                 fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M10.8215 3.17081L7.04015 0.11754C6.70916 -0.149747 6.1875 0.0672368 6.1875 0.482442V2.09065C2.73649 2.12759 0 2.77432 0 5.83243C0 7.06674 0.850373 8.28953 1.79036 8.92883C2.08368 9.12834 2.50173 8.87794 2.39357 8.55455C1.41939 5.64134 2.85564 4.86795 6.1875 4.82313V6.58927C6.1875 7.00512 6.70957 7.22114 7.04015 6.95418L10.8215 3.90061C11.0593 3.70852 11.0597 3.36316 10.8215 3.17081Z"
                                      fill="#595959"/>
                                <path d="M0.197647 13.9253L3.97849 16.9006C4.30943 17.1611 4.82904 16.9476 4.82774 16.5417L4.82273 14.9694C8.26449 14.9243 10.9917 14.2849 10.9822 11.2951C10.9783 10.0883 10.1264 8.89502 9.18692 8.27243C8.89375 8.07813 8.47759 8.32402 8.58647 8.63992C9.56715 11.4856 8.13711 12.2454 4.81421 12.2979L4.80871 10.5711C4.80741 10.1646 4.28605 9.95473 3.95718 10.2166L0.195373 13.2118C-0.0412524 13.4002 -0.0404974 13.7379 0.197647 13.9253Z"
                                      fill="#595959"/>
                            </svg>
                        </button>
                        <button onclick="dbDeleteItemMobile($(this), 'db_list-candidate')" data-id="${key}">
                            <svg width="12" height="13" viewBox="0 0 12 13"
                                 fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M3.625 1.17H3.5C3.56875 1.17 3.625 1.1115 3.625 1.04V1.17H8.375V1.04C8.375 1.1115 8.43125 1.17 8.5 1.17H8.375V2.34H9.5V1.04C9.5 0.466375 9.05156 0 8.5 0H3.5C2.94844 0 2.5 0.466375 2.5 1.04V2.34H3.625V1.17ZM11.5 2.34H0.5C0.223437 2.34 0 2.57237 0 2.86V3.38C0 3.4515 0.05625 3.51 0.125 3.51H1.06875L1.45469 12.0087C1.47969 12.5629 1.92031 13 2.45312 13H9.54688C10.0813 13 10.5203 12.5645 10.5453 12.0087L10.9312 3.51H11.875C11.9438 3.51 12 3.4515 12 3.38V2.86C12 2.57237 11.7766 2.34 11.5 2.34ZM9.42656 11.83H2.57344L2.19531 3.51H9.80469L9.42656 11.83Z"
                                      fill="#595959"/>
                            </svg>
                        </button>
                    </div>
                </div>
            </div>
        </div>`;
    }

    $('#db_list-job .height__list--body').append(htmlListJob);
}

let galleryImageList;

$(document).ready(() => {
    let errorReportModal = $('#error_report_modal');

    errorReportModal.on('hidden.bs.modal', () => {
        modalClearContent();
        modalClearFormErr();
    });

    /*==================PAGINATION=================*/
    $('.page-link').on('click', (e) => {
        let _this = $(e.target);
        let pageLink = $('.page-link')

        if (pageLink.hasClass('page-link-active')) {
            pageLink.removeClass('page-link-active');
        }
        if (!pageLink.hasClass('page-link-active')) {
            _this.addClass('page-link-active');
        }
    })

    /*===============STATUS=======================*/
    $('.search_status_select2').select2({
        placeholder: 'Trạng thái',
        minimumResultsForSearch: -1,
        dropdownCssClass: 'dashboard-select2-dropdown',
        selectionCssClass: 'dashboard-select2-selection',
        width: '140px',
    });

    $('.edit_status_modal').select2({
        placeholder: 'Thay đổi trạng thái',
        minimumResultsForSearch: -1,
        dropdownCssClass: 'dashboard-select2-dropdown',
        selectionCssClass: 'dashboard-select2-selection',
        width: '100%',
    });

    /*==================LIST SCROLL ADD SHADOW================*/
    $('.js_list-body').scroll(function () {
        if ($(this).scrollTop()) {
            $('.content__list--title').addClass('header-shadow');
        } else {
            $('.content__list--title').removeClass('header-shadow');
        }
    })

    /*===================COUNT ANIMATION======================*/
    $('.db_count').each(function () {
        $(this).prop('Counter',0).animate({
            Counter: $(this).text()
        }, {
            duration: 1000,
            easing: 'swing',
            step: function (now) {
                let number = "0" + Math.ceil(now);

                if (Math.ceil(now) < 10) {
                    $(this).html(number);
                } else {
                    $(this).text(Math.ceil(now));
                }
            }
        });
    });

    /*===================Report Error Img Modal====================*/
    $('.modal__upload-file').on('change', () => {
        $('.modal__upload-file--input').trigger('click');
    })
})

/* Generate key */
generateKey = () => {
    return Math.random().toString(20).substr(2, 10);
}

/*========================SHOW LIST ACTION=======================*/
dbShowActionContentListBody = (_this, prefixId) => {
    $(`#${prefixId} .list-title`).addClass('element-hidden');
    $(`#${prefixId} .js_content-list--action`).removeClass('element-hidden');
    $(`#${prefixId} .amount-checkbox`).html($(`#${prefixId} .list__content--item input:checked`).length);
}

/*========================SHOW LIST TITLE=======================*/
dbShowTitleContentListBody = (_this, prefixId) => {
    $(`#${prefixId} .list-title`).removeClass('element-hidden');
    $(`#${prefixId} .js_content-list--action`).addClass('element-hidden');
    $(`#${prefixId}_selectALLCheckbox`).prop('checked', false);
}

/*======================MODAL UPLOAD IMG========================*/
let galleryLoadImage = (_this) => {
    let files = _this[0].files;
    let gallery_add_list_error = $('#gallery_add_list_error');
    let imgItem = $('.gallery-item');
    let imgView = $('.gallery-item__image');
    let imgTitle = $('.gallery-item__title');
    gallery_add_list_error.html('');

    let file = files[0];

    if (file.size > 2097152) {
        let html = `<span style="display:block;">${image} ${file.name} ${too_2mb}</span>`;

        gallery_add_list_error.append(html);
    } else if (file.type !== "image/jpeg" && file.type !== "image/png" && file.type !== "image/jpg") {
        let html = `<span style="display:block;">${image} ${file.name} ${gallery_incorrect_format}</span>`;

        gallery_add_list_error.append(html);
    } else {
        let fileReader = new FileReader();

        galleryImageList = file;

        fileReader.onload = ((fileParam) => {
            return (e) => {
                imgItem.removeClass('element-hidden');
                imgView.css('background-image', `url(${e.target.result})`);
                imgTitle.text(fileParam.name);
            }
        })(file);
        fileReader.readAsDataURL(file);
    }
}

/*===============================DELETE IMG=============================*/

let deleteImgErrorReportItem = () => {
    Notiflix.Confirm.Show(
        'Xác nhận',
        'Bạn chắc chắn muốn xoá?',
        'Đồng ý',
        'Huỷ',
        () => {

            let gallery_add_list_error = $('#gallery_add_list_error');
            let imgItem = $('.gallery-item');
            let imgView = $('.gallery-item__image');
            let imgTitle = $('.gallery-item__title');
            let imgValue = $('#gallery_add_file');

            gallery_add_list_error.empty();

            imgItem.addClass('element-hidden');
            imgView.css('background-image', `unset`);
            imgTitle.empty();
            imgValue.val(null);
        },
    );
}

modalClearFormErr = () => {
    $('.error-text').text('');
    $('#gallery_file_error').text('');
    $('.modal__input--warning').text('');
    $('.invalid-input').removeClass('invalid-input');
}

modalClearContent = () => {

    $('#errorReport_form').trigger('reset');

    $('.errorReport_form').attr({
        'data-url': null,
        'data-type': null,
    });

    galleryImageList = [];

    $('#errorReport_form .modal-body--list-file').html('');
}

/* ============================= OPEN POPUP ===================================== */

modalReportErrorOpen = () => {
    $('#error_report_modal').modal('show');
}

/* ======================= STATUS CHANGE ========================== */
statusChangeCss = (prefixId) => {
    $(`#${prefixId} .js_list-content :checkbox`).prop('checked', false).attr('checked', false);
    $(`#${prefixId} .list-title`).removeClass('element-hidden');
    $(`#${prefixId} .js_content-list--action`).addClass('element-hidden');
    $(`#${prefixId} .selectAllCheckbox`).prop('checked', false);
    $(`#${prefixId}_status-change`).val(null).trigger('change');
    $(`#${prefixId} .list--checkbox`).removeClass('checkbox-all checkbox-remove');
}

/* ======================= PAGE ========================== */
$('#db_list-candidate--present').html(1);
$('#db_list-job--present').html(1);

pageCss = (prefixId, value) => {
    $(`#${prefixId}--present`).attr('data-page', `${value}`);
    $(`#${prefixId}--present`).html($(`#${prefixId}--present`).attr('data-page'));
    $(`#${prefixId} .next`).removeClass('disabled');
    $(`#${prefixId} .prev`).removeClass('disabled');
    $(`#${prefixId} .list__content--item`).remove();
}

pageNextCss = (prefixId) => {
    $(`#${prefixId} .next`).addClass('disabled');
}

pagePrevCss = (prefixId) => {
    $(`#${prefixId} .prev`).addClass('disabled');
}

/* ======================= SELECT ALL ========================== */
selectAllCheckedCss = (_this, prefixId) => {
    $(`#${prefixId} .js_list-content :checkbox`).prop('checked', true).attr('checked', true);
    $(`#${prefixId} .list--checkbox`).addClass('checkbox-all');
}

selectAllUnCheckedCss = (_this, prefixId) => {
    $(`#${prefixId} .js_list-content :checkbox`).prop('checked', false).attr('checked', false);
    $(`#${prefixId} .list--checkbox`).removeClass('checkbox-all checkbox-remove');
}

/* =================== SELECT ITEM ======================== */
selectItemCheckCss = (prefixId) => {
    $(`#${prefixId} .content__list--title :checkbox`).prop('checked', true).attr('checked', true);
    $(`#${prefixId} .list--checkbox`).addClass('checkbox-remove');
}

selectItemUnCheckCss = (prefixId) => {
    $(`#${prefixId} .list--checkbox`).removeClass('checkbox-all checkbox-remove');
    $(`#${prefixId} .content__list--title :checkbox`).prop('checked', false).attr('checked', false);
}

selectItemUnCheckAllCss = (prefixId) => {
    $(`#${prefixId} .list--checkbox`).addClass('checkbox-remove').removeClass('checkbox-all');
}

selectItemCheckAllCss = (prefixId) => {
    $(`#${prefixId} .list--checkbox`).addClass('checkbox-all');
}

/*=============================== SERVICE DETAIL MODAL ==========================*/
serviceDetail = (_this) => {
    $('#service_detail_modal').modal('show');
}
