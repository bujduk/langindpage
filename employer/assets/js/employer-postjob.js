let currentPostTabIndex = '1';
let thumbnailImage;

let handleTinyMceOnchange = (prefixClass, value) => {
    $(prefixClass).val(value);
}


let checkMaxItemInList = (prefixClass) => {
    let count = $(`.pj_${prefixClass}_item`).length;

    if (count === 0) {
        generateNewJobAddress();
    }

    $(`.pj_${prefixClass}_list`).attr('data-count', count);
    $(`.pj_${prefixClass}_add`).attr('hidden', count >= 3).is(':hidden', count >= 3);
}

let removeItem = (prefixClass, $button) => {
    let id = $button.attr('data-id')
    let count = $(`.pj_${prefixClass}_item`).length;

    if (count <= 1) {
        $(`#${prefixClass}_${id}`).find(':input').val(null).trigger('change');

    } else {
        $(`#${prefixClass}_${id}`).remove();
    }

    checkMaxItemInList(prefixClass);
}

let pjNavBorderChange = (itemHover) => {
    let navItem = $('.pj-nav');
    let itemActive = $('.pj-nav__item.active');
    let itemBorder = $('.pj-nav__border');

    let itemHoverTop = itemHover.offset().top - navItem.offset().top;
    let itemHoverHeight = itemHover.height();

    let itemActiveTop = itemActive.offset().top - navItem.offset().top;
    let itemActiveHeight = itemActive.height();

    let currentTop = itemActiveTop;
    let currentHeight = itemActiveHeight;

    if (itemHoverTop > itemActiveTop) {
        itemBorder.css({
            'top': currentTop,
            'height': currentHeight + (itemHoverTop - itemActiveTop)
        });
    } else if (itemHoverTop < itemActiveTop) {
        itemBorder.css({
            'top': currentTop - (itemActiveTop - itemHoverTop),
            'height': currentHeight + (itemActiveTop - itemHoverTop)
        });
    } else {
        itemBorder.css({
            'top': currentTop,
            'height': currentHeight
        });
    }
};

let pjBorderBack = () => {
    let navItem = $('.pj-nav');
    let itemActive = $('.pj-nav__item.active');
    let itemBorder = $('.pj-nav__border');

    itemBorder.css({
        'top': itemActive.offset().top - navItem.offset().top,
        'height': itemActive.height()
    });
}

let pjNavClicked = (navItem) => {
    let currentNavItem = $(`.pj-nav__item[data-tab="${currentPostTabIndex}"]`);
    let nextPostTabIndex = navItem.attr('data-tab');

    let currentForm = $(`.pj_form_switch[data-tab="${currentPostTabIndex}"]`);
    let nextForm = $(`.pj_form_switch[data-tab="${nextPostTabIndex}"]`);

    let formErrorBuzz = $('.form-error-buzz')

    formErrorBuzz.removeClass('animate__headShake animate__faster');

    if (currentPostTabIndex !== nextPostTabIndex) {

        if (currentPostTabIndex < nextPostTabIndex) {

            let checkValidate = checkFormValidate(currentPostTabIndex);

            if (checkValidate) {
                Notiflix.Block.Pulse('.emp-box');

                currentForm.collapse('hide');
                nextForm.collapse('show');

                currentNavItem.addClass('successfully');

                currentPostTabIndex = nextPostTabIndex;

                $('.pj-nav__item').removeClass('active');
                navItem.addClass('active');

                if (currentPostTabIndex === '4') {
                    checkAllTabSuccessed();
                }

                pjBorderBack();
            } else {
                formErrorBuzz.removeClass('animate__fadeIn').addClass('animate__headShake animate__faster');

                currentNavItem.removeClass('successfully');
            }
        } else {
            Notiflix.Block.Pulse('.emp-box');

            currentForm.collapse('hide');
            nextForm.collapse('show');

            currentPostTabIndex = nextPostTabIndex;

            $('.pj-nav__item').removeClass('active');
            navItem.addClass('active');

            pjBorderBack();
        }

    }


}

let handleToggleCheckboxChange = (prefixClass, toggleCheckbox) => {
    let formField = $(`.${prefixClass}_list`);
    let listInput = formField.find(':input');
    let toggleIsChecked = toggleCheckbox.is(":checked");
    let addButton = $(`.${prefixClass}_add`);

    listInput.attr('disabled', toggleIsChecked).is('disabled', toggleIsChecked);
    addButton.attr('disabled', toggleIsChecked).is('disabled', toggleIsChecked);

    formField.find('.error-text').text('');
    formField.find('.error-input').removeClass('error-input');
    formField.find('.error-form').removeClass('error-form');
}

let checkFormValidate = (tabIndex = null) => {
    let rulesTab = {
        '1': {
            // 'name': 'required|maxlength:255',
            // 'employment_level': 'required',
            // 'employment_type': 'required',
            // 'employment_language': 'required',
            'address[%][province]': `required`,
            'address[%][address]': `required|maxlength:255`,
            // 'email_receiving_cv': 'required|listemail'
        },
        '2': {
            'description': 'required|maxlength:2000',
            'requirement': 'required|maxlength:2000',
            'tech_stack': 'required'
        },
        '3': {
            'benefit': 'maxlength:1000',
            'salary': 'maxlength:50'
        },
    }

    return $('#pj_form').validationObject({
        rules: rulesTab[tabIndex],
        elementInnerForm: `.pj_form_switch[data-tab="${tabIndex}"]`
    })

}

let pjServiceClick = ($button) => {
    if (!$button.hasClass('disabled') && !$('#select_service_modal .have-no-job').length) {
        $('.modal-body__item').removeClass('active');

        $('#post_service').val($button.attr('data-value'));

        $('.select_service_submit').prop('disabled', false).is(':disabled', false);

        $button.addClass('active');
    } else {
        $('#post_service').val(null);
        $('.select_service_submit').prop('disabled', true).is(':disabled', true);
    }
}

(function ($) {
    $('.pj_job_address_list').on('change input', ':input', () => {
        $('#is_current_location').prop('checked', false).is(':checked', false);
    });
    $('.pj_job_address_add').on('click', () => {
        $('#is_current_location').prop('checked', false).is(':checked', false);
    });
})(jQuery)

let checkAllTabSuccessed = () => {
    let tabSuccess = $(`.pj-nav__item-check.successfully`);
    let navItem = $(`.pj-nav__item[data-tab="4"]`);
    let postButton = $('.pj_form_post');

    if (!!tabSuccess.length && tabSuccess.length === 3) {
        navItem.addClass('successfully');
        postButton.prop('disabled', false).attr('disabled', false);
    } else {
        navItem.removeClass('successfully');
        postButton.prop('disabled', true).attr('disabled', true);
    }
}

let _handleThumbnailChange = ($input) => {
    let files = $input[0].files;
    let imgThumbnailError = $('#img_thumbnail_error');
    let imgThumbnailItem = $('.pj-form__thumbnail--item');
    let imgThumbnailView = $('.pj-form__thumbnail--background');
    let imgThumbnailTitle = $('.pj-form__thumbnail--title');
    let imgThumbnailValue = $('#thumbnail_img');

    imgThumbnailError.empty();

    let file = files[0];
    if (file.size > 5097152) {
        let html = `<span style="display:block;">${image} ${file.name} too 5MB</span>`;

        imgThumbnailError.append(html);
    } else if (file.type !== "image/jpeg" && file.type !== "image/png" && file.type !== "image/jpg") {
        let html = `<span style="display:block;">${image} ${file.name} ${gallery_incorrect_format}</span>`;

        imgThumbnailError.append(html);
    } else {
        let fileReader = new FileReader();

        thumbnailImage = file;

        fileReader.onload = ((fileParam) => {
            return (e) => {
                imgThumbnailItem.removeClass('element-hidden');
                imgThumbnailView.css('background-image', `url(${e.target.result})`);
                imgThumbnailTitle.text(fileParam.name);
                imgThumbnailValue.val(e.target.result);
            }
        })(file);
        fileReader.readAsDataURL(file);
    }
}

let _handleThumbnailRemove = () => {
    let imgThumbnailError = $('#img_thumbnail_error');
    let imgThumbnailItem = $('.pj-form__thumbnail--item');
    let imgThumbnailView = $('.pj-form__thumbnail--background');
    let imgThumbnailTitle = $('.pj-form__thumbnail--title');
    let imgThumbnailValue = $('#thumbnail_img');

    imgThumbnailError.empty();

    imgThumbnailItem.addClass('element-hidden');
    imgThumbnailView.css('background-image', `unset`);
    imgThumbnailTitle.empty();
    imgThumbnailValue.val(null);
}
let listProvince = {
    'Ha Noi': 'Hà Nội',
    'Da Nang': 'Đà Nẵng',
    'Ho Chi Minh': 'Hồ Chí Minh',
}

let generateNewJobAddress = (animation = 'animate__animated animate__fadeInDown animate__faster', data = null) => {
    let jobLocationList = $('.pj_job_address_list');
    let randomKey = generateKey();
    let provinceSelect = '';

    jobLocationList.append(`
        <div id="job_address_${randomKey}"
             class="pj_job_address_item list-item row mx-n12 mb-1 d-flex position-relative ${animation}">
            <div class="col-12 col-md-5 col-xxl-4 px-12 pj-form__item">
                <div class=" d-flex align-items-center">
                    <label for="job_address_${randomKey}_province" class="pj-form__label mb-0 pj-form__label--inline ">
                        Tỉnh thành
                    </label>
                    <select id="address_${randomKey}_province" name="address[${randomKey}][province]"
                            class="pj_form_select2 invisible" data-placeholder="">
                            <option></option>
                    </select>
                </div>
                <p id="address_${randomKey}_province_error" class="error-text no-min-height"></p>
            </div>
            <div class="col-12 col-md-7 col-xxl-8 px-12 pj-form__item">
                <div class=" d-flex align-items-center">
                    <label for="address_${randomKey}_address"
                           class="pj-form__label mb-0 pj-form__label--inline ">
                        Địa chỉ
                    </label>
                    <input id="address_${randomKey}_address" name="address[${randomKey}][address]"
                             type="text" required maxlength="255"
                             class="pj-form__input w-100">
                </div>
                <p id="address_${randomKey}_address_error" class="error-text no-min-height text-left text-md-right"></p>
            </div>
            <div class="d-flex align-items-center justify-content-center pj-list-remove">
                <button type="button" tabindex="-1" data-id="${randomKey}"
                        onclick="removeItem('job_address', $(this))"
                        class="pj-list-remove__button pj_job_address_remove">
                    <i class="far fa-times-circle"></i>
                </button>
            </div>
        </div>
    `);

    $.each(listProvince, (name, value) => {
        provinceSelect += `
            <option value="${name}" 
                    ${(!!data && !!data.province && data.province === name) ? 'selected' : ''} >
                ${value}
            </option>
        `
    })

    $(`#address_${randomKey}_province`).append(provinceSelect);
    $(`#address_${randomKey}_address`).val((!!data && !!data.address) ? data.address : '');

    initSelect2Single($(`#address_${randomKey}_province`));
    checkMaxItemInList('job_address');
}

let generateListAddressFromData = (listAddress) => {
    let jobLocationList = $('.pj_job_address_list');

    jobLocationList.html('')

    _.forEach(listAddress, (item) => {
        generateNewJobAddress('', item);
    })
}
/* Address */
let handleAddressJobChange = ($toggle) => {
    let toggleValue = $toggle.is(':checked');

    if (toggleValue) {
        getFormRequest('./assets/frontend/post-job/js/json/address.json')
            .then((responseData) => {
                generateListAddressFromData(responseData.data);
            }, function (responseErr) {
                if (responseErr.status === 422) {
                    commonShowNotifyErr();
                } else {
                    commonShowNotifyErr();
                }
            });
    }
}
/* End Address */

/* Form Submit */
(function ($) {
    $('#pj_form').on('submit', (e) => {
        e.preventDefault();

        let $form = $(e.target);
        let formData = $form.serializeObject();
        let convertAddress = [];

        /* Convert Data */
        formData['salary_is_negotiate'] = (formData['salary_is_negotiate'] === 'on');

        $.each(formData, (name, value) => {
            if (name.endsWith('][province]')) {
                convertAddress.push({
                    province: value,
                    address: formData[name.replace('][province]','][address]')]
                });

                delete formData[name];
                delete formData[name.replace('][province]','][address]')];
            }
        });
        formData['address'] = convertAddress;

    })
})(jQuery)
/* End Form Submit */



let initSelect2Single = ($select) => {
    $($select).select2({
        minimumResultsForSearch: -1,
        selectionCssClass: 'pj_form_select2-selection',
        dropdownCssClass: 'pj_form_select2-dropdown',
        allowClear: (!!$select.attr('data-allowclear')),
        width: '100%',
        placeholder: $select.attr('data-placeholder'),
    });
}

let initSelect2Multiple = ($select, minimumResultsForSearch = 0) => {
    $($select).select2({
        selectionCssClass: 'pj_form_select2-multiple-selection',
        dropdownCssClass: 'pj_form_select2-multiple-dropdown',
        maximumSelectionLength: (!!$select.attr('data-length')) ? _.toNumber($select.attr('data-length')) : -1,
        width: '100%',
        tags: true,
        placeholder: $select.attr('data-placeholder'),
        language:{
            "noResults" :  () => {
                return null
            }
        }
    });
}

let pageInit = () => {
    checkMaxItemInList('job_address');
}

(function ($) {
    let pjNavItem = $('.pj-nav__item');
    let techStack = $('#tech_stack');

    Notiflix.Loading.Init({
        backgroundColor: "rgba(255,255,255,0.6)",
        svgColor: "#06202D",
    });

    pjNavItem.on('mouseover', (e) => {
        pjNavBorderChange($(e.target));
    });

    pjNavItem.on('mouseout', (e) => {
        pjBorderBack($(e.target));
    });

    $('.pj_form_select2').each((key, value) => {
        let $select = $(value);
        initSelect2Single($select);
    });

    $('.pj_form_select2_multiple').each((key, value) => {
        let $select = $(value);
        initSelect2Multiple($select);
    });

    $('.pj_form_select2_multiple_nosearch').each((key, value) => {
        let $select = $(value);
        initSelect2Multiple($select, -1);
    });

    $('.post_job_settime').daterangepicker({
        singleDatePicker: true,
        timePicker: true,
        timePicker24Hour: true,
        showDropdowns: true,
        opens: "left",
        drops: "up",
        minDate: moment().startOf('minute'),
        locale: {
            format: 'HH:mm DD/MM/YYYY'
        },
    }).on('cancel.daterangepicker', (e) => {
        $(e.target).addClass('disabled');
    }).on('apply.daterangepicker', (e) => {
        $(e.target).removeClass('disabled');
    });

    $('.pj_collapse').collapse({
        toggle: false
    });

    $('.pj_form_switch').on('shown.bs.collapse', () => {
        Notiflix.Block.Remove('.emp-box');
    });

    $('#select_service_modal').on('show.bs.modal', () => {
        if (!!$('#select_service_modal .have-no-job').length) {
            $('.post_job_settime').prop('disabled', true).is(':disabled', true);
        } else {
            $('.post_job_settime').prop('disabled', false).is(':disabled', false);
        }
    }).on('hidden.bs.modal', () => {
        $('.modal-body__item').removeClass('active');
        $('#post_service').val(null);
        $('.post_job_settime').val('').addClass('disabled');
        $('.select_service_submit').prop('disabled', true).is(':disabled', true);
    });

    techStack.select2({
        selectionCssClass: 'pj_form_select2-multiple-selection',
        dropdownCssClass: 'pj_form_select2-multiple-dropdown',
        maximumInputLength: 100,
        width: '100%',
        tags: true,
        placeholder: techStack.attr('data-placeholder'),
        ajax: {
            url: techStack.attr('data-url'),
            data: (params) => {
                console.log(params)
                return {
                    name: params.term
                }
            },
            processResults: function (response) {
                let output = {};
                let projectSelected = techStack.val();

                output.results = response.data.map((item) => {
                    let selected = _.includes(projectSelected, item.name)

                    return {
                        "id": item.name,
                        "text": item.name,
                        "selected": selected
                    }
                });

                return !!output ? output : null;
            }
        }
    })

    pageInit();
})(jQuery);