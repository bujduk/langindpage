const $ = jQuery;

let knowledgeList = [];
let serviceIconsRaw = [];
let serviceIconsChunk = [];
let serviceIconCurrent = '';
let galleryImageList = [];

$(document).ready(() => {
    let infoModal = $('#info_modal');
    let aboutModal = $('#about_modal');
    let skillModal = $('#skill_modal');
    let serviceModal = $('#service_modal');
    let educationModal = $('#education_modal');
    let experienceModal = $('#experience_modal');
    let galleryAddModal = $('#gallery_add_modal');
    let galleryEditModal = $('#gallery_edit_modal');
    let knowledgeAddModal = $('#knowledge_add_modal');
    let knowledgeEditModal = $('#knowledge_edit_modal');

    let galleryAddFile = $('#gallery_add_file');

    aboutModal.on('hidden.bs.modal', () => {
        modalClearContent('about');
        modalClearFormErr();
    });
    infoModal.on('hidden.bs.modal', () => {
        modalClearContent('info');
        modalClearFormErr();
    });
    skillModal.on('hidden.bs.modal', () => {
        modalClearContent('skill');
        modalClearFormErr();
    });
    knowledgeAddModal.on('hidden.bs.modal', () => {
        modalClearContent('knowledge_add');
        modalClearFormErr();
    });
    knowledgeEditModal.on('hidden.bs.modal', () => {
        modalClearContent('knowledge_edit');
        modalClearFormErr();
    });
    serviceModal.on('hidden.bs.modal', () => {
        modalClearContent('service');
        modalClearFormErr();
    });
    educationModal.on('hidden.bs.modal', () => {
        modalClearContent('education');
        modalClearFormErr();
    });
    experienceModal.on('hidden.bs.modal', () => {
        modalClearContent('experience');
        modalClearFormErr();
    });
    galleryAddModal.on('hidden.bs.modal', () => {
        modalClearContent('gallery_add');
        modalClearFormErr();
    });
    galleryEditModal.on('hidden.bs.modal', () => {
        modalClearContent('gallery_edit');
        modalClearFormErr();
    });

    /* Input Image File Change */
    galleryAddFile.on('change', (e) => {
        galleryLoadImage($(e.target));
    });

    /*======================SKILL========================*/
    $('#skill_name').on('input change', (e) => {
        $('#skill_skill_id').val('');
        searchSkill($(e.target));
    });
    $('#skill_score').on('input change', (e) => {
        $('#skill_score_view').text($(e.target).val());
    });

    /*======================SKILL========================*/
    $('#service_form').on('click', '.list-icon i', (e) => {
        let _this = $(e.target);
        let icon = $('#service_icon');

        $('#service_form .list-icon i').removeClass('selected');
        serviceIconCurrent = '';

        if (!_this.hasClass(icon.val())) {
            let name = _this.attr('title');

            icon.val(name);
            _this.addClass('selected');
        } else {
            icon.val('');
        }
    });
    $('.project-input-select2').select2({
        tags: true,
        dropdownCssClass: 'popup-select2-dropdown',
        selectionCssClass: 'popup-select2-select',
        width: '100%',
    });

    initData();
})

/* Init Data */
initData = () => {
    /* Get Service icon after page loaded */
    $.getJSON("../../data/icon.json", function (result) {
        $.each(result, function (name, value) {
            serviceIconsRaw.push(value);
        });
    });
};

parseURLParamToKeyValue = (data) => {
    return JSON.parse('{"' + decodeURI(data).replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g, '":"') + '"}')
};

(($) => {
    $.fn.serializeObject = function () {
        let o = {};
        let a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
})(jQuery);

/* Generate key */
generateKey = () => {
    return Math.random().toString(20).substr(2, 10);
}

/*
* @param data Dữ liệu data trên server trả về
* @param prefixClass Tiền tố class cho các thành phần
* @valueParam prefixClass in [about, service, education, experience, gallery, skill, knowledge]
*
* @call bindingData(data, 'about')
* */
bindingData = (data, prefixClass) => {
    let id = (data.id) ? data.id : 0;
    let time = null;
    let timeline = null;

    $.each(data, (key, value) => {
        let btnEdit = $(`.${prefixClass}-${id}-button_edit`);

        $(`.${prefixClass}-${id}-${key}`).text(value);

        switch (key) {
            case 'url':
                btnEdit.attr(`data-url`, value);
                break;
            case 'is_current_working':
                btnEdit.attr(`data-is_current_working`, value ? 1 : 0);
                break;
            default:
                btnEdit.attr(`data-${key}`, value);
        }
    });

    switch (prefixClass) {
        case 'about':
            $('.social-links a').addClass('hidden').attr('href', '');
            $(`.about-${id}-img_avatar`).attr('src', data.img_avatar);
            $(`.about-${id}-img_avatar-background`).css('background-image', `url(${data.img_avatar})`);
            $(`.about-${id}-email`).attr('href', `mailto:${data.email}`);
            $(`.about-${id}-img_background`).attr('src', data.img_background);
            $(`.about-${id}-img_background-background`).css('background-image', `url(${data.img_avatar})`)
            $(`.about-${id}-freelance`).html(data.freelance ? freelance_available : freelance_not_available);

            $(`.about-${id}-button_edit`).attr({
                'data-freelance': data.freelance ? 1 : 0,
                'data-freelance_editview': data.freelance ? freelance_available : freelance_not_available
            });

            $.each(data.socials, (key, value) => {
                if (value && value !== '') {
                    $(`.about-${id}-${key}`).attr('href', value).removeClass('hidden');
                }
                $(`.about-${id}-button_edit`).attr(`data-${key}`, value);
            });
            break;

        case 'education':
        case 'experience':
            time = convertTime(data.start_time, data.end_time);
            timeline = data.start_time.replace('-', '/') + ' - ';

            if (!!data.end_time) {
                timeline += data.end_time.replace('-', '/');
            } else {
                timeline += 'Hiện tại'
            }
            $(`.${prefixClass}-${id}-timeline`).text(timeline);

            $.each(time, (key, value) => {
                $(`.${prefixClass}-${id}-button_edit`).attr(`data-${key}`, value);
            });
            break;

        case 'skill':

            $(`.skill-${id}-percentage`).attr('data-value', data.score);
            $(`.skill-${id}-percentage .skill-percentage`).css('width', `${data.score}%`);
            break;

        case 'service':
            $(`.service-${id}-icon`).html(`<i class="${data.icon}"></i>`);
            break;
    }
}

/* Generate Time input */
convertTime = (start, end) => {
    start = start.split('-');
    end = !!end ? end.split('-') : null;

    return {
        start_month: start[0],
        start_year: start[1],
        end_month: !!end ? end[0] : '',
        end_year: !!end ? end[1] : ''
    };
}

/* ============================= COMMON OPEN POPUP ===================================== */
modalOpen = (_this, prefixClass, action = 'add') => {
    let modal = $(`#${prefixClass}_modal`);
    let form = $(`#${prefixClass}_form`);

    if (modal.length === 0) {
        modal = $(`#${prefixClass}_${action}_modal`);
        form = $(`#${prefixClass}_${action}_form`);
    }
    ;

    bindingDataModal(_this, form, prefixClass, action);

    modal.modal('show');
};

bindingDataModal = (_this, form, prefixClass, action = 'add') => {
    let time = null;

    form.attr({
        'data-url': _this.attr('data-url'),
        'data-type': action,
    });

    if (action === 'add') {
        switch (prefixClass) {
            case 'knowledge':
                addnewKnowledgeItemInForm();
                break;

            case 'service':
                chunkServiceIcon(serviceIconsRaw);
                generateServiceIcon();
                break;
            case 'education':
            case 'experience':
                $(`#${prefixClass}_start_month`).val(month_default);
                $(`#${prefixClass}_start_year`).val(year_default);
                $(`#${prefixClass}_end_month`).val(month_default);
                $(`#${prefixClass}_end_year`).val(year_default);
                break;
        }
    }

    if (action === 'edit') {
        let columns = form.attr('data-columns').split(',');

        columns.forEach((value) => {
            let inputField = $(`#${prefixClass}_${value}`);

            if (!inputField.length) {
                inputField = $(`#${prefixClass}_${action}_${value}`);
            }

            inputField.val((_this.attr(`data-${value}`)) ? _this.attr(`data-${value}`) : '');
        });

        switch (prefixClass) {
            case 'skill' :
                $('#skill_name').attr('readonly', true)
                $('#skill_score_view').text(_this.attr(`data-score`));
                $('#skill_form .skill__list-suggestion').addClass('hidden');
                break;

            case 'service':
                serviceIconCurrent = _this.attr('data-icon');

                chunkServiceIcon(serviceIconsRaw, true);
                generateServiceIcon();
                break;

            case 'education':
            case 'experience':
                time = convertTime(_this.attr('data-start_time'), _this.attr('data-end_time'));
                let isCurrentWorking = parseInt(_this.attr('data-is_current_working'));

                $(`#${prefixClass}_start_month`).val(time.start_month);
                $(`#${prefixClass}_start_year`).val(time.start_year);

                if (prefixClass === 'education') {
                    $(`#${prefixClass}_end_month`).val(time.end_month);
                    $(`#${prefixClass}_end_year`).val(time.end_year);
                } else {
                    if (isCurrentWorking) {
                        $(`.${prefixClass}_end_time_hidden`).addClass('hidden');
                        $(`#${prefixClass}_is_current_working`).prop('checked', true).attr('checked', true);

                        $(`#${prefixClass}_end_month`).val(month_default);
                        $(`#${prefixClass}_end_year`).val(year_default);
                    } else {
                        $(`#${prefixClass}_end_month`).val(time.end_month);
                        $(`#${prefixClass}_end_year`).val(time.end_year);

                        $(`#${prefixClass}_is_current_working`).prop('checked', false).attr('checked', false);
                    }
                }
                break;
        }
    }
};

/*============================= COMMON CLOSE POPUP =====================================*/
modalClearContent = (prefixClass) => {
    let form = $(`#${prefixClass}_form`);

    form.trigger('reset');

    form.attr({
        'data-url': null,
        'data-type': null,
    });

    switch (prefixClass) {
        case 'education':
        case 'experience':
            $(`.${prefixClass}_end_time_hidden`).removeClass('hidden');
            $(`#${prefixClass}_is_current_working`).attr('checked', false);

            $(`#${prefixClass}_start_month`).val(month_default);
            $(`#${prefixClass}_start_year`).val(year_default);
            $(`#${prefixClass}_end_month`).val(month_default).attr('required', true);
            $(`#${prefixClass}_end_year`).val(year_default).attr('required', true);
            break;

        case 'skill':
            $('#skill_score_view').text('0');
            $('#skill_name').attr('readonly', false)
            $('.skill__list-suggestion').removeClass('hidden');
            $('.skill__list-suggestion .dropdown-item').removeClass('hidden');
            break;

        case 'knowledge_add':
            knowledgeList = [];
            $('#knowledge_add_form .list_knowledge').html('');
            break;

        case 'service':
            serviceIconCurrent = '';

            $('#service_icon').val('');
            break;

        case 'gallery_add':
            galleryImageList = [];

            $('#gallery_add_form .input-gallery-list').html('');
            break;
    }
}

/*============================= COMMON CLEAR FORM ERROR =====================================*/
modalClearFormErr = () => {
    $('.error-text').text('');
    $('#gallery_file_error').text('');
    $('.modal__input--warning').text('');
    $('.invalid-input').removeClass('invalid-input');
}

commonShowNotifyErr = () => {
    Notiflix.Notify.Failure(common_error);
}

commonShowErr = (errors, prefixClass) => {
    $.each(errors, function (key, value) {
        $(`#${prefixClass}_${key}_error`).text(value[0]);
        $(`#${prefixClass}_${key}`).addClass('invalid-input');

        switch (prefixClass) {
            case 'education':
            case 'experience':
                switch (key) {
                    case 'start_time':
                        $(`#${prefixClass}_start_month`).addClass('invalid-input');
                        $(`#${prefixClass}_start_year`).addClass('invalid-input');
                        $(`#${prefixClass}_time_error`).text(value[0]);
                        break;
                    case 'end_time':
                        $(`#${prefixClass}_end_month`).addClass('invalid-input');
                        $(`#${prefixClass}_end_year`).addClass('invalid-input');
                        $(`#${prefixClass}_time_error`).text(value[0]);
                        break;
                }
                break;
            case 'knowledge':
                $(`#${prefixClass}_content`).addClass('invalid-input');
                break;
            case 'service':
                if (key === 'icon') {
                    $('.form-icon').addClass('invalid-input');
                }
                break;
        }

        if (key.indexOf('.') !== -1) {
            let keyArr = key.split('.');
            $(`#${prefixClass}_${keyArr[1]}_error`).text(value[0]);
            $(`#${prefixClass}_${keyArr[1]}`).addClass('invalid-input');
        }

        if (prefixClass === 'skill' && key === 'skill_id') {
            $(`#${prefixClass}_name_error`).text(value[0]);
            $(`#${prefixClass}_name`).addClass('invalid-input');
        }
    });
}

dropdownChangeValue = (_this, idName) => {
    $(`#${idName}`).val(`${_this.attr('data-value')}`);
    $(`#${idName}_editview`).val(`${_this.html().trim()}`);
}

/* Delete Item */
deleteItem = (_this) => {

    Notiflix.Confirm.Show(
        'Xác nhận',
        'Bạn chắc chắn muốn xoá?',
        'Đồng ý',
        'Huỷ',
        () => {
            let type = _this.attr('data-type');
            let id = _this.attr('data-id');
            $(`#${type}_${id}`).remove();
        },
        () => {
        }
    );
}

readURL = (input, target) => {
    if (input.files && input.files[0]) {
        let reader = new FileReader();

        reader.onload = function (e) {
            $(target).attr('src', e.target.result);
        };

        reader.readAsDataURL(input.files[0]);
    }
}

uploadFile = (input, target) => {
    if (input.files && input.files[0]) {

        let file = input.files[0];

        if (file.size > 2097152) {

            Notiflix.Notify.Failure(image + ' ' + file.name + ' ' + too_2mb);

        } else if (file.type !== "image/jpeg" && file.type !== "image/png" && file.type !== "image/jpg") {

            Notiflix.Notify.Failure(image + ' ' + file.name + ' ' + gallery_incorrect_format);

        } else {
            readURL(input, target);

            let formData = new FormData();

            formData.append('file', file);
            formData.append('type', $(input).attr('data-type'));
            formData.append('p_page_id', $('#page_id').val());

            postFileUploadFormRequest($(input).attr('data-upload_url'), formData)
                .then(function (responseData) {
                    Notiflix.Notify.Success(upload_file_success);
                }, function (responseErr) {
                    if (responseErr.status === 422) {
                        commonShowErr(responseErr.responseJSON.errors);
                    } else {
                        commonShowNotifyErr();
                    }
                });
        }
    } else {
        $('#img_avatar').val('');
    }
}

/*======================EXPERIENCE========================*/
experienceCurrentOnClick = (_this) => {
    let experienceEndtimeHidden = $('.experience_end_time_hidden');
    let experienceEndMonth = $('#experience_end_month');
    let experienceEndYear = $('#experience_end_year');

    if (_this.is(':checked')) {
        experienceEndtimeHidden.addClass('hidden');

        experienceEndMonth.attr('required', false);
        experienceEndYear.attr('required', false);
    } else {
        experienceEndtimeHidden.removeClass('hidden');

        experienceEndMonth.attr('required', true);
        experienceEndYear.attr('required', true);

        if (!experienceEndMonth.val()) {
            experienceEndMonth.val(month_default);
        }

        if (!experienceEndYear.val()) {
            experienceEndYear.val(year_default);
        }
    }
}

/*======================EDUCATION========================*/

/*======================SERVICE========================*/
searchAndChunkService = (index) => {
    let value = index.trim().replace(/\s+/g, "-").toLowerCase();
    let listSearch = serviceIconsRaw.filter(item => item.includes(value));

    chunkServiceIcon(listSearch);
}

chunkServiceIcon = (listIcon = serviceIconsRaw, haveLead = false) => {
    let list_icon = $('#service_form .list-icon');
    let serviceIndex = -1;
    let chunk = 100;

    serviceIconsChunk = [];
    list_icon.html('');

    if (haveLead) {
        listIcon.forEach((item, key) => {
            if (item === serviceIconCurrent) serviceIndex = key;
        })
    }

    for (let i = (serviceIndex !== -1) ? serviceIndex : 0; (listIcon && i < listIcon.length); i += chunk) {
        let temp = listIcon.slice(i, i + chunk);
        serviceIconsChunk.push(temp);
    }

    list_icon.attr('data-index', 0);
}

serviceSearchIcon = (_this) => {
    let value = _this.val();

    searchAndChunkService(value);
    generateServiceIcon();
}

generateServiceIcon = () => {
    let list_icon = $('#service_form .list-icon');
    let listIndex = list_icon.attr('data-index');

    hideViewMoreButton();

    if (serviceIconsChunk && serviceIconsChunk.length > 0) {
        serviceIconsChunk[listIndex].forEach(item => {
            list_icon.append(`
                <i class="${item}${serviceIconCurrent === item ? ' selected' : ''}" title="${item}"></i>
            `)
        })
    }

    listIndex++;
    if (listIndex < serviceIconsChunk.length) {
        showViewMoreButton();
    }
    list_icon.attr('data-index', listIndex);
}

showViewMoreButton = () => {
    $('#service_form .service-more').removeClass('hidden');
}

hideViewMoreButton = () => {
    $('#service_form .service-more').addClass('hidden');
}

/*======================SKILL========================*/
dropdownSkillChangeValue = (_this) => {
    $('#skill_skill_id').val(_this.attr('data-id'));
    $('#skill_name').val(_this.text().trim());
}

searchSkill = (_this) => {
    let value = _this.val().trim().replace(/\s+/g, " ").toLowerCase();
    let skillList = $('#skill_form .skill__list-suggestion .dropdown-item');

    skillList.addClass('hidden');

    for (let i = 0; i < skillList.length; i++) {
        let item = $(skillList[i]);
        let temp = item.html().trim().replace(/\s+/g, " ").toLowerCase();

        if (temp.includes(value)) {
            item.removeClass('hidden');
        }
    }
}

/*======================KNOWLEDGE========================*/
addnewKnowledgeItemInForm = () => {
    let length = Object.keys(knowledgeList).length;
    if (length < 10) {
        let key = generateKey();

        knowledgeList[key] = '';

        generateKnowledgeInModal(key);
    }
}

inputNewKnowledgeModal = (_this) => {
    let id = _this.parents().eq(1).attr('data-id');
    knowledgeList[id] = _this.val();
}

deleteKnowledgeItemInModal = (key) => {
    modalClearFormErr();
    if ($('#knowledge_add_form .knowledge-item').length > 1) {
        $(`.knowledge-item[data-id="${key}"]`).remove();
        delete knowledgeList[key];
    } else {
        $(`#knowledge_add_${key}_name`).val('');
    }
}

/*======================GALLERY========================*/
galleryLoadImage = (_this) => {
    let files = _this[0].files;
    let gallery_add_list_error = $('#gallery_add_list_error');

    gallery_add_list_error.html('');

    for (let i = 0; i < files.length; i++) {

        if (Object.keys(galleryImageList).length >= 12) {
            let html = `<span style="display:block;">${gallery_more_than_12_item}</span>`;

            gallery_add_list_error.append(html);
            break;
        }

        let file = files[i];
        if (file.size > 2097152) {
            let html = `<span style="display:block;">${image} ${file.name} ${too_2mb}</span>`;

            gallery_add_list_error.append(html);
        } else if (file.type !== "image/jpeg" && file.type !== "image/png" && file.type !== "image/jpg") {
            let html = `<span style="display:block;">${image} ${file.name} ${gallery_incorrect_format}</span>`;

            gallery_add_list_error.append(html);
        } else {
            let fileReader = new FileReader();
            let key = generateKey();

            galleryImageList[key] = file;

            fileReader.onload = ((fileParam) => {
                return (e) => {
                    generateGalleryItemInModal(key, e.target.result, fileParam.name);
                }
            })(file);
            fileReader.readAsDataURL(file);
        }
    }
    _this.val('');
}

deleteGalleryAddItem = (key) => {
    delete galleryImageList[key];
    $(`#gallery_add_${key}`).remove();
    modalClearFormErr();
}
