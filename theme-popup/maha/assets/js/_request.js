/*================================== COMMON REQUEST ================================================*/

getFormRequest = (url, formData = null) => {
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'json',
            data: formData,
            global: false
        }).done(function (response) {
            if (response.statusCode >= 200 || response.statusCode < 300) {
                resolve(response);
            } else {
                reject(response);
            }
        }).fail(function (error) {
            reject(error);
        });
    });
};

postFormRequest = (url, formData) => {
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: url,
            type: 'POST',
            data: formData,
            dataType: 'json',
        }).done(function (response) {
            if (response.statusCode >= 200 || response.statusCode < 300) {
                resolve(response);
            } else {
                reject(response);
            }
        }).fail(function (error) {
            reject(error);
        });
    });
};

postFileUploadFormRequest = (url, formData) => {
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: url,
            type: 'POST',
            data: formData,
            dataType: 'json',
            cache: false,
            contentType: false,
            processData: false,
        }).done(function (response) {
            if (response.statusCode >= 200 || response.statusCode < 300) {
                resolve(response);
            } else {
                reject(response);
            }
        }).fail(function (error) {
            reject(error);
        });
    });
};

putFormRequest = (url, formData) => {
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: url,
            type: 'PUT',
            data: formData,
            dataType: 'json',
        }).done(function (response) {
            if (response.statusCode >= 200 || response.statusCode < 300) {
                resolve(response);
            } else {
                reject(response);
            }
        }).fail(function (error) {
            reject(error);
        });
    });
};

deleteFormRequest = (url) => {
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: url,
            type: 'DELETE',
            dataType: 'json',
        }).done(function (response) {
            if (response.statusCode >= 200 || response.statusCode < 300) {
                resolve(response.data);
            } else {
                reject(action_failed);
            }
        }).fail(function () {
            reject(action_failed);
        });
    });
};
