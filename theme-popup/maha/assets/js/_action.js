generateKnowledgeInModal = (key) => {
    $('#knowledge_add_form .list_knowledge').append(`
        <div data-id="${key}" class="knowledge-item">
            <label class="popup-form-control form-knowledge">
                <input type="text" id="knowledge_add_${key}_name"
                        name="content" required
                        data-id="${key}"
                       oninput="inputNewKnowledgeModal($(this))"
                       class="form-input color-border-focus">
                <button type="button" 
                    onclick="deleteKnowledgeItemInModal('${key}')"
                    class="form-knowledge__button delete-button">
                    <i class="fas fa-trash"></i>
                </button>
                <button type="button" 
                    onclick="addnewKnowledgeItemInForm()"
                    class="form-knowledge__button addnew-button">
                    <i class="fas fa-plus"></i>
                </button>
            </label>
            <p id="klnew_${key}_error" class="form-error"></p>
        </div>
    `);
}

/*======================GALLERY========================*/
generateGalleryItemInModal = (key, src, name) => {
    $('#gallery_add_form .input-gallery-list').append(`
        <div class="col-md-4 col-xs-6 list-col" id="gallery_add_${key}">
            <div class="gallery-item">
                <div style="background-image: url(${src})"
                        class="gallery-item__image">
                </div>
                <div class="gallery-item__title">
                    ${name}
                </div>
                <button type="button"
                onclick="deleteGalleryAddItem('${key}')"
                 class="gallery-item__delete">
                    <i class="fas fa-trash"></i>
                </button>
            </div>
        </div>
    `)
}

bindingNewItem = (data, prefixClass) => {
    let timeline = null;

    switch (prefixClass) {
        case 'service':
            $('.service-list').prepend(`
                <div id="service_${data.id}" class="col-sm-4">
                    <div class="mh-service-item shadow-1 dark-bg position-relative timeline-edit wow fadeInUp"
                         data-wow-duration="0.8s"
                         data-wow-delay="0.3s">
                        <div class="fa color-text service-${data.id}-icon_class">
                            <i class="${data.icon}"></i>
                        </div>
                        <h3 class="service-${data.id}-name">${data.name}</h3>
                        <p class="service-${data.id}-description">${data.description}</p>
                        <div class="edit-area">
                            <button type="button"
                                    data-id="${data.id}"
                                    data-icon="${data.icon}"
                                    data-name="${data.name}"
                                    data-description="${data.description}"
                                    data-url="${data.updateUrl}"
                                    onclick="modalOpen($(this), 'service', 'edit')"
                                    class="edit-area__button color-background-hover service-${data.id}-button_edit">
                                <i class="fas fa-pen"></i>
                            </button>
                            <button type="button"
                                    data-type="service"
                                    data-id=${data.id}"
                                    data-url="${data.deleteUrl}"
                                    onclick="deleteItem($(this))"
                                    class="edit-area__button color-background-hover service-${data.id}-button_delete">
                                <i class="fas fa-trash"></i>
                            </button>
                        </div>
                    </div>
                </div>
            `);

            $(`.service-${data.id}-name`).text(data.name);
            $(`.service-${data.id}-description`).text(data.description);
            break;

        case 'skill':
            $('.skill-list').prepend(`
                <div id="skill_${data.id}" class="candidatos skill-edit">
                    <div class="parcial edit-view">
                        <div class="info">
                            <div class="nome skill-${data.id}-name"></div>
                            <div class="percentagem-num skill-${data.id}-score"></div>
                        </div>
                        <div class="progressBar skill-${data.id}-percentage">
                            <div class="percentagem skill-percentage" style="width: ${data.score}%;"></div>
                        </div>
                    </div>
                    <div class="edit-area">
                        <button type="button"
                                data-id="${data.id}"
                                data-name="${data.name}"
                                data-score="${data.score}"
                                data-url="${data.updateUrl}"
                                onclick="modalOpen($(this), 'skill', 'edit')"
                                class="edit-area__button color-background-hover skill-${data.id}-button_edit">
                            <i class="fas fa-pen"></i>
                        </button>
                        <button type="button"
                                data-type="skill"
                                data-id="${data.id}"
                                data-url="${data.deleteUrl}"
                                onclick="deleteItem($(this))"
                                class="edit-area__button color-background-hover skill-${data.id}-button_delete">
                            <i class="fas fa-trash"></i>
                        </button>
                    </div>
                </div>
            `);

            $(`.skill-${data.id}-name`).text(data.name);
            $(`.skill-${data.id}-score`).text(`${data.score}%`);
            break;

        case 'knowledge':

            let output = ``;

            data.forEach(item => {
                if ($(`#knowledge_${item.id}`).length === 0) {
                    output += `
            <li id="knowledge_${item.id}" class="info knowledge-edit">
                <span class="name knowledge-${item.id}-content"></span>
                <span class="edit-area__button color-text-hover"
                      data-id="${item.id}"
                      data-content="${item.content}"
                      data-url="${item.updateUrl}"
                      onclick="modalOpen($(this), 'knowledge_edit', 'edit')">
                    <i class="fas fa-pen"></i>
                </span>
                <span 
                    data-type="knowledge"
                    data-id="${item.id}"
                    data-url="${item.deleteUrl}"
                    onclick="deleteItem($(this))"
                    class="edit-area__button color-text-hover">
                    <i class="fas fa-trash"></i>
                </span>
            </li>
        `
                } else {
                    bindingData(item, 'knowledge');
                }
            });

            $('.knowledge-list').prepend(output);

            data.forEach(item => {
                $(`.knowledge-${item.id}-content`).text(item.content);
            });
            break;

        case 'experience':

            $('.experience-list').prepend(`
                <div id="experience_${data.id}" class="mh-work-item dark-bg wow position-relative timeline-edit fadeInUp"
                     data-wow-duration="0.8s"
                     data-wow-delay="0.4s">
                    <h4>
                        <span class="exp-title color-text experience-${data.id}-job_title"></span> 
                        From 
                        <span class="exp-title color-text experience-${data.id}-company"></span>
                    </h4>
                    <div class="mh-eduyear color-text experience-${data.id}-timeline"></div>
                    <p class="experience-${data.id}-description"></p>
                    <div class="edit-area">
                        <button type="button"
                                data-id="${data.id}"
                                data-job_title="${data.job_title}"
                                data-is_current_working="${data.is_current_working ? 1 : 0}"
                                data-start_time="${data.start_time}"
                                data-end_time="${data.end_time}"
                                data-company="${data.company}"
                                data-description="${data.description}"
                                data-url="${data.updateUrl}"
                                onclick="modalOpen($(this), 'experience', 'edit')"
                                class="edit-area__button color-background-hover experience-${data.id}-button_edit">
                            <i class="fas fa-pen"></i>
                        </button>
                        <button type="button"
                                data-type="experience"
                                data-id="${data.id}"
                                data-url="${data.deleteUrl}"
                                onclick="deleteItem($(this))"
                                class="edit-area__button color-background-hover experience-${data.id}-button_delete">
                            <i class="fas fa-trash"></i>
                        </button>
                    </div>
                </div>
            `);

            timeline = data.start_time.replace('-', '/') + ' - ';
            if (data.end_time) {
                timeline += data.end_time.replace('-', '/');
            } else {
                timeline += 'Hiện tại'
            }

            $(`.experience-${data.id}-timeline`).text(timeline);
            $(`.experience-${data.id}-company`).text(data.company);
            $(`.experience-${data.id}-job_title`).text(data.job_title);
            $(`.experience-${data.id}-description`).text(data.description);
            break;

        case 'education':
            $('.education-list').prepend(`
        <div id="education_${data.id}" 
             class="mh-education-item dark-bg wow fadeInUp position-relative timeline-edit"
             data-wow-duration="0.8s"
             data-wow-delay="0.3s">
            <h4>
                <span class="color-text education-${data.id}-majors"></span> 
                From 
                <span class="font-italic color-text education-${data.id}-studying_area"></span>
            </h4>
            <div class="mh-eduyear education-${data.id}-timeline"></div>
            <p class="education-${data.id}-description"></p>
            <div class="edit-area">
                <button type="button"
                        data-id="${data.id}"
                        data-majors="${data.majors}"
                        data-start_time="${data.start_time}"
                        data-end_time="${data.end_time}"
                        data-studying_area="${data.studying_area}"
                        data-description="${data.description}"
                        data-url="${data.updateUrl}"
                        onclick="modalOpen($(this), 'education', 'edit')"
                        class="edit-area__button color-background-hover education-${data.id}-button_edit">
                    <i class="fas fa-pen"></i>
                </button>
                <button type="button"
                        data-type="education"
                        data-id="${data.id}"
                        data-url="${data.deleteUrl}"
                        onclick="deleteItem($(this))"
                        class="edit-area__button color-background-hover education-${data.id}-button_delete">
                    <i class="fas fa-trash"></i>
                </button>
            </div>
        </div>
    `);

            timeline = data.start_time.replace('-', '/') + ' - ' + data.end_time.replace('-', '/');
            $(`.education-${data.id}-timeline`).text(timeline);
            $(`.education-${data.id}-majors`).text(data.majors);
            $(`.education-${data.id}-studying_area`).text(data.studying_area);
            $(`.education-${data.id}-description`).text(data.description);

    }
}