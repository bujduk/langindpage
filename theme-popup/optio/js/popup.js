
const $ = jQuery;
let galleryImageList = [];
let serviceIconsRaw = [];
let serviceIconsChunk = [];
let serviceIconCurrent = '';
let knowledgeList = [];

$(document).ready(() => {

    let infoModal = $('#infoModal');
    let aboutModal = $('#aboutModal');
    let editServiceModal = $('#serviceModal');
    let editSkillModal = $('#addSkillModal');
    let newKnowledgeModal = $('#newKnowledgeModal');
    let editKnowledgeModal = $('#editKnowledgeModal');
    let editExperienceModal = $('#experienceModal');
    let editEducationModal = $('#educationModal');
    let editContactModal = $('#contactModal');
    let addGalleryModal = $('#addGalleryModal');
    let editGalleryModal = $('#editGalleryModal');
    let editProjectModal = $('#projectModal');

    /*Remove Time-to*/
    $('.optio-input__currentdate').on('change', (e) => {
        let timeTo = $('.time-to') ;

        if (!timeTo.hasClass('hidden')) {
            timeTo.addClass('hidden');
            $('#experience_end_month').removeClass('error');
            $('#experience_end_year').removeClass('error');

        } else {
            timeTo.removeClass('hidden');
        }
    })


    /*Edit avatar*/
    let editImageButton = $('.hero-img .default-btn');

    editImageButton.on('click', () => {
        $('#about-img_avatar').trigger("click");
    })

    /*editHomeModal on close*/
    aboutModal.on('hidden.bs.modal', () => {
        closeEditAboutModal();
    })

    /*aboutModal on close*/
    infoModal.on('hidden.bs.modal', () => {
        closeEditInfoModal();
    })

    /*serviceModal on close*/
    editServiceModal.on('hide.bs.modal', () => {
        $('#form_edit_service .optio-input__service--list').html('');
    });

    editServiceModal.on('hidden.bs.modal', () => {
        closeEditServiceModal();
        $('.optio-input__service').removeClass('border-error');
        $('.modal__input--warning').html("");
    })

    /*editToolSkillModal on close*/
    editSkillModal.on('hidden.bs.modal', () => {
        closeEditSkillModal();
    })

    /*editExperienceModal on close*/
    editExperienceModal.on('hidden.bs.modal', () => {
        closeEditExperienceModal();
        $('#experience_start_month').removeClass('border-error');
        $('#experience_end_month').removeClass('border-error');
        $('.modal__input--warning').html("");
    })

    /*editEducationModal on close*/
    editEducationModal.on('hidden.bs.modal', () => {
        closeEditEducationModal();
        $('#education_start_month').removeClass('border-error');
        $('#education_end_month').removeClass('border-error');
        $('.modal__input--warning').html("");
    })

    /*gallerymodal close*/
    addGalleryModal.on('hidden.bs.modal', () => {
        closeAddGalleryModal();
    })

    editGalleryModal.on('hidden.bs.modal', () => {
        closeEditGalleryModal();
    })

    /*contactModal close*/
    editContactModal.on('hidden.bs.modal', () => {
        closeEditContactModal();
    })

    /*editProjectModal on close*/
    editProjectModal.on('hidden.bs.modal', () => {
        closeEditProjectModal();
        $('#project_start_month').removeClass('border-error');
        $('#project_end_month').removeClass('border-error');
        $('.modal__input--warning').html("");
    })

    $('#gallery_file').on('change', (e) => {
        galleryLoadImage($(e.target));
    })

    /* Skill Search Item */
    $('#skill_name').on('input', (e) => {
        searchSkill($(e.target));
    });

    /*Icon Service select*/
    $('#form_edit_service').on('click', '.optio-input__service--list i', (e) => {
        let _this = $(e.target);
        let icon = $('#icon');

        $('#form_edit_service .optio-input__service--list i').removeClass('selected');
        serviceIconCurrent = '';

        if (!_this.hasClass(icon.val())) {
            let name = _this.attr('title');

            icon.val(name);
            _this.addClass('selected');
        } else {
            icon.val('');
        }
    })

    /*Icon Service Search*/
    $('#icon_search').on('input change', async (e) => {
        let value = $(e.target).val();

        await searchAndChunkService(value);
        await generateServiceIcon();
    })

    init();

    /*Tool Skill Range*/
    let addNewSkillRange = $('.valueSpan');
    $('#tool_skill_slider').on('input change', (e) => {
        addNewSkillRange.html($(e.target).val());
    });

    /* newKnowledgeModal on close */
    newKnowledgeModal.on('hidden.bs.modal', () => {
        closeNewKnowledgeModal();
    });
    editKnowledgeModal.on('hidden.bs.modal', () => {
        closeEditKnowledgeModal();
    });

    /* Experience check current time */
    $('#experience_is_current_working').on('change', (e) => {
        let experienceEnd = $('.js_experience_end');

        if ($(e.target).is(':checked')) {
            experienceEnd.addClass('hidden');

            $(`#${$('#experience_end_month').attr('aria-describedby')}`).remove();
            $(`#${$('#experience_end_year').attr('aria-describedby')}`).remove();

        } else {
            experienceEnd.removeClass('hidden');
        }
    });


    /*======Select 2===========*/
    $(".js-example-tokenizer").select2({
        tags: true,
        tokenSeparators: [',', ' ']
    })
})

/*Init Data*/
init = () => {
    // Get Service icon after page loaded
    $.getJSON("./data-icon/icon.json", function (result) {
        $.each(result, function (name, value) {
            serviceIconsRaw.push(value);
        });
    });
}

/* Generate key */
generateKey = () => {
    return Math.random().toString(20).substr(2, 10);
}

closePopup = () => {
    let forms = $('form');

    for( let i = 0; i < forms.length; i++) {
        forms[i].reset();
    }

    $('.btn__popup').removeClass('btn-active');
    $('.item-option').removeClass('item-option--active');
    clearError();
}

/*editServiceModal*/

searchAndChunkService = (index) => {
    let value = index.trim().replace(/\s+/g, "-").toLowerCase();
    let listSearch = serviceIconsRaw.filter(item => item.includes(value));

    chunkServiceIcon(listSearch);
}

chunkServiceIcon = (listIcon, haveLead = false) => {
    let list_icon = $('#form_edit_service .optio-input__service--list');
    let serviceIndex = -1;
    let chunk = 100;

    serviceIconList = [];
    serviceIconsChunk = [];
    list_icon.html('');

    if (haveLead) {
        listIcon.forEach((item, key) => {
            if (item === serviceIconCurrent) serviceIndex = key;
        })
    }

    for (let i = (serviceIndex !== -1) ? serviceIndex : 0; (listIcon && i < listIcon.length); i += chunk) {
        let temp = listIcon.slice(i, i + chunk);
        serviceIconsChunk.push(temp);
    }

    list_icon.attr('data-index', 0);
}

generateServiceIcon = () => {
    let list_icon = $('#form_edit_service .optio-input__service--list');
    let listIndex = list_icon.attr('data-index');

    hideViewMoreButton();

    if (serviceIconsChunk && serviceIconsChunk.length > 0) {
        serviceIconsChunk[listIndex].forEach(item => {
            list_icon.append(`
                <i class="${item}${serviceIconCurrent === item ? ' selected' : ''}" title="${item}"></i>
            `)
        })
    }

    listIndex++;
    if (listIndex < serviceIconsChunk.length) {
        showViewMoreButton();
    }
    list_icon.attr('data-index', listIndex);
}

showViewMoreButton = () => {
    $('#form_edit_service .service-more').removeClass('hidden');
}

hideViewMoreButton = () => {
    $('#form_edit_service .service-more').addClass('hidden');
}

/*Clear Service Icon*/
clearServiceIcon = () => {
    let serviceIcon = $('.js_optio-input__service');

    serviceIcon.html('');
}

/*Show button Xem Thêm of Service Icon*/
showViewMoreButton = () => {
    $('.service-more').removeClass('hidden');
}

/*Hide button Xem Thêm of Service Icon*/
hideViewMoreButton = () => {
    $('.service-more').addClass('hidden');
}

/*edit skill*/

/*Thay đổi giá trị của dropdown
element là chính element được click
className là class hoặc id của ô input cần gán giá trị*/
dropdownChangeValue = (_this, className) => {
    let item = $(`${className}`);
    item.attr('data-id', _this.attr('data-id')).val(_this.html().trim());
}

/*Hiển thị giá trị của input range*/
rangeChangeValue = (_this, className) => {
    let item = $(`${className}`);

    item.html(_this.val());
}

/*Clear Input error and error text*/
clearError = () => {
    $('label.error').html('');
    $('input.error').removeClass('error');
    $('textarea.error').removeClass('error');
    $('.tooltip').remove();
    $('.btn__popup').removeClass('btn-active');
    $('.item-option').removeClass('item-option--active');
}

$(function () {
    $.fn.serializeObject = function () {
        let o = {};
        let a = this.serializeArray();

        $.each(a, function () {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
});


/*Mở popup Home*/
openEditAboutModal = () => {
    let formData = $('#form_edit_about').serializeObject();
    let aboutEditButton = $('.optio__edit--home');

    for (let [key] of Object.entries(formData)) {
        $(`#home_${key}`).val(aboutEditButton.attr(`data-${key}`));
    }

    $('#aboutModal').modal('show');
}

/*Close popup Home*/
closeEditAboutModal = () => {
    $('#form_edit_about')[0].reset();
    closePopup();
}

/*Mở popup about*/
openEditInfoModal = (_this) => {
    let formData = $('#form_edit_info').serializeObject();

    for (let [key] of Object.entries(formData)) {
        $(`#info_${key}`).val(_this.attr(`data-${key}`));
    }

    _this.addClass('btn-active');

    $('#infoModal').modal('show');
}

/*Close popup about*/
closeEditInfoModal = () => {
    $('#form_edit_info')[0].reset();
    closePopup();
}

/*Open popup Service*/
openAddNewServiceModal = async (_this) => {
    let form_edit_service = $('#form_edit_service');

    form_edit_service.attr('data-type', 'add');
    await chunkServiceIcon(serviceIconsRaw);
    await generateServiceIcon();
    _this.addClass('btn-active');
    $('#serviceModal').modal('show');
}

openEditServiceModal = async (_this) => {
    let form_edit_service = $('#form_edit_service');
    let formData = form_edit_service.serializeObject();

    serviceIconCurrent = _this.attr('data-icon');

    for (let [key] of Object.entries(formData)) {
        $(`#service_${key}`).val(_this.attr(`data-${key}`));
    }
    form_edit_service.attr('data-type', 'edit');


    await chunkServiceIcon(serviceIconsRaw, true);
    await generateServiceIcon();
    _this.addClass('btn-active');
    _this.parent().addClass('item-option--active');
    $('#serviceModal').modal('show');
}

generateNewService = (data) => {
    $('.service-list').prepend(`
        <div class=" col-lg-3 col-md-6 col-sm-6 " id="service_${data.id}">
            <div class="service-block service-item service-edit">
                   
                    <div class="item-option">
                        <button type="button"
                                data-id="${data.id}"
                                data-icon="${data.icon}"
                                data-name="${data.name}"
                                data-description="${data.description}"
                                data-updateUrl="${data.updateUrl}"
                                onclick="openEditServiceModal($(this))"
                                class="item-option__edit btn__popup service-${data.id}-button_edit">
                            <i class="fas fa-pen"></i>
                        </button>
                        <button type="button"
                                data-type="service"
                                data-id="${data.id}"
                                data-deleteUrl="${data.deleteUrl}"
                                onclick="deleteItem($(this))"
                                class="item-option__delete btn__popup service-${data.id}-button_delete">
                            <i class="fas fa-trash"></i>
                        </button>
                    </div>
                    <span class="service-image service-${data.id}-icon_class">
                        <i class="${data.icon}"></i>
                    </span>
                    <h4 class="service-${data.id}-name">${data.name}</h4>
                    <p class="service-${data.id}-description">${data.description}</p>
                </div>
        </div>
    `)
}

/*Close popup Service*/
closeEditServiceModal = () => {
    let form_edit_service = $('#form_edit_service');

    form_edit_service[0].reset();
    $('#form_edit_service .optio-input__service--list').html('');
    form_edit_service.attr('data-type', '');
    closePopup();
}

/*Open popup ToolSkill*/
openAddSkillModal = (_this) => {
    _this.addClass('btn-active');
    _this.parent().addClass('item-option--active');
    $('#addSkillModal').modal('show');
}

searchSkill = (_this) => {
    let value = _this.val().trim().replace(/\s+/g, " ").toLowerCase();
    let skillList = $('.skill__list-suggestion .dropdown-item');

    _this.attr('data-id', null);
    skillList.addClass('hidden');

    for (let i = 0; i < skillList.length; i++) {
        let item = $(skillList[i]);
        let temp = item.html().trim().replace(/\s+/g, " ").toLowerCase();
        if (temp.includes(value)) {
            item.removeClass('hidden');
        }
    }
}

openEditSkillModal = (_this) => {
    let formData = $('#form_edit_skill').serializeObject();

    for (let [key] of Object.entries(formData)) {
        $(`#editToolSkill_${key}`).val(_this.attr(`data-${key}`));
    }
    _this.addClass('btn-active');
    _this.parent().addClass('item-option--active');
    $('#addSkillModal').modal('show');
}

const generateNewSkill = (data) => {
    $('.toolSkill-list').prepend(`
        <div class="skill-item" id="skill_${data.id}">
            <div class="item-option">
                <button type="button"
                        data-id="${data.id}"
                        data-name="${data.name}"
                        data-score="${data.score}"
                        data-updateUrl="${data.updateUrl}"
                        onclick="openEditSkillModal($(this))"
                        class="item-option__edit btn__popup optio__edit--toolSkill 
                        toolSkill-${data.id}-button_edit">
                    <i class="fas fa-pen"></i>
                </button>
                <button type="button"
                        data-type="tool_skill"
                        data-id="${data.id}"
                        data-deleteUrl="${data.deleteUrl}"
                        onclick="deleteItem($(this))"
                        class="item-option__delete btn__popup skill-${data.id}-button_delete">
                    <i class="fas fa-trash"></i>
                </button>
            </div>
            <div class="edit-view">
                <p class="skill-${data.id}-name">${data.name}</p>
                <div id="skill-percent-${data.id}"
                     data-value="${data.score}"
                     class="skill-wrap skill-${data.id}-percentage">
                    <div class="skill-percent wow slideInLeft" style="width: ${data.score}%"></div>
                </div>
            </div>
        </div>
    `);
}

/*Close popup ToolSkill*/
closeEditSkillModal = () => {
    $('#form_edit_skill')[0].reset();
    closePopup();
}

/* ===== newKnowledgeModal ===== */
openNewKnowledgeModal = (_this) => {
    addnewKnowledgeItemInForm();
    _this.addClass('btn-active');
    $('#newKnowledgeModal').modal('show');
}

closeNewKnowledgeModal = () => {
    $('#newKnowledgeModal .list_knowledge').html('');
    closePopup();
    knowledgeList = [];
}

generateNewKnowledge = (list) => {
    let output = ``;

    list.forEach(data => {
        output += `
             <li id="knowledge_${data.id}" class="knowledge-edit">
                <span class="knowledge__item--icon">
                    <i class="fas fa-check"></i>
                </span>
                <span class="knowledge-${data.id}-content">${data.content}</span>
                <div class="item-option">
                    <button
                        type="button" 
                        data-id="${data.id}"
                        data-content="${data.content}"
                        data-updateUrl="${data.updateUrl}"
                        onclick="openEditKnowledgeModal($(this))"
                        class="edit-area__button btn__popup knowledge-${data.id}-button_edit">
                        <i class="fas fa-pen"></i>
                    </button>
                    <button
                        type="button" 
                        data-type="knowledge"
                        data-id="${data.id}"
                        data-deleteUrl="${data.deleteUrl}"
                        onclick="deleteItem($(this))"
                        class="edit-area__button btn__popup knowledge-${data.id}-button_edit">
                        <i class="fas fa-trash"></i>
                    </button>
                </div>
            </li>
        `
    })

    $('.knowledge-list').prepend(output);
}

addnewKnowledgeItemInForm = () => {
    let length = Object.keys(knowledgeList).length;
    if (length < 10) {
        let key = generateKey();
        knowledgeList[key] = '';
        $('#form_new_knowledge .list_knowledge').append(`
        <div data-id="${key}" class="knowledge-item modal__body--item">
            <label class="optio-form-control form-knowledge">
                <input type="text" id="input_knowledge_${key}_content"
                        name="content"
                        data-id="${key}"
                       oninput="inputNewKnowledgeModal($(this))"
                       class="form-input modal__body--input">
                <button type="button" 
                    onclick="deleteKnowledgeItemInForm('${key}')"
                    class="form-knowledge__button delete-button btn__popup">
                    <i class="fas fa-trash"></i>
                </button>
                <button type="button" 
                    onclick="addnewKnowledgeItemInForm()"
                    class="form-knowledge__button addnew-button btn__popup">
                    <i class="fas fa-plus"></i>
                </button>
            </label>
            <p id="klnew_${key}_error" class="form-error"></p>
        </div>
    `);
    }
}

inputNewKnowledgeModal = (_this) => {
    let id = _this.parents().eq(1).attr('data-id');
    knowledgeList[id] = _this.val();
}

deleteKnowledgeItemInForm = (key) => {
    $(`.knowledge-item[data-id="${key}"]`).remove();
    delete knowledgeList[key];
    closePopup();
    if ($('#form_new_knowledge .knowledge-item').length === 0) {
        addnewKnowledgeItemInForm();
    }
}
/* ===== End newKnowledgeModal ===== */

/* ===== editKnowledgeModal ===== */
openEditKnowledgeModal = (_this) => {
    let _form = $('#form_edit_knowledge');
    let formData = _form.serializeObject();

    for (let [key] of Object.entries(formData)) {
        $(`#knowledge_edit_${key}`).val(_this.attr(`data-${key}`));
    }
    _form.attr(`data-id`, _this.attr(`data-id`));
    _form.attr(`data-url`, _this.attr(`data-updateUrl`));
    _this.addClass('btn-active');
    _this.parent().addClass('item-option--active');
    $('#editKnowledgeModal').modal('show');
}

closeEditKnowledgeModal = () => {
    $('#form_edit_knowledge').attr('data-id', '');
    $('#edit_content').val('');
    closePopup();
}
/* ===== End editKnowledgeModal ===== */

/*Open popup Education*/
openAddEducation = (_this) => {
    _this.addClass('btn-active');
    $('#educationModal').modal('show');
}

openEditEducation = (_this) => {
    let formData = $('#form_edit_education').serializeObject();

    for (let [key] of Object.entries(formData)) {
        $(`#education_${key}`).val(_this.attr(`data-${key}`));
    };

    _this.addClass('btn-active');
    _this.parent().addClass('item-option--active');
    $('#educationModal').modal('show');
}


const generateNewEducation = (data) => {
    let time = convertTime(data.start_time, data.end_time);

    $('.education-list').prepend(`
        <div class="col-lg-4 col-md-6 col-sm-6" id="education_${data.id}">
            <div class="education-single">
                <div class="item-option">
                    <button type="button"
                            data-id="${data.id}"
                            data-majors="${data.majors}"
                            data-start_month="${time['start_month']}"
                            data-start_year="${time['start_year']}"
                            data-end_month="${time['end_month']}"
                            data-end_year="${time['end_year']}"
                            data-studying_area="${data.studying_area}"
                            data-description="${data.description}"
                            data-urlUpdate="${data.urlUpdate}"
                            onclick="openEditEducation($(this))"
                            class="item-option__edit btn__popup education-${data.id}-button_edit">
                        <i class="fas fa-pen"></i>
                    </button>
                    <button type="button"
                            data-type="education"
                            data-id="${data.id}"
                            data-urlDelete="${data.urlDelete}"
                            onclick="deleteItem($(this))"
                            class="item-option__delete btn__popup education-${data.id}-button_delete">
                        <i class="fas fa-trash"></i>
                    </button>
                </div>
            <h4 class="course-name education-${data.id}-majors">${data.majors}</h4>
            <p class="institute education-${data.id}-timeline">${data.start_time.replace('-', '/')} - ${(data.end_time.replace('-', '/'))}</p>
            <p class="mute-text education-${data.id}-studying_area">${data.studying_area}</p>
            <p class="mute-text education-${data.id}-description">${data.description}</p>            
            </div>
        </div>
    `)
}

/*Close popup Education*/
closeEditEducationModal = () => {
    $('#form_edit_education')[0].reset();
    closePopup();
}

/*Open popup Education*/
openAddExperience = (_this) => {
    let _form = $('#form_edit_experience');
    _this.addClass('btn-active');
    _form.attr('data-type', 'add');
    $('#experienceModal').modal('show');
}

openEditExperience = (_this) => {
    let _form = $('#form_edit_experience');
    let formData = _form.serializeObject();

    _form.attr('data-type', 'edit');
    for (let [key] of Object.entries(formData)) {
        console.log(key)
        $(`#experience_${key}`).val(_this.attr(`data-${key}`));
    };

    if (_this.attr(`data-is_current_working`) === "true") {
        $('#experience_is_current_working').attr('checked', true);
        $('.js_experience_end').addClass('hidden');
    };

    _this.addClass('btn-active');
    _this.parent().addClass('item-option--active');

    $('#experienceModal').modal('show');
}



const generateNewExperience = (data) => {
    let time = convertTime(data.start_time, data.end_time);

    $('.experience-list').prepend(`
        <div class="col-md-6" id="experience_${data.id}">
            <div class="experience-single">
                <div class="item-option">
                    <button type="button"
                        data-id="${data.id}"
                        data-job_title="${data.job_title}"
                        data-start_month="${time['start_month']}"
                        data-start_year="${time['start_year']}"
                        data-end_month="${time['end_month']}"
                        data-end_year="${time['end_year']}"
                        data-is_current_working="${data.is_current_working}"
                        data-company="${data.company}"
                        data-description="${data.description}"
                        data-urlUpdate="${data.urlUpdate}"
                        onclick="openEditExperience($(this))"
                        class="item-option__edit btn__popup optio__edit--experience
                         experience-${data.id}-button_edit">
                        <i class="fas fa-pen"></i>
                    </button>
                    <button type="button"
                        data-type="experience"
                        data-id="${data.id}"
                        data-urlDelete="${data.urlDelete}"
                        onclick="deleteItem($(this))"
                        class="item-option__delete btn__popup experience-${data.id}-button_delete">
                        <i class="fas fa-trash"></i>
                    </button>
                </div>
                <h4 class="company experience-${data.id}-company">${data.company}</h4>
                <p class="position-name experience-${data.id}-job_title">${data.job_title}</p>
                <span class="mute-text experience-${data.id}-timeline">
                    ${data.start_time.replace('-', '/')} - ${ (data.is_current_working) ? 'Current' : data.end_time.replace('-', '/')}
                </span>
                <p class="company-info experience-${data.id}-description">${data.description}</p>
            </div>
        </div>
    `)
}

/*Close popup Experience*/
closeEditExperienceModal = () => {
    $('#form_edit_experience')[0].reset();
    $('.time-from .modal__body--input').removeClass('border-error');
    $('.time-to .modal__body--input').removeClass('border-error');
    if ($('.time-to').hasClass('hidden')) {
        $('.time-to').removeClass('hidden');
    }
    $('#experience_is_current_working').attr('checked', false);
    closePopup();
    $('.js_experience_end').removeClass('hidden');
}

/* ===== editGalleryModal ===== */
openAddGalleryModal = () => {
    $('#addGalleryModal').modal('show');
}

galleryLoadImage = (_this) => {
    let files = _this[0].files;
    let gallery_list_error = $('#gallery_list_error');

    gallery_list_error.html('');

    for (let i = 0; i < files.length; i++) {

        if (Object.keys(galleryImageList).length >= 12) {
            let html = `<span style="display:block;">${gallery_more_than_12_item}</span>`;

            gallery_list_error.append(html);
            break;
        }

        let file = files[i];
        if (file.size > 2097152) {
            let html = `<span style="display:block;">${image} ${file.name} ${too_2mb}</span>`;

            gallery_list_error.append(html);
        } else if (file.type !== "image/jpeg" && file.type !== "image/png" && file.type !== "image/jpg") {
            let html = `<span style="display:block;">${image} ${file.name} ${gallery_incorrect_format}</span>`;

            gallery_list_error.append(html);
        } else {
            let fileReader = new FileReader();
            let key = generateKey();

            galleryImageList[key] = file;

            fileReader.onload = ((fileParam) => {
                return (e) => {
                    renderGalleryImageItem(key, e.target.result, fileParam.name);
                }
            })(file);
            fileReader.readAsDataURL(file);
        }
    }
    _this.val('');
}

renderGalleryImageItem = (key, src, name) => {
    $('#form_add_gallery .js_work_image').append(`
        <div class="col-lg-4 col-6 list-col" id="gallery_add_${key}">
            <div class="gallery-item box__drag">
                <div style="background-image: url(${src})"
                        class="gallery-item__image">
                </div>
                <div class="gallery-item__title img__name">
                    ${name}
                </div>
                <button type="button"
                onclick="deleteGalleryAddItem('${key}')"
                 class="work__delete--img delete__img">
                    <i class="fas fa-trash"></i>
                </button>
            </div>
        </div>
    `)
}

deleteGalleryAddItem = (key) => {
    delete galleryImageList['key'];
    $(`#gallery_add_${key}`).remove();
}

closeAddGalleryModal = () => {
    $('#form_edit_gallery .js_work_image').html('');
    galleryImageList = [];
    closePopup();
}

openEditGalleryModal = (_this) => {
    let formData = $('#form_edit_gallery').serializeObject();
    let aboutEditButton = $('.optio__edit--gallery');

    for (let [key] of Object.entries(formData)) {
        $(`#gallery_${key}`).val(aboutEditButton.attr(`data-${key}`));
    }

    _this.addClass('btn-active');
    _this.parent().addClass('item-option--active');

    $('#editGalleryModal').modal('show');
}

closeEditGalleryModal = () => {
    $('#form_edit_gallery')[0].reset();
    closePopup();
}

/*Open popup Project*/
openAddProject = (_this) => {
    let _form = $('#form_edit_project');
    _this.addClass('btn-active');
    _form.attr('data-type', 'add');
    $('#projectModal').modal('show');
}

openEditProject = (_this) => {
    let _form = $('#form_edit_project');
    let formData = _form.serializeObject();

    _form.attr('data-type', 'edit');
    for (let [key] of Object.entries(formData)) {
        console.log(key)
        $(`#project_${key}`).val(_this.attr(`data-${key}`));
    };

    if (_this.attr(`data-is_current_working`) === "true") {
        $('#project_is_current_working').attr('checked', true);
        $('.js_project_end').addClass('hidden');
    };

    _this.addClass('btn-active');
    _this.parent().addClass('item-option--active');

    $('#projectModal').modal('show');
}



generateNewProject = (data) => {
    let timeline = null;

    $('.project__list').prepend(`
        <div class="col-md-9" id="project_${data.id}">
            <div class="project__item">
                <div class="item-option">
                    <button type="button"
                        data-id="${data.id}"
                        data-name="${data.name}"
                        data-start_time="${data.start_time}"
                        data-end_time="${data.end_time}"
                        data-customer="${data.customer}"
                        data-team_size="${data.team_size}"
                        data-job_position="${data.job_position}"
                        data-role="${data.role}"
                        data-freelance="${data.freelance}"
                        data-description="${data.description}"
                        data-url="${data.updateUrl}"
                        onclick="openEditProject($(this))"
                        class="item-option__edit btn__popup optio__edit--project
                         project-${data.id}-button_edit">
                        <i class="fas fa-pen"></i>
                    </button>
                    <button type="button"
                        data-id="${data.id}"
                        data-type="project"
                        data-url="${data.deleteUrl}"
                        onclick="deleteItem($(this))"
                        class="item-option__delete btn__popup project-${data.id}-button_delete">
                        <i class="fas fa-trash"></i>
                    </button>
                </div>
                <!-- TIMELINE TITLE -->
                <h6 class="name project-${data.id}-name"></h6>
                <!-- TIMELINE TITLE INFO -->
                <h6 class="project__item--time">
                    <span class="project-${data.id}-timeline"></span>
                </h6>
                <!-- TIMELINE PARAGRAPH -->
                <p class="mute-text project-${data.id}-description"></p>
                <p class="project__item--content">
                    Client:
                    <span class="project-${data.id}-customer"></span>
                </p>
                <p class="project__item--content">
                    Số lượng thành viên:
                    <span class="project-${data.id}-team_size"></span>
                </p>
                <p class="project__item--content">
                    Vị trí công việc:
                    <span class="project-${data.id}-job_position"></span>
                </p>
                <p class="project__item--content">
                    Vai trò:
                    <span class="project-${data.id}-role"></span>
                </p>
                <div class="stack">
                    <span class="project__item--content">Stack sử dụng: </span>
                    <p class="project-${data.id}-technology_used"></p>
                </div>
            </div>
        </div>
    `)

    timeline = data.start_time.replace('-', '/') + ' - ' + data.end_time.replace('-', '/');
    $(`.project-${data.id}-timeline`).text(timeline);
    $(`.project-${data.id}-name`).text(data.name);
    $(`.project-${data.id}-customer`).text(data.customer);
    $(`.project-${data.id}-description`).text(data.description);
    $(`.project-${data.id}-team_size`).text(data.team_size);
    $(`.project-${data.id}-job_position`).text(data.job_position);
    $(`.project-${data.id}-role`).text(data.role);
    $(`.project-${data.id}-technology_used`).text(data.technology_used);
}

/*Close popup Project*/
closeEditProjectModal = () => {
    $('#form_edit_project')[0].reset();
    $('.time-from .modal__body--input').removeClass('border-error');
    $('.time-to .modal__body--input').removeClass('border-error');
    if ($('.time-to').hasClass('hidden')) {
        $('.time-to').removeClass('hidden');
    }
    $('#project_is_current_working').attr('checked', false);
    closePopup();
    $('.js_project_end').removeClass('hidden');
}

// Delete Item
deleteItem = (element) => {
    let _this = $(element);

    Notiflix.Confirm.Show(
        'Xác nhận',
        'Bạn chắc chắn muốn xoá?',
        'Đồng ý',
        'Huỷ',
        function () { // Yes button callback
            let itemType = _this.attr('data-type');
            let id = _this.attr('data-id');
            $(`#${itemType}_${id}`).remove();
        },
        function () { // No button callback
        }
    );
}