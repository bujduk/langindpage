parseURLParamToKeyValue = (data) => {
    return JSON.parse('{"' + decodeURI(data).replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g,'":"') + '"}')
};

$(function () {
    $.fn.serializeObject = function () {
        let o = {};
        let a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
});

/*
* @param data Dữ liệu data trên server trả về
* @param prefixClass Tiền tố class cho các thành phần
* @valueParam prefixClass in [about, service, education, experience, gallery, skill, knowledge]
*
* @call bindingData(data, 'about')
* */
bindingData = (data, prefixClass) => {
    let _id = (data.id) ? data.id : 0;

    $.each(data, (key, value) => {
        $(`.${prefixClass}-${_id}-${key}`).text(value);
        $(`.${prefixClass}-${_id}-button_edit`).attr(`data-${key}`, value);
    });

    if (prefixClass === 'about') {
        $('.social-links a').addClass('hidden').attr('href', '');
        $(`.about-${_id}-freelance`).html(data.freelance ? 'Available' : 'Not Available');
        $(`.about-${_id}-button_edit`).attr(`data-freelance_editview`, data.freelance ? 'Available' : 'Not Available');
        $(`.about-${_id}-email`).attr('href', `mailto:${data.email}`);

        $.each(data.socials, (key, value) => {
            if (value && value !== '') {
                $(`.about-${_id}-${key}`).attr('href', value).removeClass('hidden');
            }
            $(`.about-${_id}-button_edit`).attr(`data-${key}`, value);
        });
    }

    if (prefixClass === 'education') {
        $(`.education-${_id}-timeline`).html(`${data.start_time.replace('-', '/')} - ${(data.end_time.replace('-', '/'))}`);

        let time = convertTime(data.start_time, data.end_time);
        for (const [key, value] of Object.entries(time)) {
            $(`.education-${_id}-button_edit`).attr(`data-${key}`, value);
        }
    }

    if (prefixClass === 'experience') {
        $(`.experience-${_id}-timeline`).html(`${data.start_time.replace('-', '/')} - ${(data.is_current_working) ? 'Current' : data.end_time.replace('-', '/')}`);

        let time = convertTime(data.start_time, data.end_time);
        for (const [key, value] of Object.entries(time)) {
            $(`.experience-${_id}-button_edit`).attr(`data-${key}`, value);
        }
    }

    if (prefixClass === 'skill') {
        $(`.skill-${_id}-percentage`).attr('data-value', data.score);
        $(`.skill-${_id}-percentage .skill-percentage`).css('width', `${data.score}%`);
    }


}

/* Generate Time input */
convertTime = (start, end) => {
    start = start.split('-');
    end = (end) ? end.split('-') : null;

    return {
        start_month: start[0],
        start_year: start[1],
        end_month: end ? end[0] : '',
        end_year: end ? end[1] : ''
    };
}