let lang = {
  en: {
    required: "This field is required.",
    remote: "Please fix this field.",
    email: "Please enter a valid email address.",
    url: "Please enter a valid URL.",
    date: "Please enter a valid date.",
    dateISO: "Please enter a valid date (ISO).",
    number: "Please enter a valid number.",
    digits: "Please enter only digits.",
    equalTo: "Please enter the same value again.",
    maxlength: $.validator.format( "Please enter no more than {0} characters." ),
    minlength: $.validator.format( "Please enter at least {0} characters." ),
    rangelength: $.validator.format( "Please enter a value between {0} and {1} characters long." ),
    range: $.validator.format( "Please enter a value between {0} and {1}." ),
    max: $.validator.format( "Please enter a value less than or equal to {0}." ),
    min: $.validator.format( "Please enter a value greater than or equal to {0}." ),
    step: $.validator.format( "Please enter a multiple of {0}." ),
    imagerequired: "Select at least one image",
  },
  vi: {
    required: "Không được để trống.",
    remote: "Hãy sửa cho đúng.",
    email: "Email không hợp lệ.",
    url: "Hãy nhập URL.",
    date: "Hãy nhập ngày.",
    dateISO: "Hãy nhập ngày (ISO).",
    number: "Hãy nhập số.",
    digits: "Ký tự nhập phải là một số.",
    maxlength: $.validator.format("Không được nhập quá {0} kí tự."),
    minlength: $.validator.format("Không đươc nhập ít hơn {0} kí tự."),
    rangelength: $.validator.format("Hãy nhập từ {0} đến {1} kí tự."),
    range: $.validator.format("Hãy nhập từ {0} đến {1}."),
    max: $.validator.format("Hãy nhập từ {0} trở xuống."),
    min: $.validator.format("Hãy nhập từ {0} trở lên."),
    imagerequired: "Hãy chọn ít nhất một ảnh",
    iconrequired: "Hãy chọn ít nhất một icon",
  }
};

/* Language Selection */
$.extend($.validator.messages, lang['vi']);

$(document).ready(() => {

  let experience_is_current_working = $('#experience_is_current_working');

  $('#form_edit_about').validate({
    rules: {
      name: {
        required: true,
        maxlength: 30,
      },
      job_title: {
        required: true,
        maxlength: 45,
      }
    },
    onkeyup: false,
    onclick: false
  });

  $('#form_edit_info').validate({
    rules: {
      introduction: {
        required: true,
        maxlength: 230,
      },
      age: {
        required: true,
        min: 0,
        max: 99,
      },
      residence: {
        required: true,
        maxlength: 20,
      },
      address: {
        required: true,
        maxlength: 100,
      },
      email: {
        required: true,
        email: true,
      },
      phone: {
        maxlength: 20,
        minlength: 10,
        digits: true
      }
    },
    onkeyup: false,
    onclick: false
  });

  $('#form_edit_service').validate({
    rules: {
      name: {
        required: true,
        maxlength: 45,
      },
    },

    onkeyup: false,
    onclick: false
  });

  $('#form_edit_skill').validate({
    rules: {
      name: {
        required: true,
        maxlength: 100,
      },
    },

    onkeyup: false,
    onclick: false,
  });

  $('#form_new_knowledge').validate({
    rules: {
      content: {
        required: true,
        maxlength: 100
      }
    },
    onkeyup: false,
    onclick: false
  });

  $('#form_edit_knowledge').validate({
    rules: {
      content: {
        required: true,
        maxlength: 100
      }
    },
    onkeyup: false,
    onclick: false
  });

  $('#form_edit_education').validate({
    rules: {
      majors: {
        required: true,
        maxlength: 50,
      },
      studying_area: {
        required: true,
        maxlength: 100,
      },
      start_month: {
        required: true,
        maxlength: 2,
        min: 1,
        max: 12,
      },
      start_year: {
        required: true,
        maxlength: 4,
        min: 1,
        max: 9999,
      },
      end_month: {
        required: true,
        maxlength: 2,
        min: 1,
        max: 12,
      },
      end_year: {
        required: true,
        maxlength: 4,
        min: 1,
        max: 9999,
      },
      description: {
        maxlength: 500,
      }
    },
    onkeyup: false,
    onclick: false
  });

  $('#form_edit_experience').validate({
    rules: {
      job_title: {
        required: true,
        maxlength: 50,
      },
      company: {
        required: true,
        maxlength: 100,
      },
      start_month: {
        required: true,
        maxlength: 2,
        min: 1,
        max: 12,
      },
      start_year: {
        required: true,
        maxlength: 4,
        min: 1,
        max: 9999,
      },
      end_month: {
        required: () => !experience_is_current_working.is(':checked'),
        maxlength: 2,
        min: 1,
        max: 12,
      },
      end_year: {
        required: () => !experience_is_current_working.is(':checked'),
        maxlength: 4,
        min: 1,
        max: 9999,
      },
      description: {
        maxlength: 500,
      }
    },
    onkeyup: false,
    onclick: false,
  });

  $('#form_add_gallery').validate({
    rules: {
      file: {
        required: true
      },
    },
    messages: {
      file: {
        required: 'Bạn chưa thêm ảnh kìa !'
      }
    },
    onkeyup: false,
    onclick: false
  });

  $('#form_edit_gallery').validate({
    rules: {
      title: {
        required: true,
        maxlength: 100,
      },
    },

    onkeyup: false,
    onclick: false
  });

  $('#form_edit_contact').validate({
    rules: {
      email: {
        required: true,
      },
      address: {
        required: true,
        maxlength: 100,
      },
    },
    onkeyup: false,
    onclick: false
  });
});
