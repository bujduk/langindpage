$(document).ready(() => {
    $('#form_edit_about').submit((e) => {
        e.preventDefault();

        let _this = $(e.target);

        clearError();

        if (_this.valid()) {

            // Response success
            $.getJSON("./response-ex/about.json", (result) => {
                bindingData(result.data, 'about');
            });

            $('#aboutModal').modal('hide');
            $('#contactModal').modal('hide');
            // End Response success

        }
    });

    $('#form_edit_info').submit((e) => {
        e.preventDefault();

        let _this = $(e.target);

        clearError();

        if (_this.valid()) {

            /* Response success */
            $.getJSON("./response-ex/about.json", (result) => {
                bindingData(result.data, 'about');
            });

            $('#infoModal').modal('hide');
            /* End Response success */
        }
    });

    $('#form_edit_education').submit((e) => {
        e.preventDefault();

        let _this = $(e.target);

        clearError();

        if (_this.valid()) {
            /* Response success */
            $.getJSON("./response-ex/education.json", (result) => {
                if (_this.attr('data-type') === 'edit' || $(`#education_${result.data.id}`).length > 0) {
                    bindingData(result.data, 'education');
                } else {
                    generateNewEducation(result.data);
                }
                $('#educationModal').modal('hide');
            });

            /* End Response success */
        }
    });

    $('#form_edit_experience').submit((e) => {
        e.preventDefault();

        let _this = $(e.target);

        clearError();

        if (_this.valid()) {
            /* Response success */
            $.getJSON("./response-ex/experience.json", (result) => {
                if (_this.attr('data-type') === 'edit' || $(`#experience_${result.data.id}`).length > 0) {
                    bindingData(result.data, 'experience');
                } else {
                    generateNewExperience(result.data);
                }
                $('#experienceModal').modal('hide');
            });

            /* End Response success */
        }
    });

    $('#form_new_skill').submit((e) => {
        e.preventDefault();

        let _this = $(e.target);

        clearError();

        if (_this.valid()) {
            /* Response success */
            $.getJSON("./response-ex/skill.json", (result) => {
                if ($(`#skill_${result.data.id}`).length > 0) {
                    bindingData(result.data, 'skill')
                } else {
                    generateNewSkill(result.data);
                }

                $('#newCodingModal').modal('hide');
            });
            /* End Response success */
        }
    });

    $('#form_edit_skill').submit((e) => {
        e.preventDefault();

        let _this = $(e.target);

        clearError();

        if (_this.valid()) {
            /* Response success */
            $.getJSON("./response-ex/skill.json", (result) => {
                bindingData(result.data, 'skill');

                $('#editCodingModal').modal('hide');
            });
            /* End Response success */
        }
    });

    $('#form_new_knowledge').submit((e) => {
        e.preventDefault();

        let _this = $(e.target);

        clearError();

        if (_this.valid()) {
            /* Response success */
            $.getJSON("./response-ex/knowledge.json", (result) => {
                generateNewKnowledge(result.data);

                $('#newKnowledgeModal').modal('hide');
            });
            /* End Response success */
        }
    });


    $('#form_edit_knowledge').submit((e) => {
        e.preventDefault();

        let _this = $(e.target);

        clearError();

        if (_this.valid()) {
            /* Response success */
            $.getJSON("./response-ex/knowledge_edit.json", (result) => {
                bindingData(result.data, 'knowledge');

            $('#editKnowledgeModal').modal('hide');
            });
            /* End Response success */
        }
    });

    $('#form_edit_service').submit((e) => {
        e.preventDefault();

        let listIcon = $('#form_edit_service .optio-input__service');
        let selectedIcon = $('.optio-input__service--list .selected');
        let _this = $(e.target);
        let no_error = true;

        clearError();

        if (selectedIcon.length === 0) {
            no_error = false;
            $('#service_icon_error').html($.validator.messages.iconrequired);
            listIcon.addClass('border-error');
        }

        if (_this.valid() && no_error) {

            /* Response success */
            $.getJSON("./response-ex/service.json", (result) => {
                if (_this.attr('data-type') === 'edit' || $(`#service_${result.data.id}`).length > 0) {
                    bindingData(result.data, 'service');
                } else {
                    generateNewService(result.data);
                }
                $('#serviceModal').modal('hide');
            });
            /* End Response success */
        }
    });

    $('#addGalleryModal').submit((e) => {
        e.preventDefault();
        let _this = $(e.target);

        clearError();

        if (_this.valid()) {
            let list_icon = $('#form_add_gallery .js_work_image .list-col');

            if (list_icon.length === 0) {
                $('#gallery_file_error').html($.validator.messages.imagerequired);
            } else {
                $('#addGalleryModal').modal('hide');
            }

        }
    });
});
