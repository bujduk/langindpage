$(document).ready(() => {

    // Modal About button "Save" click event
    $('#js_about_modal_form').submit((e) => {
        e.preventDefault();
        alert('Modal About Clicked!');
        $('.modal__input--warning').text('');
    })

    // Modal Info button "Save" click event
    $('#js_info_modal_form').submit((e) => {
        e.preventDefault();
        alert('Modal Info Summited!')
        $('.modal__input--warning').text('');
    })

    // Modal Service button "Save" click event
    $('#js_service_modal_form').submit((e) => {
        e.preventDefault();
        alert('Modal Service Summited!')
        $('.modal__input--warning').text('');
    })

    // Modal Education button "Save" click event
    $('#js_education_modal_form').submit((e) => {
        e.preventDefault();
        alert('Modal Education Summited!')
        $('.modal__input--warning').text('');
    })

    // Modal Experience button "Save" click event
    $('#js_experience_modal_form').submit((e) => {
        e.preventDefault();
        alert('Modal Experience Summited!')
        $('.modal__input--warning').text('');
    })

    // Modal Skill button "Save" click event
    $('#js_skill_modal_form').submit((e) => {
        e.preventDefault();
        alert('Modal Skill Summited!')
        $('.modal__input--warning').text('');
    })

    // Modal Work button "Save" click event
    $('#js_work_modal_form').submit((e) => {
        e.preventDefault();
        alert('Modal Work Summited!')
        $('.modal__input--warning').text('');
    })

    // Modal Knowledge button "Save" click event
    $('#js_khowledge_modal_form').submit((e) => {
        e.preventDefault();
        alert('Modal Knowledge Summited!')
        $('.modal__input--warning').text('');
    })


})

aboutModalError = (error) => {
    if (error.name) $('#name_error').text(error.name);
    if (error.twitter) $('#twitter_error').text(error.twitter);
    if (error.facebook) $('#facebook_error').text(error.facebook);
    if (error.stackoverflow) $('#stackoverflow_error').text(error.stackoverflow);
    if (error.job) $('#job_error').text(error.job);
    if (error.linkedin) $('#linkedin_error').text(error.linkedin);
    if (error.github) $('#github_error').text(error.github);
    if (error.dribbble) $('#dribbble_error').text(error.dribbble);
}

infoModalError = (error) => {
    if (error.description) $('#description_error').text(error.description);
    if (error.age) $('#age_error').text(error.age);
    if (error.city) $('#city_error').text(error.city);
    if (error.path_name) $('#path_name_error').text(error.path_name);
    if (error.address) $('#address_error').text(error.address);
    if (error.email) $('#email_error').text(error.email);
    if (error.phone) $('#phone_error').text(error.phone);
}

serviceModalError = (error) => {
    if (error.skill_icon) $('#skill_icon_error').text(error.skill_icon);
    if (error.service_job) $('#service_job_error').text(error.service_job);
    if (error.service_description) $('#service_description_error').text(error.service_description);
}

educationModalError = (error) => {
    if (error.majors_job) $('#majors_job_error').text(error.majors_job);
    if (error.school_job) $('#school_job_error').text(error.school_job);
    if (error.education_start_time) $('#education_start_time_error').text(error.education_start_time);
    if (error.education_end_time) $('#education_end_time_error').text(error.education_end_time);
    if (error.education_description) $('#education_description_error').text(error.education_description);
}