let knowledgeList = [
    'Website hosting',
    'iOS and android apps',
    'Create logo design',
    'Design for print',
    'Modern and mobile-ready',
    'Advertising services include',
    'Graphics and animations',
    'Search engine marketing'
];
let knowledgeListAddNew = [];
let imageWorkList = [];
let serviceIconRaw = [];
let serviceIconList = [];
currentEditServiceIcon = '';

$(document).ready(() => {

    let mobileModalInfo = $('.modal__mobile--info');
    let infoBtn = $('.info-btn');
    let serviceModal = $('#serviceModal');
    let serviceIconInput = $('#service_icon');

    /*============Select2============*/
    $(".js-example-tokenizer").select2({
        tags: true,
        tokenSeparators: [',', ' '],
        minimumResultsForSearch: -1
    })

    $('.js-example-tokenizer').on('select2:open', function (e) {
        $(".js-example-tokenizer option[value='0']").remove();
        $('.select2-results').hide().slideDown("fast", "easeInOutQuint");
    });

    infoBtn.on('click', function () {
        if (mobileModalInfo.hasClass('hidden')) {
            mobileModalInfo.removeClass('hidden')
        }
    })

    // Skill Add New Range
    let addNewSkillRange = $('.valueSpan');
    $('#skill_slider').on('input change', (e) => {
        addNewSkillRange.html($(e.target).val());
    });

    // Skill Edit Range
    let editSkillRange = $('.valueSpanEdit');
    $('#skill_slider_edit').on('input change', (e) => {
        editSkillRange.html($(e.target).val());
    });


    $('#upload__file').on('change', function () {
        let files = $(this)[0].files;

        for (let i = 0; i < files.length; i++) {
            let file = files[i];
            let key = Math.random().toString(20).substr(2, 10);

            imageWorkList[key] = file;

            let fileReader = new FileReader();
            fileReader.onload = (function (fileParams) {
                return function (event) {
                    let item = {
                        src: event.target.result,
                        name: fileParams.name
                    };
                    $('.js_work_image').append(`
                        <div id="${key}" class="col-lg-4 col-6">
                            <div class="box__drag">
                                <img class="img-upload" alt="${item.name}" src="${item.src}">
                               <p class="img__name">${item.name}</p>
                                <button type="button" class="work__delete--img delete__img">
                                     <i class="fas fa-trash-alt"></i>
                                </button>
                            </div>
                        </div>`)
                };
            })(file);
            fileReader.readAsDataURL(file);
        }
        $('#upload__file').val('');
    });

    // Delete Selected Image
    $('.js_work_image').on('click', 'button.work__delete--img', (e) => {
        let itemId = $(e.target).parents().eq(2).attr('id');
        $(`#${itemId}`).remove();
        delete imageWorkList[itemId];
    })

    // Get Icon
    $.getJSON("./data-icon/icon.json", function (result) {
        let inputService = $('.js_ryan-input__service');
        inputService.html('');
        $.each(result, function (name, value) {
            serviceIconRaw.push({
                name,
                value
            })
        });
    });

    serviceModal.on('hide.bs.modal', () => {
        closePopupService()
    });

    serviceIconInput.on('input change', () => {
        let value = serviceIconInput.val().trim().replace(/\s+/g, "-").toLowerCase();
        let resultData = serviceIconRaw.filter(item => item.value.includes(value));
        clearServiceIcon();
        convertServiceIcon(resultData);
        $('.js_ryan-input__service').attr('data-index', 0);
        generateServiceIcon();
    })

    $('.js_ryan-input__service').on('click', 'i', (e) => {
        let tag = $(e.target);
        let className = tag[0].classList[1];
        let selectedIcon = $('#service_selected_icon');
        if (tag.hasClass('selected')) {
            $('.js_ryan-input__service i').removeClass('selected');
            selectedIcon.val('')
        } else {
            $('.js_ryan-input__service i').removeClass('selected');
            tag.addClass('selected');
            let name = className.substring(3, className.length);
            selectedIcon.val(name);
        }
        currentEditServiceIcon = '';
    })

    // Experience current time
    let experienceCurrent = $('#experience_current');
    experienceCurrent.on('change', () => {
        let experienceModalFormDateTo = $('#js_experience_modal_form .ryan-form-control .time-to');
        if (experienceCurrent.is(":checked")) {
            experienceModalFormDateTo.addClass('element-hidden')
        } else {
            experienceModalFormDateTo.removeClass('element-hidden')
        }
    })

    // Service delete item
    $('.service-item__edit-button .fa-trash-alt').on('click', () => {
        bootbox.confirm({
            message: "Bạn chắc chắn muốn xoá",
            buttons: {
                confirm: {
                    label: 'Đồng ý',
                    className: 'ryan-form__submit'
                },
                cancel: {
                    label: 'Huỷ',
                    className: 'ryan-form__cancel'
                }
            },
            callback: function (result) {
                console.log('This was logged in the callback: ' + result);
            }
        })
    })


    // Experience delete item
    $('.experience-col .fa-trash-alt').on('click', () => {
        bootbox.confirm({
            message: "Bạn chắc chắn muốn xoá",
            buttons: {
                confirm: {
                    label: 'Đồng ý',
                    className: 'ryan-form__submit'
                },
                cancel: {
                    label: 'Huỷ',
                    className: 'ryan-form__cancel'
                }
            },
            callback: function (result) {
                console.log('This was logged in the callback: ' + result);
            }
        })
    })


    // Experience delete item
    $('.education-col .fa-trash-alt').on('click', () => {
        bootbox.confirm({
            message: "Bạn chắc chắn muốn xoá",
            buttons: {
                confirm: {
                    label: 'Đồng ý',
                    className: 'ryan-form__submit'
                },
                cancel: {
                    label: 'Huỷ',
                    className: 'ryan-form__cancel'
                }
            },
            callback: function (result) {
                console.log('This was logged in the callback: ' + result);
            }
        })
    })


    // Skill delete item
    $('.skills-list .item-option__delete').on('click', () => {
        bootbox.confirm({
            message: "Bạn chắc chắn muốn xoá",
            buttons: {
                confirm: {
                    label: 'Đồng ý',
                    className: 'ryan-form__submit'
                },
                cancel: {
                    label: 'Huỷ',
                    className: 'ryan-form__cancel'
                }
            },
            callback: function (result) {
                console.log('This was logged in the callback: ' + result);
            }
        })
    })

    // Knowledge delete item
    $('.js_knowledge-list').on('click', '.item-option__delete', () => {
        bootbox.confirm({
            message: "Bạn chắc chắn muốn xoá",
            buttons: {
                confirm: {
                    label: 'Đồng ý',
                    className: 'ryan-form__submit'
                },
                cancel: {
                    label: 'Huỷ',
                    className: 'ryan-form__cancel'
                }
            },
            callback: function (result) {
                console.log('This was logged in the callback: ' + result);
            }
        })
    })

    // Knowledge delete item
    $('.works').on('click', '.works-item__delete', () => {
        bootbox.confirm({
            message: "Bạn chắc chắn muốn xoá",
            buttons: {
                confirm: {
                    label: 'Đồng ý',
                    className: 'ryan-form__submit'
                },
                cancel: {
                    label: 'Huỷ',
                    className: 'ryan-form__cancel'
                }
            },
            callback: function (result) {
                console.log('This was logged in the callback: ' + result);
            }
        })
    })

    $('#experienceModal').on('hide.bs.modal', () => {
        closeExperience()
    })

    $('#educationModal').on('hide.bs.modal', () => {
        closeEducation()
    })

    // Skill Coding close popup
    $('#skillModal').on('hide.bs.modal', () => {
        closePopupCodingSkill()
    })

    // Skill Coding Edit close popup
    $('#skillEditModal').on('hide.bs.modal', () => {
        closePopupCodingSkill()
    })

    // Skill Search Item
    $('#path_name').on('input', (e) => {
        let value = $(e.target).val();
    })

    // Skill Select Item
    $('#js_skill_modal_form').on('click', '.dropdown-item', (e) => {
        $('#path_name').val($(e.target).html().trim());
    })

    // Skill Knowledge close popup
    $('#knowledgeModal').on('hide.bs.modal', () => {
        closePopupKnowledgeSkill()
    })

    // Skill Knowledge Edit close popup
    $('#knowledgeEditModal').on('hide.bs.modal', () => {
        closePopupKnowledgeSkill();
    })

    generateKnowledgeScreen();
    generateKnowledge();



})


// Generate Knowledge Screen
generateKnowledgeScreen = () => {
    if (knowledgeList && knowledgeList.length > 0) {
        let knowledgeListScreen = $('.js_knowledge-list');
        knowledgeList.forEach(item => {
            knowledgeListScreen.append(`
            <li class="list-item">
                <div class="name">${item}</div>
                <div class="item-option">
                    <button type="button" 
                    class="item-option__edit" 
                    data-id="1114"
                    data-name="${item}"
                    onclick="editPopupKnowledgeSkill(this)" ">
                        <i class="fas fa-pen"></i>
                    </button>
                    <button type="button" class="item-option__delete">
                        <i class="fas fa-trash"></i>
                    </button>
                </div>
            </li>
            `)
        })
    }
}

// ***** Popup Action *****
// Add new Service
addnewService = () => {
    let serviceForm = $('#js_service_modal_form');
    let inputService = $('.js_ryan-input__service');

    //Set data to popup
    serviceForm.attr('data-type', 'addnew');

    convertServiceIcon(serviceIconRaw);
    generateServiceIcon();
    inputService.attr('data-index', 0);
    $('#serviceModal').modal('show');
}

// Edit selected Sevice
editService = (element) => {
    let serviceForm = $('#js_service_modal_form');
    let inputService = $('.js_ryan-input__service');
    let _this = $(element);

    //Set data to popup
    currentEditServiceIcon = _this.data('icon');
    serviceForm.attr('data-type', 'edit');
    serviceForm.attr('data-id', _this.data('icon'));
    $('#service_selected_icon').val(_this.data('icon'));
    $('#service_job').val(_this.data('title'));
    $('#service_description').val(_this.data('description'));

    // Filter icon from data
    filterListService('', true);

    generateServiceIcon(true);
    inputService.attr('data-index', 0);
    $('#serviceModal').modal('show');
}

// Close Service Popup
closePopupService = () => {
    let inputService = $('.js_ryan-input__service');
    let serviceForm = $('#js_service_modal_form');
    inputService.attr('data-index', 0);
    serviceForm.attr('data-type', '');
    serviceForm.attr('data-id', '');
    $('#service_icon').val('');
    $('#service_selected_icon').val('');
    $('#service_job').val('');
    $('#service_description').val('');
    clearServiceIcon();
}

// ***** Education
// Addnew Education
addnewEducation = () => {
    let educationForm = $('#js_education_modal_form');
    educationForm.attr('data-type', 'addnew');
    $('#educationModal').modal('show');
}

// Edit Education
editEducation = (element) => {
    let _this = $(element);
    let educationForm = $('#js_education_modal_form');

    // Set data to popup
    educationForm.attr('data-type', 'edit');
    $('#majors_job').val(_this.attr('data-major'));
    $('#school_job').val(_this.data('school'));
    $('#education_start_month').val(_this.data('startmonth'));
    $('#education_start_year').val(_this.data('startyear'));
    $('#education_start_time').val(_this.data('starttime'));
    $('#education_end_month').val(_this.data('endmonth'));
    // $('#education_end_month').val(_this.data('endmonth'));
    $('#education_end_time').val(_this.data('endtime'));
    $('#education_description').val(_this.data('description'));

    $('#educationModal').modal('show');
}

// Close Education
closeEducation = () => {
    let educationForm = $('#js_education_modal_form');

    // Set data to popup
    educationForm.attr('data-type', '');
    $('#majors_job').val('');
    $('#school_job').val('');
    $('#education_start_month').val('');
    $('#education_start_year').val('');
    $('#education_start_time').val('');
    $('#education_end_month').val('');
    $('#education_end_year').val('');
    $('#education_end_time').val('');
    $('#education_description').val('');
}


// ***** Experience
// Addnew Experience
addnewExperience = () => {
    let experienceForm = $('#js_experience_modal_form');
    experienceForm.attr('data-type', 'addnew');
    $('#experienceModal').modal('show');
}

// Edit Experience
editExperience = (element) => {
    let _this = $(element);
    let experienceForm = $('#js_experience_modal_form');

    // Set data to popup
    experienceForm.attr('data-type', 'edit');
    $('#experience_job').val(_this.data('jobname'));
    $('#company').val(_this.data('company'));
    $('#experience_current').prop('checked', _this.data('currentjob') !== 0);
    $('#experience_start_month').val(_this.data('startmonth'));
    $('#experience_start_year').val(_this.data('startyear'));
    $('#experience_start_time').val(_this.data('starttime'));
    if (_this.data('currentjob') === 0) {
        $('#experience_end_month').val(_this.data('endmonth'));
        $('#experience_end_year').val(_this.data('endyear'));
        $('#experience_end_time').val(_this.data('endtime'));
    } else {
        let experienceModalFormDateTo = $('#js_experience_modal_form .ryan-form-control .time-to');
        experienceModalFormDateTo.addClass('element-hidden')
    }
    $('#experience_description').val(_this.data('description'));

    $('#experienceModal').modal('show');
}

// Close Experience
closeExperience = () => {
    let experienceForm = $('#js_experience_modal_form');

    experienceForm.attr('data-type', '');
    $('#experience_job').val('');
    $('#company').val('');
    $('#experience_current').prop('checked', false);
    $('#experience_start_month').val('');
    $('#experience_start_year').val('');
    $('#experience_start_time').val('');
    $('#experience_end_month').val('');
    $('#experience_end_year').val('');
    $('#experience_end_time').val('');
    $('#experience_description').val('');

    let experienceModalFormDateTo = $('#js_experience_modal_form .ryan-form-control .time-to');
    experienceModalFormDateTo.removeClass('element-hidden')
}

// ***** Knowledge skill
// Addnew Knowledge Skill Popup
addnewPopupKnowledgeSkill = () => {
    generateKnowledge();
    $('#knowledgeModal').modal('show');
}

// Edit Knowledge Skill Popup
editPopupKnowledgeSkill = (element) => {
    let knowledgeSkillForm = $('#js_khowledge_edit_modal_form');
    let _this = $(element);

    knowledgeSkillForm.attr('data-id', _this.data('id'))
    $('#knowledge_edit_input').val(_this.data('name'))
    $('#knowledgeEditModal').modal('show');
}

// Close Knowledge Skill Popup
closePopupKnowledgeSkill = () => {
    let knowledgeSkillForm = $('#js_khowledge_edit_modal_form');
    $('#knowledgeModal .knowledge__body').html('');
    knowledgeListAddNew = [];
    $('#knowledge_edit_input').val('');
    knowledgeSkillForm.attr('data-id', '')
}

// Add new Coding Skill
addnewCoddingSkill = (element) => {
    let codingSkillEditForm = $('#js_skill-edit-modal_form');
    let _this = $(element);

    //Set data to popup
    codingSkillEditForm.attr('data-id', _this.data('id'));
    $('#path_name_edit').val(_this.data('name'));
    $('#skill_slider_edit').val(_this.data('range'));
    $('.valueSpanEdit').html(_this.data('range'));

    $('#skillEditModal').modal('show');
}

// Generate List Knowledges
generateKnowledge = () => {
    $('.knowledge__body ').html('');
    if (knowledgeListAddNew && knowledgeListAddNew.length > 0) {
        knowledgeListAddNew.forEach((item, key) => {
            $('.knowledge__body ').append(`
                <div class="col-12">
                    <div id="knowledge_item_${key}" class="ryan-form-control pb-2 mb-0">
                    <div class="row mr-sm-0 ml-sm-0">
                       <div class="col-9 col-sm-10">
                           <input class="ryan-input__text" placeholder="Alex Johnson" value="${item}" oninput="onChangeSeletecKnowledge(${key}, event)">
                       </div>
                       <div class="col-1 delete_col">
                           <button class="btn__delete--input" onclick="deleteSeletedKnowledge(${key})">
                               <i class="far fa-trash-alt"></i>
                           </button>
                       </div>
                       <div class="col-1 create_col">
                           ${key === knowledgeListAddNew.length - 1 ? `
                            <button class="btn__create--input" onclick="createNewKnowledge()">
                               <i class="fas fa-plus"></i>
                           </button>` : ''}
                       </div>
                    </div>
                    </div>      
                </div>`)
        })
    } else {
        $('.knowledge__body').append(`
            <div class="col-12">
                <div class="row pb-2">
                   <div class="col-12 create_col">
                       <button class="btn__create--input ml-auto mr-auto" onclick="createNewKnowledge()">
                           <i class="fas fa-plus"></i>
                       </button>
                   </div>
                </div>
            </div>`)
    }
}

// Create New Knowledge item
createNewKnowledge = () => {
    knowledgeListAddNew.push('');
    generateKnowledge();
}

// Save change of each Knowledge item
onChangeSeletecKnowledge = (index, evt) => {
    knowledgeListAddNew[index] = evt.target.value;
}

// Delete selected Knowledge item
deleteSeletedKnowledge = (index) => {
    knowledgeListAddNew.splice(index, 1);
    generateKnowledge();
}

// ***** Image Work
// Generate List Work Image
generateWorkImage = () => {
    $('.js_work_image').html('');
    if (imageWorkList && imageWorkList.length > 0) {
        imageWorkList.forEach((item, key) => {

        })
    }
}

// Delete selected Work Image
deleteSelectedImage = (index) => {
    imageWorkList.splice(index, 1);
    generateWorkImage();
}

// ***** Service
// Filter List Service Icon
filterListService = (value, haveLead = false) => {
    let resultData = serviceIconRaw.filter(item => item.value.includes(value));
    clearServiceIcon();
    convertServiceIcon(resultData, haveLead);
}

// Chunk list Service Icon
convertServiceIcon = (list, haveLead = false) => {
    let serviceTerm = -1;
    serviceIconList = [];
    if (haveLead) {
        list.forEach((item, key) => {
            if (item.value === currentEditServiceIcon) serviceTerm = key;
        })
    }
    let chunk = 100;
    for (let i = (serviceTerm !== -1) ? serviceTerm : 0; (list && i < list.length); i += chunk) {
        let temp = list.slice(i, i + chunk);
        serviceIconList.push(temp);
    }
}

// Generate list Service Icon
generateServiceIcon = (onEdit = false) => {
    let serviceIcon = $('.js_ryan-input__service');
    let nextList = serviceIcon.attr('data-index');

    hideViewMoreButton();
    if (serviceIconList && serviceIconList.length > 0) {
        serviceIconList[nextList].forEach(item => {
            serviceIcon.append(`
           <i class="${item.value} ${ currentEditServiceIcon === item.value ? 'selected' : ''}" title="${item.name}"></i>
        `);
        })
    }
    nextList++;
    if (nextList < serviceIconList.length) {
        showViewMoreButton();
    }
    serviceIcon.attr('data-index', nextList);
}

// Clear Service Icon
clearServiceIcon = () => {
    let serviceIcon = $('.js_ryan-input__service');
    serviceIcon.html('');
}

// Show button Xem Thêm of Service Icon
showViewMoreButton = () => {
    $('.service-more').removeClass('element-hidden');
}

// Hide button Xem Thêm of Service Icon
hideViewMoreButton = () => {
    $('.service-more').addClass('element-hidden');
}

// ***** Coding skill
// Close Coding Skill Popup
closePopupCodingSkill = () => {
    $('#js_skill_modal_form .dropdown-body').html('');
    $('#skill_slider').val(0);
    $('#skill_slider_edit').val(0);
    $('.valueSpan').html('0');
    $('.valueSpanEdit').html('0');
}

/*generateProject*/
generateNewProject = (data) => {
    let timeline = null;
    $('.resume-items').prepend(`
        <div class="resume-item border-line-h active" id="project_${data.id}">
            <!-- TIMELINE CONTENT -->
            <div class="card timeline-content">
                <div class="card-content">
                    <div class="item-option">
                        <button type="button"
                                class="item-option__edit btn__popup"
                                data-id="${data.id}"
                                data-name="${data.name}"
                                data-start_time="${data.start_time}"
                                data-end_time="${data.end_time}"
                                data-customer="${data.customer}"
                                data-team_size="${data.team_size}"
                                data-job_position="${data.job_position}"
                                data-role="${data.role}"
                                data-freelance="${data.freelance}"
                                data-description="${data.description}"
                                data-url="${data.updateUrl}"
                                onclick="modalOpen($(this), 'project', 'edit')">
                            <i class="fas fa-pen"></i>
                        </button>
                        <button type="button"
                                class="item-option__delete btn__popup"
                                data-id="${data.id}"
                                data-type="project"
                                data-url="${data.deleteUrl}"
                                onclick="deleteItem($(this))">
                            <i class="fas fa-trash"></i>
                        </button>
                    </div>
                    <!-- TIMELINE TITLE -->
                    <div class="name project-${data.id}-name"></div>
                    <!-- TIMELINE TITLE INFO -->
                    <div class="resume-item--top">
                        <div class="project-${data.id}-timeline date"></div>
                    </div>
                    <!-- TIMELINE PARAGRAPH -->
                    <p class="project-${data.id}-description project-des"></p>
                    <p class="project__item--content name">
                        Client:
                        <span class="project-${data.id}-customer company"></span>
                    </p>
                    <p class="project__item--content name">
                        Số lượng thành viên:
                        <span class="project-${data.id}-team_size company"></span>
                    </p>
                    <p class="project__item--content name">
                        Vị trí công việc:
                        <span class="project-${data.id}-job_position company"></span>
                    </p>
                    <p class="project__item--content name">
                        Vai trò:
                        <span class="project-${data.id}-role company"></span>
                    </p>
                    <div class="stack">
                        <span class="project__item--content name">Stack sử dụng: </span>
                        <div class="project-${data.id}-technology_used stack-item"></div>
                    </div>
                </div>
            </div>
        </div>
        `);

    timeline = data.start_time.replace('-', '/') + ' - ' + data.end_time.replace('-', '/');
    $(`.project-${data.id}-timeline`).text(timeline);
    $(`.project-${data.id}-name`).text(data.name);
    $(`.project-${data.id}-customer`).text(data.customer);
    $(`.project-${data.id}-description`).text(data.description);
    $(`.project-${data.id}-team_size`).text(data.team_size);
    $(`.project-${data.id}-job_position`).text(data.job_position);
    $(`.project-${data.id}-role`).text(data.role);
    $(`.project-${data.id}-technology_used`).text(data.technology_used);
}