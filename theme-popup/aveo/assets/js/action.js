
let knowledgeList = [];
let serviceIconsRaw = [];
let serviceIconsChunk = [];
let serviceIconCurrent = '';
let galleryImageList = [];

$(document).ready(() => {
    let editInfoModal = $('#editInfoModal');
    let editAboutModal = $('#editAboutModal');
    let editSkillModal = $('#editSkillModal');
    let addGalleryModal = $('#addGalleryModal');
    let editGalleryModal = $('#editGalleryModal');
    let editServiceModal = $('#editServiceModal');
    let editProjectModal = $('#editProjectModal');
    let newKnowledgeModal = $('#newKnowledgeModal');
    let editEducationModal = $('#editEducationModal');
    let editKnowledgeModal = $('#editKnowledgeModal');
    let editExperienceModal = $('#editExperienceModal');

    /* editAboutModal on close */
    editAboutModal.on('hidden.bs.modal', () => {
        closeEditAboutModal();
    })

    /* editInfoModal on close */
    editInfoModal.on('hidden.bs.modal', () => {
        closeEditInfoModal();
    })

    /* editProjectModal on close */
    editProjectModal.on('hidden.bs.modal', () => {
        closeEducationModal();
    })

    /* editEducationModal on close */
    editEducationModal.on('hidden.bs.modal', () => {
        closeEducationModal();
    })

    /* editExperienceModal on close */
    editExperienceModal.on('hidden.bs.modal', () => {
        closeExperienceModal();
    })

    /* editSkillModal on close */
    editSkillModal.on('hidden.bs.modal', () => {
        closeEditSkillModal();
    })

    /* editServiceModal on close */
    editServiceModal.on('hide.bs.modal', () => {
        $('#form_edit_service .list-icon').html('');
    });
    editServiceModal.on('hidden.bs.modal', () => {
        closeEditServiceModal();
    });

    /* newKnowledgeModal on close */
    newKnowledgeModal.on('hidden.bs.modal', () => {
        closeNewKnowledgeModal();
    });
    editKnowledgeModal.on('hidden.bs.modal', () => {
        closeEditKnowledgeModal();
    });

    /* addGalleryModal on close */
    addGalleryModal.on('hidden.bs.modal', () => {
        closeAddGalleryModal();
    });

    /* editGalleryModal on close */
    editGalleryModal.on('hidden.bs.modal', () => {
        closeEditGalleryModal();
    })

    /* About Upload Profile Image */
    $('#about_img_avatar').change(function (e) {
        aboutUploadImage($(e.target), this.files[0]);
    })

    /* Experience check current time */
    $('#experience_is_current_working').on('change', (e) => {
        let experienceEnd = $('.js_experience_end');
        if ($(e.target).is(':checked')) {
            experienceEnd.addClass('hidden');
        } else {
            experienceEnd.removeClass('hidden');
        }
    });

    /* Skill Search Item */
    $('#skill_name').on('input change', (e) => {
        searchSkill($(e.target));
    });

    /* Icon Service select */
    $('#form_edit_service').on('click', '.list-icon i', (e) => {
        let _this = $(e.target);
        let icon = $('#icon');

        $('#form_edit_service .list-icon i').removeClass('selected');
        serviceIconCurrent = '';

        if (!_this.hasClass(icon.val())) {
            let name = _this.attr('title');

            icon.val(name);
            _this.addClass('selected');
        } else {
            icon.val('');
        }
    })

    /* Icon Service Search */
    $('#service_icon_search').on('input change', async (e) => {
        let value = $(e.target).val();

        await searchAndChunkService(value);
        await generateServiceIcon();
    })

    /* Input Image File Change */
    $('#gallery_file').on('change', (e) => {
        galleryLoadImage($(e.target));
    })
    let demoPanel = $("#lm_demo_panel"), head = $('head'), demoPanelSwitcher = $("#lm_demo_panel_switcher");

    $('#main_color').on("click", "a", function () {
        let selector = $(this).data("id");
        $('.main-color-switcher').removeClass("current-main-color");
        $('#main_color a').removeClass('current-main-color');
        $(this).addClass('current-main-color');
        $('link[data-id="custom"]').remove();
        head.append('<link rel="stylesheet" href="./assets/css/color/main-' + selector + '.css" type="text/css" data-id="custom">');
        demoPanel.removeClass(function (index, className) {
            return (className.match(/(^|\s)panel-color\S+/g) || []).join(' ')
        });
        demoPanel.addClass("panel-color-" + selector)
    });
    demoPanelSwitcher.on("click", function (event) {
        event.stopPropagation();
        demoPanel.toggleClass("active")
    });

    init();
});

/* Init Data */
init = () => {
    /* Get Service icon after page loaded */
    $.getJSON("./assets/data-icon/icon.json", function (result) {
        $.each(result, function (name, value) {
            serviceIconsRaw.push(value);
        });
    });

    $('#stack').select2({
        tags: true,
        dropdownCssClass: 'popup-select2-dropdown',
        selectionCssClass: 'popup-select2-select'
    })

}

/* Generate key */
generateKey = () => {
    return Math.random().toString(20).substr(2, 10);
}

/* Close popup event */
closePopup = () => {
    let forms = $('form');

    for (let i = 0; i < forms.length; i++) {
        forms[i].reset();
    }

    clearError();
}

/* Clear Input error and error text */
clearError = () => {
    $('.form-error').html('');
    $('.form-submit-error').html('');
    $('label.error').html('');
    $('.error').removeClass('error');
    $('.tooltip').remove();
}

aboutUploadImage = (element, file) => {
    if (file.size > 2097152) {

        Notiflix.Notify.Failure(image + ' ' + file.name + ' ' + too_2mb);

    } else if (file.type !== "image/jpeg" && file.type !== "image/png" && file.type !== "image/jpg") {

        Notiflix.Notify.Failure(image + ' ' + file.name + ' ' + gallery_incorrect_format);

    } else {
        let formData = new FormData();
        formData.append('file', file);
        formData.append('type', element.attr('data-type'));

        $.ajax({
            url: element.attr('data-url'),
            type: 'POST',
            data: formData,
            dataType: 'json',
            cache: false,
            contentType: false,
            processData: false,
        }).done(function (response) {
            if (response.statusCode >= 200 || response.statusCode < 300) {
                Notiflix.Notify.Success(create_success);
                element.parent().children('div').css('background-image', 'url(' + response.data.url + ')');
            } else {
                commonShowNotifyErr();
            }
        }).fail(function (error) {
            commonShowNotifyErr();
        });
    }
}

/* Thay đổi giá trị của dropdown
* element là chính element được click
* className là class hoặc id của ô input cần gán giá trị
*/
dropdownChangeValue = (_this, className) => {
    let item = $(`${className}`);
    item.attr('data-id', _this.attr('data-id')).val(_this.html().trim());
}

/* Hiển thị giá trị của input range */
rangeChangeValue = (_this, className) => {
    let item = $(`${className}`);
    item.html(_this.val());
}


/* ===== editAboutModal ===== */
openEditAboutModal = (_this) => {
    let formData = $('#form_edit_about').serializeObject();

    for (let [key] of Object.entries(formData)) {
        $(`#about_${key}`).val(_this.attr(`data-${key}`));
    }

    $('#editAboutModal').modal('show');
}

closeEditAboutModal = () => {
    closePopup();
}

/* ===== editInfoModal ===== */
openEditInfoModal = (_this) => {
    let formData = $('#form_edit_info').serializeObject();

    for (let [key] of Object.entries(formData)) {
        $(`#info_${key}`).val(_this.attr(`data-${key}`));
    }

    $('#info_freelance').val((_this.attr('data-freelance') === '1') ? 'Available' : 'Not available')

    $('#editInfoModal').modal('show');
}

closeEditInfoModal = () => {
    closePopup();
}
/* ===== End editInfoModal ===== */

/* ===== editEducationModal ===== */
openNewEducationModal = () => {
    let _form = $('#form_edit_education');

    _form.attr('data-type', 'add');

    $('#editEducationModal').modal('show');
}

openEditEducationModal = (_this) => {
    let _form = $('#form_edit_education');
    let formData = _form.serializeObject();

    _form.attr('data-type', 'edit');
    _form.attr('data-updateUrl', _this.attr('data-updateUrl'));

    for (let [key] of Object.entries(formData)) {
        $(`#education_${key}`).val(_this.attr(`data-${key}`));
    }

    $('#editEducationModal').modal('show');
}

const generateNewEducation = (data) => {
    let time = convertTime(data.start_time, data.end_time);

    $('.education-list').prepend(`
        <div class="timeline-item timeline-edit" id="education_${data.id}">
            <h4 class="item-title education-${data.id}-majors">${data.majors}</h4>
            <span class="item-period education-${data.id}-timeline">${data.start_time.replace('-', '/')} - ${(data.end_time.replace('-', '/'))}</span>
            <span class="item-small education-${data.id}-studying_area">${data.studying_area}</span>
            <p class="education-${data.id}-description">${data.description}</p>

            <div class="edit-area">
                <button type="button"
                        data-id="${data.id}"
                        data-majors="${data.majors}"
                        data-start_month="${time['start_month']}"
                        data-start_year="${time['start_year']}"
                        data-end_month="${time['end_month']}"
                        data-end_year="${time['end_year']}"
                        data-studying_area="${data.studying_area}"
                        data-description="${data.description}"
                        data-updateUrl="${data.updateUrl}"
                        onclick="openEditEducationModal($(this))"
                        class="edit-area__button education-${data.id}-button_edit">
                    <i class="fas fa-pen"></i>
                </button>
                <button type="button"
                        data-type="education"
                        data-id="${data.id}"
                        data-deleteUrl="${data.deleteUrl}"
                        onclick="deleteItem($(this))"
                        class="edit-area__button education-${data.id}-button_delete">
                    <i class="fas fa-trash"></i>
                </button>
            </div>
        </div>
    `)
}

closeEducationModal = () => {
    closePopup();
}
/* ===== End editEducationModal ===== */

/* ===== editExperienceModal ===== */
openNewExperienceModal = () => {
    let _form = $('#form_edit_experience');

    _form.attr('data-type', 'add');

    $('#editExperienceModal').modal('show');
}

openEditExperienceModal = (_this) => {
    let _form = $('#form_edit_experience');
    let formData = _form.serializeObject();

    _form.attr('data-type', 'edit');
    _form.attr('data-updateUrl', _this.attr('data-updateUrl'));

    for (let [key] of Object.entries(formData)) {
        $(`#experience_${key}`).val(_this.attr(`data-${key}`));
    }

    if (_this.attr(`data-is_current_working`) === "true") {
        $('#experience_is_current_working').attr('checked', true);
        $('.js_experience_end').addClass('hidden');
    }

    $('#editExperienceModal').modal('show');
}

const generateNewExperience = (data) => {
    let time = convertTime(data.start_time, data.end_time);

    $('.experience-list').prepend(`
        <div class="timeline-item timeline-edit" id="experience_${data.id}">
            <h4 class="item-title experience-${data.id}-job_title">${data.job_title}</h4>
            <span class="item-period experience-${data.id}-timeline">
                ${data.start_time.replace('-', '/')} - ${(data.is_current_working) ? 'Current' : data.end_time.replace('-', '/')}
            </span>
            <span class="item-small experience-${data.id}-company">${data.company}</span>
            <p class="experience-${data.id}-description">${data.description}</p>

            <div class="edit-area">
                <button type="button"
                        data-id="${data.id}"
                        data-job_title="${data.job_title}"
                        data-start_month="${time['start_month']}"
                        data-start_year="${time['start_year']}"
                        data-end_month="${time['end_month']}"
                        data-end_year="${time['end_year']}"
                        data-is_current_working="${data.is_current_working}"
                        data-company="${data.company}"
                        data-description="${data.description}"
                        data-updateUrl="${data.updateUrl}"
                        onclick="openEditExperienceModal($(this))"
                        class="edit-area__button experience-${data.id}-button_edit">
                    <i class="fas fa-pen"></i>
                </button>
                <button type="button"
                        data-type="experience"
                        data-id="${data.id}"
                        data-deleteUrl="${data.deleteUrl}"
                        onclick="deleteItem($(this))"
                        class="edit-area__button experience-${data.id}-button_delete">
                    <i class="fas fa-trash"></i>
                </button>
            </div>
        </div>
    `)
}

closeExperienceModal = () => {
    $('.js_experience_end').removeClass('hidden');
    $('#experience_is_current_working').attr('checked', false);
    closePopup();
}
/* ===== End editExperienceModal ===== */

/* ===== editSkillModal ===== */
openNewSkillModal = () => {
    $('#editSkillModal').modal('show');
}

openEditSkillModal = (_this) => {
    let _form = $('#form_edit_skill');
    let formData = _form.serializeObject();

    for (let [key] of Object.entries(formData)) {
        $(`#skill_${key}`).val(_this.attr(`data-${key}`));
    }
    $('#skill_score_view').html(_this.attr('data-score'));
    _form.attr(`data-id`, _this.attr(`data-id`));
    _form.attr('data-updateUrl', _this.attr('data-updateUrl'));

    $('.skill__list-suggestion').addClass('hidden');
    $('#skill_name').attr('readonly', true)

    $('#editSkillModal').modal('show');
}

searchSkill = (_this) => {
    let value = _this.val().trim().replace(/\s+/g, " ").toLowerCase();
    let skillList = $('.skill__list-suggestion .dropdown-item');

    _this.attr('data-id', null);
    skillList.addClass('hidden');

    for (let i = 0; i < skillList.length; i++) {
        let item = $(skillList[i]);
        let temp = item.html().trim().replace(/\s+/g, " ").toLowerCase();
        if (temp.includes(value)) {
            item.removeClass('hidden');
        }
    }
}

const generateNewSkill = (data) => {
    $('.skill-list').prepend(`
        <div class="skill-edit" id="skill_${data.id}">
            <div class="edit-view">
                <h4 class="skill-${data.id}-name">${data.name}</h4>
                <div id="skill-percent-${data.id}"
                     data-value="${data.score}"
                     class="skill-container skill-${data.id}-percentage">
                    <div class="skill-percentage" style="width: ${data.score}%"></div>
                </div>
            </div>
            <div class="edit-area">
                <button type="button"
                        data-id="${data.id}"
                        data-name="${data.name}"
                        data-score="${data.score}"
                        data-updateUrl="${data.updateUrl}"
                        onclick="openEditSkillModal($(this))"
                        class="edit-area__button skill-${data.id}-button_edit">
                    <i class="fas fa-pen"></i>
                </button>
                <button type="button"
                        data-type="skill"
                        data-id="${data.id}"
                        data-deleteUrl="${data.deleteUrl}"
                        onclick="deleteItem($(this))"
                        class="edit-area__button skill-${data.id}-button_delete">
                    <i class="fas fa-trash"></i>
                </button>
            </div>
        </div>
    `);
}

closeEditSkillModal = () => {
    let _form = $('#form_edit_skill');

    $('#skill_score_view').html('0');
    $('.skill__list-suggestion .dropdown-item').removeClass('hidden');
    _form.attr(`data-id`, null);
    _form.attr(`data-url`, null);

    $('.skill__list-suggestion').removeClass('hidden');
    $('#skill_name').attr('readonly', false)


    closePopup();
}
/* ===== End editCodingModal ===== */

/* ===== newKnowledgeModal ===== */
openNewKnowledgeModal = () => {
    addnewKnowledgeItemInForm();
    $('#newKnowledgeModal').modal('show');
}

closeNewKnowledgeModal = () => {
    $('#newKnowledgeModal .list_knowledge').html('');
    closePopup();
    knowledgeList = [];
}

generateNewKnowledge = (list) => {
    let output = ``;

    list.forEach(data => {
        output += `
             <li id="knowledge_${data.id}" class="knowledge-edit">
                <span class="knowledge-${data.id}-content">${data.content}</span>
                <span 
                      data-id="${data.id}"
                      data-content="${data.content}"
                      data-updateUrl="${data.updateUrl}"
                      onclick="openEditKnowledgeModal($(this))"
                      class="edit-area__button knowledge-${data.id}-button_edit">
                    <i class="fas fa-pen"></i>
                </span>
                <span 
                      data-type="knowledge"
                      data-id="${data.id}"
                      data-deleteUrl="${data.deleteUrl}"
                      onclick="deleteItem($(this))"
                      class="edit-area__button knowledge-${data.id}-button_edit">
                    <i class="fas fa-trash"></i>
                </span>
            </li>
        `
    })

    $('.knowledge-list').prepend(output);
}

addnewKnowledgeItemInForm = () => {
    let length = Object.keys(knowledgeList).length;
    if (length < 10) {
        let key = generateKey();
        knowledgeList[key] = '';
        $('#form_new_knowledge .list_knowledge').append(`
        <div data-id="${key}" class="knowledge-item">
            <label class="aveo-form-control form-knowledge">
                <input type="text" id="input_knowledge_${key}_content"
                        name="content"
                        data-id="${key}"
                       oninput="inputNewKnowledgeModal($(this))"
                       class="form-input">
                <button type="button" 
                    onclick="deleteKnowledgeItemInForm('${key}')"
                    class="form-knowledge__button delete-button">
                    <i class="fas fa-trash"></i>
                </button>
                <button type="button" 
                    onclick="addnewKnowledgeItemInForm()"
                    class="form-knowledge__button addnew-button">
                    <i class="fas fa-plus"></i>
                </button>
            </label>
            <p id="klnew_${key}_error" class="form-error"></p>
        </div>
    `);
    }
}

inputNewKnowledgeModal = (_this) => {
    let id = _this.parents().eq(1).attr('data-id');
    knowledgeList[id] = _this.val();
}

deleteKnowledgeItemInForm = (key) => {
    clearError();
    if ($('#form_new_knowledge .knowledge-item').length > 1) {
        $(`.knowledge-item[data-id="${key}"]`).remove();
        delete knowledgeList[key];
    } else {
        $(`#input_knowledge_${key}_content`).val('');
    }
}
/* ===== End newKnowledgeModal ===== */

/* ===== editKnowledgeModal ===== */
openEditKnowledgeModal = (_this) => {
    let _form = $('#form_edit_knowledge');
    let formData = _form.serializeObject();

    for (let [key] of Object.entries(formData)) {
        $(`#knowledge_edit_${key}`).val(_this.attr(`data-${key}`));
    }
    _form.attr(`data-id`, _this.attr(`data-id`));
    _form.attr('data-updateUrl', _this.attr('data-updateUrl'));

    $('#editKnowledgeModal').modal('show');
}

closeEditKnowledgeModal = () => {
    $('#form_edit_knowledge').attr('data-id', '');
    $('#edit_content').val('');
    closePopup();
}
/* ===== End editKnowledgeModal ===== */

/* ===== editServiceModal ===== */
openAddNewServiceModal = async () => {
    let form_edit_service = $('#form_edit_service');

    form_edit_service.attr('data-type', 'add');

    await chunkServiceIcon(serviceIconsRaw);
    await generateServiceIcon();

    $('#editServiceModal').modal('show');
}

openEditServiceModal = async (_this) => {
    let _form = $('#form_edit_service');
    let formData = _form.serializeObject();
    serviceIconCurrent = _this.attr('data-icon');

    for (let [key] of Object.entries(formData)) {
        $(`#service_${key}`).val(_this.attr(`data-${key}`));
    }
    _form.attr('data-type', 'edit');
    _form.attr('data-updateUrl', _this.attr('data-updateUrl'));

    await chunkServiceIcon(serviceIconsRaw, true);
    await generateServiceIcon();

    $('#editServiceModal').modal('show');
}

generateNewService = (data) => {
    $('.service-list').prepend(`
        <div class=" col-xs-12 col-sm-4 col-md-3 " id="service_${data.id}">
            <div class="fw-col-inner"
                 data-paddings="0px 0px 0px 0px">

                <div class="service-block service-edit">
                    <div class="service-info">
                        <div class="service-image service-${data.id}-icon_class">
                            <i class="${data.icon}"></i>
                        </div>
                        <h4 class="service-${data.id}-name">${data.name}</h4>
                        <p class="service-${data.id}-description">${data.description}</p>
                    </div>

                    <div class="edit-area">
                        <button type="button"
                                data-id="${data.id}"
                                data-icon="${data.icon}"
                                data-name="${data.name}"
                                data-description="${data.description}"
                                data-updateUrl="${data.updateUrl}"
                                onclick="openEditServiceModal($(this))"
                                class="edit-area__button service-${data.id}-button_edit">
                            <i class="fas fa-pen"></i>
                        </button>
                        <button type="button"
                                data-type="service"
                                data-id="${data.id}"
                                data-deleteUrl="${data.deleteUrl}"
                                onclick="deleteItem($(this))"
                                class="edit-area__button service-${data.id}-button_delete">
                            <i class="fas fa-trash"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
    `)
}

searchAndChunkService = (index) => {
    let value = index.trim().replace(/\s+/g, "-").toLowerCase();
    let listSearch = serviceIconsRaw.filter(item => item.includes(value));

    chunkServiceIcon(listSearch);
}

chunkServiceIcon = (listIcon, haveLead = false) => {
    let list_icon = $('#form_edit_service .list-icon');
    let serviceIndex = -1;
    let chunk = 100;

    serviceIconsChunk = [];
    list_icon.html('');

    if (haveLead) {
        listIcon.forEach((item, key) => {
            if (item === serviceIconCurrent) serviceIndex = key;
        })
    }

    for (let i = (serviceIndex !== -1) ? serviceIndex : 0; (listIcon && i < listIcon.length); i += chunk) {
        let temp = listIcon.slice(i, i + chunk);
        serviceIconsChunk.push(temp);
    }

    list_icon.attr('data-index', 0);
}

generateServiceIcon = () => {
    let list_icon = $('#form_edit_service .list-icon');
    let listIndex = list_icon.attr('data-index');

    hideViewMoreButton();

    if (serviceIconsChunk && serviceIconsChunk.length > 0) {
        serviceIconsChunk[listIndex].forEach(item => {
            list_icon.append(`
                <i class="${item}${serviceIconCurrent === item ? ' selected' : ''}" title="${item}"></i>
            `)
        })
    }

    listIndex++;
    if (listIndex < serviceIconsChunk.length) {
        showViewMoreButton();
    }
    list_icon.attr('data-index', listIndex);
}

showViewMoreButton = () => {
    $('#form_edit_service .service-more').removeClass('hidden');
}

hideViewMoreButton = () => {
    $('#form_edit_service .service-more').addClass('hidden');
}

closeEditServiceModal = () => {
    serviceIconCurrent = '';

    $('#form_edit_service .list-icon').html('');
    $('#form_edit_service').attr('data-type', '');
    closePopup();
}
/* ===== End editServiceModal ===== */

/* ===== addGalleryModal ===== */
openAddGalleryModal = () => {
    $('#addGalleryModal').modal('show');
}

galleryLoadImage = (_this) => {
    let files = _this[0].files;
    let gallery_list_error = $('#gallery_list_error');

    gallery_list_error.html('');

    for (let i = 0; i < files.length; i++) {

        if (Object.keys(galleryImageList).length >= 12) {
            let html = `<span style="display:block;">${gallery_more_than_12_item}</span>`;

            gallery_list_error.append(html);
            break;
        }

        let file = files[i];
        if (file.size > 2097152) {
            let html = `<span style="display:block;">${image} ${file.name} ${too_2mb}</span>`;

            gallery_list_error.append(html);
        } else if (file.type !== "image/jpeg" && file.type !== "image/png" && file.type !== "image/jpg") {
            let html = `<span style="display:block;">${image} ${file.name} ${gallery_incorrect_format}</span>`;

            gallery_list_error.append(html);
        } else {
            let fileReader = new FileReader();
            let key = generateKey();

            galleryImageList[key] = file;

            fileReader.onload = ((fileParam) => {
                return (e) => {
                    renderGalleryImageItem(key, e.target.result, fileParam.name);
                }
            })(file);
            fileReader.readAsDataURL(file);
        }
    }
    _this.val('');
}

renderGalleryImageItem = (key, src, name) => {
    $('#form_add_gallery .input-gallery-list').append(`
        <div class="col-md-4 col-xs-6 list-col" id="gallery_add_${key}">
            <div class="gallery-item">
                <div style="background-image: url(${src})"
                        class="gallery-item__image">
                </div>
                <div class="gallery-item__title">
                    ${name}
                </div>
                <button type="button"
                onclick="deleteGalleryAddItem('${key}')"
                 class="gallery-item__delete">
                    <i class="fas fa-trash"></i>
                </button>
            </div>
        </div>
    `)
}

deleteGalleryAddItem = (key) => {
    delete galleryImageList['key'];
    $(`#gallery_add_${key}`).remove();
}

closeAddGalleryModal = () => {
    $('#form_add_gallery .input-gallery-list').html('');
    galleryImageList = [];
    closePopup();
}
/* ===== End addGalleryModal ===== */

/* ===== editGalleryModal ===== */
openEditGalleryModal = async (_this) => {
    let form_edit_gallery = $('#form_edit_gallery');
    let formData = form_edit_gallery.serializeObject();

    for (let [key] of Object.entries(formData)) {
        $(`#gallery_edit_${key}`).val(_this.attr(`data-${key}`));
    }

    $('#editGalleryModal').modal('show');
}

closeEditGalleryModal = () => {
    closePopup();
}
/* ===== End editGalleryModal ===== */

/* ===== editProjectModal ===== */
openNewProjectModal = () => {
    $('#editProjectModal').modal('show');
}
openEditProjectModal = () => {
    $('#editProjectModal').modal('show');
}
/* ===== End editProjectModal ===== */

/* Delete Item */
deleteItem = (_this) => {

    Notiflix.Confirm.Show(
        'Xác nhận',
        'Bạn chắc chắn muốn xoá?',
        'Đồng ý',
        'Huỷ',
        () => {
            let type = _this.attr('data-type');
            let id = _this.attr('data-id');
            $(`#${type}_${id}`).remove();
        },
        () => {
        }
    );
}
/* End Delete Item */

/*===========================PROJECT==================================*/
const generateNewProject = (data) => {

    $('.project-list').prepend(`
        <div class="timeline-item timeline-edit" id="project_${data.id}">
            <h4 class="item-title project-${data.id}-name"></h4>
            <span class="item-period project-${data.id}-timeline"></span>
            <p class="color-gray project-${data.id}-description"></p>
            <ul class="project-info">
                <li class="info-item">
                    <span class="info-title">Khách hàng:</span>
                    <span class="info-value project-${data.id}-customer"></span>
                </li>
                <li class="info-item">
                    <span class="info-title">Số lượng thành viên:</span>
                    <span class="info-value project-${data.id}-team_size"></span>
                </li>
                <li class="info-item">
                    <span class="info-title">Vị trí công việc:</span>
                    <span class="info-value project-${data.id}-job_position"></span>
                </li>
                <li class="info-item">
                    <span class="info-title">Vai trò:</span>
                    <span class="info-value project-${data.id}-role"></span>
                </li>
                <li class="info-item">
                    <span class="info-title">Skill:</span>
                    <div class="info-tag project-${data.id}-technology_used"></div>
                </li>
            </ul>

            <div class="edit-area">
                <button type="button"
                        data-id="${data.id}"
                        data-name=""
                        data-role=""
                        data-customer=""
                        data-end_time=""
                        data-start_time=""
                        data-description=""
                        data-job_position=""
                        data-team_size="${data.team_size}"
                        data-technology_used="${data.technology_used}"
                        data-url="${data.updateUrl}"
                        onclick="modalOpen($(this), 'project', 'edit')"
                        class="edit-area__button project-${data.id}-button_edit color-text-i color-text-hover-i-white color-background-before">
                    <i class="fas fa-pen"></i>
                </button>
                <button type="button"
                        data-type="project"
                        data-id="${data.id}"
                        data-delete_url="${data.deleteUrl}"
                        onclick="deleteItem($(this))"
                        class="edit-area__button color-text-i color-text-hover-i-white color-background-before">
                    <i class="fas fa-trash"></i>
                </button>
            </div>
        </div>
    `);

    $(`.project-${data.id}-name`).text(data.name);
    $(`.project-${data.id}-role`).text(data.role);
    $(`.project-${data.id}-customer`).text(data.customer);
    $(`.project-${data.id}-team_size`).text(data.team_size);
    $(`.project-${data.id}-description`).text(data.description);
    $(`.project-${data.id}-job_position`).text(data.job_position);
    $(`.project-${data.id}-timeline`).text(` ${data.start_time} - ${data.end_time}`);

    $(`.project-${data.id}-button_edit`).attr({
        'data-technology_used': data.technology_used,
        'data-job_position': data.job_position,
        'data-description': data.description,
        'data-start_time': data.start_time,
        'data-end_time': data.end_time,
        'data-customer': data.customer,
        'data-name': data.name
    });

    data.technology_used.split(',').forEach(item => {
        $(`.project-${data.id}-technology_used`).append( $('<span></span>').text(item) );
    })
};
