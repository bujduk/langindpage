$(document).ready(() => {
    $('#form_edit_about').submit((e) => {
        e.preventDefault();

        let _this = $(e.target);

        clearError();

        if (_this.valid()) {

            /* Response success */
            $.getJSON("../../data/response-ex/about.json", (result) => {
                bindingData(result.data, 'about');
            });

            $('#editAboutModal').modal('hide');
            /* End Response success */
        }
    });

    $('#form_edit_info').submit((e) => {
        e.preventDefault();

        let _this = $(e.target);

        clearError();

        if (_this.valid()) {

            /* Response success */
            $.getJSON("../../data/response-ex/about.json", (result) => {
                bindingData(result.data, 'about');
            });

            $('#editInfoModal').modal('hide');
            /* End Response success */
        }
    });

    $('#form_edit_education').submit((e) => {
        e.preventDefault();

        let _this = $(e.target);

        clearError();

        if (_this.valid()) {
            /* Response success */
            $.getJSON("./assets/response-ex/education.json", (result) => {
                if (_this.attr('data-type') === 'edit' || $(`#education_${result.data.id}`).length > 0) {
                    bindingData(result.data, 'education');
                } else {
                    generateNewEducation(result.data);
                }
                $('#editEducationModal').modal('hide');
            });

            /* End Response success */
        }
    });

    $('#form_edit_experience').submit((e) => {
        e.preventDefault();

        let _this = $(e.target);

        clearError();

        if (_this.valid()) {
            /* Response success */
            $.getJSON("./assets/response-ex/experience.json", (result) => {
                if (_this.attr('data-type') === 'edit' || $(`#experience_${result.data.id}`).length > 0) {
                    bindingData(result.data, 'experience');
                } else {
                    generateNewExperience(result.data);
                }
                $('#editExperienceModal').modal('hide');
            });

            /* End Response success */
        }
    });

    $('#form_edit_skill').submit((e) => {
        e.preventDefault();

        let _this = $(e.target);

        clearError();

        if (_this.valid()) {
            /* Response success */
            $.getJSON("./assets/response-ex/skill.json", (result) => {
                if ($(`#skill_${result.data.id}`).length > 0) {
                    bindingData(result.data, 'skill')
                } else {
                    generateNewSkill(result.data);
                }

                $('#editSkillModal').modal('hide');
            });
            /* End Response success */
        }
    });

    $('#form_new_knowledge').submit((e) => {
        e.preventDefault();

        let _this = $(e.target);

        clearError();

        if (_this.valid()) {
            /* Response success */
            $.getJSON("./assets/response-ex/knowledge.json", (result) => {
                generateNewKnowledge(result.data);

                $('#newKnowledgeModal').modal('hide');
            });
            /* End Response success */
        }
    });

    $('#form_edit_knowledge').submit((e) => {
        e.preventDefault();

        let _this = $(e.target);

        clearError();

        if (_this.valid()) {
            /* Response success */
            $.getJSON("./assets/response-ex/knowledge_edit.json", (result) => {
                bindingData(result.data, 'knowledge');

                $('#editKnowledgeModal').modal('hide');
            });
            /* End Response success */
        }
    });

    $('#form_edit_service').submit((e) => {
        e.preventDefault();

        let listIcon = $('#form_edit_service .form-icon');
        let selectedIcon = $('.service_icon_list .selected');
        let _this = $(e.target);
        let no_error = true;

        clearError();

        if (selectedIcon.length === 0) {
            no_error = false;
            $('#service_icon_error').html($.validator.messages.iconrequired);
            listIcon.addClass('error');
        }

        if (_this.valid() && no_error) {

            /* Response success */
            $.getJSON("./assets/response-ex/service.json", (result) => {
                if (_this.attr('data-type') === 'edit' || $(`#service_${result.data.id}`).length > 0) {
                    bindingData(result.data, 'service');
                } else {
                    generateNewService(result.data);
                }
                $('#editServiceModal').modal('hide');
            });
            /* End Response success */
        }
    });

    $('#form_add_gallery').submit((e) => {
        e.preventDefault();
        let _this = $(e.target);

        clearError();

        if (_this.valid()) {
            let list_icon = $('#form_add_gallery .input-gallery-list .list-col');

            if (list_icon.length === 0) {
                $('#gallery_file_error').html($.validator.messages.imagerequired);
            } else {
                $('#addGalleryModal').modal('hide');
            }

            /* Response success */
            // $.getJSON("./assets/response-ex/skill.json", (result) => {
            //     // bindingData(result.data, 'skill');
            //

            // });
            /* End Response success */
        }
    });

    $('#form_edit_gallery').submit((e) => {
        e.preventDefault();

        let _this = $(e.target);

        clearError();

        if (_this.valid()) {
            /* Response success */

            $('#editGalleryModal').modal('hide');
            /* End Response success */
        }
    });
});
