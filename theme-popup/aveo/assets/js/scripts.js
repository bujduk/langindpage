(function($) {
"use strict";
	$(document).ready(function ($) {
		var custom_styles = "";

		function skillsStyles() {
			$( '.skill-container' ).each( function() {
				var value = $(this).attr('data-value');

				if( typeof value != 'undefined' ) {
					var id = $(this).attr('id'),
					$custom_style = '#' + id + ' .skill-percentage { width: ' + value + '%; } ';
					custom_styles += $custom_style;
				}
			});
			$('head').append('<style data-styles="aveo-theme-skills-css" type="text/css">' + custom_styles  + '</style>');
		}

		skillsStyles();

		$(this).ajaxComplete(function() {
			$('style[data-styles="aveo-theme-skills-css"]').remove().detach();
			skillsStyles();
		});
	});
})(jQuery);

(function($) {
    "use strict";
    $(document).ready(function ($) {
        // Testimonials Slider
        var mobile_mode_items = "",
            tablet_mode_items = "",
            items = "";

        $( '.testimonials' ).each( function() {
            var mobile_mode_items = $(this).attr('data-mobile-items'),
                tablet_mode_items = $(this).attr('data-tablet-items'),
                items = $(this).attr('data-items'),
                id = $(this).attr('id');

            $("#" + id + ".testimonials.owl-carousel").owlCarousel({
                nav: true, // Show next/prev buttons.
                items: 2, // The number of items you want to see on the screen.
                loop: false, // Infinity loop. Duplicate last and first items to get loop illusion.
                navText: false,
                margin: 10,
                responsive : {
                    // breakpoint from 0 up
                    0 : {
                        items: mobile_mode_items,
                    },
                    // breakpoint from 768 up
                    768 : {
                        items: tablet_mode_items,
                    },
                    1200 : {
                        items: items,
                    }
                }
            });
        });
    });
})(jQuery);

jQuery(document).ready(function ($) {
    var custom_styles = "";

    function columnStyles() {
        custom_styles = "";
        $( '.fw-col-inner' ).each( function() {
            var paddings = $(this).attr('data-paddings');

            if( typeof paddings != 'undefined' || paddings != '0px 0px 0px 0px' ) {
                var id = $(this).attr('id'),
                    $custom_style = '#' + id + '{ padding: ' + paddings + '; } ';
                custom_styles += $custom_style;
            }
        });
        $('head').append('<style data-styles="aveo-theme-columns-css" type="text/css">' + custom_styles  + '</style>');
    }

    columnStyles();

    $(this).ajaxComplete(function() {
        $('style[data-styles="aveo-theme-columns-css"]').remove().detach();
        columnStyles();
    });
});


jQuery(document).ready(function ($) {
    var custom_styles = "";

    function buttonStyles() {
        custom_styles = "";
        $( 'a.btn' ).each( function() {
            var margin_top = $(this).attr('data-mtop'),
                margin_bottom = $(this).attr('data-mbottom');

            if( typeof margin_top != 'undefined' ) {
                var id = $(this).attr('id'),
                    $custom_style = '#' + id + '.btn { margin-top: ' + margin_top + 'px; margin-bottom: ' + margin_bottom + 'px; } ';
                custom_styles += $custom_style;
            }
        });
        $('head').append('<style data-styles="aveo-theme-button-css" type="text/css">' + custom_styles  + '</style>');
    }

    buttonStyles();

    $(this).ajaxComplete(function() {
        $('style[data-styles="aveo-theme-button-css"]').remove().detach();
        buttonStyles();
    });
});

