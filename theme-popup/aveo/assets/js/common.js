parseURLParamToKeyValue = (data) => {
    return JSON.parse('{"' + decodeURI(data).replace(/"/g, '\\"').replace(/&/g, '","').replace(/=/g,'":"') + '"}')
};

$(function () {
    $.fn.serializeObject = function () {
        let o = {};
        let a = this.serializeArray();
        $.each(a, function () {
            if (o[this.name]) {
                if (!o[this.name].push) {
                    o[this.name] = [o[this.name]];
                }
                o[this.name].push(this.value || '');
            } else {
                o[this.name] = this.value || '';
            }
        });
        return o;
    };
});

/*
* @param data Dữ liệu data trên server trả về
* @param prefixClass Tiền tố class cho các thành phần
* @valueParam prefixClass in [about, service, education, experience, gallery, skill, knowledge]
*
* @call bindingData(data, 'about')
* */
bindingData = (data, prefixClass) => {
    let _id = (data.id) ? data.id : 0;

    $.each(data, (key, value) => {
        $(`.${prefixClass}-${_id}-${key}`).text(value);
        $(`.${prefixClass}-${_id}-button_edit`).attr(`data-${key}`, value);
    });

    if (prefixClass === 'about') {
        $('.social-links a').addClass('hidden').attr('href', '');
        $(`.about-${_id}-freelance`).html(data.freelance ? 'Available' : 'Not Available');
        $(`.about-${_id}-button_edit`).attr(`data-freelance_editview`, data.freelance ? 'Available' : 'Not Available');
        $(`.about-${_id}-email`).attr('href', `mailto:${data.email}`);
        $(`.about-${_id}-img_avatar`).attr('src', data.img_avatar);
        // $(`.about-${_id}-img_background`).attr('src', data.img_background);

        $.each(data.socials, (key, value) => {
            if (value && value !== '') {
                $(`.about-${_id}-${key}`).attr('href', value).removeClass('hidden');
            }
            $(`.about-${_id}-button_edit`).attr(`data-${key}`, value);
        });
    }

    if (prefixClass === 'education') {
        $(`.education-${_id}-timeline`).html(`${data.start_time.replace('-', '/')} - ${(data.end_time.replace('-', '/'))}`);

        let time = convertTime(data.start_time, data.end_time);
        for (const [key, value] of Object.entries(time)) {
            $(`.education-${_id}-button_edit`).attr(`data-${key}`, value);
        }
    }

    if (prefixClass === 'experience') {
        $(`.experience-${_id}-timeline`).html(`${data.start_time.replace('-', '/')} - ${(data.is_current_working) ? 'Current' : data.end_time.replace('-', '/')}`);

        let time = convertTime(data.start_time, data.end_time);
        for (const [key, value] of Object.entries(time)) {
            $(`.experience-${_id}-button_edit`).attr(`data-${key}`, value);
        }
    }

    if (prefixClass === 'skill') {
        $(`.skill-${_id}-percentage`).attr('data-value', data.score);
        $(`.skill-${_id}-percentage .skill-percentage`).css('width', `${data.score}%`);
    }

    if (prefixClass === 'service') {
        $(`.service-${_id}-icon_class i`).attr('class', data.icon);
    }
}

/* Generate Time input */
convertTime = (start, end) => {
    start = start.split('-');
    end = (end) ? end.split('-') : null;

    return {
        start_month: start[0],
        start_year: start[1],
        end_month: end ? end[0] : '',
        end_year: end ? end[1] : ''
    };
}



/* ============================= COMMON CLEAR FORM ERROR ===================================== */
modalClearFormErr = () => {
    $('.ryan-error__input').removeClass('ryan-error__input');
    $('.ryan-error__text').html('')
}

commonShowNotifyErr = () => {
    Notiflix.Notify.Failure(common_error);
}

commonShowErr = (errors, obj) => {
    $.each(errors, function (key, value) {
        if (key === 'start_time') {
            $(`#${obj}start_month`).addClass('ryan-error__input');
            $(`#${obj}start_year`).addClass('ryan-error__input');
            $(`#${obj}time_error`).text(value[0]);
        } else if (key === 'end_time') {
            $(`#${obj}end_month`).addClass('ryan-error__input');
            $(`#${obj}end_year`).addClass('ryan-error__input');
            $(`#${obj}time_error`).text(value[0]);
        } else {
            $(`#${obj}${key}_error`).text(value[0]);
            $(`#${obj}${key}`).addClass('ryan-error__input');
        }
    });
}
