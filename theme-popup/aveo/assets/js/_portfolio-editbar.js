$(document).ready(() => {
    $('.portfolio-editbar-button').on('click', (e) => {
        e.stopPropagation();
        $('.portfolio-editbar-bg').addClass('anima').toggleClass('show')
    })

    $('body').on('click', () => {
        $('.portfolio-editbar-bg.anima.show').removeClass('show');
    })
})