$(document).ready(() => {
    $('#about_form').submit((e) => {
        e.preventDefault();

        modalClearFormErr();

        let form = $(e.target);
        let formData = form.serializeObject();

        formData.p_page_id = $('#page_id').val();

        getFormRequest('../../data/response-ex/about.json', formData)
            .then(function (responseData) {
                bindingData(responseData.data, 'about');

                $('#about_modal').modal('hide');
            }, function (responseErr) {
                if (responseErr.status === 422) {
                    commonShowErr(responseErr.responseJSON.errors, 'about');
                } else {
                    commonShowNotifyErr();
                }
            });
    });

    $('#info_form').submit((e) => {
        e.preventDefault();

        modalClearFormErr();

        let form = $(e.target);
        let formData = form.serializeObject();

        formData.p_page_id = $('#page_id').val();

        getFormRequest('../../data/response-ex/about.json', formData)
            .then(function (responseData) {
                bindingData(responseData.data, 'about');

                $('#info_modal').modal('hide');
            }, function (responseErr) {
                if (responseErr.status === 422) {
                    commonShowErr(responseErr.responseJSON.errors, 'info');
                } else {
                    commonShowNotifyErr();
                }
            });
    });

    $('#education_form').submit((e) => {
        e.preventDefault();

        modalClearFormErr();

        let form = $(e.target);
        let formData = form.serializeObject();

        formData.p_page_id = $('#page_id').val();
        formData.start_time = formData['start_month'].padStart(2, '0') + '-' + formData['start_year'].padStart(4, '0');
        formData.end_time = formData['end_month'].padStart(2, '0') + '-' + formData['end_year'].padStart(4, '0');

        if (form.attr('data-type') === action_add) {
            postFormRequest('../../data/response-ex/education.json', formData)
                .then(function (responseData) {
                    bindingNewItem(responseData.data, 'education');

                    $('#education_modal').modal('hide');
                }, function (responseErr) {
                    if (responseErr.status === 422) {
                        commonShowErr(responseErr.responseJSON.errors, 'education');
                    } else {
                        commonShowNotifyErr();
                    }
                });
        }

        if (form.attr('data-type') === action_edit) {
            postFormRequest('../../data/response-ex/education.json', formData)
                .then(function (responseData) {
                    bindingData(responseData.data, 'education');

                    $('#education_modal').modal('hide');
                }, function (responseErr) {
                    if (responseErr.status === 422) {
                        commonShowErr(responseErr.responseJSON.errors, 'education');
                    } else {
                        commonShowNotifyErr();
                    }
                });
        }
    });

    $('#experience_form').submit((e) => {
        e.preventDefault();

        modalClearFormErr();

        let form = $(e.target);
        let formData = form.serializeObject();
        let elementIsCurrentWorking = $('#experience_is_current_working');
        let isCurrentWorking = elementIsCurrentWorking.prop("checked") ? true : elementIsCurrentWorking.is(":checked");

        formData.p_page_id = $('#page_id').val();
        formData.start_time = formData['start_month'].padStart(2, '0') + '-' + formData['start_year'].padStart(4, '0');
        formData.end_time = !isCurrentWorking ? formData['end_month'].padStart(2, '0') + '-' + formData['end_year'].padStart(4, '0') : '';
        formData.is_current_working = isCurrentWorking ? 1 : 0;

        if (form.attr('data-type') === action_add) {
            postFormRequest('../../data/response-ex/experience.json', formData)
                .then(function (responseData) {
                    bindingNewItem(responseData.data, 'experience');

                    $('#experience_modal').modal('hide');
                }, function (responseErr) {
                    if (responseErr.status === 422) {
                        commonShowErr(responseErr.responseJSON.errors, 'experience');
                    } else {
                        commonShowNotifyErr();
                    }
                });
        }

        if (form.attr('data-type') === action_edit) {
            postFormRequest('../../data/response-ex/experience.json', formData)
                .then(function (responseData) {
                    bindingData(responseData.data, 'experience');

                    $('#experience_modal').modal('hide');
                }, function (responseErr) {
                    if (responseErr.status === 422) {
                        commonShowErr(responseErr.responseJSON.errors, 'experience');
                    } else {
                        commonShowNotifyErr();
                    }
                });
        }
    });

    $('#service_form').submit((e) => {
        e.preventDefault();

        modalClearFormErr();

        let form = $(e.target);
        let listIcon = $('#service_form .form-icon');
        let serviceIcon = $('#service_icon');
        let formData = form.serializeObject();
        let submitData = {
            'name': formData.name,
            'icon': formData.icon,
            'description': formData.description,
            'p_page_id': $('#page_id').val()
        };

        if (form.attr('data-type') === 'add') {
            postFormRequest('../../data/response-ex/service.json', submitData).then(function (responseData) {
                bindingNewItem(responseData.data, 'service');

                Notiflix.Notify.Success(create_success);

                $('#service_modal').modal('hide');
            }, function (responseErr) {
                if (responseErr.status === 422) {
                    commonShowErr(responseErr.responseJSON.errors, 'service');
                } else {
                    commonShowNotifyErr();
                }
            });
        } else {
            postFormRequest('../../data/response-ex/service.json', submitData).then(function (responseData) {
                bindingData(responseData.data, 'service');

                Notiflix.Notify.Success(update_success);

                $('#service_modal').modal('hide');
            }, function (responseErr) {
                if (responseErr.status === 422) {
                    commonShowErr(responseErr.responseJSON.errors, 'service');
                } else {
                    commonShowNotifyErr();
                }
            });
        }
    });

    $('#gallery_add_form').submit((e) => {
        e.preventDefault();

        modalClearFormErr();

        let form = $(e.target);
        let formData = new FormData();

        formData.p_page_id = $('#page_id').val();

        for (const [key, value] of Object.entries(galleryImageList)) {
            formData.append("fileGalleries[]", value);
        }

        postFileUploadFormRequest(form.attr('data-url'), formData).then(function (responseData) {

            if (responseData.data.flagReload) {
                window.location.reload();
            } else {
                generateNewGallery(responseData.data.images);
            }

            galleryImageList = [];

            $('#gallery_add_modal').modal('hide');
        }, function (responseErr) {
            if (responseErr.status === 422) {
                commonShowErr(responseErr.responseJSON.errors, 'gallery');
            } else {
                commonShowNotifyErr();
            }
        });
    });

    $('#gallery_edit_form').submit((e) => {
        e.preventDefault();

        modalClearFormErr();

        let form = $(e.target);

        let formData = {
            'title': form.find('#gallery_title').val(),
            'p_page_id': $('#page_id').val()
        }

        postFormRequest('../../data/response-ex/gallery_edit.json', formData)
            .then(function (responseData) {
                bindingData(responseData.data, 'gallery');

                $('#gallery_edit_modal').modal('hide');
            }, function (responseErr) {
                if (responseErr.status === 422) {
                    commonShowErr(responseErr.responseJSON.errors, 'gallery');
                } else {
                    commonShowNotifyErr();
                }
            });
    });

    $('#knowledge_add_form').submit((e) => {
        e.preventDefault();

        modalClearFormErr();

        let form = $(e.target);
        let submitData = form.serializeObject();

        submitData.p_page_id = $('#page_id').val();

        if (!Array.isArray(submitData.content)) {
            submitData.content = [submitData.content];
        }

        postFormRequest('../../data/response-ex/knowledge.json', submitData).then(function (responseData) {

            bindingNewItem(responseData.data, 'knowledge');

            Notiflix.Notify.Success(create_success);

            $('#knowledge_add_modal').modal('hide');
        }, function (responseErr) {
            if (responseErr.status === 422) {
                commonShowErr(responseErr.responseJSON.errors, 'knowledge');
            } else {
                commonShowNotifyErr();
            }
        });
    });

    $('#knowledge_edit_form').submit((e) => {
        e.preventDefault();

        modalClearFormErr();

        let form = $(e.target);
        let submitData = form.serializeObject();

        submitData.p_page_id = $('#page_id').val();

        if (!Array.isArray(submitData.content)) {
            submitData.content = [submitData.content];
        }

        getFormRequest('../../data/response-ex/knowledge_edit.json', submitData).then(function (responseData) {
            bindingData(responseData.data, 'knowledge');

            Notiflix.Notify.Success(update_success);

            $('#knowledge_edit_modal').modal('hide');
        }, function (responseErr) {
            if (responseErr.status === 422) {
                commonShowErr(responseErr.responseJSON.errors, 'knowledge');
            } else {
                commonShowNotifyErr();
            }
        });
    });

    $('#skill_form').submit((e) => {
        e.preventDefault();

        let form = $(e.target);
        let skillName = $('#skill_name');
        let submitData = form.serializeObject();

        submitData['p_page_id'] = $('#page_id').val();
        submitData['skill_id'] = skillName.attr('data-skill_id');
        submitData['type'] = skillName.attr('data-type');
        submitData['id'] = skillName.attr('data-id');

        modalClearFormErr();

        if (form.attr('data-type') === 'add') {
            getFormRequest('../../data/response-ex/skill.json', submitData).then(function (responseData) {
                bindingNewItem(responseData.data, 'skill');

                Notiflix.Notify.Success(create_success);

                $('#skill_modal').modal('hide');
            }, function (responseErr) {
                if (responseErr.status === 422) {
                    commonShowErr(responseErr.responseJSON.errors, 'skill');
                } else {
                    commonShowNotifyErr();
                }
            });
        } else {
            getFormRequest('../../data/response-ex/skill.json').then(function (responseData) {
                bindingData(responseData.data, 'skill', 'optio');

                Notiflix.Notify.Success(update_success);

                $('#skill_modal').modal('hide');
            }, function (responseErr) {
                if (responseErr.status === 422) {
                    commonShowErr(responseErr.responseJSON.errors, 'skill');
                } else {
                    commonShowNotifyErr();
                }
            });
        }
    });
});
