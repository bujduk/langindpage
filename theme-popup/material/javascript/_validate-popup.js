let lang = {
  en: {
    required: "This field is required.",
    remote: "Please fix this field.",
    email: "Please enter a valid email address.",
    url: "Please enter a valid URL.",
    date: "Please enter a valid date.",
    dateISO: "Please enter a valid date (ISO).",
    number: "Please enter a valid number.",
    digits: "Please enter only digits.",
    equalTo: "Please enter the same value again.",
    maxlength: $.validator.format( "Please enter no more than {0} characters." ),
    minlength: $.validator.format( "Please enter at least {0} characters." ),
    rangelength: $.validator.format( "Please enter a value between {0} and {1} characters long." ),
    range: $.validator.format( "Please enter a value between {0} and {1}." ),
    max: $.validator.format( "Please enter a value less than or equal to {0}." ),
    min: $.validator.format( "Please enter a value greater than or equal to {0}." ),
    step: $.validator.format( "Please enter a multiple of {0}." ),
    imagerequired: "Select at least one image",
    iconrequired: "Select at least one icon",
  },
  vi: {
    required: "Không được để trống.",
    remote: "Hãy sửa cho đúng.",
    email: "Email không hợp lệ.",
    url: "Hãy nhập URL.",
    date: "Hãy nhập ngày.",
    dateISO: "Hãy nhập ngày (ISO).",
    number: "Hãy nhập số.",
    digits: "Ký tự nhập phải là một số.",
    maxlength: $.validator.format("Không được nhập quá {0} kí tự."),
    minlength: $.validator.format("Không đươc nhập ít hơn {0} kí tự."),
    rangelength: $.validator.format("Hãy nhập từ {0} đến {1} kí tự."),
    range: $.validator.format("Hãy nhập từ {0} đến {1}."),
    max: $.validator.format("Hãy nhập từ {0} trở xuống."),
    min: $.validator.format("Hãy nhập từ {0} trở lên."),
    imagerequired: "Hãy chọn ít nhất một ảnh",
    iconrequired: "Hãy chọn ít nhất một icon",
  }
};

/* Language Selection */
$.extend($.validator.messages, lang['vi']);

