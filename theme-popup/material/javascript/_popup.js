$(document).ready(() => {
    $(".js-example-tokenizer").select2({
        tags: true,
        tokenSeparators: [',', ' '],
        minimumResultsForSearch: -1
    })

    $('.js-example-tokenizer').on('select2:open', function (e) {
        $(".js-example-tokenizer option[value='0']").remove();
        $('.select2-results').hide().slideDown("fast", "easeInOutQuint");
    });
})

/*======================KNOWLEDGE========================*/
generateKnowledgeInModal = (key) => {
    $('#knowledge_add_form .list_knowledge').append(`
        <div data-id="${key}" class="knowledge-item">
            <label class="popup-form-control form-knowledge">
                <input type="text" required id="knowledge_add_${key}_name"
                        name="content"
                        data-id="${key}"
                       oninput="inputNewKnowledgeModal($(this))"
                       class="form-input">
                <button type="button" 
                    onclick="deleteKnowledgeItemInModal('${key}')"
                    class="form-knowledge__button delete-button">
                    <i class="fas fa-trash"></i>
                </button>
                <button type="button" 
                    onclick="addnewKnowledgeItemInForm()"
                    class="form-knowledge__button addnew-button">
                    <i class="fas fa-plus"></i>
                </button>
            </label>
            <p id="klnew_${key}_error" class="form-error"></p>
        </div>
      `);
}

/*======================GALLERY========================*/
generateGalleryItemInModal = (key, src, name) => {
    $('#gallery_add_form .input-gallery-list').append(`
        <div class="col-md-4 col-xs-6 list-col" id="gallery_add_${key}">
            <div class="gallery-item">
                <div style="background-image: url(${src})"
                        class="gallery-item__image">
                </div>
                <div class="gallery-item__title">
                    ${name}
                </div>
                <button type="button"
                onclick="deleteGalleryAddItem('${key}')"
                 class="gallery-item__delete">
                    <i class="fas fa-trash"></i>
                </button>
            </div>
        </div>
      `)
}

/*=====================BINDINGNEWITEM===================*/

generateNewEducation = (data) => {
    let timeline = null;
    $('.education-list').prepend(`
                <div class="timeline-block" id="education_${data.id}">
                    <!-- DOT -->
                    <div class="timeline-dot"><h6>${(`${data.majors}`).charAt(0)}</h6></div>
                    <!-- TIMELINE CONTENT -->
                    <div class="card timeline-content">
                        <div class="card-content">
                            <div class="item-option">
                                <button type="button"
                                        class="item-option__edit btn__popup"
                                        data-id="${data.id}"
                                        data-majors="${data.majors}"
                                        data-start_time="${data.start_time}"
                                        data-end_time="${data.end_time}"
                                        data-studying_area="${data.studying_area}"
                                        data-description="${data.description}"
                                        data-url="${data.updateUrl}"
                                        onclick="modalOpen($(this), 'education', 'edit')">
                                    <i class="fas fa-pen"></i>
                                </button>
                                <button type="button"
                                        class="item-option__delete btn__popup"
                                        data-id="${data.id}"
                                        data-type="education"
                                        data-url="${data.deleteUrl}"
                                        onclick="deleteItem($(this))">
                                    <i class="fas fa-trash"></i>
                                </button>
                            </div>
                            <!-- TIMELINE TITLE -->
                            <h6 class="timeline-title education-${data.id}-majors"></h6>
                            <!-- TIMELINE TITLE INFO -->
                            <div class="timeline-info">
                                <h6>
                                    <small class="education-${data.id}-studying_area"></small>
                                </h6>
                                <h6>
                                    <small class="education-${data.id}-timeline"></small>
                                </h6>
                            </div>
                            <!-- TIMELINE PARAGRAPH -->
                            <p class="education-${data.id}-description"></p>
                        </div>
                    </div>
            </div>
        `);

    timeline = data.start_time.replace('-', '/') + ' - ' + data.end_time.replace('-', '/');
    $(`.education-${data.id}-timeline`).text(timeline);
    $(`.education-${data.id}-majors`).text(data.majors);
    $(`.education-${data.id}-studying_area`).text(data.studying_area);
    $(`.education-${data.id}-description`).text(data.description);
}

generateNewExperience = (data) => {
    let timeline = null;

    $('.experience-list').prepend(`
                <div class="timeline-block" id="experience_${data.id}">
                    <!-- DOT -->
                    <div class="timeline-dot"><h6>${(`${data.majors}`).charAt(0)}</h6></div>
                    <!-- TIMELINE CONTENT -->
                    <div class="card timeline-content">
                        <div class="card-content">
                            <div class="item-option">
                                <button type="button"
                                        class="item-option__edit btn__popup"
                                        data-id="${data.id}"
                                        data-job_title="${data.job_title}"
                                        data-start_time="${data.start_time}"
                                        data-end_time="${data.end_time}"
                                        data-company="${data.company}"
                                        data-is_current_working="${data.is_current_working ? 1 : 0}"
                                        data-description="${data.description}"
                                        data-url="${data.updateUrl}"
                                        onclick="modalOpen($(this), 'experience', 'edit')">
                                    <i class="fas fa-pen"></i>
                                </button>
                                <button type="button"
                                        class="item-option__delete btn__popup"
                                        data-id="${data.id}"
                                        data-type="experience"
                                        data-url="${data.deleteUrl}"
                                        onclick="deleteItem($(this))">
                                    <i class="fas fa-trash"></i>
                                </button>
                            </div>
                            <!-- TIMELINE TITLE -->
                            <h6 class="timeline-title experience-${data.id}-job_title"></h6>
                            <!-- TIMELINE TITLE INFO -->
                            <div class="timeline-info">
                                <h6>
                                    <small class="experience-${data.id}-company"></small>
                                </h6>
                                <h6>
                                    <small class="experience-${data.id}-timeline"></small>
                                </h6>
                            </div>
                            <!-- TIMELINE PARAGRAPH -->
                            <p class="experience-${data.id}-description"></p>
                        </div>
                    </div>
              </div>            
            `);

    timeline = data.start_time.replace('-', '/') + ' - ';
    if (data.end_time) {
        timeline += data.end_time.replace('-', '/');
    } else {
        timeline += 'Hiện tại'
    }

    $(`.experience-${data.id}-timeline`).text(timeline);
    $(`.experience-${data.id}-company`).text(data.company);
    $(`.experience-${data.id}-job_title`).text(data.job_title);
    $(`.experience-${data.id}-description`).text(data.description);
}

generateNewSkill = (data) => {
    let output = ``;

    data.forEach(item => {
        if ($(`#knowledge_${item.id}`).length === 0) {
            output += `
                            <li id="knowledge_${item.id}" class="knowledge-edit timeline-content">
                                <span class="knowledge__item--icon ">
                                    <i class="fas fa-check"></i>
                                    <span class="knowledge-${item.id}-content"></span>
                                </span>
                                <div class="item-option">
                                    <button class="btn__popup"
                                      type="button"
                                      data-id="${item.id}"
                                      data-content="${item.content}"
                                      data-url="${item.updateUrl}"
                                      onclick="modalOpen($(this), 'knowledge_edit', 'edit')">
                                        <i class="fas fa-pen"></i>
                                    </button>
                                    <button 
                                        type="button"
                                        data-type="knowledge"
                                        data-id="${item.id}"
                                        data-url="${item.deleteUrl}"
                                        onclick="deleteItem($(this))"
                                        class="btn__popup">
                                        <i class="fas fa-trash"></i>
                                    </button>
                                </div>
                            </li>
                        `
        } else {
            bindingData(item, 'knowledge');
        }
    });

    $('.knowledge-list').prepend(output);

    data.forEach(item => {
        $(`.knowledge-${item.id}-content`).text(item.content);
    });
}

generateNewService = (data) => {
    $('.skill-list').prepend(`
                <div id="skill_${data.id}" class="skill__item">
                    <div class="skillbar" data-percent="${data.score}%">
                        <div class="skillbar-item">
                            <div class="skillbar-title">
                                <span class="skill-${data.id}-name"></span>
                            </div>
                            <div class="skillbar-bar skill-${data.id}-percentage" style="width: ${data.score}%"></div>
                            <div class="skill-bar-percent skill-${data.id}-score"></div>
                        </div>
                    </div>
                    <div class="item-option">
                        <button type="button"
                                data-id="${data.id}"
                                data-name="${data.name}"
                                data-score="${data.score}"
                                data-url="${data.updateUrl}"
                                onclick="modalOpen($(this), 'skill', 'edit')"
                                class="item-option__edit btn__popup
                                skill-${data.id}-button_edit">
                            <i class="fas fa-pen"></i>
                        </button>
                        <button type="button"
                                data-type="skill"
                                data-id="${data.id}"
                                data-url="${data.deleteUrl}"
                                onclick="deleteItem($(this))"
                                class="item-option__delete btn__popup skill-${data.id}-button_delete">
                            <i class="fas fa-trash"></i>
                        </button>
                  </div>
                </div>
            `);

    $(`.skill-${data.id}-name`).text(data.name);
    $(`.skill-${data.id}-score`).text(`${data.score}%`);
}

generateNewKnowledge = (data) => {
    $('.service-list').prepend(`
           <div class=" col-md-4 col-sm-4 col-xs-12 " id="service_${data.id}">
              <div class="pricing">
                  <div class="card timeline-content">
                      <!--PRICING TOP-->
                      <div class="pricing-top">
                          <div class="item-option">
                              <button type="button"
                                      class="item-option__edit btn__popup"
                                      data-id="${data.id}"
                                      data-icon="${data.icon}"
                                      data-name="${data.name}"
                                      data-description="${data.description}"
                                      data-url="${data.updateUrl}"
                                      onclick="modalOpen($(this), 'service', 'edit')">
                                      <i class="fas fa-pen"></i>
                              </button>
                              <button type="button"
                                      class="item-option__delete btn__popup"
                                      data-id="${data.id}"
                                      data-url="${data.deleteUrl}"
                                      data-type="service"
                                      onclick="deleteItem($(this))">
                                      <i class="fas fa-trash"></i>
                              </button>
                          </div>
                          <span class="service-${data.id}-icon_class"><i class="${data.icon}"></i></span>
                      </div>
                      <!--PRICING DETAILS-->
                      <div class="pricing-bottom text-center text-capitalize">
                          <h3 class="service-${data.id}-name"></h3>
                          <p class="service-${data.id}-description"></p>
                      </div>
                  </div>
              </div>    
          </div>
                  `);

    $(`.service-${data.id}-name`).text(data.name);
    $(`.service-${data.id}-description`).text(data.description);
}

generateNewProject = (data) => {
    let timeline = null;
    $('.project-list').prepend(`
        <div class="timeline-block" id="project_${data.id}">
            <!-- DOT -->
            <div class="timeline-dot"><h6>${(`${data.majors}`).charAt(0)}</h6></div>
            <!-- TIMELINE CONTENT -->
            <div class="card timeline-content">
                <div class="card-content">
                    <div class="item-option">
                        <button type="button"
                                class="item-option__edit btn__popup"
                                data-id="${data.id}"
                                data-name="${data.name}"
                                data-start_time="${data.start_time}"
                                data-end_time="${data.end_time}"
                                data-customer="${data.customer}"
                                data-team_size="${data.team_size}"
                                data-job_position="${data.job_position}"
                                data-role="${data.role}"
                                data-freelance="${data.freelance}"
                                data-description="${data.description}"
                                data-url="${data.updateUrl}"
                                onclick="modalOpen($(this), 'project', 'edit')">
                            <i class="fas fa-pen"></i>
                        </button>
                        <button type="button"
                                class="item-option__delete btn__popup"
                                data-id="${data.id}"
                                data-type="project"
                                data-url="${data.deleteUrl}"
                                onclick="deleteItem($(this))">
                            <i class="fas fa-trash"></i>
                        </button>
                    </div>
                    <!-- TIMELINE TITLE -->
                    <h6 class="timeline-title project-${data.id}-name"></h6>
                    <!-- TIMELINE TITLE INFO -->
                    <h6 class="timeline-time">
                        <small class="project-${data.id}-timeline"></small>
                    </h6>
                    <!-- TIMELINE PARAGRAPH -->
                    <p class="project-${data.id}-description"></p>
                    <p class="project__item--content">
                        Client:
                        <span class="project-${data.id}-customer"></span>
                    </p>
                    <p class="project__item--content">
                        Số lượng thành viên:
                        <span class="project-${data.id}-team_size"></span>
                    </p>
                    <p class="project__item--content">
                        Vị trí công việc:
                        <span class="project-${data.id}-job_position"></span>
                    </p>
                    <p class="project__item--content">
                        Vai trò:
                        <span class="project-${data.id}-role"></span>
                    </p>
                    <div class="stack">
                        <span class="project__item--content">Stack sử dụng: </span>
                        <div class="project-${data.id}-technology_used"></div>
                    </div>
                </div>
            </div>
        </div>
        `);

    timeline = data.start_time.replace('-', '/') + ' - ' + data.end_time.replace('-', '/');
    $(`.project-${data.id}-timeline`).text(timeline);
    $(`.project-${data.id}-name`).text(data.name);
    $(`.project-${data.id}-customer`).text(data.customer);
    $(`.project-${data.id}-description`).text(data.description);
    $(`.project-${data.id}-team_size`).text(data.team_size);
    $(`.project-${data.id}-job_position`).text(data.job_position);
    $(`.project-${data.id}-role`).text(data.role);
    $(`.project-${data.id}-technology_used`).text(data.technology_used);
}