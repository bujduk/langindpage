const title_delete = `{{ __('ITNavi Confirm') }}`;
const message_delete = `{{ __('Do you want to delete it now? This cannot be undone.') }}`;
const label_confirm = `{{ __('Yes') }}`;
const label_cancel = `{{ __('No') }}`;
const action_failed = `{{ __('Failed action!') }}`;
const update_success = `{{ __('Update successfully!') }}`;
const upload_file_success = `{{ __('Upload file successfully!') }}`;
const create_success = `{{ __('Create successfully!') }}`;
const delete_success = `{{ __('Deleted successfully!') }}`;
const save_them_success = `{{ __('Save theme successfully!') }}`;
const common_error = `{{ __('An error has occurred') }}`;
const present = `{{ __('Present') }}`;

const title_education_add = `{{ __('Add education') }}`;
const title_education_edit = `{{ __('Edit education') }}`;
const title_experience_add = `{{ __('Add experience') }}`;
const title_experience_edit = `{{ __('Edit experience') }}`;
const title_skill_add = `{{ __('Add skill') }}`;
const title_skill_edit = `{{ __('Edit skill') }}`;
const title_knowledge_add = `{{ __('Add knowledge') }}`;
const title_knowledge_edit = `{{ __('Edit knowledge') }}`;
const title_service_add = `{{ __('Add service') }}`;
const title_service_edit = `{{ __('Edit service') }}`;
const title_gallery = `{{ __('Gallery') }}`;

const freelance_available = 'Available';
const freelance_not_available = 'Not Available';

const image = `Image`;
const too_2mb = `have size too 2MB`;

const knowledge_more_than_10_item = `{{ __('Up to 10 records per new creation!') }}`;
const gallery_more_than_12_item = `{{ __('Up to 12 records per file download!') }}`;
const gallery_incorrect_format = `{{ __('incorrect format allowed!') }}`;

const month_default = 1;
const year_default = 2020;


const prefix_about = '{{ \Modules\Portfolio\Entities\PPage::TYPE_ABOUT }}';
const prefix_skill = '{{ \Modules\Portfolio\Entities\PPage::TYPE_SKILL }}';
const prefix_service = '{{ \Modules\Portfolio\Entities\PPage::TYPE_SERVICE }}';
const prefix_gallery = '{{ \Modules\Portfolio\Entities\PPage::TYPE_GALLERY }}';
const prefix_education = '{{ \Modules\Portfolio\Entities\PPage::TYPE_EDUCATION }}';
const prefix_experience = '{{ \Modules\Portfolio\Entities\PPage::TYPE_EXPERIENCE }}';

const action_edit = 'edit';
const action_add = 'add';
