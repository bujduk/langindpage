const define_virus={Coronaviridae:{title:"Coronaviridae",subTitle:"8000 TCN",content:"Coronaviridae là một họ virus trong bộ Nidovirales. Một số loài của chúng gây ra các bệnh rất khác nhau ở các động vật có xương sống khác nhau như động vật có vú, chim và cá.Virus corona có tính biến đổi cao về mặt di truyền và một số loài vi rút riêng lẻ đồng gia huy lây nhiễm nhiều loài vật chủ bằng cách vượt qua hàng rào loài ngăn cản.",link:"https://vi.wikipedia.org/wiki/Alphacoronavirus"},Alphacoronavirus:{title:"Alphacoronavirus",subTitle:"2400 TCN",content:"Alphacoronaviruses (Alpha-CoV) là loài đầu tiên trong bốn chi, Alpha -, Beta-, Gamma- và Deltacoronavirus thuộc phân họ coronavirinae thuộc họ coronaviridae. Các coronavirus là các virut ARN sợi đơn, dương tính, bao gồm cả các loài sinh sống trên loài người và động vật. Trong phân họ này, virus có virion hình cầu với các hình chiếu bề mặt hình gậy và vỏ lõi. Tên này là bắt nguồn từ corona của tiếng Latin, có nghĩa là vương miện, mô tả sự xuất hiện của các hình chiếu nhìn thấy dưới kính hiển vi điện tử giống với corona mặt trời. Chi này chứa những gì trước đây được coi là coronavirus phylogroup 1.[1] Cả hai dòng Alpha - và Beta-coronavirus đều xuất phát từ nhóm gen loài dơi.",link:"https://vi.wikipedia.org/wiki/Alphacoronavirus"},Setracovirus:{title:"Setracovirus",subTitle:"2004",content:"Đang cập nhật",link:"",},Duvinacovirus:{title:"Duvinacovirus",subTitle:"",content:"Đang cập nhật",link:"",},HCoV_229E:{title:"HCoV_229E",subTitle:"1960",content:"Những người đầu tiên được phát hiện là nhiễm virus viêm phế quản truyền nhiễm ở gà và hai loại virus từ khoang mũi của bệnh nhân bị cảm lạnh thông thường được đặt tên là coronavirus 229E ở người và coronavirus OC43 ở người.",link:"https://en.wikipedia.org/wiki/Human_coronavirus_229E"},HCoV_NL63:{title:"HCoV_NL63",subTitle:"2004",content:"Đến cuối năm 2004, ba phòng thí nghiệm nghiên cứu độc lập đã báo cáo về việc phát hiện ra một loại coronavirus thứ tư ở người. Nó đã được các nhóm nghiên cứu khác nhau đặt tên là NL63, NL và New Haven coronavirus. Ba phòng thí nghiệm liên quan vẫn còn tranh luận về việc ai phát hiện ra virus đầu tiên và ai có quyền đặt tên cho nó.",link:"https://en.wikipedia.org/wiki/Human_coronavirus_NL63"},Betacoronavirus:{title:"Betacoronavirus",subTitle:"3300",content:"Betacoronavirus (β-CoV hoặc Beta-CoV) là một trong bốn chi coronavirus thuộc phân họ Orthocoronavirinae trong họ Coronaviridae của bộ Nidovirales. Chúng là các virus RNA có màng bọc, chiều dương, đơn chuỗi với nguồn gốc từ động vật. Mỗi chi coronavirus bao gồm các dòng dõi virus khác nhau, với chi Betacoronavirus chứa 4 dòng dõi như vậy. Trong các tài liệu cũ, chi này còn được gọi là coronavirus nhóm 2.",link:"https://vi.wikipedia.org/wiki/Betacoronavirus"},Embecovirus:{title:"Embecovirus",subTitle:"",content:"Đang cập nhật",link:"",},HCoV_OC43:{title:"HCoV_OC43",subTitle:"1960",content:"Những người đầu tiên được phát hiện là nhiễm virus viêm phế quản truyền nhiễm ở gà và hai loại virus từ khoang mũi của bệnh nhân bị cảm lạnh thông thường được đặt tên là coronavirus 229E ở người và coronavirus OC43 ở người.",link:"https://en.wikipedia.org/wiki/Human_coronavirus_OC43"},HCoV_HKU1:{title:"HCoV_HKU1",subTitle:"2005",content:"Đầu năm 2005, một nhóm nghiên cứu tại Đại học Hồng Kông đã báo cáo tìm thấy một loại coronavirus thứ năm ở hai bệnh nhân bị viêm phổi. Họ đặt tên cho nó là Human coronavirus HKU1.",link:"https://en.wikipedia.org/wiki/Human_coronavirus_HKU1"},Hibecovirus:{title:"Hibecovirus",subTitle:"",content:"Đang cập nhật",link:"",},Merbecovirus:{title:"Merbecovirus",subTitle:"",content:"Đang cập nhật",link:"",},Nobecovirus:{title:"Nobecovirus",subTitle:"",content:"Đang cập nhật",link:"",},Sarbecovirus:{title:"Sarbecovirus",subTitle:"",content:"Đang cập nhật",link:"",},MERS_CoV:{title:"MERS_CoV",subTitle:"",content:"Virus corona gây hội chứng hô hấp cấp Trung Đông, viết tắt MERS-CoV (tiếng Anh: Middle East respiratory syndrome coronavirus) là một chủng virus lần đầu tiên được nhận dạng vào năm 2012, thuộc họ Coronaviridae, gây ra bệnh hô hấp, sưng phổi và suy thận ở con người. Cho tới bây giờ những lan truyền phát xuất từ Tây Á, chủ yếu từ Ả Rập Xê Út. Những người bị mắc phải thường bị bệnh nặng đưa tới chết người. Hiện tại chưa biết được bao nhiêu phần trăm bị nhiễm trùng trở bệnh nặng. Tuy nhiên theo quan sát hiện thời thì virut này ít có lan từ người sang người[2] và con vật truyền bệnh chính có thể là dơi,rồi truyền sang Lạc đà một bướu – thỉnh thoảng lây sang con người.",link:""},SARS_CoV:{title:"SARS_CoV",subTitle:"2002",content:"Dịch SARS 2002–2004 do hội chứng hô hấp cấp tính nặng (SARS) gây ra bắt đầu từ Phật Sơn, Trung Quốc vào tháng 11 năm 2002. Hơn 8.000 người đã bị nhiễm và ít nhất 774 người tử vong trên toàn thế giới. Vào ngày 16 tháng 11 năm 2002, dịch SARS bắt đầu tại tỉnh Quảng Đông, Trung Quốc, giáp ranh với Hồng Kông. Ca nhiễm đầu tiên nhiều khả năng là một nông dân tại quận Thuận Đức của thành phố Phật Sơn. Trung Quốc thông báo cho Tổ chức Y tế Thế giới (WHO) về dịch bệnh vào ngày 10 tháng 2 năm 2003, bao gồm 305 ca nhiễm, trong đó có 105 nhân viên y tế và 5 ca tử vong.[5] Báo cáo sau đó cho biết dịch tại Quảng Đông đã đạt đỉnh vào giữa tháng 2 năm 2003. Tuy nhiên điều này có thể coi là không chính xác khi sau đó có thêm 806 ca nhiễm và 34 ca tử vong được báo cáo.[6] Ở giai đoạn đầu của dịch, chính quyền Trung Quốc ngăn cản báo chí đưa tin về SARS, trì hoãn việc báo cáo lên WHO, và ban đầu không cung cấp thông tin nào tới người dân Trung Quốc ngoài tỉnh Quảng Đông, được cho là nơi bắt nguồn dịch bệnh.[7] Ngoài ra, một nhóm công tác của WHO tới Bắc Kinh đã không được tới Quảng Đông trong vài tuần.[8] Hành động này khiến chính quyền bị nhiều nước chỉ trích và buộc phải thay đổi chính sách vào đầu tháng 4.",link:"https://vi.wikipedia.org/wiki/D%E1%BB%8Bch_SARS_2002%E2%80%932004"},SARS_CoV_2:{title:"SARS_CoV_2",subTitle:"2019",content:"Những người đầu tiên được phát hiện là nhiễm virus viêm phế quản truyền nhiễm ở gà và hai loại virus từ khoang mũi của bệnh nhân bị cảm lạnh thông thường được đặt tên là coronavirus 229E ở người và coronavirus OC43 ở người.",link:"https://vi.wikipedia.org/wiki/B%E1%BB%87nh_virus_corona_2019"},Gammacoronavirus:{title:"Gammacoronavirus",subTitle:"2800 TCN",content:"Đang cập nhật",link:"",},Deltacoronavirus:{title:"Deltacoronavirus",subTitle:"3000 TCN",content:"Đang cập nhật",link:"",},},blog=[{title:"Phần lớn các ca lây nhiễm COVID-19 do tiếp xúc rất gần, không đeo khẩu trang",image:"web/images/career-path/covid/example-blog.png",source:"Nguồn: Bộ Y Tế",link:"https://ncov.moh.gov.vn/en/-/canh-bao-phan-lon-cac-ca-lay-nhiem-covid-19-do-tiep-xuc-rat-gan-khong-eo-khau-trang"},{title:"Những cách phòng nCoV cho dân công sở",image:"web/images/career-path/covid/example-blog-2.png",source:"Nguồn: VnExprsss.net",link:"https://vnexpress.net/nhung-cach-phong-ncov-cho-dan-cong-so-4053691.html"},{title:"9 biện pháp mới nhất phòng chống dịch COVID-19 người dân cần biết",image:"web/images/career-path/covid/example-blog-3.png",source:"Nguồn: Bộ Y Tế",link:"https://ncov.moh.gov.vn/en/-/9-bien-phap-moi-nhat-phong-chong-dich-covid-19-nguoi-dan-can-biet"},{title:"Tăng cường sức khỏe trong mùa dịch",image:"web/images/career-path/covid/example-blog-4.png",source:"Nguồn: Bộ Y Tế",link:"https://moh.gov.vn/tin-tong-hop/-/asset_publisher/k206Q9qkZOqn/content/can-biet-tang-cuong-suc-khoe-trong-mua-dich"},{title:"Thứ tự xuất hiện triệu chứng Covid-19",image:"web/images/career-path/covid/example-blog-5.png",source:"Nguồn: VnExpress.net",link:"https://vnexpress.net/thu-tu-xuat-hien-trieu-chung-covid-19-4147331.html"},{title:"Các cách khai báo y tế ngừa COVID-19",image:"web/images/career-path/covid/example-blog-6.png",source:"Nguồn: Tuoitre.vn",link:"https://tuoitre.vn/cac-cach-khai-bao-y-te-ngua-covid-19-2020073009050305.htm"}];

$(function () {
    $('.cp-head--link').click(function (event) {
        var headerHeight = $('.cp-head').outerHeight();
        event.preventDefault();
        var linkHref = $(this).attr('href');

        $('html, body').animate({
            scrollTop: $(linkHref).offset().top - headerHeight
        }, 500);
    });

    $('.path_name')
        .keyup(function () {
            actionKeyupSearchTitle($('.path_name'));
        })
        .focus(function () {
            let errorMessage = $('#error-message');
            if ($('.path_name').val() === '' && !errorMessage.hasClass('hidden')) {
                errorMessage.addClass('hidden');
            }
        });

    $('.item-popup').click(function () {
        const dataId = $(this).attr('id');
        let dataItem = define_virus[dataId];

        if (dataItem) {
            setDataToModal(dataItem);
        }
    });

    blog.forEach((item, index) => {
        let html = `<div class="cp-body--bottom__blog">
                    <a target="_blank" href="${item.link}" class="blog-link">
                        <div class="blog-img">
                            <img src="${item.image}">
                        </div>
                        <div class="blog-text">
                            <h5 class="blog-title">${item.title}</h5>
                            <p class="blog-subtitle">${item.source}</p>
                        </div>
                    </a>
                </div>`;

        (index < 3) ? $('#blog-left').append(html) : $('#blog-right').append(html);
    });
});

setDataToModal = (dataItem) => {
    $('#title').text(dataItem.title);
    $('#modal-link').text(dataItem.link).attr('href', dataItem.link);
    $('#content').text(dataItem.content);
    $('#subTitle').text(dataItem.subTitle);
    $('#define').modal('show');
}

rootInfo = (name) => {
    let dataItem = define_virus[name];
    setDataToModal(dataItem);
}