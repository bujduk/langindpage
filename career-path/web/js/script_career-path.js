$(document).ready(() => {
    $('.candidate-path .similar-job .sj-list').slick({
        rows: 2,
        slidesPerRow: 3,
        prevArrow: $(".sj-pagging--prev"),
        nextArrow: $(".sj-pagging--next"),
        dots: true,
        adaptiveHeight: false,
        responsive: [
            {
                breakpoint: 992,
                settings: {
                    rows: 3,
                    slidesPerRow: 2
                }
            },
            {
                breakpoint: 677,
                settings: {
                    rows: 6,
                    slidesPerRow: 1
                }
            },
        ],
    }).init(() => {
        $('.candidate-path .similar-job .sj-item').removeClass('hide');
    })

    var headerHeight = $('.cp-head').outerHeight();
    $('.cp-head--link').click(function () {
        let id = $(this).attr('id');

        var linkHref = $(this).attr('href');

        $('html, body').animate({
            scrollTop: $(linkHref).offset().top - headerHeight
        }, 500);

        if (id === 'item-2') {

            $(".job-related-nav").trigger("click");

        } else if (id === 'item-3') {

            $(".skill-nav").trigger("click");

        } else if (id === 'item-4') {

            $(".blog-nav").trigger("click");

        }
    })
    

    $('.overlay').click(function () {
        removeOverlay();
        if ($('.item-popup').hasClass('active')){
            $('.item-popup').removeClass('active');
            $('.item-popup--next').html("");
            $('.item-popup').removeAttr('style');
        }
        
    })



    $('.item-popup').click(function () {
        let element = $(this)

        if ( !$('.item-popup--next').children().length > 0 ) {
            showClass(element);  
       } else {
        removeClass(element);
       }
    })

    function removeOverlay() {
        $('.overlay').addClass('hidden');
        $('.overlay-body--bottom').addClass('hidden');
        $('.overlay-bottom').addClass('hidden');
        $('body').removeAttr('style');
    }

    function removeClass(element) {
        $('.overlay').addClass('hidden');
        element.parent().children('.item-popup--next').html("");
        element.removeClass('active');
        element.removeAttr('style');
        $('.overlay-body--bottom').addClass('hidden');
        $('.overlay-bottom').addClass('hidden');
        $('body').removeAttr('style');
    }

    function showClass(element) {
        $('.overlay').removeClass('hidden');
        element.parent().children('.item-popup--next').append(`<button class="item-popup--define" onclick="jobDescription('jobDescription_id')">
            
            <img class="mo-ta-text app-desktop" src="asset/image/career-path/mo-ta-text.png"
                alt="">
            <img class="mo-ta-text app-mobile" src="asset/image/career-path/mo-ta-text-mobile.png" alt="">
            <img class="mo-ta-icon" src="asset/image/career-path/mo-ta-icon.png"
                alt="">
            </button>
            <button class="item-popup--continue" onclick="getModaData()">
            <img class="continue-icon"
                src="asset/image/career-path/continue-icon.png" alt="">
            <img class="continue-text app-desktop"
                src="asset/image/career-path/continue-text.png" alt="">
            <img class="continue-text app-mobile" src="asset/image/career-path/continue-text-mobile.png" alt="">
            </button>`);
            element.addClass('active');
            element.css("z-index", "1050");
            $('.overlay-body--bottom').removeClass('hidden');
            $('.overlay-bottom').removeClass('hidden');
            $('body').css('overflow', 'hidden');
    }
})

selectSearch = (index) => {
    $('#path_name').val(index);
}


itemClick = (itemId) => {
    $('.modal-header--text .modal__title').append(Alphacoronavirus.title);
}


jobDescription = (jobDescriptionId) => {
    console.log(jobDescriptionId);
    $('#define').modal('show');
}



