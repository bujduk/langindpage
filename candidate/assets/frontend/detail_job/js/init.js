let initPage = () => {
    initMediaPictureCarousel();
    closeApplyModal();
}

(function ($) {
    Notiflix.Loading.Init({
        backgroundColor: "rgba(255,255,255,0.6)",
        svgColor: "#06202D",
    });

    $('[data-toggle="popover"]').popover();

    $('#apply_upload_new').on('change', (e) => {
        _handleApplyFileChange($(e.target));
    });

    $('#apply_form').on('submit', (e) => {
        e.preventDefault();

        uploadSubmit($(e.target));
    });

    $('#apply_modal').on('hidden.bs.modal', () => {
        closeApplyModal();
    });

    initPage();
})(jQuery)