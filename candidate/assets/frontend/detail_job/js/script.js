let startLoadingNotiflix = (prefixClass = '') => {
    if (!!prefixClass && window.innerWidth >= 992) {
        Notiflix.Block.Pulse(prefixClass);
    } else {
        Notiflix.Loading.Pulse();
    }
}

let stopLoadingNotiflix = (prefixClass = '') => {
    if (!!prefixClass && window.innerWidth >= 992) {
        Notiflix.Block.Remove(prefixClass);
    } else {
        Notiflix.Loading.Remove();
    }
}

let initMediaPictureCarousel = () => {
    $(".sj-description__media").lightGallery();
}

let _handleCopyToClipboard = (textToCopy) => {
    var inputText;

    function isOS() {
        //can use a better detection logic here
        return navigator.userAgent.match(/ipad|iphone/i);
    }

    function createInput(text) {
        inputText = document.createElement('INPUT');
        inputText.setAttribute("type", "text");
        inputText.value = text;
        document.body.appendChild(inputText);
    }

    function selectText() {
        var range, selection;

        if (isOS()) {
            range = document.createRange();
            range.selectNodeContents(inputText);
            selection = window.getSelection();
            selection.removeAllRanges();
            selection.addRange(range);
        }

        inputText.select();
        inputText.setSelectionRange(0, 999999);
    }

    function copyTo() {
        document.execCommand('copy');
        document.body.removeChild(inputText);
        Notiflix.Notify.Success("Copy thành công");
    }

    createInput(textToCopy);
    selectText();
    copyTo(textToCopy);
}

let _handlePaginationArrowClick = (prefixClass, nextIndex) => {
    getListJobs(prefixClass, nextIndex);
}

let _handlePaginationInputChange = ($input, prefixClass) => {

    let totalPages = Math.floor(totalJobs[prefixClass] / 10);
    let nextIndex = _.toNumber($input.val());

    console.log(nextIndex)

    clearTimeout(paginationListTimeout);

    paginationListTimeout = setTimeout(() => {
        if (!!nextIndex && nextIndex <= totalPages) {
            getListJobs(prefixClass, nextIndex);

            $input.blur();
        }
    }, ($input[0].which === 13) ? 0 : 1000);
}

let _handleOpenApplyModal = ($button) => {
    let $form = $('#apply_modal .modal-form');
    let $formAlreadyApply = $('#apply_modal .modal-form__success.already_apply');
    let status = $button.attr('data-detail-already_apply');

    if (status === '1') {
        $formAlreadyApply.show();
    } else {
        $form.show();
    }

    $('#apply_modal').modal('show');
}

let closeApplyModal = () => {
    let $form = $('#apply_modal .modal-form');
    let $formSuccess = $('#apply_modal .modal-form__success.successfully');
    let $formAlreadyApply = $('#apply_modal .modal-form__success.already_apply');

    $form.hide();
    $formSuccess.hide();
    $formAlreadyApply.hide();
}

let _handleApplyFileChange = ($input) => {
    let files = $input[0].files;
    let applyUploadError = $('#apply_upload_error');

    applyUploadError.html('');

    let file = files[0];

    if (file.size > 2097152) {
        let html = `<span style="display:block;">File không được quá 2MB</span>`;

        applyUploadError.append(html);

    } else if (!file.type.includes("application/vnd") && file.type !== "application/pdf") {
        let html = `<span style="display:block;">File ${file.name} ${gallery_incorrect_format}</span>`;

        applyUploadError.append(html);

    } else {
        let fileReader = new FileReader();

        uploadFile = file;

        fileReader.onload = ((fileParam) => {
            return (e) => {
                $('.modal_form_toggle_new').remove();

                generateApplyCVNewFile(file.name);

                $('#apply_cv_new').prop('checked', true).is(':checked', true);
            }
        })(file);
        fileReader.readAsDataURL(file);
    }
}

let _handleDeleteNewCV = () => {
    let applyPortfolio = $('#apply_portfolio');
    let applyNewInput = $('#apply_cv_new');
    let applyNewToggle = $('.modal_form_toggle_new');
    let applyNewUpload = $('#apply_upload_new');

    if (applyNewInput.is(':checked')) {
        applyPortfolio.prop('checked', true).is(':checked', true);
    }

    uploadFile = null;

    applyNewToggle.remove();

    applyNewUpload.val(null);
}