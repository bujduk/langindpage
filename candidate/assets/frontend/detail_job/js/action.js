let _handleSaveJobClicked = ($button) => {
    let id = $button.attr('data-id');
    let saveButtonDetail = $(`.job-detail-is_saved-button[data-id="${id}"]`);

    startLoadingNotiflix();

    setTimeout(() => {

        if (saveButtonDetail.hasClass('saved')) {
            saveButtonDetail
                .attr('data-content', 'Lưu công việc')
                .removeClass('saved');

            Notiflix.Notify.Success('Hủy thành công');
        } else {
            saveButtonDetail
                .attr('data-content', 'Đã lưu công việc')
                .addClass('saved');

            Notiflix.Notify.Success('Lưu công việc thành công');
        }

        stopLoadingNotiflix();
    }, 1000)
}

let uploadSubmit = ($form) => {
    let checkValidate = $form.validationObject({
        rules: {
            'phone': 'required|number|maxlength:11|minlength:10',
        },
        customTranslateName: {
            'phone': 'Số điện thoại',
        }
    });

    if (checkValidate) {

        let formData = $form.serializeObject();
        let $formSuccessfully = $('#apply_modal .modal-form__success.successfully');
        let $detailApplyButton = $('.job_detail_apply_button');

        startLoadingNotiflix();

        setTimeout(() => {

            if (formData.file_type === '2') {
                formData.file_upload = uploadFile;
            }

            $detailApplyButton.attr('data-detail-already_apply', '1')

            $form.hide();
            $formSuccessfully.show();

            stopLoadingNotiflix();
        }, 1000);
    }
}