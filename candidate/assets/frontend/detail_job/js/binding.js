let generateApplyCVNewFile = (name) => {

    $('.upload_radio_list').append(`
        <label class="modal-form__toggle modal_form_toggle_new">
            <span class="modal-form__toggle--item">
                <input id="apply_cv_new" name="file_type" hidden
                       value="2"
                       type="radio" class="modal-form__toggle--input">
                <span class="modal-form__toggle--radio"></span>
            </span>
            <span class="d-flex flex-wrap align-items-center">
                <span class="modal-form__toggle--title">
                    ${name}
                    <img src="assets/images/icon-sj-file-pdf.svg" alt="">
                </span>
                <span onclick="_handleDeleteNewCV()"
                      class="modal-form__toggle--info modal-form__toggle--delete">
                    Delete
                </span>
            </span>
        </label>
    `);
}