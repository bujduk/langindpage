const submitForm = ($form) => {

    let checkValidate = $form.validationObject(validateForm[$form.attr('id')]);

    if (checkValidate) {
        let url = $form.attr('data-url');

        Notiflix.Block.Pulse('.auth-form');
        setTimeout(() => {

            Notiflix.Block.Remove('.auth-form');
            location.href = $form.attr('data-redirect');
        }, 2000)
    }
}