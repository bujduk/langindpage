const navbarScroll = () => {
    let top = $(this).scrollTop();

    $('.auth-navbar')
        .addClass((top > 250) ? 'scroll' : '')
        .removeClass((top > 250) ? '' : 'scroll');
}

const showPassword = ($button) => {
    let $icon = $button.find('i');
    let $input = $button.next();
    let isShow = $button.attr('data-show') === '1';

    $button.attr({
        'data-show': isShow ? '0' : '1',
        'title': isShow ? 'Hiển thị mật khẩu' : 'Ẩn mật khẩu'
    });

    $icon.removeClass('fa-eye-slash fa-eye')
        .addClass(isShow ? 'fa-eye-slash': 'fa-eye');

    $input.attr('type', isShow ? 'password' : 'text');
}