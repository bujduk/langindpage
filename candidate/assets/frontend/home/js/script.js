const paggingTurnLeft = () => {

    $('.hps2_paging_animation')
        .removeClass('turntoright turntoleft')
        .addClass('turntoleft');

    setTimeout(() => {
        $('.hps2_paging_animation').removeClass('turntoleft');
    }, 200);
}

const paggingTurnRight = () => {

    $('.hps2_paging_animation')
        .removeClass('turntoright turntoleft')
        .addClass('turntoright');

    setTimeout(() => {
        $('.hps2_paging_animation').removeClass('turntoright');
    }, 200);
}

const navbarScroll = () => {
    let top = $(this).scrollTop();

    $('.hp-navbar')
        .addClass((top > 250) ? 'scroll' : '')
        .removeClass((top > 250) ? '' : 'scroll');
}

const featureEmployerRun = () => {
    let length = $('.hps4_item').length;

    if (length > 1) {
        let activeIndex = 0;

        setInterval(() => {
            $(`.hps4_item[data-index="${activeIndex}"]`).removeClass('active');
            $(`.hps4_item[data-index="${(activeIndex >= length - 1) ? activeIndex = 0 : ++activeIndex}"]`).addClass('active');
        }, 4000);
    }
}