(function ($) {
    $('.collapse').collapse({
        toggle: false
    })

    $('.hps1_select2').select2({
        minimumResultsForSearch: -1,
        selectionCssClass: 'hps1-select2-selection',
        dropdownCssClass: 'hps1-select2-dropdown',
        allowClear: true,
        width: '100%',
        placeholder: $(this).attr('data-placeholder'),
    });

    $('.hps2_slick').slick({
        rows: 4,
        dots: false,
        slidesPerRow: 2,
        lazyLoad: 'ondemand',
        autoplay: true,
        autoplaySpeed: 5000,
        adaptiveHeight: true,
        prevArrow: $(".hps2_previous"),
        nextArrow: $(".hps2_next"),
        responsive: [{
            breakpoint: 992,
            settings: {
                rows: 8,
                slidesPerRow: 1
            }
        }],
    }).on('beforeChange', function (event, slick, currentSlide, nextSlide) {
        if (currentSlide > nextSlide) {
            if (nextSlide === 0 && currentSlide !== 1) {
                paggingTurnLeft()
            } else {
                paggingTurnRight()
            }
        } else if (currentSlide < nextSlide) {
            if (currentSlide === 0 && nextSlide !== 1) {
                paggingTurnRight()
            } else {
                paggingTurnLeft()
            }
        }
    });

    $('.hps3_slick').slick({
        infinite: true,
        slidesToShow: 3,
        slidesToScroll: 1,
        focusOnSelect: true,
        autoplay: true,
        autoplaySpeed: 4000,
        centerMode: true,
        prevArrow: false,
        nextArrow: false
    }).on('beforeChange', function (event, slick, currentSlide, nextSlide) {
        if (currentSlide !== nextSlide) {

            $('.hps3_image_item').fadeOut(300);
            $(`.hps3_image_item[data-hps3-item="${nextSlide}"]`).fadeIn(300);

            $('.hps3_content_item.collapse').collapse('hide');
            $(`.hps3_content_item[data-hps3-item="${nextSlide}"]`).collapse('show');
        }

    });

    $(document).on('scroll', () => {
        navbarScroll();
    });

    $('.hps1_background').append(
        (window.innerWidth >= 992) ? `
                <img src="assets/images/image-hp-video-first.png" alt="">
                <video autoplay="" loop="" muted="">
                    <source src="assets/video/itnavi.mp4" type="video/mp4">
                </video>
            ` : `
                <img src="assets/images/image-hp-mobile-background.png" alt="">
                `);

    featureEmployerRun();

})(jQuery);