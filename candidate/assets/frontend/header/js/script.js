$(function () {
    $('.n-search__location--select').select2({
        dropdownCssClass: "navi-location-select2",
        minimumResultsForSearch: -1,
    });

    $('.itnavi-header .header-desktop .n-search__name--input').click(() => {
        $('.itnavi-header .navbar-nav').addClass('hide');
        $('.itnavi-header .n-search').removeClass('hide');
    });

    $('.itnavi-header .header-desktop .nav-item__clicktoshow--button').click(() => {
        $('.itnavi-header .navbar-nav').removeClass('hide');
        $('.itnavi-header .n-search').addClass('hide');
    });

    $('.candidate-tab, .candidate-path, .app-footer').click(() => {
        let mobileMenu = $('.itnavi-header .n-mobile__menu');
        if (mobileMenu.hasClass('show')) {
            mobileMenu.removeClass('show');
        }
    });

    $('.itnavi-header .header-mobile .n-search__name--input').focus(() => {
        $('.itnavi-header .header-mobile .n-search--mobile').addClass('show');
        $('.itnavi-header .n-mobile__menu').removeClass('show');
        $('body').addClass('banned-scroll');
    });

    $('.itnavi-header .header-mobile .js_n-search__mobile--close').focus(() => {
        $('.itnavi-header .header-mobile .n-search--mobile').removeClass('show');
        $('body').removeClass('banned-scroll');
    });
});
