getFormRequest = (url, formData = null) => {
    return new Promise(function (resolve, reject) {
        $.ajax({
            url: url,
            type: 'GET',
            dataType: 'json',
            data: formData,
            global: false
        }).done(function (response) {
            if (response.statusCode >= 200 || response.statusCode < 300) {
                resolve(response);
            } else {
                reject(response);
            }
        }).fail(function (error) {
            reject(error);
        });
    });
};