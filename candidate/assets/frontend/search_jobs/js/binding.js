let generateListJobs = (prefixClass) => {

    $(`.${prefixClass}_list`).empty('');

    $.each(listJobs[prefixClass], (name, value) => {
        generateJobItem(value, prefixClass);

        if (!!value.is_first_item) {
            detailJob = value;

            generateJobDetail();
        }
    })
}

let generateJobItem = (data, prefixClass) => {
    let isActive = '';
    let tag = '';

    if (window.innerWidth >= 992 && !!data.is_first_item) isActive = 'active';

    if (!!data.is_amazing) {
        tag = `
            <div class="sj-item__tag-amazing">
                <i class="fas fa-crown"></i>
                Amazing
            </div>
            `
    } else if (!!data.is_premium) {
        tag = `
            <div class="sj-item__tag-premium">
                PREMIUM
            </div>
            `
    } else if (!!data.service && !!data.service.hot_job) {
        tag = `
            <div class="sj-item__tag-hotjob" style="font-weight: 500">
                HOT
            </div>
            `
    }

    $(`.${prefixClass}_list`).append(`
        <div id="job_${data.id}"
            class="sj-item ${!!data.is_amazing ? 'is-amazing' : !!data.is_premium ? 'is-premium' : ''} ${isActive}">
            <div onclick="_handleJobItemClicked($(this), '${prefixClass}')" data-id="${data.id}"
            class="sj-item__left">
                <div style="background-image: url(${!!data.company && !!data.company.file_image ? data.company.file_image : 'assets/images/image-example-company-logo.png'})"
                     class="sj-item__logo"></div>
                <div class="sj-item__middle">
                    <h5 class="sj-item__name job-${data.id}-name"></h5>
                    <h6 class="sj-item__job job-${data.id}-company-name"></h6>
                    <div class="sj-item__more sj-item__salary">
                        <img src="assets/images/icon-sj-wallet.svg" alt="">
                        <p class="sj-item__more--text job-${data.id}-salary"></p>
                    </div>
                    <div class="sj-item__more sj-item__province">
                        <i class="fas fa-map-marker-alt"></i>
                        <p class="sj-item__more--text job-${data.id}-address"></p>
                    </div>
                </div>
            </div>
            <div class="sj-item__right">
                <button type="button" data-id="${data.id}"
                        data-toggle="popover" data-placement="top"
                        onclick="_handleSaveJobClicked($(this))"
                        data-content="${!!data.is_saved ? 'Đã Lưu Job' : 'Lưu Job'}" data-trigger="hover"
                        class="sj-item__save job-${data.id}-is_saved ${!!data.is_saved ? 'saved' : ''}">
                    <i class="far fa-heart"></i>
                </button>
            </div>
            ${tag}
        </div>
    `);

    $(`.job-${data.id}-name`).text(data.name);
    $(`.job-${data.id}-company-name`).text(data.company.name);
    $(`.job-${data.id}-salary`).text(!!data.salary_is_negotiate ? 'Thỏa thuận' : data.salary);
    $(`.job-${data.id}-address`).text(data.address[0].province);

    $('[data-toggle="popover"]').popover();
}

let generateJobDetail = () => {

    /* Default */
    $.each(detailJob, (name, value) => {
        $(`.job-detail-${name}`).text(!!value ? value : '');
    });

    /* Company */
    $.each(detailJob.company, (name, value) => {
        $(`.job-detail-company-${name}`).text(!!value ? value : '');
        $(`.job-detail-company-${name}-background`)
            .css(
                'background-image',
                `url(${!!value ? value : 'assets/images/image-example-company-logo.png'})`
            );
        $(`.job-detail-company-${name}-image`)
            .attr(
                'src',
                !!value ? value : 'assets/images/image-example-company-logo.png'
            );
    });

    /* ID */
    $('#apply_form #apply_id').val(detailJob.id)

    /* Salary */
    if (!!detailJob.salary_is_negotiate) $(`.job-detail-salary`).text('Thỏa thuận');

    /* Tech Stack */
    let techStackTags = $('.job-detail-tech_stack-tags');

    techStackTags.empty();

    detailJob.tech_stack.forEach(item => {
        techStackTags.append(!!item ? `<span>${item}</span>` : '')
    })

    /* Province */
    $('.job-detail-address-province').text((!!detailJob.address && !!detailJob.address[0] && !!detailJob.address[0].province) ? detailJob.address[0].province : '');

    /* Is saved */
    let saveButton = $('.job-detail-is_saved-button');
    saveButton
        .attr({
            'data-content': !!detailJob.is_saved ? 'Đã Lưu Job' : 'Lưu Job',
            'data-id': detailJob.id
        });
    if (!!detailJob.is_saved) {
        saveButton.addClass('saved');
    } else {
        saveButton.removeClass('saved');
    }

    /* Copy Link */
    $('.job-detail-copy_link-button').attr('onclick', `_handleCopyToClipboard('${detailJob.slug}')`)

    /* Media Pictures */
    let imgPictures = $('.job-detail-company-img_pictures-image');
    $(".sj-description__media").data('lightGallery').destroy(true);
    imgPictures.empty();
    detailJob.company.img_pictures.forEach(item => {
        imgPictures.append(!!item ? `
            <a class="col-6 col-sm-6 col-lg-4 px-1 px-sm-2 mb-3 mb-sm-4" 
               href="${item}">
                <img class="img-fluid lazy" src="${item}" alt="">
            </a>
        ` : '');
    });
    initMediaPictureCarousel();
    $('.lazy').Lazy();

    /* Already Apply */
    $('.job_detail_apply_button').attr('data-detail-already_apply', detailJob.already_apply);
}

let generatePagination = (prefixClass = 'all_jobs') => {
    let current = $(`.${prefixClass}_pagination_current`);
    let total = $(`.${prefixClass}_pagination_total`);
    let previous = $(`.${prefixClass}_pagination_previous`);
    let next = $(`.${prefixClass}_pagination_next`);
    let input = $(`.${prefixClass}_pagination_input`);

    current.text(perPageJobs[prefixClass]);

    total.text(Math.ceil(totalJobs[prefixClass] / 10));

    if (perPageJobs[prefixClass] <= 1) {
        previous
            .prop('disabled', true)
            .attr('onclick', '');
    } else {
        previous
            .prop('disabled', false)
            .attr('onclick', `_handlePaginationArrowClick('${prefixClass}', ${perPageJobs[prefixClass] - 1})`);
    }

    if (perPageJobs[prefixClass] >= Math.ceil(totalJobs[prefixClass] / 10)) {
        next
            .prop('disabled', true)
            .attr('onclick', '');

    } else {
        next
            .prop('disabled', false)
            .attr('onclick', `_handlePaginationArrowClick('${prefixClass}', ${perPageJobs[prefixClass] + 1})`);
    }

    input.val(null);
}

let generateApplyCVNewFile = (name) => {

    $('.upload_radio_list').append(`
        <label class="modal-form__toggle modal_form_toggle_new">
            <span class="modal-form__toggle--item">
                <input id="apply_cv_new" name="file_type" hidden
                       value="2"
                       type="radio" class="modal-form__toggle--input">
                <span class="modal-form__toggle--radio"></span>
            </span>
            <span class="d-flex flex-wrap align-items-center">
                <span class="modal-form__toggle--title">
                    ${name}
                    <img src="assets/images/icon-sj-file-pdf.svg" alt="">
                </span>
                <span onclick="_handleDeleteNewCV()"
                      class="modal-form__toggle--info modal-form__toggle--delete">
                    Delete
                </span>
            </span>
        </label>
    `);
}