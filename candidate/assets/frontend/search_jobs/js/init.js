let initPage = () => {
    getListJobs();
    getListJobs('saved_jobs');
    initMediaPictureCarousel();
    closeApplyModal();
}

(function ($) {
    imgPictures = $(".job-detail-company-img_pictures-image");

    Notiflix.Loading.Init({
        backgroundColor: "rgba(255,255,255,0.6)",
        svgColor: "#06202D",
    });

    $('.search_collapse').collapse({
        toggle: false
    })

    $('[data-toggle="popover"]').popover();

    $('.sj-tab__link').on('show.bs.tab', (e) => {
        let $navLink = $(e.target);
        let $navItem = $navLink.parent();

        $('.sj-tab__item').removeClass('active');
        $navItem.addClass('active');
    });

    $(window).on('scroll', () => {
        let heightSet = 55 + $('.sj-detail__top').outerHeight(true) + $('.sj-back').outerHeight(true) + $('.sj-detail__nav').outerHeight(true);
        let scrollB = $(window).scrollTop();
        let mobileNavigation = $('.sj-mobile__navigation');

        if (scrollB - heightSet >= 0) {
            mobileNavigation.addClass('show');
        } else {
            mobileNavigation.removeClass('show');
        }
    })

    $('.sj-list__scroll').on('scroll', () => {
        setShadowOnScroll('sj-list');
    });

    $('.sj-detail__scroll').on('scroll', () => {
        setShadowOnScroll('sj-detail');
    });

    $('#apply_upload_new').on('change', (e) => {
        _handleApplyFileChange($(e.target));
    });

    $('#apply_form').on('submit', (e) => {
        e.preventDefault();

        uploadSubmit($(e.target));
    });

    $('#apply_modal').on('hidden.bs.modal', () => {
        closeApplyModal();
    });

    initPage();
})(jQuery)