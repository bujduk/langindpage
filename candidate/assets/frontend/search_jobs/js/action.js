let getListJobs = (prefixClass = 'all_jobs', perPage = 1) => {
    startLoadingNotiflix('.sj-body__left, .sj-body__right');

    let url = (prefixClass === 'all_jobs') ? './assets/frontend/search_jobs/json/list_jobs.json' : './assets/frontend/search_jobs/json/list_saved_jobs.json';

    setTimeout(() => {
        getFormRequest(url)
            .then((responseData) => {
                    listJobs[prefixClass] = {};

                    responseData.data.jobs.forEach((item, key) => {
                        listJobs[prefixClass][item.id] = item;

                        if (key === 0 && prefixClass === 'all_jobs') {
                            listJobs[prefixClass][item.id].is_first_item = 1;
                        }
                    })

                    totalJobs[prefixClass] = responseData.data.total;
                    perPageJobs[prefixClass] = responseData.data.perPage;

                    generateListJobs(prefixClass);
                    generatePagination(prefixClass);

                    $(`.${prefixClass}_total`).text(totalJobs[prefixClass]);

                    stopLoadingNotiflix('.sj-body__left, .sj-body__right');
                }, function (responseErr) {
                    if (responseErr.status === 422) {
                        commonShowNotifyErr();
                    } else {
                        commonShowNotifyErr();
                    }
                }
            );
    }, 1000);
}

let _handleSaveJobClicked = ($button) => {
    let id = $button.attr('data-id');
    let saveButton = $(`.job-${id}-is_saved`);
    let saveButtonDetail = $(`.job-detail-is_saved-button[data-id="${id}"]`);

    startLoadingNotiflix('.sj-body__left, .sj-body__right');

    setTimeout(() => {

        if (!!listJobs['all_jobs'] && !!listJobs['all_jobs'][id] ) {
            listJobs['all_jobs'][id].is_saved = saveButton.hasClass('saved') ? 0 : 1;
        }

        if (!!listJobs['saved_jobs'] && !!listJobs['saved_jobs'][id] ) {
            listJobs['saved_jobs'][id].is_saved = saveButton.hasClass('saved') ? 0 : 1;
        }

        if (saveButton.hasClass('saved')) {
            saveButton
                .attr('data-content', 'Lưu công việc')
                .removeClass('saved');
            saveButtonDetail
                .attr('data-content', 'Lưu công việc')
                .removeClass('saved');

            Notiflix.Notify.Success('Hủy thành công');
        } else {

            saveButton
                .attr('data-content', 'Đã lưu công việc')
                .addClass('saved');
            saveButtonDetail
                .attr('data-content', 'Đã lưu công việc')
                .addClass('saved');

            Notiflix.Notify.Success('Lưu công việc thành công');
        }

        stopLoadingNotiflix('.sj-body__left, .sj-body__right');
    }, 1000)
}

let uploadSubmit = ($form) => {
    let checkValidate = $form.validationObject({
        rules: {
            'phone': 'required|number|maxlength:11|minlength:10',
        },
        customTranslateName: {
            'phone': 'Số điện thoại',
        }
    });

    if (checkValidate) {

        let formData = $form.serializeObject();
        let $formSuccessfully = $('#apply_modal .modal-form__success.successfully');
        let $detailApplyButton = $('.job_detail_apply_button');

        console.log(formData);

        startLoadingNotiflix();

        setTimeout(() => {

            if (formData.file_type === '2') {
                formData.file_upload = uploadFile;
            }

            if (!!listJobs['all_jobs'] && !!listJobs['all_jobs'][formData.id] ) {
                listJobs['all_jobs'][formData.id].already_apply = 1;
            }

            if (!!listJobs['saved_jobs'] && !!listJobs['saved_jobs'][formData.id] ) {
                listJobs['saved_jobs'][formData.id].already_apply = 1;
            }

            $detailApplyButton.attr('data-detail-already_apply', '1')

            $form.hide();
            $formSuccessfully.show();

            stopLoadingNotiflix();
        }, 1000);
    }
}