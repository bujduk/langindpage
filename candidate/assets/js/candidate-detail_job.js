let totalPerPage = 10;

let listJobs = {
    all_jobs: {},
    saved_jobs: {}
};
let detailJob = null;
let totalJobs = {};
let perPageJobs = {};

let paginationListTimeout;

let uploadFile;
let startLoadingNotiflix = (prefixClass = '') => {
    if (!!prefixClass && window.innerWidth >= 992) {
        Notiflix.Block.Pulse(prefixClass);
    } else {
        Notiflix.Loading.Pulse();
    }
}

let stopLoadingNotiflix = (prefixClass = '') => {
    if (!!prefixClass && window.innerWidth >= 992) {
        Notiflix.Block.Remove(prefixClass);
    } else {
        Notiflix.Loading.Remove();
    }
}

let initMediaPictureCarousel = () => {
    $(".sj-description__media").lightGallery();
}

let _handleCopyToClipboard = (textToCopy) => {
    var inputText;

    function isOS() {
        //can use a better detection logic here
        return navigator.userAgent.match(/ipad|iphone/i);
    }

    function createInput(text) {
        inputText = document.createElement('INPUT');
        inputText.setAttribute("type", "text");
        inputText.value = text;
        document.body.appendChild(inputText);
    }

    function selectText() {
        var range, selection;

        if (isOS()) {
            range = document.createRange();
            range.selectNodeContents(inputText);
            selection = window.getSelection();
            selection.removeAllRanges();
            selection.addRange(range);
        }

        inputText.select();
        inputText.setSelectionRange(0, 999999);
    }

    function copyTo() {
        document.execCommand('copy');
        document.body.removeChild(inputText);
        Notiflix.Notify.Success("Copy thành công");
    }

    createInput(textToCopy);
    selectText();
    copyTo(textToCopy);
}

let _handlePaginationArrowClick = (prefixClass, nextIndex) => {
    getListJobs(prefixClass, nextIndex);
}

let _handlePaginationInputChange = ($input, prefixClass) => {

    let totalPages = Math.floor(totalJobs[prefixClass] / 10);
    let nextIndex = _.toNumber($input.val());

    console.log(nextIndex)

    clearTimeout(paginationListTimeout);

    paginationListTimeout = setTimeout(() => {
        if (!!nextIndex && nextIndex <= totalPages) {
            getListJobs(prefixClass, nextIndex);

            $input.blur();
        }
    }, ($input[0].which === 13) ? 0 : 1000);
}

let _handleOpenApplyModal = ($button) => {
    let $form = $('#apply_modal .modal-form');
    let $formAlreadyApply = $('#apply_modal .modal-form__success.already_apply');
    let status = $button.attr('data-detail-already_apply');

    if (status === '1') {
        $formAlreadyApply.show();
    } else {
        $form.show();
    }

    $('#apply_modal').modal('show');
}

let closeApplyModal = () => {
    let $form = $('#apply_modal .modal-form');
    let $formSuccess = $('#apply_modal .modal-form__success.successfully');
    let $formAlreadyApply = $('#apply_modal .modal-form__success.already_apply');

    $form.hide();
    $formSuccess.hide();
    $formAlreadyApply.hide();
}

let _handleApplyFileChange = ($input) => {
    let files = $input[0].files;
    let applyUploadError = $('#apply_upload_error');

    applyUploadError.html('');

    let file = files[0];

    if (file.size > 2097152) {
        let html = `<span style="display:block;">File không được quá 2MB</span>`;

        applyUploadError.append(html);

    } else if (!file.type.includes("application/vnd") && file.type !== "application/pdf") {
        let html = `<span style="display:block;">File ${file.name} ${gallery_incorrect_format}</span>`;

        applyUploadError.append(html);

    } else {
        let fileReader = new FileReader();

        uploadFile = file;

        fileReader.onload = ((fileParam) => {
            return (e) => {
                $('.modal_form_toggle_new').remove();

                generateApplyCVNewFile(file.name);

                $('#apply_cv_new').prop('checked', true).is(':checked', true);
            }
        })(file);
        fileReader.readAsDataURL(file);
    }
}

let _handleDeleteNewCV = () => {
    let applyPortfolio = $('#apply_portfolio');
    let applyNewInput = $('#apply_cv_new');
    let applyNewToggle = $('.modal_form_toggle_new');
    let applyNewUpload = $('#apply_upload_new');

    if (applyNewInput.is(':checked')) {
        applyPortfolio.prop('checked', true).is(':checked', true);
    }

    uploadFile = null;

    applyNewToggle.remove();

    applyNewUpload.val(null);
}
let generateApplyCVNewFile = (name) => {

    $('.upload_radio_list').append(`
        <label class="modal-form__toggle modal_form_toggle_new">
            <span class="modal-form__toggle--item">
                <input id="apply_cv_new" name="file_type" hidden
                       value="2"
                       type="radio" class="modal-form__toggle--input">
                <span class="modal-form__toggle--radio"></span>
            </span>
            <span class="d-flex flex-wrap align-items-center">
                <span class="modal-form__toggle--title">
                    ${name}
                    <img src="assets/images/icon-sj-file-pdf.svg" alt="">
                </span>
                <span onclick="_handleDeleteNewCV()"
                      class="modal-form__toggle--info modal-form__toggle--delete">
                    Delete
                </span>
            </span>
        </label>
    `);
}
let _handleSaveJobClicked = ($button) => {
    let id = $button.attr('data-id');
    let saveButtonDetail = $(`.job-detail-is_saved-button[data-id="${id}"]`);

    startLoadingNotiflix();

    setTimeout(() => {

        if (saveButtonDetail.hasClass('saved')) {
            saveButtonDetail
                .attr('data-content', 'Lưu công việc')
                .removeClass('saved');

            Notiflix.Notify.Success('Hủy thành công');
        } else {
            saveButtonDetail
                .attr('data-content', 'Đã lưu công việc')
                .addClass('saved');

            Notiflix.Notify.Success('Lưu công việc thành công');
        }

        stopLoadingNotiflix();
    }, 1000)
}

let uploadSubmit = ($form) => {
    let checkValidate = $form.validationObject({
        rules: {
            'phone': 'required|number|maxlength:11|minlength:10',
        },
        customTranslateName: {
            'phone': 'Số điện thoại',
        }
    });

    if (checkValidate) {

        let formData = $form.serializeObject();
        let $formSuccessfully = $('#apply_modal .modal-form__success.successfully');
        let $detailApplyButton = $('.job_detail_apply_button');

        startLoadingNotiflix();

        setTimeout(() => {

            if (formData.file_type === '2') {
                formData.file_upload = uploadFile;
            }

            $detailApplyButton.attr('data-detail-already_apply', '1')

            $form.hide();
            $formSuccessfully.show();

            stopLoadingNotiflix();
        }, 1000);
    }
}
let initPage = () => {
    initMediaPictureCarousel();
    closeApplyModal();
}

(function ($) {
    Notiflix.Loading.Init({
        backgroundColor: "rgba(255,255,255,0.6)",
        svgColor: "#06202D",
    });

    $('[data-toggle="popover"]').popover();

    $('#apply_upload_new').on('change', (e) => {
        _handleApplyFileChange($(e.target));
    });

    $('#apply_form').on('submit', (e) => {
        e.preventDefault();

        uploadSubmit($(e.target));
    });

    $('#apply_modal').on('hidden.bs.modal', () => {
        closeApplyModal();
    });

    initPage();
})(jQuery)