let totalPerPage = 10;

let listJobs = {
    all_jobs: {},
    saved_jobs: {}
};
let detailJob = null;
let totalJobs = {};
let perPageJobs = {};

let paginationListTimeout;

let uploadFile;
startLoadingNotiflix = (prefixClass = '') => {
    if (!!prefixClass && window.innerWidth >= 992) {
        Notiflix.Block.Pulse(prefixClass);
    } else {
        Notiflix.Loading.Pulse();
    }
}

stopLoadingNotiflix = (prefixClass = '') => {
    if (!!prefixClass && window.innerWidth >= 992) {
        Notiflix.Block.Remove(prefixClass);
    } else {
        Notiflix.Loading.Remove();
    }
}

let initMediaPictureCarousel = () => {
    $(".sj-description__media").lightGallery();
}

let setShadowOnScroll = (prefixClass) => {
    let parent = $(`.${prefixClass}__scroll`);
    let scroll = $(`.${prefixClass}__inner`);

    let parentPosition = parent.position().top;
    let scrollPosition = scroll.position().top;

    let parentHeight = parent.height();
    let scrollHeight = scroll.height();

    let differentTop = parentPosition - scrollPosition;
    let differentBottom = scrollHeight + scrollPosition - parentHeight;

    let shadowTop = $(`.${prefixClass}__shadow--top`);
    let shadowBottom = $(`.${prefixClass}__shadow--bottom`);

    if (differentTop <= 300) {
        shadowTop.css('box-shadow', `0 0 16px ${Math.floor(differentTop / 30)}px #eee`)
    } else {
        shadowTop.css('box-shadow', '0 0 16px 10px #eee')
    }

    if (differentBottom <= 300) {
        shadowBottom.css('box-shadow', `0 0 16px ${Math.floor(differentBottom / 30)}px #eee`)
    } else {
        shadowBottom.css('box-shadow', '0 0 16px 10px #eee')
    }
}

let _handleBackToList = () => {
    let moduleMobileList = $('.module_mobile_list');
    let moduleMobileDetail = $('.module_mobile_detail');
    let jobContentTab = $(`.sj-detail__nav--link[href="#job_content"]`)

    moduleMobileList.find('.search_collapse').collapse('show');
    moduleMobileDetail.find('.search_collapse').collapse('hide');

    jobContentTab.trigger('click');

    window.scrollTo(0, 0);
}

let _handleJobItemClicked = ($item, prefixClass) => {
    let id = $item.attr('data-id');
    let moduleMobileList = $('.module_mobile_list');
    let moduleMobileDetail = $('.module_mobile_detail');
    let jobContentTab = $(`.sj-detail__nav--link[href="#job_content"]`)

    moduleMobileList.find('.search_collapse').collapse('hide');
    moduleMobileDetail.find('.search_collapse').collapse('show');

    $('.sj-item').removeClass('active');
    $(`#job_${id}`).addClass('active');

    jobContentTab.trigger('click');

    window.scrollTo(0, 0);
    detailJob = listJobs[prefixClass][id];
    generateJobDetail();
}

let _handleCopyToClipboard = (textToCopy) => {
    var inputText;

    function isOS() {
        //can use a better detection logic here
        return navigator.userAgent.match(/ipad|iphone/i);
    }

    function createInput(text) {
        inputText = document.createElement('INPUT');
        inputText.setAttribute("type", "text");
        inputText.value = text;
        document.body.appendChild(inputText);
    }

    function selectText() {
        var range, selection;

        if (isOS()) {
            range = document.createRange();
            range.selectNodeContents(inputText);
            selection = window.getSelection();
            selection.removeAllRanges();
            selection.addRange(range);
        }

        inputText.select();
        inputText.setSelectionRange(0, 999999);
    }

    function copyTo() {
        document.execCommand('copy');
        document.body.removeChild(inputText);
        Notiflix.Notify.Success("Copy thành công");
    }

    createInput(textToCopy);
    selectText();
    copyTo(textToCopy);
}

let _handlePaginationArrowClick = (prefixClass, nextIndex) => {
    getListJobs(prefixClass, nextIndex);
}

let _handlePaginationInputChange = ($input, prefixClass) => {

    let totalPages = Math.floor(totalJobs[prefixClass] / 10);
    let nextIndex = _.toNumber($input.val());

    console.log(nextIndex)

    clearTimeout(paginationListTimeout);

    paginationListTimeout = setTimeout(() => {
        if (!!nextIndex && nextIndex <= totalPages) {
            getListJobs(prefixClass, nextIndex);

            $input.blur();
        }
    }, ($input[0].which === 13) ? 0 : 1000);
}

let _handleOpenApplyModal = ($button) => {
    let $form = $('#apply_modal .modal-form');
    let $formAlreadyApply = $('#apply_modal .modal-form__success.already_apply');
    let status = $button.attr('data-detail-already_apply');

    if (status === '1') {
        $formAlreadyApply.show();
    } else {
        $form.show();
    }

    $('#apply_modal').modal('show');
}

let closeApplyModal = () => {
    let $form = $('#apply_modal .modal-form');
    let $formSuccess = $('#apply_modal .modal-form__success.successfully');
    let $formAlreadyApply = $('#apply_modal .modal-form__success.already_apply');

    $form.hide();
    $formSuccess.hide();
    $formAlreadyApply.hide();
}

let _handleApplyFileChange = ($input) => {
    let files = $input[0].files;
    let applyUploadError = $('#apply_upload_error');

    applyUploadError.html('');

    let file = files[0];

    if (file.size > 2097152) {
        let html = `<span style="display:block;">File không được quá 2MB</span>`;

        applyUploadError.append(html);

    } else if (!file.type.includes("application/vnd") && file.type !== "application/pdf") {
        let html = `<span style="display:block;">File ${file.name} ${gallery_incorrect_format}</span>`;

        applyUploadError.append(html);

    } else {
        let fileReader = new FileReader();

        uploadFile = file;

        fileReader.onload = ((fileParam) => {
            return (e) => {
                $('.modal_form_toggle_new').remove();

                generateApplyCVNewFile(file.name);

                $('#apply_cv_new').prop('checked', true).is(':checked', true);
            }
        })(file);
        fileReader.readAsDataURL(file);
    }
}

let _handleDeleteNewCV = () => {
    let applyPortfolio = $('#apply_portfolio');
    let applyNewInput = $('#apply_cv_new');
    let applyNewToggle = $('.modal_form_toggle_new');
    let applyNewUpload = $('#apply_upload_new');

    if (applyNewInput.is(':checked')) {
        applyPortfolio.prop('checked', true).is(':checked', true);
    }

    uploadFile = null;

    applyNewToggle.remove();

    applyNewUpload.val(null);
}
let generateListJobs = (prefixClass) => {

    $(`.${prefixClass}_list`).empty('');

    $.each(listJobs[prefixClass], (name, value) => {
        generateJobItem(value, prefixClass);

        if (!!value.is_first_item) {
            detailJob = value;

            generateJobDetail();
        }
    })
}

let generateJobItem = (data, prefixClass) => {
    let isActive = '';
    let tag = '';

    if (window.innerWidth >= 992 && !!data.is_first_item) isActive = 'active';

    if (!!data.is_amazing) {
        tag = `
            <div class="sj-item__tag-amazing">
                <i class="fas fa-crown"></i>
                Amazing
            </div>
            `
    } else if (!!data.is_premium) {
        tag = `
            <div class="sj-item__tag-premium">
                PREMIUM
            </div>
            `
    } else if (!!data.service && !!data.service.hot_job) {
        tag = `
            <div class="sj-item__tag-hotjob" style="font-weight: 500">
                HOT
            </div>
            `
    }

    $(`.${prefixClass}_list`).append(`
        <div id="job_${data.id}"
            class="sj-item ${!!data.is_amazing ? 'is-amazing' : !!data.is_premium ? 'is-premium' : ''} ${isActive}">
            <div onclick="_handleJobItemClicked($(this), '${prefixClass}')" data-id="${data.id}"
            class="sj-item__left">
                <div style="background-image: url(${!!data.company && !!data.company.file_image ? data.company.file_image : 'assets/images/image-example-company-logo.png'})"
                     class="sj-item__logo"></div>
                <div class="sj-item__middle">
                    <h5 class="sj-item__name job-${data.id}-name"></h5>
                    <h6 class="sj-item__job job-${data.id}-company-name"></h6>
                    <div class="sj-item__more sj-item__salary">
                        <img src="assets/images/icon-sj-wallet.svg" alt="">
                        <p class="sj-item__more--text job-${data.id}-salary"></p>
                    </div>
                    <div class="sj-item__more sj-item__province">
                        <i class="fas fa-map-marker-alt"></i>
                        <p class="sj-item__more--text job-${data.id}-address"></p>
                    </div>
                </div>
            </div>
            <div class="sj-item__right">
                <button type="button" data-id="${data.id}"
                        data-toggle="popover" data-placement="top"
                        onclick="_handleSaveJobClicked($(this))"
                        data-content="${!!data.is_saved ? 'Đã Lưu Job' : 'Lưu Job'}" data-trigger="hover"
                        class="sj-item__save job-${data.id}-is_saved ${!!data.is_saved ? 'saved' : ''}">
                    <i class="far fa-heart"></i>
                </button>
            </div>
            ${tag}
        </div>
    `);

    $(`.job-${data.id}-name`).text(data.name);
    $(`.job-${data.id}-company-name`).text(data.company.name);
    $(`.job-${data.id}-salary`).text(!!data.salary_is_negotiate ? 'Thỏa thuận' : data.salary);
    $(`.job-${data.id}-address`).text(data.address[0].province);

    $('[data-toggle="popover"]').popover();
}

let generateJobDetail = () => {

    /* Default */
    $.each(detailJob, (name, value) => {
        $(`.job-detail-${name}`).text(!!value ? value : '');
    });

    /* Company */
    $.each(detailJob.company, (name, value) => {
        $(`.job-detail-company-${name}`).text(!!value ? value : '');
        $(`.job-detail-company-${name}-background`)
            .css(
                'background-image',
                `url(${!!value ? value : 'assets/images/image-example-company-logo.png'})`
            );
        $(`.job-detail-company-${name}-image`)
            .attr(
                'src',
                !!value ? value : 'assets/images/image-example-company-logo.png'
            );
    });

    /* ID */
    $('#apply_form #apply_id').val(detailJob.id)

    /* Salary */
    if (!!detailJob.salary_is_negotiate) $(`.job-detail-salary`).text('Thỏa thuận');

    /* Tech Stack */
    let techStackTags = $('.job-detail-tech_stack-tags');

    techStackTags.empty();

    detailJob.tech_stack.forEach(item => {
        techStackTags.append(!!item ? `<span>${item}</span>` : '')
    })

    /* Province */
    $('.job-detail-address-province').text((!!detailJob.address && !!detailJob.address[0] && !!detailJob.address[0].province) ? detailJob.address[0].province : '');

    /* Is saved */
    let saveButton = $('.job-detail-is_saved-button');
    saveButton
        .attr({
            'data-content': !!detailJob.is_saved ? 'Đã Lưu Job' : 'Lưu Job',
            'data-id': detailJob.id
        });
    if (!!detailJob.is_saved) {
        saveButton.addClass('saved');
    } else {
        saveButton.removeClass('saved');
    }

    /* Copy Link */
    $('.job-detail-copy_link-button').attr('onclick', `_handleCopyToClipboard('${detailJob.slug}')`)

    /* Media Pictures */
    let imgPictures = $('.job-detail-company-img_pictures-image');
    $(".sj-description__media").data('lightGallery').destroy(true);
    imgPictures.empty();
    detailJob.company.img_pictures.forEach(item => {
        imgPictures.append(!!item ? `
            <a class="col-6 col-sm-6 col-lg-4 px-1 px-sm-2 mb-3 mb-sm-4" 
               href="${item}">
                <img class="img-fluid lazy" src="${item}" alt="">
            </a>
        ` : '');
    });
    initMediaPictureCarousel();
    $('.lazy').Lazy();

    /* Already Apply */
    $('.job_detail_apply_button').attr('data-detail-already_apply', detailJob.already_apply);
}

let generatePagination = (prefixClass = 'all_jobs') => {
    let current = $(`.${prefixClass}_pagination_current`);
    let total = $(`.${prefixClass}_pagination_total`);
    let previous = $(`.${prefixClass}_pagination_previous`);
    let next = $(`.${prefixClass}_pagination_next`);
    let input = $(`.${prefixClass}_pagination_input`);

    current.text(perPageJobs[prefixClass]);

    total.text(Math.ceil(totalJobs[prefixClass] / 10));

    if (perPageJobs[prefixClass] <= 1) {
        previous
            .prop('disabled', true)
            .attr('onclick', '');
    } else {
        previous
            .prop('disabled', false)
            .attr('onclick', `_handlePaginationArrowClick('${prefixClass}', ${perPageJobs[prefixClass] - 1})`);
    }

    if (perPageJobs[prefixClass] >= Math.ceil(totalJobs[prefixClass] / 10)) {
        next
            .prop('disabled', true)
            .attr('onclick', '');

    } else {
        next
            .prop('disabled', false)
            .attr('onclick', `_handlePaginationArrowClick('${prefixClass}', ${perPageJobs[prefixClass] + 1})`);
    }

    input.val(null);
}

let generateApplyCVNewFile = (name) => {

    $('.upload_radio_list').append(`
        <label class="modal-form__toggle modal_form_toggle_new">
            <span class="modal-form__toggle--item">
                <input id="apply_cv_new" name="file_type" hidden
                       value="2"
                       type="radio" class="modal-form__toggle--input">
                <span class="modal-form__toggle--radio"></span>
            </span>
            <span class="d-flex flex-wrap align-items-center">
                <span class="modal-form__toggle--title">
                    ${name}
                    <img src="assets/images/icon-sj-file-pdf.svg" alt="">
                </span>
                <span onclick="_handleDeleteNewCV()"
                      class="modal-form__toggle--info modal-form__toggle--delete">
                    Delete
                </span>
            </span>
        </label>
    `);
}
let getListJobs = (prefixClass = 'all_jobs', perPage = 1) => {
    startLoadingNotiflix('.sj-body__left, .sj-body__right');

    let url = (prefixClass === 'all_jobs') ? './assets/frontend/search_jobs/json/list_jobs.json' : './assets/frontend/search_jobs/json/list_saved_jobs.json';

    setTimeout(() => {
        getFormRequest(url)
            .then((responseData) => {
                    listJobs[prefixClass] = {};

                    responseData.data.jobs.forEach((item, key) => {
                        listJobs[prefixClass][item.id] = item;

                        if (key === 0 && prefixClass === 'all_jobs') {
                            listJobs[prefixClass][item.id].is_first_item = 1;
                        }
                    })

                    totalJobs[prefixClass] = responseData.data.total;
                    perPageJobs[prefixClass] = responseData.data.perPage;

                    generateListJobs(prefixClass);
                    generatePagination(prefixClass);

                    $(`.${prefixClass}_total`).text(totalJobs[prefixClass]);

                    stopLoadingNotiflix('.sj-body__left, .sj-body__right');
                }, function (responseErr) {
                    if (responseErr.status === 422) {
                        commonShowNotifyErr();
                    } else {
                        commonShowNotifyErr();
                    }
                }
            );
    }, 1000);
}

let _handleSaveJobClicked = ($button) => {
    let id = $button.attr('data-id');
    let saveButton = $(`.job-${id}-is_saved`);
    let saveButtonDetail = $(`.job-detail-is_saved-button[data-id="${id}"]`);

    startLoadingNotiflix('.sj-body__left, .sj-body__right');

    setTimeout(() => {

        if (!!listJobs['all_jobs'] && !!listJobs['all_jobs'][id] ) {
            listJobs['all_jobs'][id].is_saved = saveButton.hasClass('saved') ? 0 : 1;
        }

        if (!!listJobs['saved_jobs'] && !!listJobs['saved_jobs'][id] ) {
            listJobs['saved_jobs'][id].is_saved = saveButton.hasClass('saved') ? 0 : 1;
        }

        if (saveButton.hasClass('saved')) {
            saveButton
                .attr('data-content', 'Lưu công việc')
                .removeClass('saved');
            saveButtonDetail
                .attr('data-content', 'Lưu công việc')
                .removeClass('saved');

            Notiflix.Notify.Success('Hủy thành công');
        } else {

            saveButton
                .attr('data-content', 'Đã lưu công việc')
                .addClass('saved');
            saveButtonDetail
                .attr('data-content', 'Đã lưu công việc')
                .addClass('saved');

            Notiflix.Notify.Success('Lưu công việc thành công');
        }

        stopLoadingNotiflix('.sj-body__left, .sj-body__right');
    }, 1000)
}

let uploadSubmit = ($form) => {
    let checkValidate = $form.validationObject({
        rules: {
            'phone': 'required|number|maxlength:11|minlength:10',
        },
        customTranslateName: {
            'phone': 'Số điện thoại',
        }
    });

    if (checkValidate) {

        let formData = $form.serializeObject();
        let $formSuccessfully = $('#apply_modal .modal-form__success.successfully');
        let $detailApplyButton = $('.job_detail_apply_button');

        console.log(formData);

        startLoadingNotiflix();

        setTimeout(() => {

            if (formData.file_type === '2') {
                formData.file_upload = uploadFile;
            }

            if (!!listJobs['all_jobs'] && !!listJobs['all_jobs'][formData.id] ) {
                listJobs['all_jobs'][formData.id].already_apply = 1;
            }

            if (!!listJobs['saved_jobs'] && !!listJobs['saved_jobs'][formData.id] ) {
                listJobs['saved_jobs'][formData.id].already_apply = 1;
            }

            $detailApplyButton.attr('data-detail-already_apply', '1')

            $form.hide();
            $formSuccessfully.show();

            stopLoadingNotiflix();
        }, 1000);
    }
}
let initPage = () => {
    getListJobs();
    getListJobs('saved_jobs');
    initMediaPictureCarousel();
    closeApplyModal();
}

(function ($) {
    imgPictures = $(".job-detail-company-img_pictures-image");

    Notiflix.Loading.Init({
        backgroundColor: "rgba(255,255,255,0.6)",
        svgColor: "#06202D",
    });

    $('.search_collapse').collapse({
        toggle: false
    })

    $('[data-toggle="popover"]').popover();

    $('.sj-tab__link').on('show.bs.tab', (e) => {
        let $navLink = $(e.target);
        let $navItem = $navLink.parent();

        $('.sj-tab__item').removeClass('active');
        $navItem.addClass('active');
    });

    $(window).on('scroll', () => {
        let heightSet = 55 + $('.sj-detail__top').outerHeight(true) + $('.sj-back').outerHeight(true) + $('.sj-detail__nav').outerHeight(true);
        let scrollB = $(window).scrollTop();
        let mobileNavigation = $('.sj-mobile__navigation');

        if (scrollB - heightSet >= 0) {
            mobileNavigation.addClass('show');
        } else {
            mobileNavigation.removeClass('show');
        }
    })

    $('.sj-list__scroll').on('scroll', () => {
        setShadowOnScroll('sj-list');
    });

    $('.sj-detail__scroll').on('scroll', () => {
        setShadowOnScroll('sj-detail');
    });

    $('#apply_upload_new').on('change', (e) => {
        _handleApplyFileChange($(e.target));
    });

    $('#apply_form').on('submit', (e) => {
        e.preventDefault();

        uploadSubmit($(e.target));
    });

    $('#apply_modal').on('hidden.bs.modal', () => {
        closeApplyModal();
    });

    initPage();
})(jQuery)