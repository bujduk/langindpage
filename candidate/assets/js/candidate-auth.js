const validateForm = {
    'employer_login': {
        rules: {
            'auth_email': 'required|email|maxlength:255',
            'auth_password': 'required',
        },
        customTranslateName: {
            'auth_email': 'Email',
            'auth_password': 'Mật khẩu'
        }
    },
    'employer_register': {
        rules: {
            'auth_email': 'required|email|maxlength:255',
            'auth_password': 'required',
            'auth_confirm_password': 'required|equal:auth_password',
            'auth_company': 'required|maxlength:255',
            'auth_phone': 'required|number|maxlength:20',
        },
        customTranslateName: {
            'auth_email': 'Email',
            'auth_password': 'Mật khẩu',
            'auth_confirm_password': 'Xác nhận mật khẩu',
            'auth_company': 'Tên công ty',
            'auth_phone': 'Số điện thoại',
        }
    },
    'auth_send_email': {
        rules: {
            'auth_email': 'required|email|maxlength:255'
        },
        customTranslateName: {
            'auth_email': 'Email'
        }
    },
    'auth_reset_password': {
        rules: {
            'auth_password': 'required',
            'auth_confirm_password': 'required|equal:auth_password',
        },
        customTranslateName: {
            'auth_password': 'Mật khẩu',
            'auth_confirm_password': 'Xác nhận mật khẩu',
        }
    }
}
const navbarScroll = () => {
    let top = $(this).scrollTop();

    $('.auth-navbar')
        .addClass((top > 250) ? 'scroll' : '')
        .removeClass((top > 250) ? '' : 'scroll');
}

const showPassword = ($button) => {
    let $icon = $button.find('i');
    let $input = $button.next();
    let isShow = $button.attr('data-show') === '1';

    $button.attr({
        'data-show': isShow ? '0' : '1',
        'title': isShow ? 'Hiển thị mật khẩu' : 'Ẩn mật khẩu'
    });

    $icon.removeClass('fa-eye-slash fa-eye')
        .addClass(isShow ? 'fa-eye-slash': 'fa-eye');

    $input.attr('type', isShow ? 'password' : 'text');
}

const submitForm = ($form) => {

    let checkValidate = $form.validationObject(validateForm[$form.attr('id')]);

    if (checkValidate) {
        let url = $form.attr('data-url');

        Notiflix.Block.Pulse('.auth-form');
        setTimeout(() => {

            Notiflix.Block.Remove('.auth-form');
            location.href = $form.attr('data-redirect');
        }, 2000)
    }
}
(function ($) {
    new WOW().init();

    $(document).on('scroll', () => {
        navbarScroll();
    });

    $('.form_show_password').on('click', (e) => {
        e.stopPropagation();
        showPassword($(e.target));
    })

    $('.auth-form').on('submit', (e) => {
        e.preventDefault();
        submitForm($(e.target));
    })
})(jQuery);