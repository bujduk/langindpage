$(document).ready(() => {
    /* Research */
    // reSearchJob = (route) => {
    //     window.event.preventDefault();
    //     let keyword = $('#navbar_search_input').val();
    //     let location = $('#navbar_location').val();
    //     let jobLevel = $('#job_level').val();
    //
    //     let url = route;
    //
    //     if (keyword) {
    //         keyword = keyword.toLowerCase().trim();
    //         keyword = keyword.replace('c#', 'csharp');
    //         url += '/' + keyword;
    //     }
    //
    //     url += jobLevel ? ('/' + jobLevel) : '';
    //     url += location ? ('/' + location) : '';
    //
    //     window.location.href = url;
    // };

    /* Setting for AJAX */
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    Notiflix.Notify.Init({
        rtl: true,
        useFontAwesome: true,
        borderRadius: "3px",
        fontSize: "14px",
        position: "right-bottom",
        fontFamily: "Roboto",
        useGoogleFont: false,
        cssAnimation: true,
        cssAnimationStyle: "from-right",
        success: {
            background: "rgba(24, 65, 83, 0.7)",
            textColor: "#fff",
            fontAwesomeIconColor: "#32c682",
        },
        failure: {
            textColor: "#fff",
            fontAwesomeIconColor: "rgba(255,94,94,0.633)",
        },
    });

    $('.n-search__location--select').select2({
        dropdownCssClass: "navi-location-select2",
        minimumResultsForSearch: -1,
    });

    $('.itnavi-header .header-desktop .n-search__name--input').click(() => {
        let nNaviList = $('.itnavi-header .navbar-nav');
        let nSearch = $('.itnavi-header .n-search');
        nNaviList.addClass('hide');
        nSearch.removeClass('hide');
    });

    $('.itnavi-header .header-desktop .nav-item__clicktoshow--button').click(() => {
        let nNaviList = $('.itnavi-header .navbar-nav');
        let nSearch = $('.itnavi-header .n-search');
        nNaviList.removeClass('hide');
        nSearch.addClass('hide');
    });
    $('.candidate-tab, .candidate-path, .app-footer').click(() => {
        let mobileMenu = $('.itnavi-header .n-mobile__menu');
        if (mobileMenu.hasClass('show')) {
            mobileMenu.removeClass('show');
        }
    });
    $('.itnavi-header .header-mobile .n-search__name--input').focus(() => {
        let mobileMenu = $('.itnavi-header .n-mobile__menu');
        let nSearch = $('.itnavi-header .header-mobile .n-search--mobile');
            nSearch.addClass('show');
            mobileMenu.removeClass('show');
            $('body').addClass('banned-scroll');
    })
    $('.itnavi-header .header-mobile .js_n-search__mobile--close').focus(() => {
        let nSearch = $('.itnavi-header .header-mobile .n-search--mobile');
            nSearch.removeClass('show');
            $('body').removeClass('banned-scroll');
    })
    $('#js_n-search__form').submit(() => {
        console.log('Form submit!');
        event.preventDefault();
    });
})